Imports BackStock.Classes
Public Class ReserveBatchDetailsReport
    Inherits System.Web.UI.Page
    Protected WithEvents dgAA As System.Web.UI.WebControls.DataGrid
    Protected WithEvents dgWMS As System.Web.UI.WebControls.DataGrid
    Private _ds, _ds1 As DataSet
    Private _workFlowManager As New WorkFlowManager
    Private _fileName As String
    Private _warehouseNbr As Int16
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Response.ContentType = "Application/x-msexcel"
        _fileName = "BATCH_#" & Request("batchID") & ".xls"
        _warehouseNbr = IIf(IsNumeric(Request("WarehouseNbr").Trim), Request("WarehouseNbr").Trim, 0)
        Response.AppendHeader("content-disposition", _
                "attachment; filename=" & _fileName)
        RunReserveBatchReport()
        'Response.Flush()
        'Response.Close()
    End Sub
    Public Sub RunReserveBatchReport()
        'Response.ContentType = "Application/x-msexcel"
        Try
            _ds = _workFlowManager.RunReserveBatchReport(Request("batchID"), _warehouseNbr)
            If _ds.Tables.Count > 0 Then
                'dgAA.DataSource = _ds.Tables("AAAllocs")
                dgAA.DataSource = _ds.Tables(0)
                dgAA.DataBind()
                dgWMS.DataSource = _ds.Tables("WMDistros")
                dgWMS.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

End Class
