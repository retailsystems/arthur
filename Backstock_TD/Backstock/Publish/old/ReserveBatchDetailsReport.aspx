<%@ Page Language="vb" EnableViewState=False  EnableViewStateMac="False" AutoEventWireup="false" Codebehind="ReserveBatchDetailsReport.aspx.vb" Inherits="BackStock.ReserveBatchDetailsReport"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ReserveBatchDetailsReport</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="3" align="middle"><B>RESERVE BATCH DETAILS REPORT (BATCH # <%=Request("batchID")%>)</B></td>
				</tr>
				<tr>
					<td colspan="3"></td>
				</tr>
				<tr>
					<td align="middle" bgcolor="#dcdcdc"><b>AA Allocations</b></td>
					<td width="10px"></td>
					<td align="middle" bgcolor="#dcdcdc"><b>WMS Allocations</b></td>
				</tr>
				<tr>
					<td valign="top">
						<asp:DataGrid ID="dgAA" runat="server" AutoGenerateColumns="False" HeaderStyle-Font-Bold="True" HeaderStyle-BackColor="#ffa500" HeaderStyle-Font-Size="8pt" ItemStyle-Font-Size="8pt">
							<Columns>
								<asp:BoundColumn DataField="DEPT_NBR" HeaderText="DEPT"></asp:BoundColumn>
								<asp:BoundColumn DataField="ALLOC_NBR" HeaderText="ALLOC_NBR"></asp:BoundColumn>
								<asp:BoundColumn DataField="ITM_DESC1" HeaderText="ITM_DESC1"></asp:BoundColumn>
								<asp:BoundColumn DataField="SKU_NBR" HeaderText="SKU_NBR"></asp:BoundColumn>
								<asp:BoundColumn DataField="QTY" HeaderText="QTY"></asp:BoundColumn>
								<asp:BoundColumn DataField="RETAIL_PRICE" HeaderText="RETAIL_PRICE"></asp:BoundColumn>
								<asp:BoundColumn DataField="TOTAL_PRICE" HeaderText="TOTAL_RETAIL"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
					<td width="10px"></td>
					<td valign="top">
						<asp:DataGrid ID="dgWMS" runat="server" AutoGenerateColumns="False" HeaderStyle-Font-Bold="True" HeaderStyle-BackColor="#ffa500" HeaderStyle-Font-Size="8pt" ItemStyle-Font-Size="8pt">
							<Columns>
								<asp:BoundColumn DataField="DEPT" HeaderText="DEPT"></asp:BoundColumn>
								<asp:BoundColumn DataField="DISTRO_NBR" HeaderText="DISTRO_NBR"></asp:BoundColumn>
								<asp:BoundColumn DataField="SKU_NUM" HeaderText="SKU_NUM"></asp:BoundColumn>
								<asp:BoundColumn DataField="REQD_QTY" HeaderText="REQD_QTY"></asp:BoundColumn>
								<asp:BoundColumn DataField="ORIG_REQ_QTY" HeaderText="ORIG_REQ_QTY"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
