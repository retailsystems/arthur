<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CancelClosingStoreDistro.aspx.vb" Inherits="BackStock.CancelClosingStoreDistro" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<!-- #include file="include/header.inc" -->    
<script language="javascript">            			
			function CancelDistros(Store_No,WarehouseNbr){
				//alert(Store_No);
				//alert(WarehouseNbr);
				if (confirm('Are you sure to cancel the closing store ' + Store_No + ' distros for the warehouse '+ WarehouseNbr + '?')){
					document.form1.hdnStore_No.value = Store_No;
					document.form1.hdnWarehouseNbr.value = WarehouseNbr;
					showWait();
					document.form1.btnCancelClosingStore.click();					
				}
			}
		</script>
</head>
<body>
    <form id="form1" method="post" runat="server">
    <br>
			<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
				cellSpacing="0" cellPadding="2" width="750" align="center" border="0">
				<TR height="15">
					<TD class="cellvaluecaption" align="center" bgColor="#990000">Search Store Distro's
					</TD>
				</TR>
				<TR height="5">
					<TD></TD>
				</TR>
				<TR>
					<TD vAlign="top" align="center">
						<TABLE borderColor="#990000" height="100%" cellSpacing="0" cellPadding="0" width="100%"
							border="0">
							<TR height="3"> 
							<TD align="center" style="height: 3px">
							    <asp:label id="lblError" Runat="server" CssClass="errStyle" Visible="False"></asp:label>
								<asp:validationsummary id="ValidationSummary1" Runat="server" CssClass="errStyle" EnableClientScript="false" DisplayMode="BulletList" ShowMessageBox="False"></asp:validationsummary>
							</TD>
							</TR>
							<tr>
								<td class="cellvaluecaption" align="center">Warehouse:	
								<asp:dropdownlist id="lstWhse" Runat="server" CssClass="cellvalueleft">
										<asp:ListItem Value="">--Select One--</asp:ListItem>
										<asp:ListItem Value="985">DC-WestJefferson,OH</asp:ListItem>
										<asp:ListItem Value="997">DC-LaVergne,TN</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="reqValidator1" Runat="server" CssClass="reqStyle" EnableClientScript="false"
										ControlToValidate="lstWhse" Display="None" ErrorMessage="Please select a Warehouse." InitialValue=""></asp:requiredfieldvalidator>&nbsp;&nbsp;							
								Store #: 
									<asp:textbox id="txtStore_No" Runat="server" CssClass="cellvalueleft" Width="170px" MaxLength="20"></asp:textbox>
									<asp:comparevalidator id="validateStore_No" Runat="server" EnableClientScript="false" ControlToValidate="txtStore_No" Display="None" ErrorMessage="Invalid Store_No #." Type="Double" Operator="DataTypeCheck"></asp:comparevalidator>
									<asp:requiredfieldvalidator id="reqStore_No" Runat="server" CssClass="reqStyle" EnableClientScript="false" ControlToValidate="txtStore_No" Display="None" ErrorMessage="Please enter Store Number." InitialValue=""></asp:requiredfieldvalidator>                                
									<asp:button id="btnSubmit" Runat="server" CssClass="btnSmall" Text="Submit"></asp:button>&nbsp;&nbsp;<input class="btnSmall" id="btnClear" onclick="location.href=location.href" type="button" value="Clear" name="btnClear">
								</td>
							</tr>
							<!--
							<TR height="5">
								<TD class="cellvaluecaption" align="center"><asp:checkbox id="chkCopyToArthur" Visible="true" Runat="server" Text="Populate this distro to Arthur worklist" Checked="True"></asp:checkbox></TD>
							</TR>
							-->
						</TABLE>
					</TD>
				</TR>
				<TR height="20">
					<TD></TD>
				</TR>
				</TABLE>
    <!--#include file="include/wait.inc"--><br>
			<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
				cellSpacing="0" cellPadding="2" width="750" align="center" border="0">
				<TR height="15">
					<TD class="cellvaluecaption" align="center" bgColor="#990000">Search Results
					</TD>
				</TR>
				<TR height="5">
					<TD></TD>
				</TR>
				<TR>
					<TD vAlign="top" align="center">
						<TABLE borderColor="#990000" height="100%" cellSpacing="0" cellPadding="0" width="100%"
							border="0">
							<TR height="3">
								<TD align="center" style="height: 3px"></TD>
							</TR>
							<tr>
								<td align="center"><asp:panel id="pnlResults" Runat="server" Visible="False" EnableViewState="False">
										<DIV id="divCancelClosingStore" style="DISPLAY: none">
                                            &nbsp;<INPUT id="hdnStore_No" type="hidden" value="0" name="hdnStore_No" runat="server">&nbsp;
                                            &nbsp;<INPUT id="hdnWarehouseNbr" type="hidden" value="0" name="hdnWarehouseNbr" runat="server">&nbsp;
											<asp:Button id="btnCancel" Runat="server" Width="0px" Text="" Height="0px"></asp:Button>
											<asp:Button id="btnCancelClosingStore" Runat="server" Width="0px" Text="" Height="0px"></asp:Button></DIV>
											<%If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then%>
										    
										    <TABLE class="DatagridSm" id="Table6" style="BORDER-COLLAPSE: collapse" cellSpacing="0"
											cellPadding="2" width="350" align="center" border="1">
											<TR class="DATAGRID_HeaderSm">
												<TD width="75">DC</TD>
												<TD width="75">Store Number</TD>
												<TD width="75">Distro Number</TD>
												<TD width="75">Sum of Qty</TD>												
											</TR>
										
											
										    <%
												dim rowStyle as string = "BC333333sm"
											    Dim i As Int16 = 0
										        'Dim cancelButton As String = "<TD align=left ><input type=button class=btnSmallgrey value=Cancel onClick=""CancelWL('" & hdnStore_No.Value & "','" & hdnWarehouseNbr.Value & "')""></TD>"
										        Dim ConfirmToCancleTheDistros As String = "<TD align=left ><input type=button class=btnSmallgrey value='Cancel Distros' onClick=""CancelDistros('" & hdnStore_No.Value & "','" & hdnWarehouseNbr.Value & "')""></TD>"
											for each dr in ds.tables(0).rows
											%>
																						
											<TR class="<%=rowStyle%>">
												<TD vAlign="middle"><%=dr("WHSE")%></TD>
												<TD vAlign="middle"><%=dr("Store_NBR")%></TD>
												<TD vAlign="middle"><%=dr("DISTRO_NBR")%></TD>																						
												<TD vAlign="middle"><%=dr("TOTAL_QTY")%></TD>											
											<%								
											next
											'response.write ("</TD><TD valign=middle height='100%' align=center ><input type=button class=btnSmallgrey value=Cancel " & cancelBtnStatus & " onClick=""CancelDistro('" & prevDistroNbr & "')""></TD></TR>")
											'response.write ("</TD><TD valign=middle height='100%' align=center ><input type=button class=btnSmallgrey value=Cancel " & cancelBtnStatus & " onClick=""CancelDistro('" & prevDistroNbr & "','N')"">&nbsp;&nbsp;&nbsp;&nbsp;<input type=button class=btnSmallgrey value=Cancel&UpdateArthur " & cancelArthurBtnStatus & " onClick=""CancelAndUpdateArthur('" & prevDistroNbr & "','Y')""></TD></TR>")											
											%>																								
											</TR>											
											<tr><td></td><td></td><td></td><%'Response.Write(cancelButton)%><%Response.Write(ConfirmToCancleTheDistros)%></tr>
										</TABLE>
										<%ElseIf (strErrorMsg.Length > 0) Then%>
										<FONT class="errStyle"><%Response.Write(strErrorMsg)%></FONT>
										<%Else%>
										<FONT class="errStyle">No records found.</FONT>
										<%END IF%>
									</asp:panel></td>
							</tr>							
						</TABLE>
					</TD>
				</TR>
				<TR height="5">
					<TD></TD>
				</TR>
			</TABLE>
    </form>
    <!-- #include file="include/footer.inc" -->
</body>
</html>
