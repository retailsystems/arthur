Imports System.IO
Imports System.Text
Imports Backstock.Classes.SqlServices
Imports System.Data.OracleClient
Imports BackStock.Classes.WebServiceRef
Imports Microsoft.Office.Interop
Imports System.Security.Principal

Public Class WorkFlowManager
    Private _aaCn As New AAOracleDataManager
    Private _gersCn As New GersOracleDataManager
    Private _wmsCn As New WMSOracleDataManager
    Private _tnDcCn As New TNDCWMSOracleDataManager
    Private _strSql As String
    Private _errMsg As String = ""
    'Private Messageblock As New Object
    Public Function ReserveClear(ByVal WarehouseNbr As Int16, ByVal deptStr As String, Optional ByVal emailFlag As Boolean = True) As Int16
        Dim rtnVal As Int16
        Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}

        cmdParameters(0) = New OracleParameter("p_warehouse_nbr", OracleType.VarChar, 4)
        cmdParameters(0).Value = WarehouseNbr

        cmdParameters(1) = New OracleParameter("p_dept_str", OracleType.VarChar, 4000)
        cmdParameters(1).Value = deptStr
        _errMsg = ""
        Try
            rtnVal = _aaCn.ExecuteNonQuery(AppSetting.AABackstockPkg & ".PROC_WORKLIST_CLEAR_BY_DEPT", cmdParameters)
            If rtnVal = 1 AndAlso emailFlag Then
                SendReserveClearEmail(WarehouseNbr, deptStr, True)
                UpdateBackStockHistory(WarehouseNbr, deptStr, "RESERVE_CLEAR_DATE")
            End If
            Return rtnVal
        Catch ex As OracleException
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Catch ex As Exception
            _errMsg = ex.Message
            Throw ex
        Finally
            If _errMsg <> "" Then
                SendReserveClearEmail(WarehouseNbr, deptStr, False, _errMsg)
            End If
        End Try

    End Function

    Public Function ReserveClear(ByVal WarehouseNbr As Int16, Optional ByVal emailFlag As Boolean = True) As Int16
        Try
            Dim rtnVal As Int16
            Dim cmdParameters() As OracleParameter = New OracleParameter(0) {}
            cmdParameters(0) = New OracleParameter("p_warehouse_nbr", OracleType.VarChar, 4)
            'cmdParameters(0).Value = AppSetting.WarehouseNbr
            cmdParameters(0).Value = WarehouseNbr
            _errMsg = ""
            'rtnVal = _aaCn.ExecuteNonQuery("HT_RESERVE.PROC_WORKLIST_RESERVE_CLEAR", cmdParameters)
            rtnVal = _aaCn.ExecuteNonQuery(AppSetting.AABackstockPkg & ".PROC_WORKLIST_RESERVE_CLEAR", cmdParameters)
            'If rtnVal = 1 AndAlso emailFlag Then
            '    SendReserveClearEmail(WarehouseNbr, True)
            'End If
            Return rtnVal
        Catch ex As OracleException
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Catch ex As Exception
            _errMsg = ex.Message
            Throw ex
        Finally
            If _errMsg <> "" Then
                SendReserveClearEmail(WarehouseNbr, False, _errMsg)
            End If
        End Try
    End Function
  
    Public Function UpdateBackStockHistory(ByRef ds As DataSet)
        If ds.Tables.Count > 0 And ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow
            Dim strBD As New StringBuilder
            Dim now As String = DateTime.Now.ToString("dd-MMM-yy hh:mm:ss")
            Try
                For Each dr In ds.Tables(0).Rows
                    strBD.Append("INSERT INTO HT_BACKSTOCK_HISTORY " & _
                              "(WAREHOUSE_NBR,DEPT_NBR,DIV_NBR,ITM_NBR,AVAIL_QTY) " & _
                              " VALUES (")

                    strBD.Append("'" & dr("WHSE") & "'")
                    strBD.Append(",'" & dr("MISC_ALPHA_1") & "'")
                    strBD.Append(",'" & dr("STORE_DEPT") & "'")
                    strBD.Append(",'" & dr("STYLE") & "'")
                    strBD.Append("," & dr("QTY_AVAIL") & ")")
                    'strBD.Append(",'" & DateTime.Now.ToString("dd-MMM-yy HH:mm:ss") & "')")
                    'strBD.Append(",'" & Now().ToString("dd-MMM-yy") & "')")

                    If (strBD.ToString().Length > 0) Then
                        _aaCn.ExecuteNonQuery(strBD.ToString())
                    End If
                    strBD.Length = 0
                Next
            Catch ex As OracleException
                _errMsg = ex.Message
                Throw New BackStockAppException(ex.Message)
            End Try
        End If
    End Function
    Public Function UpdateBackStockHistory(ByVal warehouseNbr As String, ByVal depts As String, ByVal task As String)
        Dim strBD As New StringBuilder
        Dim arrDepts() As String = depts.Split(",")
        Dim i As Int16 = 0
        If (arrDepts.Length > 0) Then
            Try
                For i = 0 To arrDepts.GetUpperBound(0)
                    If HasRun(warehouseNbr, arrDepts(i)) Then
                        UpdateBackStockHist(warehouseNbr, arrDepts(i), task)
                    Else
                        InsertBackStockHist(warehouseNbr, arrDepts(i), task)
                    End If

                    If (strBD.ToString().Length > 0) Then
                        _aaCn.ExecuteNonQuery(strBD.ToString())
                    End If

                    strBD.Length = 0
                Next
            Catch ex As Exception
                _errMsg = ex.Message
                Throw New BackStockAppException(ex.Message)
            End Try
        End If
    End Function
    Private Function UpdateBackStockHist(ByVal warehouseNbr, ByVal dept, ByVal task)
        Dim cmdParameters() As OracleParameter = New OracleParameter(2) {}
        Dim strBD As New StringBuilder

        cmdParameters(0) = New OracleParameter("warehouse_nbr", OracleType.VarChar, 4)
        cmdParameters(0).Value = warehouseNbr

        cmdParameters(1) = New OracleParameter("dept_str", OracleType.VarChar, 4000)
        cmdParameters(1).Value = dept

        cmdParameters(2) = New OracleParameter("date_run", OracleType.DateTime)
        cmdParameters(2).Value = DateTime.Now

        strBD.Append("UPDATE HT_BACKSTOCK_HISTORY SET ")
        strBD.Append(task & " = :date_run ")
        strBD.Append("WHERE WAREHOUSE_NBR = :warehouse_nbr AND DEPT_NBR = :dept_str")


        Dim aaCon As New OracleConnection(_aaCn.strConn)

        Dim cmd As New OracleCommand
        cmd.Connection = aaCon

        cmd.Parameters.Add(cmdParameters(0))
        cmd.Parameters.Add(cmdParameters(1))
        cmd.Parameters.Add(cmdParameters(2))

        Try
            aaCon.Open()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = strBD.ToString()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Finally
            If aaCon.State = ConnectionState.Open Then
                aaCon.Close()
                aaCon.Dispose()
            End If
        End Try

    End Function
    Private Function InsertBackStockHist(ByVal warehouseNbr, ByVal dept, ByVal task)

        Dim cmdParameters() As OracleParameter = New OracleParameter(2) {}
        Dim strBD As New StringBuilder

        cmdParameters(0) = New OracleParameter("warehouse_nbr", OracleType.VarChar, 4)
        cmdParameters(0).Value = warehouseNbr

        cmdParameters(1) = New OracleParameter("dept_str", OracleType.VarChar, 4000)
        cmdParameters(1).Value = dept

        cmdParameters(2) = New OracleParameter("date_run", OracleType.DateTime)
        cmdParameters(2).Value = DateTime.Now

        strBD.Append("INSERT INTO HT_BACKSTOCK_HISTORY (WAREHOUSE_NBR,DEPT_NBR, " & task & ") VALUES ( :warehouse_nbr, :dept_str, :date_run)")

        Dim aaCon As New OracleConnection(_aaCn.strConn)

        Dim cmd As New OracleCommand
        cmd.Connection = aaCon

        cmd.Parameters.Add(cmdParameters(0))
        cmd.Parameters.Add(cmdParameters(1))
        cmd.Parameters.Add(cmdParameters(2))

        Try
            aaCon.Open()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = strBD.ToString()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Finally
            If aaCon.State = ConnectionState.Open Then
                aaCon.Close()
                aaCon.Dispose()
            End If
        End Try
    End Function

    Public Function HasRun(ByVal warehouseNbr As String, ByVal dept As String) As Boolean
        Dim strBD As New StringBuilder
        Dim ds As DataSet

        strBD.Append("SELECT DEPT_NBR FROM HT_BACKSTOCK_HISTORY WHERE ")
        strBD.Append("DEPT_NBR = " & dept)
        strBD.Append(" AND")
        strBD.Append(" WAREHOUSE_NBR = " & warehouseNbr)

        Try
            ds = _aaCn.ExecuteDataSet(strBD.ToString())
            If ds.Tables(0).Rows.Count >= 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    'Public Function ReserveUpdateNew(ByVal WarehouseNbr As Int16) As String
    '    Dim reserveFile As String = ""
    '    Dim successMsg As String = ""
    '    Dim i, skuCnt As Int16
    '    Try
    '        'Call Resserve clear to backstock data in Arthur
    '        'ReserveClear(WarehouseNbr, False)
    '        _errMsg = ""

    '        Dim dr, dr1 As DataRow
    '        Dim ds, ds1 As DataSet

    '        Dim strSql, cur_style, UDF1, UDF2, UDF3, UDF4, UDF5, UDF6, _timeStamp, _filePath, _localFilePath As String

    '        Dim cmdParameters() As OracleParameter = New OracleParameter(0) {}

    '        cmdParameters(0) = New OracleParameter("p_cur_Backstock", OracleType.Cursor)
    '        cmdParameters(0).Direction = ParameterDirection.Output


    '        Select Case WarehouseNbr
    '            Case EnumWarehouse.CADC
    '                ds = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_GET_BACKSTOCK_DATA", cmdParameters)
    '            Case EnumWarehouse.TNDC
    '                ds = _tnDcCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_GET_BACKSTOCK_DATA", cmdParameters)
    '        End Select
    '        'ds = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_GET_BACKSTOCK_DATA", cmdParameters)
    '        Dim objStreamWriter As StreamWriter
    '        _timeStamp = Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Year.ToString & "_" & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0")
    '        reserveFile = "Reserve_" & WarehouseNbr & "_" & _timeStamp & ".txt"
    '        _filePath = AppSetting.FtpFolder & IIf(AppSetting.FtpFolder.EndsWith("\"), "", "\") & reserveFile
    '        _localFilePath = AppSetting.WmsLocalFileDir & IIf(AppSetting.WmsLocalFileDir.EndsWith("\"), "", "\") & reserveFile

    '        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
    '            objStreamWriter = New StreamWriter(_localFilePath, False)
    '            'objStreamWriter.WriteLine("RESERVE_HDR:" & _timeStamp)
    '            skuCnt = ds.Tables(0).Rows.Count
    '            For Each dr In ds.Tables(0).Rows
    '                'If n > 0 Then
    '                '    objStreamWriter.Write(objStreamWriter.NewLine)
    '                'End If
    '                'n += 1
    '                'write to backstock file
    '                'objStreamWriter.Write("ITM_DTL:")
    '                objStreamWriter.Write(dr("WHSE") & "|") ' -- Warehouse #
    '                objStreamWriter.Write(dr("QTY_AVAIL") & "|") '-- Qty Available
    '                objStreamWriter.Write(dr("STYLE") & "|") '-- Item #
    '                objStreamWriter.Write(dr("DFLT_VENDOR") & "|") '-- Vendor #
    '                objStreamWriter.Write(dr("STORE_DEPT") & "|") ' -- Div #
    '                objStreamWriter.Write(dr("MISC_ALPHA_1") & "|") '-- Dept #
    '                objStreamWriter.Write(UtilityManager.NullToString(dr("MISC_NUMERIC_1")).PadLeft(4, "0") & "|") '-- Class #
    '                objStreamWriter.Write(dr("SALE_GRP") & "|") ' -- Sub Class #
    '                objStreamWriter.Write(dr("SKU_DESC") & "|") '-- Sku Desc
    '                objStreamWriter.Write(dr("PROD_GROUP") & "|") '-- UOM Cd
    '                objStreamWriter.Write(dr("PROD_GROUP") & "|") '-- UOM Qty
    '                objStreamWriter.Write(dr("RETAIL_PRICE") & "|") '-- Retail Price
    '                objStreamWriter.Write(dr("UNIT_PRICE") & "|") '-- Cost Price
    '                objStreamWriter.Write(dr("STYLE") & "-" & dr("STYLE_SFX") & "|") '-- SKU Nbr
    '                objStreamWriter.Write(dr("PROD_SUB_GRP") & "|") '--Size Of Multiple
    '                objStreamWriter.Write(dr("MISC_ALPHA_3") & "|") ' -- size #
    '                objStreamWriter.Write(dr("MISC_ALPHA_2") & dr("COLOR_SFX") & "|") ' -- color #

    '                '******************************************************
    '                'GET UDF info from GERS
    '                '******************************************************
    '                If cur_style <> dr("style") Then
    '                    cur_style = dr("style")
    '                    UDF1 = ""
    '                    UDF2 = ""
    '                    UDF3 = ""
    '                    UDF4 = ""
    '                    UDF5 = ""
    '                    UDF6 = ""

    '                    strSql = "select UDF1,UDF2, UDF3,UDF4,UDF5,UDF6 from GM_ITM WHERE ITM_CD = '" & dr("style") & "'"
    '                    ds1 = _gersCn.ExecuteDataSet(strSql)

    '                    'Exit For
    '                    If ds1.Tables.Count > 0 AndAlso ds1.Tables(0).Rows.Count > 0 Then
    '                        dr1 = ds1.Tables(0).Rows(0)
    '                        UDF1 = UtilityManager.NullToString(dr1("UDF1"))
    '                        UDF2 = UtilityManager.NullToString(dr1("UDF2"))
    '                        UDF3 = UtilityManager.NullToString(dr1("UDF3"))
    '                        UDF4 = UtilityManager.NullToString(dr1("UDF4"))
    '                        UDF5 = UtilityManager.NullToString(dr1("UDF5"))
    '                        UDF6 = UtilityManager.NullToString(dr1("UDF6"))
    '                    End If
    '                End If


    '                objStreamWriter.Write(UDF1 & "|" & UDF2 & "|" & UDF3 & "|" & UDF4 & "|" & UDF5 & "|" & UDF6)
    '                objStreamWriter.Write("|" & Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year)
    '                'objStreamWriter.Write("|")
    '                objStreamWriter.Write(vbCrLf)
    '            Next
    '            objStreamWriter.Close()
    '            Dim _file As File
    '            If _file.Exists(_localFilePath) Then
    '                _file.Move(_localFilePath, _filePath)
    '                SendAckXmlFile(AppSetting.AckFileDir, reserveFile, AppSetting.UtlFileDir, WarehouseNbr)
    '            End If
    '            successMsg = skuCnt & " records found, "
    '            successMsg &= "Backstock File '" & reserveFile & "' submitted to Biztalk process"
    '            'SendReserveUpdateEmail(WarehouseNbr, True, reserveFile, skuCnt)
    '        Else
    '            successMsg = "No records found"
    '        End If
    '        Return successMsg
    '    Catch ex As OracleException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As BackStockAppException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As FileNotFoundException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As DirectoryNotFoundException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As Security.SecurityException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As Exception
    '        _errMsg = ex.Message
    '        Throw ex
    '    Finally
    '        If _errMsg <> "" Then
    '            SendReserveUpdateEmail(WarehouseNbr, False, reserveFile, skuCnt, _errMsg)
    '        End If
    '    End Try
    'End Function

    Public Function ReserveUpdateNew(ByVal WarehouseNbr As Int16, ByVal deptStr As String) As String
        ' VNaidu 6-16-2014 -- Reworked this function in order to call the ETL web service for initiating the BackStock Nightly demand job

        Dim successMsg As String = ""
        Try

            'SyncLock Messageblock

            'Call Resserve clear to backstock data in Arthur
            ReserveClear(WarehouseNbr, deptStr, False)
            _errMsg = ""

            Dim strJob_Status, strSql As String
            Dim intJobInitiateProcess As Integer = 0 ' 0 - don't initiate the job, 1 = initiate the job

            Dim aaCon As New OracleConnection(_aaCn.strConn)
            Dim cmd, cmd1 As New OracleCommand
            Dim reader As OracleDataReader

            cmd.Connection = aaCon
            cmd.CommandText = "SELECT * FROM AAM.HT_WMS_ETL_ON_DEMAND_LOG WHERE ID = (SELECT MAX(ID) FROM AAM.HT_WMS_ETL_ON_DEMAND_LOG)"

            aaCon.Open()
            cmd.CommandType = CommandType.Text
            reader = cmd.ExecuteReader()
            If (reader.HasRows = True) Then
                While (reader.Read())
                    strJob_Status = reader("JOB_STATUS")
                    If (strJob_Status = "STARTED") Then
                        successMsg = "The previous Reserve update process request is still in progress. please wait for the process to complete and try again later."
                        intJobInitiateProcess = 0
                    Else
                        successMsg = "The Reserve update process has been initiated. An email will be sent out as soon as the reserve update is complete"
                        intJobInitiateProcess = 1
                    End If
                    Exit While 'we are going to see only one record
                End While
            Else
                successMsg = "The Reserve update process has been initiated. An email will be sent out as soon as the reserve update is complete"
                intJobInitiateProcess = 1
            End If
            reader.Close()

            'insert new row and initiate the job
            If (intJobInitiateProcess = 1) Then
                Dim ETLWebService As New WebService()
                Dim strPkgLaunchStatus As String = "Success"
                strPkgLaunchStatus = ETLWebService.LaunchPackage(AppSetting.bsETLSourceType.ToString(), AppSetting.bsETLSourceLocation.ToString(), AppSetting.bsETLPkgName.ToString(), AppSetting.bsETLPkgConfigName.ToString(), deptStr, WarehouseNbr)
                If (UCase(strPkgLaunchStatus) = "SUCCESS") Then

                    ETLWebService = Nothing

                    cmd.CommandType = CommandType.Text
                    strSql = "INSERT INTO AAM.HT_WMS_ETL_ON_DEMAND_LOG (JOB_INIT_DATETIME, JOB_STATUS,JOB_STATUS_DESC, PARAMETER_LIST, JOB_END_DATETIME) VALUES (sysdate, 'STARTED', NULL, 'dept= " + deptStr.ToString() + "|wh=" + WarehouseNbr.ToString() + "' , null)"
                    cmd.CommandText = strSql
                    cmd.ExecuteNonQuery()
                Else
                    cmd.Dispose()
                    aaCon.Close()
                    Throw New Exception(strPkgLaunchStatus)
                End If
            End If

            cmd.Dispose()
            aaCon.Close()
            'End SyncLock

            Return successMsg

        Catch ex As OracleException
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Catch ex As BackStockAppException
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Catch ex As Security.SecurityException
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Catch ex As Exception
            _errMsg = ex.ToString()
            Throw ex
        Finally
            If _errMsg <> "" Then
                SendReserveUpdateEmail(WarehouseNbr, deptStr, False, Nothing, Nothing, _errMsg)
            End If

        End Try


    End Function
    'Commented the below method as part of AA Upgrade 2014 phase-2 changes

    'Public Function ReserveUpdateNew(ByVal WarehouseNbr As Int16, ByVal deptStr As String) As String
    '    ' KJZ 8-27-08 -- Reworked this function in order to add PrePack functionality and pull additional UDF's
    '    '' HG  12-18-2008 -- commented out prepack functionality and leave only pull additional UDF's 
    '    Dim reserveFile As String = ""
    '    Dim successMsg As String = ""
    '    Dim i, skuCnt  '', PrePackskuCnt As Int16
    '    Try
    '        'Call Resserve clear to backstock data in Arthur
    '        ReserveClear(WarehouseNbr, deptStr, False)
    '        _errMsg = ""

    '        Dim drWMS, drGERS As DataRow
    '        Dim dsWMS, dsGERS As DataSet
    '        'Dim strSql, cur_style, _timeStamp, _filePath, _localFilePath As String
    '        Dim strSql, _timeStamp, _filePath, _localFilePath As String
    '        Dim cmdParametersWMS() As OracleParameter = New OracleParameter(1) {}
    '        Dim cmdParametersGERS() As OracleParameter = New OracleParameter(1) {}

    '        'Create an array to add an empty row incase we have no data in our dataset.
    '        Dim EmptyRowArray As Array = New ArrayList(35) {}

    '        ' Setup for WMS 
    '        cmdParametersWMS(0) = New OracleParameter("p_cur_Backstock", OracleType.Cursor)
    '        cmdParametersWMS(0).Direction = ParameterDirection.Output
    '        cmdParametersWMS(1) = New OracleParameter("p_dept_str", OracleType.VarChar, 4000)
    '        cmdParametersWMS(1).Value = deptStr

    '        ' Setup for GERS
    '        cmdParametersGERS(0) = New OracleParameter("p_Sku", OracleType.VarChar)
    '        cmdParametersGERS(1) = New OracleParameter("p_Cur_UDF", OracleType.Cursor)
    '        cmdParametersGERS(1).Direction = ParameterDirection.Output

    '        'Get data from WMS backstock
    '        Select Case WarehouseNbr
    '            Case EnumWarehouse.CADC
    '                dsWMS = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_GET_BACKSTOCK_BY_DEPT", cmdParametersWMS)
    '            Case EnumWarehouse.TNDC
    '                dsWMS = _tnDcCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_GET_BACKSTOCK_BY_DEPT", cmdParametersWMS)
    '        End Select

    '        Dim objStreamWriter As StreamWriter
    '        _timeStamp = Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Year.ToString & "_" & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0")
    '        reserveFile = "Reserve_" & WarehouseNbr & "_" & _timeStamp & ".txt"
    '        _filePath = AppSetting.FtpFolder & IIf(AppSetting.FtpFolder.EndsWith("\"), "", "\") & reserveFile
    '        _localFilePath = AppSetting.WmsLocalFileDir & IIf(AppSetting.WmsLocalFileDir.EndsWith("\"), "", "\") & reserveFile

    '        '''Initilaze PrePack sku count.
    '        'PrePackskuCnt = 0

    '        If dsWMS.Tables.Count > 0 AndAlso dsWMS.Tables(0).Rows.Count > 0 Then
    '            objStreamWriter = New StreamWriter(_localFilePath, False)
    '            skuCnt = dsWMS.Tables(0).Rows.Count

    '            For Each drWMS In dsWMS.Tables(0).Rows
    '                'Set parameters & get data from GERS passing sku from drWMS
    '                cmdParametersGERS(0).Value = drWMS("STYLE") & "-" & drWMS("STYLE_SFX")
    '                dsGERS = _gersCn.ExecuteDataSet(AppSetting.GERSBackstockPkg & ".GET_UDF_DATA", cmdParametersGERS)

    '                ''If dsGERS.Tables.Count > 0 AndAlso dsGERS.Tables(0).Rows.Count > 1 Then
    '                ''PrePackskuCnt = PrePackskuCnt + 1
    '                '' ' ** PRE-PACK ** We have a Pre-Pack sku if more than 1 row so loop thru each row
    '                ''For Each drGERS In dsGERS.Tables(0).Rows
    '                ''With objStreamWriter
    '                ''.Write(drWMS("WHSE") & "|")                               '-- Warehouse #
    '                ''Write(drWMS("QTY_AVAIL") * drGERS("PRE_PACK_QTY") & "|") '-- Qty Available * Prepack
    '                ''.Write(drWMS("STYLE") & "|")                              '-- Item #
    '                ''.Write(drWMS("DFLT_VENDOR") & "|")                        '-- Vendor #
    '                ''.Write(drWMS("STORE_DEPT") & "|")                         '-- Div #
    '                ''.Write(drWMS("MISC_ALPHA_1") & "|")                       '-- Dept #
    '                ''.Write(UtilityManager.NullToString(drWMS("MISC_NUMERIC_1")).PadLeft(4, "0") & "|") '-- Class #
    '                ''.Write(drWMS("SALE_GRP") & "|")                           '-- Sub Class #
    '                ''.Write(drWMS("SKU_DESC") & "|")                           '-- Sku Desc
    '                ''.Write(drWMS("PROD_GROUP") & "|")                         '-- UOM Cd
    '                ''.Write(drWMS("PROD_SUB_GRP") & "|")                       '-- UOM Qty 
    '                ''.Write(drWMS("RETAIL_PRICE") & "|")                       '-- Retail Price
    '                ''.Write(drGERS("LIST_PRC") & "|")                          '-- List Price from GERS                                
    '                ''.Write(drGERS("SKU_NUM") & "|")                           '-- Component Sku from GERS
    '                ''.Write(drWMS("PROD_SUB_GRP") & "|")                       '-- Size Of Multiple
    '                ''.Write(drGERS("SIZE_CD") & "|")                           '-- size # from GERS
    '                ''.Write(drGERS("COLOR_CD") & "|")                          '-- color # from GERS
    '                ''.Write(drGERS("UDF1") & "|")
    '                ''.Write(drGERS("UDF2") & "|")
    '                ''.Write(drGERS("UDF3") & "|")
    '                ''.Write(drGERS("UDF4") & "|")
    '                ''.Write(drGERS("UDF5") & "|")
    '                ''.Write(drGERS("UDF6") & "|")
    '                ''.Write(Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year & "|")
    '                ''.Write(drGERS("PACK_ID") & "|")                           '-- Prepack ID from GERS
    '                ''.Write(drGERS("PRE_PACK_QTY") & "|")                      '-- Prepack quantity
    '                ''.Write(drGERS("UDF030") & "|")
    '                ''.Write(drGERS("UDF072") & "|")
    '                ''.Write(drGERS("UDF073") & "|")
    '                ''.Write(drGERS("UDF074") & "|")
    '                ''.Write(drGERS("UDF075") & "|")
    '                ''.Write(drGERS("UDF076") & "|")
    '                ''.Write(drGERS("UDF077") & "|")
    '                ''.Write(drGERS("UDF078") & "|")
    '                ''.Write(drGERS("UDF079") & "|")
    '                ''.Write(drGERS("UDF080") & "|")
    '                ''.Write(drGERS("UDF081") & "|")
    '                ''.Write(drGERS("UDF082") & "|")
    '                ''.Write(drGERS("UDF083") & "|")
    '                ''.Write(drGERS("UDF084") & "|")
    '                ''.Write(drGERS("UDF085") & "|")
    '                ''.Write(drGERS("UDF086") & "|")
    '                ''.Write(drGERS("UDF087") & "|")
    '                ''.Write(drGERS("UDF088") & "|")
    '                ''.Write(drGERS("UDF089") & "|")
    '                ''.Write(drGERS("UDF090") & "|")
    '                ''.Write(drGERS("UDF091") & "|")
    '                ''.Write(drGERS("UDF092") & "|")
    '                ''.Write(drGERS("UDF093") & "|")
    '                ''.Write(drGERS("UDF094") & "|")
    '                ''.Write(drGERS("UDF095") & "|")
    '                ''.Write(drGERS("UDF096") & "|")
    '                ''.Write(drGERS("UDF097") & "|")
    '                ''.Write(drGERS("UDF098") & "|")
    '                ''.Write(drGERS("UDF099") & "|")
    '                ''.Write(drGERS("UDF029"))
    '                ' .Write(Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year)
    '                ''.Write(vbCrLf)
    '                ''End With
    '                ''Next
    '                ''Else ' We have a single sku from GERS (Non PrePack) or no row from GERS
    '                'Add empty row if we have none in GERS to write null values in file
    '                If dsGERS.Tables(0).Rows.Count = 0 Then
    '                    drGERS = dsGERS.Tables(0).Rows.Add(EmptyRowArray)
    '                End If
    '                For Each drGERS In dsGERS.Tables(0).Rows
    '                    With objStreamWriter
    '                        .Write(drWMS("WHSE") & "|")                               '-- Warehouse #
    '                        .Write(drWMS("QTY_AVAIL") & "|")                          '-- Qty Available 
    '                        .Write(drWMS("STYLE") & "|")                              '-- Item #
    '                        .Write(drWMS("DFLT_VENDOR") & "|")                        '-- Vendor #
    '                        .Write(drWMS("STORE_DEPT") & "|")                         '-- Div #
    '                        .Write(drWMS("MISC_ALPHA_1") & "|")                       '-- Dept #
    '                        .Write(UtilityManager.NullToString(drWMS("MISC_NUMERIC_1")).PadLeft(4, "0") & "|") '-- Class #
    '                        .Write(drWMS("SALE_GRP") & "|")                           '-- Sub Class #
    '                        .Write(drWMS("SKU_DESC") & "|")                           '-- Sku Desc
    '                        .Write(drWMS("PROD_GROUP") & "|")                         '-- UOM Cd
    '                        .Write(drWMS("PROD_SUB_GRP") & "|")                       '-- UOM Qty 
    '                        .Write(drWMS("RETAIL_PRICE") & "|")                       '-- Retail Price
    '                        .Write(drWMS("UNIT_PRICE") & "|")                         '-- Cost Price
    '                        .Write(drWMS("STYLE") & "-" & drWMS("STYLE_SFX") & "|")   '-- SKU Nbr
    '                        .Write(drWMS("PROD_SUB_GRP") & "|")                       '-- Size Of Multiple 
    '                        .Write(drWMS("MISC_ALPHA_3") & "|")                       '-- size #
    '                        .Write(drWMS("MISC_ALPHA_2") & drWMS("COLOR_SFX") & "|")  '-- color #
    '                        .Write(drGERS("UDF1") & "|")
    '                        .Write(drGERS("UDF2") & "|")
    '                        .Write(drGERS("UDF3") & "|")
    '                        .Write(drGERS("UDF4") & "|")
    '                        .Write(drGERS("UDF5") & "|")
    '                        .Write(drGERS("UDF6") & "|")
    '                        .Write(Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year & "|")
    '                        ''.Write(drGERS("PACK_ID") & "|")                           '-- Prepack ID from GERS (Will be NULL)
    '                        ''.Write(drGERS("PRE_PACK_QTY") & "|")                      '-- Prepack quantity from GERS (WIll be NULL) 
    '                        '.Write(drGERS("UDF029") & "|")
    '                        .Write(drGERS("UDF030") & "|")
    '                        .Write(drGERS("UDF072") & "|")
    '                        .Write(drGERS("UDF073") & "|")
    '                        .Write(drGERS("UDF074") & "|")
    '                        .Write(drGERS("UDF075") & "|")
    '                        .Write(drGERS("UDF076") & "|")
    '                        .Write(drGERS("UDF077") & "|")
    '                        .Write(drGERS("UDF078") & "|")
    '                        .Write(drGERS("UDF079") & "|")
    '                        .Write(drGERS("UDF080") & "|")
    '                        .Write(drGERS("UDF081") & "|")
    '                        .Write(drGERS("UDF082") & "|")
    '                        .Write(drGERS("UDF083") & "|")
    '                        .Write(drGERS("UDF084") & "|")
    '                        .Write(drGERS("UDF085") & "|")
    '                        .Write(drGERS("UDF086") & "|")
    '                        .Write(drGERS("UDF087") & "|")
    '                        .Write(drGERS("UDF088") & "|")
    '                        .Write(drGERS("UDF089") & "|")
    '                        .Write(drGERS("UDF090") & "|")
    '                        .Write(drGERS("UDF091") & "|")
    '                        .Write(drGERS("UDF092") & "|")
    '                        .Write(drGERS("UDF093") & "|")
    '                        .Write(drGERS("UDF094") & "|")
    '                        .Write(drGERS("UDF095") & "|")
    '                        .Write(drGERS("UDF096") & "|")
    '                        .Write(drGERS("UDF097") & "|")
    '                        .Write(drGERS("UDF098") & "|")
    '                        .Write(drGERS("UDF099") & "|")
    '                        .Write(drGERS("UDF029"))
    '                        '.Write(Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year)
    '                        .Write(vbCrLf)
    '                    End With


    '                    ''End If

    '                Next
    '            Next

    '            'clean house 
    '            objStreamWriter.Close()
    '            Dim _file As File

    '            'Send xml file to BizTalk if the file exists
    '            If _file.Exists(_localFilePath) Then
    '                _file.Move(_localFilePath, _filePath)
    '                SendAckXmlFile(AppSetting.AckFileDir, reserveFile, AppSetting.UtlFileDir, WarehouseNbr)
    '                UpdateBackStockHistory(WarehouseNbr.ToString(), deptStr, "RESERVE_UPDATE_DATE")
    '            End If

    '            successMsg = skuCnt & " records found, "
    '            successMsg &= "Backstock File '" & reserveFile & "' submitted to Biztalk process"
    '            SendReserveUpdateEmail(WarehouseNbr, deptStr, True, reserveFile, skuCnt) '', PrePackskuCnt)
    '        Else
    '                successMsg = "No records found"
    '        End If
    '        Return successMsg

    '    Catch ex As OracleException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As BackStockAppException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As FileNotFoundException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As DirectoryNotFoundException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As Security.SecurityException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As Exception
    '        _errMsg = ex.Message
    '        Throw ex
    '    Finally
    '        If _errMsg <> "" Then
    '            SendReserveUpdateEmail(WarehouseNbr, deptStr, False, reserveFile, skuCnt, _errMsg)
    '            ''PrePackskuCnt, _errMsg)
    '        End If
    '    End Try

    'End Function


    '*********** kjz version 1.0 of this function backup.  See above and delete when above is working *******
    '''Public Function ReserveUpdateNew(ByVal WarehouseNbr As Int16, ByVal deptStr As String) As String
    '''    Dim reserveFile As String = ""
    '''    Dim successMsg As String = ""
    '''    Dim i, skuCnt As Int16
    '''    Try
    '''        'Call Resserve clear to backstock data in Arthur
    '''        ReserveClear(WarehouseNbr, deptStr, False)
    '''        _errMsg = ""

    '''        Dim dr, dr1 As DataRow
    '''        Dim ds, ds1 As DataSet
    '''        'Dim sku As String
    '''        Dim strSql, cur_style, _timeStamp, _filePath, _localFilePath As String
    '''        Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
    '''        Dim cmdParametersUDF() As OracleParameter = New OracleParameter(1) {}

    '''        cmdParameters(0) = New OracleParameter("p_cur_Backstock", OracleType.Cursor)
    '''        cmdParameters(0).Direction = ParameterDirection.Output
    '''        cmdParameters(1) = New OracleParameter("p_dept_str", OracleType.VarChar, 4000)
    '''        cmdParameters(1).Value = deptStr

    '''        cmdParametersUDF(0) = New OracleParameter("p_Sku", OracleType.VarChar)
    '''        cmdParametersUDF(1) = New OracleParameter("p_Cur_UDF_PrePack", OracleType.Cursor)
    '''        cmdParametersUDF(1).Direction = ParameterDirection.Output

    '''        Select Case WarehouseNbr
    '''            Case EnumWarehouse.CADC
    '''                ds = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_GET_BACKSTOCK_BY_DEPT", cmdParameters)
    '''            Case EnumWarehouse.TNDC
    '''                ds = _tnDcCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_GET_BACKSTOCK_BY_DEPT", cmdParameters)
    '''        End Select

    '''        Dim objStreamWriter As StreamWriter
    '''        _timeStamp = Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Year.ToString & "_" & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0")
    '''        reserveFile = "Reserve_" & WarehouseNbr & "_" & _timeStamp & ".txt"
    '''        _filePath = AppSetting.FtpFolder & IIf(AppSetting.FtpFolder.EndsWith("\"), "", "\") & reserveFile
    '''        _localFilePath = AppSetting.WmsLocalFileDir & IIf(AppSetting.WmsLocalFileDir.EndsWith("\"), "", "\") & reserveFile

    '''        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
    '''            objStreamWriter = New StreamWriter(_localFilePath, False)
    '''            'objStreamWriter.WriteLine("RESERVE_HDR:" & _timeStamp)
    '''            skuCnt = ds.Tables(0).Rows.Count
    '''            For Each dr In ds.Tables(0).Rows
    '''                'If n > 0 Then
    '''                '    objStreamWriter.Write(objStreamWriter.NewLine)
    '''                'End If
    '''                'n += 1
    '''                'write to backstock file
    '''                'objStreamWriter.Write("ITM_DTL:")
    '''                objStreamWriter.Write(dr("WHSE") & "|") ' -- Warehouse #
    '''                objStreamWriter.Write(dr("QTY_AVAIL") & "|") '-- Qty Available
    '''                objStreamWriter.Write(dr("STYLE") & "|") '-- Item #
    '''                objStreamWriter.Write(dr("DFLT_VENDOR") & "|") '-- Vendor #
    '''                objStreamWriter.Write(dr("STORE_DEPT") & "|") ' -- Div #
    '''                objStreamWriter.Write(dr("MISC_ALPHA_1") & "|") '-- Dept #
    '''                objStreamWriter.Write(UtilityManager.NullToString(dr("MISC_NUMERIC_1")).PadLeft(4, "0") & "|") '-- Class #
    '''                objStreamWriter.Write(dr("SALE_GRP") & "|") ' -- Sub Class #
    '''                objStreamWriter.Write(dr("SKU_DESC") & "|") '-- Sku Desc
    '''                objStreamWriter.Write(dr("PROD_GROUP") & "|") '-- UOM Cd
    '''                objStreamWriter.Write(dr("PROD_GROUP") & "|") '-- UOM Qty
    '''                objStreamWriter.Write(dr("RETAIL_PRICE") & "|") '-- Retail Price
    '''                objStreamWriter.Write(dr("UNIT_PRICE") & "|") '-- Cost Price
    '''                objStreamWriter.Write(dr("STYLE") & "-" & dr("STYLE_SFX") & "|") '-- SKU Nbr
    '''                objStreamWriter.Write(dr("PROD_SUB_GRP") & "|") '--Size Of Multiple
    '''                objStreamWriter.Write(dr("MISC_ALPHA_3") & "|") ' -- size #
    '''                objStreamWriter.Write(dr("MISC_ALPHA_2") & dr("COLOR_SFX") & "|") ' -- color #

    '''                '******************************************************
    '''                'GET UDF info from GERS
    '''                '******************************************************
    '''                If cur_style <> dr("style") Then
    '''                    cur_style = dr("style")

    '''                    'Set SKU value  
    '''                    cmdParametersUDF(0).Value = dr("STYLE") & "-" & dr("STYLE_SFX")
    '''                    ds1 = _gersCn.ExecuteDataSet(AppSetting.GERSBackstockPkg & ".GET_UDF_PREPACK_DATA", cmdParametersUDF)

    '''                    If ds1.Tables.Count > 0 AndAlso ds1.Tables(0).Rows.Count > 0 Then

    '''                        ' not sure if this next line is needed but was in old code
    '''                        dr1 = ds1.Tables(0).Rows(0)

    '''                        For Each dr1 In ds1.Tables(0).Rows
    '''                            With objStreamWriter
    '''                                .Write(dr1("PACK_ID") & "|")
    '''                                .Write(dr1("UDF1") & "|")
    '''                                .Write(dr1("UDF2") & "|")
    '''                                .Write(dr1("UDF3") & "|")
    '''                                .Write(dr1("UDF4") & "|")
    '''                                .Write(dr1("UDF5") & "|")
    '''                                .Write(dr1("UDF6") & "|")
    '''                                .Write(dr1("UDF030") & "|")
    '''                                .Write(dr1("UDF072") & "|")
    '''                                .Write(dr1("UDF073") & "|")
    '''                                .Write(dr1("UDF074") & "|")
    '''                                .Write(dr1("UDF075") & "|")
    '''                                .Write(dr1("UDF076") & "|")
    '''                                .Write(dr1("UDF077") & "|")
    '''                                .Write(dr1("UDF078") & "|")
    '''                                .Write(dr1("UDF079") & "|")
    '''                                .Write(dr1("UDF080") & "|")
    '''                                .Write(dr1("UDF081") & "|")
    '''                                .Write(dr1("UDF082") & "|")
    '''                                .Write(dr1("UDF083") & "|")
    '''                                .Write(dr1("UDF084") & "|")
    '''                                .Write(dr1("UDF085") & "|")
    '''                                .Write(dr1("UDF086") & "|")
    '''                                .Write(dr1("UDF087") & "|")
    '''                                .Write(dr1("UDF088") & "|")
    '''                                .Write(dr1("UDF089") & "|")
    '''                                .Write(dr1("UDF090") & "|")
    '''                                .Write(dr1("UDF091") & "|")
    '''                                .Write(dr1("UDF092") & "|")
    '''                                .Write(dr1("UDF093") & "|")
    '''                                .Write(dr1("UDF094") & "|")
    '''                                .Write(dr1("UDF095") & "|")
    '''                                .Write(dr1("UDF096") & "|")
    '''                                .Write(dr1("UDF097") & "|")
    '''                                .Write(dr1("UDF098") & "|")
    '''                                .Write(dr1("UDF099") & "|")

    '''                                .Write(Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year)
    '''                                .Write(vbCrLf)
    '''                            End With
    '''                        Next

    '''                    Else
    '''                        'we have no data return from GERS so write blanks
    '''                        With objStreamWriter
    '''                            For i = 1 To 36
    '''                                .Write("" & "|")
    '''                            Next
    '''                            .Write(Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year)
    '''                            .Write(vbCrLf)
    '''                        End With

    '''                    End If
    '''                Else
    '''                    ' we have the same sku so write blanks
    '''                    With objStreamWriter
    '''                        For i = 1 To 36
    '''                            .Write("" & "|")
    '''                        Next
    '''                        .Write(Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year)
    '''                        .Write(vbCrLf)
    '''                    End With

    '''                End If

    '''            Next
    '''            objStreamWriter.Close()
    '''            Dim _file As File
    '''            If _file.Exists(_localFilePath) Then
    '''                _file.Move(_localFilePath, _filePath)
    '''                SendAckXmlFile(AppSetting.AckFileDir, reserveFile, AppSetting.UtlFileDir, WarehouseNbr)
    '''                UpdateBackStockHistory(WarehouseNbr.ToString(), deptStr, "RESERVE_UPDATE_DATE")
    '''            End If
    '''            successMsg = skuCnt & " records found, "
    '''            successMsg &= "Backstock File '" & reserveFile & "' submitted to Biztalk process"
    '''            SendReserveUpdateEmail(WarehouseNbr, deptStr, True, reserveFile, skuCnt)
    '''        Else
    '''            successMsg = "No records found"
    '''        End If
    '''        Return successMsg
    '''    Catch ex As OracleException
    '''        _errMsg = ex.Message
    '''        Throw New BackStockAppException(ex.Message)
    '''    Catch ex As BackStockAppException
    '''        _errMsg = ex.Message
    '''        Throw New BackStockAppException(ex.Message)
    '''    Catch ex As FileNotFoundException
    '''        _errMsg = ex.Message
    '''        Throw New BackStockAppException(ex.Message)
    '''    Catch ex As DirectoryNotFoundException
    '''        _errMsg = ex.Message
    '''        Throw New BackStockAppException(ex.Message)
    '''    Catch ex As Security.SecurityException
    '''        _errMsg = ex.Message
    '''        Throw New BackStockAppException(ex.Message)
    '''    Catch ex As Exception
    '''        _errMsg = ex.Message
    '''        Throw ex
    '''    Finally
    '''        If _errMsg <> "" Then
    '''            SendReserveUpdateEmail(WarehouseNbr, deptStr, False, reserveFile, skuCnt, _errMsg)
    '''        End If
    '''    End Try

    '''End Function

    'Public Function ReserveUpdate() As String
    '    Dim reserveFile As String = ""
    '    Dim successMsg As String = ""
    '    Try
    '        'Call Resserve clear to backstock data in Arthur
    '        ReserveClear(False)
    '        _errMsg = ""
    '        Dim dr As OracleDataReader
    '        Dim cmdParameters() As OracleParameter = New OracleParameter(6) {}
    '        cmdParameters(0) = New OracleParameter("p_localfile_dir", OracleType.VarChar, 40)
    '        cmdParameters(0).Value = AppSetting.WmsLocalFileDir
    '        cmdParameters(1) = New OracleParameter("p_ftp_user", OracleType.VarChar, 30)
    '        cmdParameters(1).Value = AppSetting.FtpUser
    '        cmdParameters(2) = New OracleParameter("p_ftp_pwd", OracleType.VarChar, 30)
    '        cmdParameters(2).Value = AppSetting.FtpPwd
    '        cmdParameters(3) = New OracleParameter("p_ftp_host", OracleType.VarChar, 30)
    '        cmdParameters(3).Value = AppSetting.FtpHost
    '        cmdParameters(4) = New OracleParameter("p_ftp_remotepath", OracleType.VarChar, 40)
    '        cmdParameters(4).Value = AppSetting.FtpFolder
    '        cmdParameters(5) = New OracleParameter("p_file_name", OracleType.VarChar, 40)
    '        cmdParameters(5).Direction = ParameterDirection.Output
    '        cmdParameters(6) = New OracleParameter("p_rec_cnt", OracleType.Int32)
    '        cmdParameters(6).Direction = ParameterDirection.Output

    '        'dr = _wmsCn.ExecuteReader("HT_BACKSTOCK.PROC_PIC_LOCN_FTP", cmdParameters)
    '        dr = _wmsCn.ExecuteReader(AppSetting.WMBackstockPkg & ".PROC_PIC_LOCN_FTP", cmdParameters)
    '        reserveFile = cmdParameters(5).Value
    '        If reserveFile <> "" Then
    '            _errMsg = ""
    '            successMsg = cmdParameters(6).Value & " records found, "
    '            successMsg &= "Backstock File '" & reserveFile & "' submitted to Biztalk process"
    '            SendReserveUpdateEmail(True, reserveFile, cmdParameters(6).Value)
    '        Else
    '            _errMsg = "Failed to create backstock file"
    '            Throw New BackStockAppException(_errMsg)
    '        End If
    '        Return successMsg
    '    Catch ex As OracleException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As Exception
    '        _errMsg = ex.Message
    '        Throw ex
    '    Finally
    '        If _errMsg <> "" Then
    '            SendReserveUpdateEmail(False, reserveFile, _errMsg)
    '        End If
    '    End Try
    'End Function
    Public Function ReserveBatch(ByVal WarehouseNbr As Int16, Optional ByVal deptStr As String = "") As Int16
        Try
            Dim rtnVal As Int16

            If deptStr.Length > 4000 Then
                Throw New BackStockAppException("Too many departments selected, max 80  departments can be selected.")
            End If
            Dim cmdParameters() As OracleParameter = New OracleParameter(4) {}
            cmdParameters(0) = New OracleParameter("p_warehouse_nbr", OracleType.VarChar, 4)
            cmdParameters(0).Value = WarehouseNbr
            cmdParameters(1) = New OracleParameter("p_biztalkfile_path", OracleType.VarChar, 40)
            cmdParameters(1).Value = AppSetting.BatchBizTalkFileDir
            cmdParameters(2) = New OracleParameter("p_dept_str", OracleType.VarChar, 4000)
            cmdParameters(2).Value = deptStr
            cmdParameters(3) = New OracleParameter("p_rec_cnt", OracleType.Int32)
            cmdParameters(3).Direction = ParameterDirection.Output
            cmdParameters(3).Value = 0
            cmdParameters(4) = New OracleParameter("p_batch_id", OracleType.Int32)
            cmdParameters(4).Direction = ParameterDirection.Output
            cmdParameters(4).Value = 0

            _errMsg = ""
            rtnVal = _aaCn.ExecuteNonQuery(AppSetting.AABackstockPkg & ".PROC_WORKLIST_RESERVE_BATCH", cmdParameters)
            If rtnVal = 1 Then
                If cmdParameters(3).Value > 0 Then
                    SendReserveBatchEmail(WarehouseNbr, True, deptStr, cmdParameters(3).Value, cmdParameters(4).Value)
                    UpdateBackStockHistory(WarehouseNbr.ToString(), deptStr, "RESERVE_BATCH_DATE")
                Else
                    Throw New BackStockAppException("No records found")
                End If
            End If
            Return rtnVal
        Catch ex As OracleException
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Catch ex As Exception
            _errMsg = ex.Message
            Throw ex
        End Try
    End Function
    Public Function ListDepartMents_AA(Optional ByVal divCd As EnumDivCd = EnumDivCd.None) As DataSet
        Try
            Dim companyFilterStr As String = ""
            Select Case divCd
                Case EnumDivCd.HotTopic
                    companyFilterStr = "DIV_CD IN (" & EnumDivCd.HotTopic & ") AND DEPT_CD NOT BETWEEN '7000' AND '7099'"
                Case EnumDivCd.Torrid
                    companyFilterStr = "DIV_CD IN (" & EnumDivCd.Torrid & ")"
                Case EnumDivCd.Lovesick
                    companyFilterStr = "DIV_CD IN (" & EnumDivCd.Lovesick & ")"
                Case EnumDivCd.ShockHound
                    companyFilterStr = "DIV_CD IN (" & EnumDivCd.ShockHound & ")  "
                Case Else
                    companyFilterStr = EnumDivCd.HotTopic & "," & EnumDivCd.Torrid & "," & EnumDivCd.ShockHound
            End Select
            'Added filter not to include dept # 36
            _strSql = "SELECT LPAD(DEPT_CD, 4, '0')as DEPT_CD, DIV_CD , DES as Description FROM C$_LS_DEPT " & _
                        " WHERE " & companyFilterStr & " and dept_cd not in ('36') ORDER BY DIV_CD, DEPT_CD"
            Return _aaCn.ExecuteDataSet(_strSql)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'Public Function ListDepartMents(Optional ByVal divCd As EnumDivCd = EnumDivCd.None) As DataSet
    '    Try
    '        Dim companyFilterStr As String = ""
    '        Select Case divCd
    '            Case EnumDivCd.HotTopic
    '                companyFilterStr = "DIV_CD IN (" & EnumDivCd.HotTopic & ") AND DEPT_CD NOT BETWEEN '7000' AND '7099'"
    '            Case EnumDivCd.Torrid
    '                companyFilterStr = "DIV_CD IN (" & EnumDivCd.Torrid & ")"
    '            Case EnumDivCd.HotTopicCanada
    '                companyFilterStr = "DEPT_CD BETWEEN '7000' AND '7099' "
    '            Case EnumDivCd.ShockHound
    '                companyFilterStr = "DIV_CD IN (" & EnumDivCd.ShockHound & ")  "
    '            Case Else
    '                companyFilterStr = EnumDivCd.HotTopic & "," & EnumDivCd.Torrid & "," & EnumDivCd.ShockHound
    '        End Select
    '        _strSql = "SELECT DEPT_CD, DIV_CD , DES as Description FROM DEPT " & _
    '                    " WHERE " & companyFilterStr & " ORDER BY DIV_CD, DEPT_CD"
    '        Return _gersCn.ExecuteDataSet(_strSql)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    Private Sub SendReserveClearEmail(ByVal WarehouseNbr As Int16, ByVal deptStr As String, ByVal successFlag As Boolean, Optional ByVal failureReason As String = "N/A")
        Try
            Dim emailMgr As EmailManager = New EmailManager(AppSetting.SmtpServer)
            Dim mailBody, sucessStr, subject As String
            If successFlag Then
                sucessStr = "SUCCESS"
                subject = "Reserve clear completed"
            Else
                sucessStr = "FAILED"
                subject = "Reserve clear failed"
            End If
            mailBody = AppSetting.ReserveClearEmailBody
            mailBody = mailBody.Replace("%processDate%", Now)
            mailBody = mailBody.Replace("%whse%", WarehouseNbr)
            mailBody = mailBody.Replace("%deptStr%", deptStr)
            mailBody = mailBody.Replace("%status%", sucessStr)
            mailBody = mailBody.Replace("%reason%", failureReason)
            With emailMgr
                .AddRecipient(AppSetting.EMailReceivers)
                .Sender = AppSetting.SmtpSender
                .Subject = subject
                .Format = Web.Mail.MailFormat.Html
                .Priority = Web.Mail.MailPriority.Normal
                .Send(mailBody)
            End With
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    Public Sub SendReserveBatchEmail(ByVal WarehouseNbr As Int16, ByVal successFlag As Boolean, ByVal deptStr As String, ByVal skuCnt As Int32, ByVal batchID As Int32, Optional ByVal failureReason As String = "N/A")
        Try
            Dim emailMgr As EmailManager = New EmailManager(AppSetting.SmtpServer)
            Dim mailBody, sucessStr, subject As String
            If successFlag Then
                sucessStr = "SUCCESS"
                subject = "Reserve batch completed"
            Else
                sucessStr = "FAILED"
                subject = "Reserve batch failed"
            End If
            mailBody = AppSetting.ReserveBatchEmailBody
            mailBody = mailBody.Replace("%processDate%", Now)
            mailBody = mailBody.Replace("%whse%", WarehouseNbr)
            mailBody = mailBody.Replace("%deptStr%", deptStr)
            mailBody = mailBody.Replace("%skuCnt%", skuCnt)
            mailBody = mailBody.Replace("%batchID%", batchID)
            mailBody = mailBody.Replace("%status%", sucessStr)
            mailBody = mailBody.Replace("%reason%", failureReason)
            With emailMgr
                .AddRecipient(AppSetting.EMailReceivers)
                .Sender = AppSetting.SmtpSender
                .Subject = subject
                .Format = Web.Mail.MailFormat.Html
                .Priority = Web.Mail.MailPriority.Normal
                .Send(mailBody)
            End With
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    Private Sub SendReserveUpdateEmail(ByVal WarehouseNbr As Int16, ByVal deptStr As String, ByVal successFlag As Boolean, ByVal reserveFile As String, ByVal skuCnt As Int32, Optional ByVal failureReason As String = "N/A")
        '' ByVal PrePackskuCnt As Int32, Optional ByVal failureReason As String = "N/A")
        Try
            Dim emailMgr As EmailManager = New EmailManager(AppSetting.SmtpServer)
            Dim mailBody, sucessStr, subject As String
            If successFlag Then
                sucessStr = "SUCCESS"
                'subject = "Reserve update - backstock file submitted to biztalk process"
                subject = "Reserve update - backstock on demand ETL is initiated"
            Else
                sucessStr = "FAILED"
                subject = "Reserve update failed"
            End If
            mailBody = AppSetting.ReserveUpdateEmailBody
            mailBody = mailBody.Replace("%processDate%", Now)
            mailBody = mailBody.Replace("%whse%", WarehouseNbr)
            mailBody = mailBody.Replace("%deptStr%", deptStr)
            mailBody = mailBody.Replace("%reserveFile%", reserveFile)
            mailBody = mailBody.Replace("%skuCnt%", skuCnt)
            mailBody = mailBody.Replace("%status%", sucessStr)
            mailBody = mailBody.Replace("%reason%", failureReason)
            With emailMgr
                .AddRecipient(AppSetting.EMailReceivers)
                .Sender = AppSetting.SmtpSender
                .Subject = subject
                .Format = Web.Mail.MailFormat.Html
                .Priority = Web.Mail.MailPriority.Normal
                .Send(mailBody)
            End With
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    Public Sub CheckExportStatus(ByVal WHSE As Int16)
        Try
            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            Dim dr As DataRow
            Dim ds As DataSet
            Dim dr1 As OracleDataReader

            cmdParameters(0) = New OracleParameter("p_warehouse_nbr", OracleType.VarChar)
            cmdParameters(0).Value = WHSE
            cmdParameters(1) = New OracleParameter("p_cur_Allocs", OracleType.Cursor)
            cmdParameters(1).Direction = ParameterDirection.Output
            ds = _aaCn.ExecuteDataSet(AppSetting.AABackstockPkg & ".LIST_EXPORT_PENDING_ALLOCS", cmdParameters)

            For Each dr In ds.Tables(0).Rows
                'get the count from WM STORE_DISTRO table
                Dim cmdParams() As OracleParameter = New OracleParameter(2) {}
                cmdParams(0) = New OracleParameter("p_distro_nbr", OracleType.VarChar)
                cmdParams(0).Value = dr("ALLOC_NBR")
                cmdParams(1) = New OracleParameter("p_batch_id", OracleType.VarChar)
                cmdParams(1).Value = dr("BATCH_ID")
                cmdParams(2) = New OracleParameter("p_rec_cnt", OracleType.Int16)
                cmdParams(2).Direction = ParameterDirection.Output
                Select Case WHSE
                    Case EnumWarehouse.OHDC
                        dr1 = _wmsCn.ExecuteReader(AppSetting.WMBackstockPkg & ".PROC_GET_DISTRO_COUNT", cmdParams)
                    Case EnumWarehouse.TNDC
                        dr1 = _tnDcCn.ExecuteReader(AppSetting.WMBackstockPkg & ".PROC_GET_DISTRO_COUNT", cmdParams)
                End Select
                'dr1 = _wmsCn.ExecuteReader(AppSetting.WMBackstockPkg & ".PROC_GET_DISTRO_COUNT", cmdParams)
                If cmdParams(2).Value > 0 Then
                    '-- update AA 
                    Dim cmdParams1() As OracleParameter = New OracleParameter(5) {}
                    cmdParams1(0) = New OracleParameter("p_batch_id", OracleType.Int32)
                    cmdParams1(0).Value = dr("BATCH_ID")
                    cmdParams1(1) = New OracleParameter("p_alloc_nbr", OracleType.Int32)
                    cmdParams1(1).Value = dr("ALLOC_NBR")
                    cmdParams1(2) = New OracleParameter("p_warehouse_nbr", OracleType.VarChar)
                    cmdParams1(2).Value = WHSE
                    cmdParams1(3) = New OracleParameter("p_export_flag", OracleType.Int32)
                    cmdParams1(3).Value = 1
                    cmdParams1(4) = New OracleParameter("p_email_sent_flag", OracleType.Int32)
                    cmdParams1(4).Value = 0
                    cmdParams1(5) = New OracleParameter("p_user_id", OracleType.VarChar)
                    cmdParams1(5).Value = "SYSTEM"
                    _aaCn.ExecuteNonQuery(AppSetting.AABackstockPkg & ".PROC_UPDATE_HT_RESERVE_BATCH", cmdParams1)
                End If
            Next
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    Public Sub CheckExportStatusAndCreateRsvBtchRpt(ByVal WHSE As Int16)
        Try
            Dim intGenRptFlag As Int16 = 0
            Dim intWHSERecCnt As Int16 = 0
            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            Dim dr As DataRow
            Dim ds, dsRpt As DataSet
            Dim dr1 As OracleDataReader
            Dim drWMSTable() As DataRow

            cmdParameters(0) = New OracleParameter("p_warehouse_nbr", OracleType.VarChar)
            cmdParameters(0).Value = WHSE
            cmdParameters(1) = New OracleParameter("p_cur_Allocs", OracleType.Cursor)
            cmdParameters(1).Direction = ParameterDirection.Output
            ds = _aaCn.ExecuteDataSet(AppSetting.AABackstockPkg & ".LIST_EXPORT_PENDING_ALLOCS", cmdParameters)

            For Each dr In ds.Tables(0).Rows
                intWHSERecCnt = 0
                If (dr("ALLOC_NBR") <> 0) Then
                    'get the count from WM STORE_DISTRO table
                    Dim cmdParams() As OracleParameter = New OracleParameter(2) {}
                    cmdParams(0) = New OracleParameter("p_distro_nbr", OracleType.VarChar)
                    cmdParams(0).Value = dr("ALLOC_NBR")
                    cmdParams(1) = New OracleParameter("p_batch_id", OracleType.VarChar)
                    cmdParams(1).Value = dr("BATCH_ID")
                    cmdParams(2) = New OracleParameter("p_rec_cnt", OracleType.Int16)
                    cmdParams(2).Direction = ParameterDirection.Output
                    Select Case WHSE
                        Case EnumWarehouse.OHDC
                            dr1 = _wmsCn.ExecuteReader(AppSetting.WMBackstockPkg & ".PROC_GET_DISTRO_COUNT", cmdParams)
                        Case EnumWarehouse.TNDC
                            dr1 = _tnDcCn.ExecuteReader(AppSetting.WMBackstockPkg & ".PROC_GET_DISTRO_COUNT", cmdParams)
                    End Select
                    intWHSERecCnt = cmdParams(2).Value
                    'we need to close the reader in order to close the connection
                    dr1.Close()

                End If

                'dr1 = _wmsCn.ExecuteReader(AppSetting.WMBackstockPkg & ".PROC_GET_DISTRO_COUNT", cmdParams)
                If intWHSERecCnt > 0 Or dr("ALLOC_NBR") = 0 Then

                    Dim strFileName As String = AppSetting.BSRptFolderLoc & "\Batch_" & dr("BATCH_ID").ToString() & "_DC_" & WHSE.ToString() & "_DIV_" & "X_DIV_NBR_X" & "_" & DateTime.Now.ToString("yyyy") & DateTime.Now.ToString("MM") & DateTime.Now.ToString("dd") & DateTime.Now.ToString("HH") & DateTime.Now.ToString("mm") & DateTime.Now.ToString("ss") & DateTime.Now.ToString("fff") & ".xls"

                    If dr("ALLOC_NBR") <> 0 Then

                        '-- update AA 
                        Dim cmdParams1() As OracleParameter = New OracleParameter(6) {}
                        cmdParams1(0) = New OracleParameter("p_batch_id", OracleType.Int32)
                        cmdParams1(0).Value = dr("BATCH_ID")
                        cmdParams1(1) = New OracleParameter("p_alloc_nbr", OracleType.Int32)
                        cmdParams1(1).Value = dr("ALLOC_NBR")
                        cmdParams1(2) = New OracleParameter("p_warehouse_nbr", OracleType.VarChar)
                        cmdParams1(2).Value = WHSE
                        cmdParams1(3) = New OracleParameter("p_export_flag", OracleType.Int32)
                        cmdParams1(3).Value = 1
                        cmdParams1(4) = New OracleParameter("p_email_sent_flag", OracleType.Int32)
                        cmdParams1(4).Value = 0
                        cmdParams1(5) = New OracleParameter("p_user_id", OracleType.VarChar)
                        cmdParams1(5).Value = "SYSTEM"
                        cmdParams1(6) = New OracleParameter("P_RPT_LINK", OracleType.VarChar)
                        cmdParams1(6).Value = strFileName
                        _aaCn.ExecuteNonQuery(AppSetting.AABackstockPkg & ".PROC_UPDATE_HT_RESERVE_BATCH", cmdParams1)

                    End If

                    'check to see if you have any record
                    Dim strSQL As String = "SELECT COUNT(*) as Total_Count FROM HT_RESERVE_BATCH_HISTORY WHERE BATCH_ID = '" & dr("BATCH_ID").ToString() & "' AND EXPORT_FLAG = 0"
                    Dim reader As OracleDataReader
                    reader = _aaCn.ExecuteReader(strSQL)

                    While (reader.Read())
                        If (System.Convert.ToInt32(reader("Total_Count").ToString()) = 0) Then
                            intGenRptFlag = 1
                        Else
                            intGenRptFlag = 0
                        End If
                    End While

                    reader.Close()

                    If (intGenRptFlag = 1) Then

                        Dim strDIV_NBRs As String = String.Empty
                        Dim htDIV_NBR As New Hashtable()

                        'Call RunReserveBatchReport 
                        dsRpt = RunReserveBatchReport(dr("BATCH_ID"), WHSE)

                        If dsRpt.Tables.Count > 0 Then
                            'dgAA.DataSource = _ds.Tables("AAAllocs")
                            'Add the required fields
                            dsRpt.Tables(0).Columns.Add("REQD_QTY", Type.GetType("System.Decimal"))
                            dsRpt.Tables(0).Columns.Add("Variance", Type.GetType("System.Decimal"))


                            'Merge and Evaluate Variance column value
                            For Each row As DataRow In dsRpt.Tables(0).Rows

                                If (htDIV_NBR.ContainsKey(row("DIV_NBR").ToString()) = False) Then
                                    htDIV_NBR.Add(row("DIV_NBR").ToString(), row("DIV_NBR").ToString())
                                    'construct DIV_NBR list
                                    If (strDIV_NBRs.Length > 0) Then
                                        strDIV_NBRs = strDIV_NBRs + "_" + row("DIV_NBR").ToString()
                                    Else
                                        strDIV_NBRs = row("DIV_NBR").ToString()
                                    End If
                                End If

                                drWMSTable = dsRpt.Tables("WMDistros").Select("SKU_NBR = '" + row("SKU_NBR").ToString() + "' and ALLOC_NBR='" + row("ALLOC_NBR").ToString() + "' and DEPT_NBR ='" + row("DEPT_NBR").ToString() + "'")
                                If (drWMSTable.Length > 0) Then
                                    row("REQD_QTY") = drWMSTable(0).ItemArray(3)
                                Else
                                    row("REQD_QTY") = 0
                                End If
                                If (row("QTY") Is DBNull.Value) Then
                                    row("QTY") = 0
                                End If
                                row("Variance") = row("QTY") - row("REQD_QTY")
                            Next row
                        End If
                        drWMSTable = Nothing

                        'populating the DIV NBR list
                        strFileName = strFileName.Replace("X_DIV_NBR_X", strDIV_NBRs)

                        Dim excel As New Microsoft.Office.Interop.Excel.ApplicationClass
                        Dim wBook As Microsoft.Office.Interop.Excel.Workbook
                        Dim wSheet As Microsoft.Office.Interop.Excel.Worksheet
                        Dim misValue As Object = System.Reflection.Missing.Value

                        Try
                            wBook = excel.Workbooks.Add()
                            wSheet = wBook.ActiveSheet()

                            'excel.Calculation = Microsoft.Office.Interop.Excel.XlCalculation.xlCalculationManual

                            Dim dc As System.Data.DataColumn
                            Dim drow As System.Data.DataRow
                            Dim colIndex As Integer = 0
                            Dim rowIndex As Integer = 0

                            'header-1
                            Dim style1 As Excel.Style = wSheet.Application.ActiveWorkbook.Styles.Add("Header-1")
                            style1.Font.Bold = True
                            'style1.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGray)

                            'header-2
                            Dim style2 As Excel.Style = wSheet.Application.ActiveWorkbook.Styles.Add("Header-2")
                            style2.Font.Bold = True
                            style2.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGray)

                            'header-3
                            Dim style3 As Excel.Style = wSheet.Application.ActiveWorkbook.Styles.Add("Header-3")
                            style3.Font.Bold = True
                            style3.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Orange)

                            rowIndex += 1
                            excel.Cells(rowIndex, 1) = "RESERVE BATCH DETAILS REPORT (BATCH # " & dr("BATCH_ID") & " )"
                            excel.Cells(rowIndex, 1).Style = "Header-1"
                            excel.Range(excel.Cells(rowIndex, 1), excel.Cells(rowIndex, 10)).Merge()

                            excel.Range(excel.Cells(rowIndex, 1), excel.Cells(rowIndex, 10)).HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter


                            rowIndex += 1
                            excel.Cells(rowIndex, 1) = "AA Allocations"
                            excel.Cells(rowIndex, 1).Style = "Header-2"
                            excel.Range(excel.Cells(rowIndex, 1), excel.Cells(rowIndex, 8)).Merge()

                            excel.Cells(rowIndex, 9) = "WM Distros"
                            excel.Cells(rowIndex, 9).Style = "Header-2"
                            excel.Range(excel.Cells(rowIndex, 9), excel.Cells(rowIndex, 10)).Merge()

                            dsRpt.Tables(0).Columns.Remove("UNIQUE_MER_KEY")
                            dsRpt.Tables(0).Columns.Remove("DIV_NBR")

                            For Each dc In dsRpt.Tables(0).Columns
                                colIndex = colIndex + 1
                                excel.Cells(3, colIndex) = dc.ColumnName
                                excel.Cells(3, colIndex).Style = "Header-3"
                            Next


                            For Each drow In dsRpt.Tables(0).Rows
                                rowIndex = rowIndex + 1
                                colIndex = 0
                                For Each dc In dsRpt.Tables(0).Columns
                                    colIndex = colIndex + 1
                                    excel.Cells(rowIndex + 1, colIndex) = drow(dc.ColumnName)

                                Next
                            Next

                            wSheet.Columns.AutoFit()

                            'excel.Calculation = Microsoft.Office.Interop.Excel.XlCalculation.xlCalculationAutomatic



                            'Dim blnFileOpen As Boolean = False

                            'Try
                            '    Dim fileTemp As System.IO.FileStream = System.IO.File.OpenWrite(strFileName)
                            '    fileTemp.Close()
                            'Catch ex As Exception
                            '    blnFileOpen = False
                            'End Try

                            'If System.IO.File.Exists(strFileName) Then
                            '    System.IO.File.Delete(strFileName)
                            'End If

                            wBook.Application.DisplayAlerts = False
                            wBook.SaveAs(strFileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlShared, misValue, misValue, misValue, misValue, misValue)

                            excel.Workbooks.Close()
                            excel.Quit()


                            ReleaseObject(wSheet)
                            ReleaseObject(wBook)
                            ReleaseObject(excel)

                            wSheet = Nothing
                            wBook = Nothing
                            excel = Nothing

                        Catch ex As Exception
                            If (IsNothing(excel) = False) Then
                                excel.Workbooks.Close()
                                excel.Quit()
                                ReleaseObject(excel)
                                wSheet = Nothing
                                wBook = Nothing
                                excel = Nothing
                            End If
                            Throw New BackStockAppException(ex.Message)
                        End Try


                        'excel.Workbooks.Open(strFileName)
                        'excel.Visible = True

                        'Send an email
                        Dim emailMgr As EmailManager = New EmailManager(AppSetting.SmtpServer)
                        Dim mailBody, subject As String
                        subject = "Back Stock Alert"
                        mailBody = " Reserve batch ID # " & dr("BATCH_ID").ToString() & " exported to Warehouse " & WHSE.ToString() & " successfully. The report can be found in the following location " & strFileName

                        With emailMgr
                            .AddRecipient(AppSetting.EMailReceivers)
                            .Sender = AppSetting.SmtpSender
                            .Subject = subject
                            .Format = Web.Mail.MailFormat.Text
                            .Priority = Web.Mail.MailPriority.Normal
                            .Send(mailBody)
                        End With

                        'Update Arthur and send a email Notification
                        Dim strSQL1 As String = "UPDATE HT_RESERVE_BATCH_HISTORY SET EMAIL_SENT_FLAG = 1 WHERE BATCH_ID = '" & dr("BATCH_ID").ToString() & "'"
                        _aaCn.ExecuteNonQuery(strSQL1)

                    End If

                End If
            Next
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    Private Sub ReleaseObject(ByVal obj As Object)
        Try
            Dim intRel As Integer = 0
            Do
                intRel = System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            Loop While intRel > 0
            'MsgBox("Final Released obj # " & intRel)
        Catch ex As Exception
            'MsgBox("Error releasing object" & ex.ToString)
            System.Diagnostics.EventLog.WriteEntry("BackstockWinApp_Error", "Error releasing object" & ex.ToString())
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
    Public Function GetWaveCheckReport(ByVal waveNbr As String, ByVal WarehouseNbr As Int16) As DataSet
        Try
            Dim ds As DataSet
            _strSql = "SELECT IWM.MISC_ALPHA_1 AS DEPT, DISTRO_NBR,SHIP_WAVE_NBR, " & _
                        " I.STYLE || '-' || I.STYLE_SFX AS SKU_NUM, SUM(REQD_QTY)  AS REQD_QTY, " & _
                        " SUM(ORIG_REQ_QTY) AS ORIG_REQ_QTY " & _
                        " FROM STORE_DISTRO D , ITEM_MASTER I, ITEM_WHSE_MASTER IWM " & _
                        " WHERE SHIP_WAVE_NBR = '" & waveNbr & "' AND D.SKU_ID = I.SKU_ID AND DISTRO_TYPE = '1'  AND I.SKU_ID = IWM.SKU_ID " & _
                        " GROUP BY IWM.MISC_ALPHA_1,DISTRO_NBR, SHIP_WAVE_NBR, I.STYLE || '-' || I.STYLE_SFX  " & _
                        " ORDER BY 1,2,3,4"
            Select Case WarehouseNbr
                Case EnumWarehouse.CADC
                    ds = _wmsCn.ExecuteDataSet(_strSql)
                Case EnumWarehouse.TNDC
                    ds = _tnDcCn.ExecuteDataSet(_strSql)
            End Select
            'ds = _wmsCn.ExecuteDataSet(_strSql)
            Return ds
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function ReserveUpdateStatus() As DataSet
        Dim ds As DataSet
        Dim sb As New StringBuilder

        sb.Append("SELECT his.WAREHOUSE_NBR AS WAREHOUSE , his.DEPT_NBR, his.RESERVE_UPDATE_DATE, his.RESERVE_BATCH_DATE , his.RESERVE_CLEAR_DATE ")
        sb.Append("FROM ht_backstock_history his ")
        'sb.Append("RIGHT JOIN ls_dept dep ")
        'sb.Append("ON his.dept_nbr = dep.dept_cd ")
        'sb.Append("INNER JOIN ls_div div ")
        'sb.Append("ON dep.DIV_CD = div.DIV_CD ")
        'sb.Append("WHERE dep.DIV_CD in (1,5) ")
        sb.Append("ORDER BY his.WAREHOUSE_NBR, his.DEPT_NBR")

        Try
            ds = _aaCn.ExecuteDataSet(sb.ToString())
            Return ds
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function RunStatusCheckByBatch(ByVal batchID As Int16) As DataSet
        Try
            Dim ds As DataSet
            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            cmdParameters(0) = New OracleParameter("p_batch_id", OracleType.Int32)
            cmdParameters(0).Value = batchID
            cmdParameters(1) = New OracleParameter("p_cur_Allocs", OracleType.Cursor)
            cmdParameters(1).Direction = ParameterDirection.Output
            ds = _aaCn.ExecuteDataSet(AppSetting.AABackstockPkg & ".EXPORT_STATUS_BY_BATCH", cmdParameters)
            Return ds
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function RunReserveBatchReport(ByVal batchID As Int32, ByVal WarehouseNbr As Int16) As DataSet
        Try
            Dim ds, ds1, ds2 As DataSet
            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            cmdParameters(0) = New OracleParameter("p_batch_id", OracleType.Int32)
            cmdParameters(0).Value = batchID
            cmdParameters(1) = New OracleParameter("p_cur_Allocs", OracleType.Cursor)
            cmdParameters(1).Direction = ParameterDirection.Output
            ds1 = _aaCn.ExecuteDataSet(AppSetting.AABackstockPkg & ".LIST_ALLOCS_BY_BATCH", cmdParameters)
            ds = New DataSet
            ds.Tables.Add(ds1.Tables(0).Copy)
            ds.Tables(0).TableName = "AAAllocs"
            Dim cmdParams() As OracleParameter = New OracleParameter(1) {}
            cmdParams(0) = New OracleParameter("p_batch_id", OracleType.VarChar)
            cmdParams(0).Value = batchID
            cmdParams(1) = New OracleParameter("p_cur_distros", OracleType.Cursor)
            cmdParams(1).Direction = ParameterDirection.Output
            'ds2 = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_DISTROS_BY_BACTH", cmdParams)
            Select Case WarehouseNbr
                Case EnumWarehouse.CADC
                    ds2 = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_DISTROS_BY_BACTH", cmdParams)
                Case EnumWarehouse.TNDC
                    ds2 = _tnDcCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_DISTROS_BY_BACTH", cmdParams)
            End Select
            ds.Tables.Add(ds2.Tables(0).Copy)
            ds.Tables(1).TableName = "WMDistros"
            Return ds
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    'RunReserveBatchReport
    Public Function GetDbServerName() As String
        Dim dbServer As String = ""
        Dim strConn As String = ""
        Dim i As Int16 = 0
        Try
            strConn = AppSetting.AAConnectionString
            Dim strArr As String() = strConn.Split(";")
            For i = 0 To UBound(strArr)
                If strArr(i).ToUpper.IndexOf("DATA SOURCE") > -1 Then
                    dbServer = strArr(i).Substring(strArr(i).IndexOf("=") + 1)
                    Exit For
                End If
            Next
        Catch ex As Exception

        End Try
        Return dbServer
    End Function
    Private Sub SendAckXmlFile(ByVal biztalkFilePath As String, ByVal fileNme As String, ByVal filePath As String, ByVal whse As String)
        Try
            Dim xmlStr As String = ""
            Dim ackFile As String = fileNme.Replace(".txt", "_ACK.xml")
            biztalkFilePath = biztalkFilePath.TrimEnd("/")
            biztalkFilePath &= IIf(biztalkFilePath.EndsWith("\"), "", "\") & ackFile

            Dim objStreamWriter As StreamWriter
            objStreamWriter = New StreamWriter(biztalkFilePath, False)
            xmlStr = "<ReserveUpdateAck>" & _
                    "<FileName>" & fileNme & "</FileName>" & _
                    "<FilePath>" & filePath & "</FilePath>" & _
                    "<WarehouseNbr>" & whse & "</WarehouseNbr>" & _
                    "</ReserveUpdateAck>"
            objStreamWriter.Write(xmlStr)
            objStreamWriter.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    '*********************************************************
    '##BEGIN## 6/2/05 Sri: Cancel distro code
    '********************************************************
    Public Function ListDistros(ByVal whse As Int16, ByVal shpmtNbr As String, ByVal distroNbr As String) As DataSet
        Try
            Dim ds As DataSet
            Dim cmdParameters() As OracleParameter = New OracleParameter(2) {}
            cmdParameters(0) = New OracleParameter("p_shpmt_nbr", OracleType.VarChar, 20)
            cmdParameters(0).Value = shpmtNbr.Trim
            cmdParameters(1) = New OracleParameter("p_distro_nbr", OracleType.VarChar, 12)
            cmdParameters(1).Value = distroNbr.Trim
            cmdParameters(2) = New OracleParameter("DistroSummary", OracleType.Cursor)
            cmdParameters(2).Direction = ParameterDirection.Output
            Select Case whse
                Case EnumWarehouse.CADC
                    ds = _wmsCn.ExecuteDataSet("HT_WAVE_BY_TOOL.PROC_CANCELDISTRO_SUMMARY", cmdParameters)
                Case EnumWarehouse.TNDC
                    ds = _tnDcCn.ExecuteDataSet("HT_WAVE_BY_TOOL.PROC_CANCELDISTRO_SUMMARY", cmdParameters)
            End Select
            Return ds
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function

    Public Sub CancelDistro(ByVal whse As Int16, ByVal shpmtNbr As String, ByVal distroNbr As String)
        Try
            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            cmdParameters(0) = New OracleParameter("p_shpmt_nbr", OracleType.VarChar, 20)
            cmdParameters(0).Value = shpmtNbr.Trim
            cmdParameters(1) = New OracleParameter("p_distro_nbr", OracleType.VarChar, 12)
            cmdParameters(1).Value = distroNbr.Trim
            Select Case whse
                Case EnumWarehouse.CADC
                    _wmsCn.ExecuteNonQuery("HT_WAVE_BY_TOOL.PROC_CANCELDISTRO", cmdParameters)
                Case EnumWarehouse.TNDC
                    _tnDcCn.ExecuteNonQuery("HT_WAVE_BY_TOOL.PROC_CANCELDISTRO", cmdParameters)
            End Select
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    Public Sub PopulateWorkList(ByVal whse As Int16, ByVal shpmtNbr As String, ByVal distroNbr As String)
        Try
            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            cmdParameters(0) = New OracleParameter("p_shpmt_nbr", OracleType.VarChar, 20)
            cmdParameters(0).Value = shpmtNbr.Trim
            cmdParameters(1) = New OracleParameter("p_distro_nbr", OracleType.VarChar, 12)
            cmdParameters(1).Value = distroNbr.Trim
            Select Case whse
                Case EnumWarehouse.CADC
                    _wmsCn.ExecuteNonQuery("HT_WAVE_BY_TOOL.PROC_POPULATE_WORKLIST", cmdParameters)
                Case EnumWarehouse.TNDC
                    _tnDcCn.ExecuteNonQuery("HT_WAVE_BY_TOOL.PROC_POPULATE_WORKLIST", cmdParameters)
            End Select
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    '*********************************************************
    '##END## 6/2/05 Sri: Cancel distro code
    '********************************************************
    '*********************************************************
    '##BEGIN## 7/18/05 Sri: Distro Priority code
    '********************************************************
    'Public Sub SavePriorityGroup(ByVal prtyGrp As PriorityGroup)
    '    Try
    '        Dim cmdParameters() As OracleParameter = New OracleParameter(5) {}
    '        cmdParameters(0) = New OracleParameter("p_grp_id", OracleType.Int32)
    '        cmdParameters(0).Value = prtyGrp.grpId
    '        cmdParameters(1) = New OracleParameter("p_grp_name", OracleType.VarChar, 100)
    '        cmdParameters(1).Value = prtyGrp.grpName
    '        cmdParameters(2) = New OracleParameter("p_grp_desc", OracleType.VarChar, 100)
    '        cmdParameters(2).Value = prtyGrp.grpDesc
    '        cmdParameters(3) = New OracleParameter("p_min_locn_id", OracleType.Int32)
    '        cmdParameters(3).Value = prtyGrp.minLocnID
    '        cmdParameters(4) = New OracleParameter("p_max_locn_id", OracleType.Int32)
    '        cmdParameters(4).Value = prtyGrp.maxLocnId
    '        cmdParameters(5) = New OracleParameter("p_priority_code", OracleType.Int32)
    '        cmdParameters(5).Value = prtyGrp.priorityCode
    '        _aaCn.ExecuteNonQuery("HT_STORE_DISTRO_PRIORITY.SAVE_PRIORITY_GROUP", cmdParameters)
    '    Catch ex As OracleException
    '        If ex.Message.IndexOf("Duplicate") > 0 Then
    '            Throw New BackStockAppException("There is a conflict with another group, please enter valid store range!")
    '        Else
    '            Throw New BackStockAppException(ex.Message)
    '        End If
    '    Catch ex As Exception
    '        Throw New BackStockAppException(ex.Message)
    '    End Try
    'End Sub
    'Public Function ListPriorityGroups() As DataSet
    '    Try
    '        Dim ds As DataSet
    '        Dim cmdParameters() As OracleParameter = New OracleParameter(0) {}
    '        cmdParameters(0) = New OracleParameter("lstPriorityGrp", OracleType.Cursor)
    '        cmdParameters(0).Direction = ParameterDirection.Output
    '        ds = _aaCn.ExecuteDataSet("HT_STORE_DISTRO_PRIORITY.PROC_LIST_PRIORITY_GROUPS", cmdParameters)
    '        Return ds
    '    Catch ex As Exception
    '        Throw New BackStockAppException(ex.Message)
    '    End Try
    'End Function
    'Public Sub DeletePriorityGroupByID(ByVal grpID As Int16)
    '    Try
    '        Dim cmdParameters() As OracleParameter = New OracleParameter(0) {}
    '        cmdParameters(0) = New OracleParameter("p_grp_id", OracleType.Int32)
    '        cmdParameters(0).Value = grpID
    '        _aaCn.ExecuteNonQuery("HT_STORE_DISTRO_PRIORITY.DELETE_PRIORITY_GROUP", cmdParameters)

    '    Catch ex As Exception
    '        Throw New BackStockAppException(ex.Message)
    '    End Try
    'End Sub
    Public Function ListPriorityCodes() As DataSet
        Try
            Dim ds As DataSet
            Dim cmdParameters() As OracleParameter = New OracleParameter(0) {}
            cmdParameters(0) = New OracleParameter("lstPrtyCodes", OracleType.Cursor)
            cmdParameters(0).Direction = ParameterDirection.Output
            ds = _aaCn.ExecuteDataSet("HT_STORE_DISTRO_PRIORITY.PROC_LIST_PRTYCODE", cmdParameters)
            Return ds
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function ListStoresByPriority(ByVal priorityCode As Int16) As DataSet
        Try
            Dim ds As DataSet
            Dim cmdParameters() As OracleParameter = New OracleParameter(2) {}
            cmdParameters(0) = New OracleParameter("p_prtyCode", OracleType.VarChar, 2)
            cmdParameters(0).Value = priorityCode.ToString.PadLeft(2, "0")
            cmdParameters(1) = New OracleParameter("lstSelectedStore", OracleType.Cursor)
            cmdParameters(1).Direction = ParameterDirection.Output
            cmdParameters(2) = New OracleParameter("lstOtherStore", OracleType.Cursor)
            cmdParameters(2).Direction = ParameterDirection.Output
            ds = _aaCn.ExecuteDataSet("HT_STORE_DISTRO_PRIORITY.PROC_LIST_STORES_BY_PRTYCODE", cmdParameters)
            Return ds
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Sub SaveStorePriority(ByVal priorityCode As Int16, ByVal storeStr As String)
        Try
            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            cmdParameters(0) = New OracleParameter("p_prtyCode", OracleType.VarChar, 2)
            cmdParameters(0).Value = priorityCode.ToString.PadLeft(2, "0")
            cmdParameters(1) = New OracleParameter("p_StoreStr", OracleType.VarChar, 4000)
            cmdParameters(1).Value = storeStr
            _aaCn.ExecuteNonQuery("HT_STORE_DISTRO_PRIORITY.SAVE_STORE_PRIORITY", cmdParameters)

        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    '*********************************************************
    '##END## 7/18/05 Sri: Distro Priority code
    '********************************************************
    Public Function ValidateUser(ByVal userId As String, ByVal pwd As String) As String
        Dim userName As String = ""
        Try
            Dim ds As DataSet
            Dim cmdParameters() As OracleParameter = New OracleParameter(3) {}
            cmdParameters(0) = New OracleParameter("p_userid", OracleType.VarChar, 30)
            cmdParameters(0).Value = userId
            cmdParameters(1) = New OracleParameter("p_password", OracleType.VarChar, 30)
            cmdParameters(1).Value = pwd
            cmdParameters(2) = New OracleParameter("p_validflag", OracleType.VarChar, 5)
            cmdParameters(2).Direction = ParameterDirection.Output
            cmdParameters(3) = New OracleParameter("p_fullname", OracleType.VarChar, 100)
            cmdParameters(3).Direction = ParameterDirection.Output
            _aaCn.ExecuteNonQuery("HT_VALIDATE_USER", cmdParameters)
            If Not cmdParameters(2).Value.ToString.ToUpper = "TRUE" Then
                Throw New BackStockAppException("Invalid userId/Password,please try again!")
            End If
            userName = cmdParameters(3).Value
            Return userName
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function

    Public Function GetResetWLLineAllocs(ByVal strAlloc_Nbr As String, ByVal strUserName as String, ByRef strErrorMsg As String) As DataSet
        Try
            Dim dsAA, dsAARpt As New DataSet
            Dim dr As DataRow

            Dim cmdParameters() As OracleParameter = New OracleParameter(3) {}


            cmdParameters(0) = New OracleParameter("p_alloc_nbr", OracleType.Int32)
            cmdParameters(0).Value = strAlloc_Nbr

            cmdParameters(1) = New OracleParameter("p_req_type", OracleType.Number)
            cmdParameters(1).Value = 0 'Option to query distinct WL line records for the given alloc_nbr

            cmdParameters(2) = New OracleParameter("p_user_ID", OracleType.VarChar, 50)
            cmdParameters(2).Value = strUserName

            cmdParameters(3) = New OracleParameter("p_cur_Allocs", OracleType.Cursor)
            cmdParameters(3).Direction = ParameterDirection.Output


            dsAA = _aaCn.ExecuteDataSet(AppSetting.AABackstockPkg & ".LIST_RESET_WLLINES_BY_ALLOC", cmdParameters)

            If (dsAA.Tables(0).Rows.Count = 0) Then
                strErrorMsg = "The allocation number with status code 20 or 40 not found in Arthur"
                Return dsAARpt
            Else
                'check the exported status
                For Each dr In dsAA.Tables(0).Rows
                    If (dr("Exported").ToString.Trim().ToLower() = "exported") Then
                        'check it in corresponding DC to see if the allocation is in cancel/do not exist status
                        Dim cmdParams() As OracleParameter = New OracleParameter(2) {}

                        cmdParams(0) = New OracleParameter("P_DISTRO_NBR", OracleType.VarChar)
                        cmdParams(0).Value = strAlloc_Nbr

                        cmdParams(1) = New OracleParameter("p_validate_desc", OracleType.VarChar, 100)
                        cmdParams(1).Direction = ParameterDirection.Output
                        'cmdParams(1).Value = String.Empty

                        cmdParams(2) = New OracleParameter("p_validation_status", OracleType.VarChar, 10)
                        cmdParams(2).Direction = ParameterDirection.Output
                        'cmdParams(2).Value = False

                        Select Case dr("WAREHOUSE_NBR").ToString
                            Case "9985" 'CADC
                                _wmsCn.ExecuteNonQuery(AppSetting.WMBackstockPkg & ".PROC_VALIDATE_DISTRO", cmdParams)
                            Case "9997" 'TNDC
                                _tnDcCn.ExecuteNonQuery(AppSetting.WMBackstockPkg & ".PROC_VALIDATE_DISTRO", cmdParams)
                        End Select

                        If (cmdParams(2).Value = "INVALID") Then
                            strErrorMsg = cmdParams(1).Value
                            Return dsAARpt
                        End If
                    End If
                Next
            End If

            Dim cmdParams1() As OracleParameter = New OracleParameter(3) {}

            cmdParams1(0) = New OracleParameter("p_alloc_nbr", OracleType.Int32)
            cmdParams1(0).Value = strAlloc_Nbr

            cmdParams1(1) = New OracleParameter("p_req_type", OracleType.Number)
            cmdParams1(1).Value = 1 'Option to query all WL line records for the given alloc_nbr

            cmdParams1(2) = New OracleParameter("p_User_ID", OracleType.VarChar, 50)
            cmdParams1(2).Value = strUserName

            cmdParams1(3) = New OracleParameter("p_cur_Allocs", OracleType.Cursor)
            cmdParams1(3).Direction = ParameterDirection.Output

            dsAARpt = _aaCn.ExecuteDataSet(AppSetting.AABackstockPkg & ".LIST_RESET_WLLINES_BY_ALLOC", cmdParams1)


            Return dsAARpt

        Catch ex As Exception
            strErrorMsg = "An error occurred in GetResetWLLineAllocs, " + ex.ToString()
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function SetResetWLLineAllocs(ByVal strAlloc_Nbr As String, ByVal strUserName As String, ByRef strErrorMsg As String) As DataSet
        Try
            Dim dsAA As New DataSet

            strErrorMsg = String.Empty

            Dim cmdParameters() As OracleParameter = New OracleParameter(3) {}


            cmdParameters(0) = New OracleParameter("p_alloc_nbr", OracleType.Int32)
            cmdParameters(0).Value = strAlloc_Nbr

            cmdParameters(1) = New OracleParameter("p_req_type", OracleType.Number)
            cmdParameters(1).Value = 2 'Option to insert the updated WL line records in audit log table

            cmdParameters(2) = New OracleParameter("p_user_ID", OracleType.VarChar, 50)
            cmdParameters(2).Value = strUserName

            cmdParameters(3) = New OracleParameter("p_cur_Allocs", OracleType.Cursor)
            cmdParameters(3).Direction = ParameterDirection.Output


            dsAA = _aaCn.ExecuteDataSet(AppSetting.AABackstockPkg & ".LIST_RESET_WLLINES_BY_ALLOC", cmdParameters)

            strErrorMsg = "Successfully Reset the WL lines for the allocation # " & strAlloc_Nbr

            Return dsAA

        Catch ex As Exception
            strErrorMsg = "An error occurred in SetResetWLLineAllocs, " + ex.ToString()
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function GetWLLine(ByVal strWL_Key As String, ByVal strASN As String, ByVal strUserName As String, ByRef strErrorMsg As String) As DataSet
        Try
            Dim dsAA, dsAARpt As New DataSet
            Dim dr As DataRow

            Dim cmdParameters() As OracleParameter = New OracleParameter(4) {}


            cmdParameters(0) = New OracleParameter("p_wl_key", OracleType.Number)
            cmdParameters(0).Value = strWL_Key

            cmdParameters(1) = New OracleParameter("p_asn", OracleType.VarChar, 20)
            cmdParameters(1).Value = strASN

            cmdParameters(2) = New OracleParameter("p_req_type", OracleType.Number)
            cmdParameters(2).Value = 0 'Option to query WL line records for a given WL_Key

            cmdParameters(3) = New OracleParameter("p_user_ID", OracleType.VarChar, 50)
            cmdParameters(3).Value = strUserName

            cmdParameters(4) = New OracleParameter("p_cur_WL", OracleType.Cursor)
            cmdParameters(4).Direction = ParameterDirection.Output


            dsAA = _aaCn.ExecuteDataSet(AppSetting.AABackstockPkg & ".LIST_WLLINES_BY_WL_key", cmdParameters)

            If (dsAA.Tables(0).Rows.Count = 0) Then
                strErrorMsg = "The worklist key does not exist in Arthur"
                Return dsAARpt
            Else
                'check the exported status
                For Each dr In dsAA.Tables(0).Rows
                    'check it in corresponding DC to see if the ASN does exist
                    Dim cmdParams() As OracleParameter = New OracleParameter(3) {}

                    cmdParams(0) = New OracleParameter("P_ASN", OracleType.VarChar)
                    cmdParams(0).Value = strASN

                    cmdParams(1) = New OracleParameter("P_PO_NBR", OracleType.VarChar)
                    cmdParams(1).Value = dr("po_nbr")

                    cmdParams(2) = New OracleParameter("p_validate_desc", OracleType.VarChar, 100)
                    cmdParams(2).Direction = ParameterDirection.Output
                    'cmdParams(1).Value = String.Empty

                    cmdParams(3) = New OracleParameter("p_validation_status", OracleType.VarChar, 10)
                    cmdParams(3).Direction = ParameterDirection.Output
                    'cmdParams(2).Value = False

                    Select Case dr("WAREHOUSE_NBR").ToString
                        Case "9985" 'OHDC 03-jan-2020
                            _wmsCn.ExecuteNonQuery(AppSetting.WMBackstockPkg & ".PROC_VALIDATE_ASN", cmdParams)
                        Case "9997" 'TNDC
                            _tnDcCn.ExecuteNonQuery(AppSetting.WMBackstockPkg & ".PROC_VALIDATE_ASN", cmdParams)
                    End Select

                    If (cmdParams(3).Value = "NOT_EXIST") Then
                        strErrorMsg = cmdParams(2).Value
                        Return dsAARpt
                    End If
                Next
            End If

            Return dsAA

        Catch ex As Exception
            strErrorMsg = "An error occurred in GetWLLine, " + ex.ToString()
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function SetASNonWLLine(ByVal strWL_Key As String, ByVal strASN As String, ByVal strUserName As String, ByRef strErrorMsg As String) As DataSet
        Try
            Dim dsAA, dsAARpt As New DataSet

            strErrorMsg = String.Empty

            Dim cmdParameters() As OracleParameter = New OracleParameter(4) {}


            cmdParameters(0) = New OracleParameter("p_wl_key", OracleType.Number)
            cmdParameters(0).Value = strWL_Key

            cmdParameters(1) = New OracleParameter("p_asn", OracleType.VarChar, 20)
            cmdParameters(1).Value = strASN

            cmdParameters(2) = New OracleParameter("p_req_type", OracleType.Number)
            cmdParameters(2).Value = 1 'Option to update an ASN on WL line record for a given WL_Key

            cmdParameters(3) = New OracleParameter("p_user_ID", OracleType.VarChar, 50)
            cmdParameters(3).Value = strUserName

            cmdParameters(4) = New OracleParameter("p_cur_WL", OracleType.Cursor)
            cmdParameters(4).Direction = ParameterDirection.Output

            dsAA = _aaCn.ExecuteDataSet(AppSetting.AABackstockPkg & ".LIST_WLLINES_BY_WL_key", cmdParameters)

            strErrorMsg = "Successfully updated ASN # " & strASN & " for the worklist key # " & strWL_Key

            Return dsAA

        Catch ex As Exception
            strErrorMsg = "An error occurred in SetASNonWLLine, " + ex.ToString()
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function GetClosingStoreDistros(ByVal strWarehousenbr As String, ByVal strStorenbr As String, ByVal strUserName As String, ByRef strErrorMsg As String) As DataSet
        Try
            Dim reader As OracleDataReader
            Dim strSQL As String
            Dim dsWMSRpt As New DataSet

            strSQL = "select * from LOCATIONS where location_id = ' " + strStorenbr + "'"

            reader = _aaCn.ExecuteReader(strSQL)

            If (reader.HasRows =False) Then
                strErrorMsg = "Store not found in Arthur"
                reader.Close()
                Return dsWMSRpt
            Else
                While (reader.Read())
                    If (reader("AUTH_FOR_MERCH").ToString().Trim().ToUpper() <> "N") Then
                        strErrorMsg = "Store has not been turned off for allocations in Arthur"
                        reader.Close()
                        Return dsWMSRpt
                    Else
                        'check it in corresponding DC to see if the store does exist
                        Dim cmdParams() As OracleParameter = New OracleParameter(4) {}

                        cmdParams(0) = New OracleParameter("P_Store", OracleType.VarChar)
                        cmdParams(0).Value = strStorenbr

                        cmdParams(1) = New OracleParameter("p_req_type", OracleType.Number)
                        cmdParams(1).Value = 0 'Option to list the Distros

                        cmdParams(2) = New OracleParameter("p_user_ID", OracleType.VarChar, 50)
                        cmdParams(2).Value = strUserName

                        cmdParams(3) = New OracleParameter("p_validate_desc", OracleType.VarChar, 100)
                        cmdParams(3).Direction = ParameterDirection.Output

                        cmdParams(4) = New OracleParameter("p_cur_distros", OracleType.Cursor)
                        cmdParams(4).Direction = ParameterDirection.Output

                        Select Case strWarehousenbr
                            Case "985" 'OHDC 03-jan-2020
                                dsWMSRpt = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_CLOSEDSTORE_DISTROS", cmdParams)
                            Case "997" 'TNDC
                                dsWMSRpt = _tnDcCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_CLOSEDSTORE_DISTROS", cmdParams)
                        End Select

                        If (cmdParams(3).Value.ToString().Length > 0) Then
                            strErrorMsg = cmdParams(3).Value
                            reader.Close()
                            Return dsWMSRpt
                        End If

                    End If
                End While
            End If

            reader.Close()
            Return dsWMSRpt

        Catch ex As Exception
            strErrorMsg = "An error occurred in GetClosingStoreDistros, " + ex.ToString()
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function CancelClosingStoreDistros(ByVal strWarehousenbr As String, ByVal strStorenbr As String, ByVal strUserName As String, ByRef strErrorMsg As String) As DataSet
        Try

            Dim dsWMSRpt As New DataSet
            'check it in corresponding DC to see if the store does exist
            Dim cmdParams() As OracleParameter = New OracleParameter(4) {}

            cmdParams(0) = New OracleParameter("P_Store", OracleType.VarChar)
            cmdParams(0).Value = strStorenbr

            cmdParams(1) = New OracleParameter("p_req_type", OracleType.Number)
            cmdParams(1).Value = 1 'Option to cancel the distros and list the Distros

            cmdParams(2) = New OracleParameter("p_user_ID", OracleType.VarChar, 50)
            cmdParams(2).Value = strUserName

            cmdParams(3) = New OracleParameter("p_validate_desc", OracleType.VarChar, 100)
            cmdParams(3).Direction = ParameterDirection.Output

            cmdParams(4) = New OracleParameter("p_cur_distros", OracleType.Cursor)
            cmdParams(4).Direction = ParameterDirection.Output

            Select Case strWarehousenbr
                Case "985" 'OHDC 03-jan-2020
                    dsWMSRpt = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_CLOSEDSTORE_DISTROS", cmdParams)
                Case "997" 'TNDC
                    dsWMSRpt = _tnDcCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_CLOSEDSTORE_DISTROS", cmdParams)
            End Select

            If (cmdParams(3).Value.ToString().Length > 0) Then
                strErrorMsg = cmdParams(3).Value
                Return dsWMSRpt
            Else
                strErrorMsg = "Successfully Cancelled the distros for the store " & strStorenbr
            End If


            Return dsWMSRpt

        Catch ex As Exception
            strErrorMsg = "An error occurred in GetClosingStoreDistros, " + ex.ToString()
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function ListShipmentDistros(ByVal strWhse As Int16, ByVal strShpmtNbr As String, ByVal strDistroNbr As String, ByVal strUserName As String, ByRef strErrorMsg As String) As DataSet
        Try
            Dim ds As New DataSet
            Dim cmdParams() As OracleParameter = New OracleParameter(5) {}
            cmdParams(0) = New OracleParameter("P_SHPMT_NBR", OracleType.VarChar)
            cmdParams(0).Value = strShpmtNbr

            cmdParams(1) = New OracleParameter("P_DISTRO_NBR", OracleType.VarChar)
            cmdParams(1).Value = strDistroNbr

            cmdParams(2) = New OracleParameter("P_REQ_TYPE", OracleType.Number)
            cmdParams(2).Value = 0 'Option to list the Distros

            cmdParams(3) = New OracleParameter("P_USER_ID", OracleType.VarChar, 50)
            cmdParams(3).Value = strUserName

            cmdParams(4) = New OracleParameter("P_VALIDATE_DESC", OracleType.VarChar, 1000)
            cmdParams(4).Direction = ParameterDirection.Output

            cmdParams(5) = New OracleParameter("P_CUR_DISTROS", OracleType.Cursor)
            cmdParams(5).Direction = ParameterDirection.Output


            Select Case strWhse
                Case EnumWarehouse.CADC
                    ds = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_SHIPMENT_DISTROS", cmdParams)
                Case EnumWarehouse.TNDC
                    ds = _tnDcCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_SHIPMENT_DISTROS", cmdParams)
            End Select

            strErrorMsg = cmdParams(4).Value.ToString()

            Return ds
        Catch ex As Exception
            strErrorMsg = "An error occurred in ListShipmentDistros, " + ex.ToString()
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function CancelDistroShipment(ByVal intWHSE As Int16, ByVal strShpmtNbr As String, ByVal strDistroNbr As String, ByVal strUserName As String, ByRef strErrorMsg As String) As DataSet
        Try
            Dim ds As New DataSet
            Dim cmdParams() As OracleParameter = New OracleParameter(5) {}
            cmdParams(0) = New OracleParameter("P_SHPMT_NBR", OracleType.VarChar)
            cmdParams(0).Value = strShpmtNbr

            cmdParams(1) = New OracleParameter("P_DISTRO_NBR", OracleType.VarChar)
            cmdParams(1).Value = strDistroNbr

            cmdParams(2) = New OracleParameter("P_REQ_TYPE", OracleType.Number)
            cmdParams(2).Value = 1 'Option to cancel the Distros

            cmdParams(3) = New OracleParameter("P_USER_ID", OracleType.VarChar, 50)
            cmdParams(3).Value = strUserName

            cmdParams(4) = New OracleParameter("P_VALIDATE_DESC", OracleType.VarChar, 1000)
            cmdParams(4).Direction = ParameterDirection.Output

            cmdParams(5) = New OracleParameter("P_CUR_DISTROS", OracleType.Cursor)
            cmdParams(5).Direction = ParameterDirection.Output

            Select Case intWHSE
                Case EnumWarehouse.CADC
                    ds = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_SHIPMENT_DISTROS", cmdParams)
                Case EnumWarehouse.TNDC
                    ds = _tnDcCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_SHIPMENT_DISTROS", cmdParams)
            End Select

            strErrorMsg = cmdParams(4).Value.ToString()

            Return ds

        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function ListBackstockDistros(ByVal strWhse As Int16, ByVal strDistroNbr As String, ByVal strUserName As String, ByRef strErrorMsg As String) As DataSet
        Try
            Dim ds As New DataSet
            Dim cmdParams() As OracleParameter = New OracleParameter(4) {}
            cmdParams(0) = New OracleParameter("P_DISTRO_NBR", OracleType.VarChar)
            cmdParams(0).Value = strDistroNbr

            cmdParams(1) = New OracleParameter("P_REQ_TYPE", OracleType.Number)
            cmdParams(1).Value = 0 'Option to list the Distros

            cmdParams(2) = New OracleParameter("P_USER_ID", OracleType.VarChar, 50)
            cmdParams(2).Value = strUserName

            cmdParams(3) = New OracleParameter("P_VALIDATE_DESC", OracleType.VarChar, 1000)
            cmdParams(3).Direction = ParameterDirection.Output

            cmdParams(4) = New OracleParameter("P_CUR_DISTROS", OracleType.Cursor)
            cmdParams(4).Direction = ParameterDirection.Output


            Select Case strWhse
                Case EnumWarehouse.CADC
                    ds = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_BACKSTOCK_DISTROS", cmdParams)
                Case EnumWarehouse.TNDC
                    ds = _tnDcCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_BACKSTOCK_DISTROS", cmdParams)
            End Select

            strErrorMsg = cmdParams(3).Value.ToString()

            Return ds
        Catch ex As Exception
            strErrorMsg = "An error occurred in ListBackstockDistros, " + ex.ToString()
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function CancelDistroBackstock(ByVal intWHSe As Int16, ByVal strDistroNbr As String, ByVal strUserName As String, ByRef strErrorMsg As String) As DataSet
        Try
            Dim ds As New DataSet
            Dim cmdParams() As OracleParameter = New OracleParameter(4) {}
            cmdParams(0) = New OracleParameter("P_DISTRO_NBR", OracleType.VarChar)
            cmdParams(0).Value = strDistroNbr

            cmdParams(1) = New OracleParameter("P_REQ_TYPE", OracleType.Number)
            cmdParams(1).Value = 1 'Option to cancel the Distros

            cmdParams(2) = New OracleParameter("P_USER_ID", OracleType.VarChar, 50)
            cmdParams(2).Value = strUserName

            cmdParams(3) = New OracleParameter("P_VALIDATE_DESC", OracleType.VarChar, 1000)
            cmdParams(3).Direction = ParameterDirection.Output

            cmdParams(4) = New OracleParameter("P_CUR_DISTROS", OracleType.Cursor)
            cmdParams(4).Direction = ParameterDirection.Output


            Select Case intWHSe
                Case EnumWarehouse.CADC
                    ds = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_BACKSTOCK_DISTROS", cmdParams)
                Case EnumWarehouse.TNDC
                    ds = _tnDcCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_BACKSTOCK_DISTROS", cmdParams)
            End Select

            strErrorMsg = cmdParams(3).Value.ToString()

            Return ds

        Catch ex As Exception
            strErrorMsg = "An error occurred in CancelDistroBackstock, " + ex.ToString()
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function IsInGroup(ByVal GroupName As String) As Boolean
        Dim User As New WindowsPrincipal(System.Web.HttpContext.Current.User.Identity)
        If (User.IsInRole(GroupName)) Then
            Return True
        Else
            Return False
        End If
    End Function
    
    Public Function IsItemNbrInRMS(ByVal strITM_NBR As String) As Boolean
        Dim strErrorMsg As String = String.Empty
        Dim blnItemNbrFound As Boolean
        Dim strSQL As String
        Dim RMSConn As OracleConnection = Nothing
        Dim cmd As OracleCommand = Nothing
        Dim reader As OracleDataReader = Nothing
        Try
            RMSConn = New OracleConnection(AppSetting.RMSConnectionString)
            strSQL = "select item from RMS13.Item_Master where item = '" + strITM_NBR.Trim() + "'"
            cmd = New OracleCommand(strSQL, RMSConn)
            cmd.CommandType = CommandType.Text
            RMSConn.Open()
            reader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

            If (reader.HasRows = True) Then
                blnItemNbrFound = True
            Else
                blnItemNbrFound = False
            End If

            reader.Close()
            cmd.Dispose()
            RMSConn.Close()
            RMSConn.Dispose()
            Return blnItemNbrFound
        Catch ex As Exception
            strErrorMsg = "An error occurred in IsItemNbrInRMS, " + ex.ToString()
            Throw New BackStockAppException(ex.Message)
        Finally
            If (IsNothing(reader) = False) Then
                reader.Close()
            End If
            If (IsNothing(cmd) = False) Then
                cmd.Dispose()
            End If
            If (IsNothing(RMSConn) = False) Then
                RMSConn.Close()
                RMSConn.Dispose()
            End If
        End Try
    End Function
End Class
