Imports BackStock.Classes
Partial Class Home
    Inherits System.Web.UI.Page
    Protected WithEvents cbDept As System.Web.UI.WebControls.CheckBoxList
    Private _workFlowManager As New WorkFlowManager
    Private _htDeptDs As DataSet
    Private _tdDeptDs As DataSet
    Private _shDeptDs As DataSet
    Protected dr As DataRow
    Private _appLock As New ApplicationLock
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If User.Identity.IsAuthenticated Then
            Session("UserName") = User.Identity.Name
        Else
            Session("UserName") = "No user identity available."
        End If
        'Put user code to initialize the page here
        lblError.Text = ""
        If Not IsPostBack Then
            InitializeForm()
        End If

    End Sub
    Private Sub BindDeptData()
        ' Hottopic dept dataset
        If Not IsNothing(Session("HtDeptDs")) Then
            _htDeptDs = Session("HtDeptDs")
        Else
            _htDeptDs = _workFlowManager.ListDepartMents_AA(EnumDivCd.HotTopic)
            Session("HtDeptDs") = _htDeptDs
        End If
        ' Torrid dept dataset
        If Not IsNothing(Session("TdDeptDs")) Then
            _tdDeptDs = Session("TdDeptDs")
        Else
            _tdDeptDs = _workFlowManager.ListDepartMents_AA(EnumDivCd.Torrid)
            Session("TdDeptDs") = _tdDeptDs
        End If
        ' ShockHound dept dataset
        If Not IsNothing(Session("LSDeptDs")) Then
            _shDeptDs = Session("LSDeptDs")
        Else
            _shDeptDs = _workFlowManager.ListDepartMents_AA(EnumDivCd.Lovesick)
            Session("LSDeptDs") = _shDeptDs
        End If
        ' ShockHound dept dataset
        If Not IsNothing(Session("ShDeptDs")) Then
            _shDeptDs = Session("ShDeptDs")
        Else
            _shDeptDs = _workFlowManager.ListDepartMents_AA(EnumDivCd.ShockHound)
            Session("ShDeptDs") = _shDeptDs
        End If
    End Sub
    Private Sub InitializeForm()
        If IsNothing(Application("AASchema")) Then
            Application("AASchema") = _workFlowManager.GetDbServerName
        End If
        If Not IsNothing(Application("AppLock")) Then
            pnlMain.Visible = False
            pnlLock.Visible = True
            _appLock = Application("AppLock")
            lblLockMsg.Text = "Application currently processing '" & _appLock.lockMsg.ToUpper & "' since " & _appLock.lockDateTime
        Else
            pnlMain.Visible = True
            pnlLock.Visible = False
            BindDeptData()
            btnReserveUpdate.Attributes.Add("onclick", "return ShowProgessBar('Update');")
            btnReserveBatch.Attributes.Add("onclick", "return ShowProgessBar('Batch');")
            btnClearReserveByDept.Attributes.Add("onclick", "return ShowProgessBar('Clear');")
            'btnReserveClear.Attributes.Add("onclick", "return ShowProgessBar('Clear');")
            hdnDeptIdStr.Value = ""
        End If
    End Sub

    Private Sub btnReserveClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            Select Case Trim(lstWhse.SelectedItem.Value).ToUpper
                Case ""
                    ShowMsg("Please select a Warehouse!")
                Case Else
                    LockApplication(EnumActionType.ReserveClear, "Reserve Clear")
                    '_workFlowManager.ReserveClear(Trim(lstWhse.SelectedItem.Value))
                    ' 01\11\06 - Sri Bajjuri- loop thru the pipe delimited listItem value          
                    Dim whseArr As String() = lstWhse.SelectedItem.Value.Split("|")
                    Dim i As Int16
                    For i = 0 To UBound(whseArr)
                        If IsNumeric(whseArr(i)) Then
                            _workFlowManager.ReserveClear(whseArr(i))
                        End If
                    Next
                    ShowMsg(lstWhse.SelectedItem.Text & " Reserve clear completed")
            End Select
            'lock the application
            'LockApplication(EnumActionType.ReserveClear, "Reserve Clear")
            '_workFlowManager.ReserveClear()
            'ShowMsg("Reserve clear completed")
        Catch ex As BackStockAppException
            ShowMsg(ex.Message)
            'ShowMsg(ex.ToString)
        Catch ex As Exception
            Throw ex
        Finally
            UnlockApplication()
        End Try
    End Sub

    Private Sub btnReserveUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReserveUpdate.Click
        Try
            Dim fileName, successMsg As String
            Select Case Trim(lstWhse.SelectedItem.Value).ToUpper
                Case ""
                    ShowMsg("Please select a Warehouse!")
                    'Case "BOTH"
                    '    LockApplication(EnumActionType.ReserveUpdate, "Reserve Update")
                    '    'run reserve update for CADC(999)                 
                    '    successMsg &= _workFlowManager.ReserveUpdateNew(999)
                    '    'run reserve update for TNDC(997) 
                    '    successMsg &= "<br>"
                    '    successMsg &= _workFlowManager.ReserveUpdateNew(997)

                    '10/2/08 KJZ:  Added the below case to prevent multiple warehouse runs during the reserve update.
                    '              we can only run one at a time, otherwise the oracle PROC_WORKLIST_RESERVE_UPDATE will    
                    '              crash, because both warehouses are submitted to oracle at the same time and we get
                    '              a duplicate worklist key.
                Case "985|997"
                    ShowMsg("Please run only 1 Warehouse location at a time!")

                Case Else
                    LockApplication(EnumActionType.ReserveUpdate, "Reserve Update")
                    ' 01\11\06 - Sri Bajjuri- loop thru the pipe delimited listItem value          
                    Dim whseArr As String() = lstWhse.SelectedItem.Value.Split("|")
                    Dim i As Int16
                    For i = 0 To UBound(whseArr)
                        If IsNumeric(whseArr(i)) Then
                            successMsg &= _workFlowManager.ReserveUpdateNew(whseArr(i), hdnDeptIdStr.Value)
                            successMsg &= "<br>"
                        End If
                    Next
                    ShowMsg(successMsg)
            End Select
        Catch ex As BackStockAppException
            ShowMsg(ex.Message)
        Catch ex As Exception
            Throw ex
        Finally
            UnlockApplication()
        End Try
    End Sub

    Private Sub btnReserveBatch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReserveBatch.Click
        Dim msg As String = ""
        Try
            Select Case Trim(lstWhse.SelectedItem.Value).ToUpper
                Case ""
                    ShowMsg("Please select a Warehouse!")
                Case Else
                    LockApplication(EnumActionType.ReserveBatch, "Reserve Batch")
                    ' 01\11\06 - Sri Bajjuri- loop thru the pipe delimited listItem value          
                    Dim whseArr As String() = lstWhse.SelectedItem.Value.Split("|")
                    Dim i As Int16
                    For i = 0 To UBound(whseArr)
                        If IsNumeric(whseArr(i)) Then
                            Try
                                _workFlowManager.ReserveBatch(whseArr(i), hdnDeptIdStr.Value)
                                msg &= "Reserve batch for " & GetWhseName(whseArr(i)) & " ran successfully" & "<br>"
                            Catch ex As BackStockAppException
                                msg &= "Reserve batch failed for " & GetWhseName(whseArr(i)) & ", Reason:" & ex.Message & "<br>"
                            Catch ex As Exception
                                Throw ex
                            End Try
                        End If
                    Next
                    ShowMsg(msg)
            End Select
        Catch ex As BackStockAppException
            ShowMsg(ex.Message)
        Catch ex As Exception
            Throw ex
        Finally
            UnlockApplication()
        End Try
    End Sub
    Private Sub LockApplication(ByVal type As EnumActionType, ByVal msg As String)

        With _appLock
            .lockType = type
            .lockDateTime = Now
            .lockMsg = msg
        End With
        Application("AppLock") = _appLock
    End Sub
    Private Sub UnlockApplication()
        Application("AppLock") = Nothing
    End Sub

    Private Sub lnkLock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLock.Click
        UnlockApplication()
        InitializeForm()
    End Sub

    Private Sub ShowMsg(ByVal msg As String)
        UnlockApplication()
        BindDeptData()
        lblError.Text = msg
        lblError.Visible = True
    End Sub
    Private Function GetWhseName(ByVal whseNbr As Int16) As String
        Dim li As ListItem
        Dim whseName As String = ""
        For Each li In lstWhse.Items
            If li.Value = whseNbr.ToString Then
                whseName = li.Text
                Exit For
            End If
        Next
        Return whseName
    End Function

    Private Sub btnClearReserveByDept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearReserveByDept.Click
        Dim successMsg As String
        Try
            Select Case Trim(lstWhse.SelectedItem.Value).ToUpper
                Case ""
                    ShowMsg("Please select a warehouse!")
                Case Else
                    LockApplication(EnumActionType.ReserveClear, "Reserve Clear")
                    Dim whseArr As String() = lstWhse.SelectedItem.Value.Split("|")
                    Dim i As Int16
                    For i = 0 To UBound(whseArr)
                        If IsNumeric(whseArr(i)) Then
                            Dim mes As String = _workFlowManager.ReserveClear(whseArr(i), hdnDeptIdStr.Value, True)
                            successMsg &= "Clear update for " & GetWhseName(whseArr(i)) & " ran successfully" & "<br>"
                            successMsg &= "<br>"
                        End If
                    Next
                    ShowMsg(successMsg)
            End Select
        Catch ex As BackStockAppException
            ShowMsg(ex.Message)
        Catch ex As Exception
            Throw ex
        Finally
            UnlockApplication()
        End Try
            End Sub
End Class
