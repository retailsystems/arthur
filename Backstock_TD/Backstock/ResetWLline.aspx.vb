Imports BackStock.Classes
Public Partial Class ResetWLline
    Inherits System.Web.UI.Page
    Protected ds As New DataSet
    Protected dr As DataRow
    Protected strErrorMsg As String = String.Empty
    Private _workFlowManager As New WorkFlowManager

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblError.Text = ""
        lblError.Visible = False
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        BindData()
    End Sub
    Private Sub BindData()
        Try
            If ValidateRequest() Then
                'Check the allocation/WMS number does exist in Arthur
                ds = _workFlowManager.GetResetWLLineAllocs(txtAllocationNbr.Text.Trim, Session("UserName"), strErrorMsg)
                'If (strErrorMsg.Length > 0) Then
                'ShowMsg(strErrorMsg)
                'End If
                pnlResults.Visible = True
                hdnAllocNbr.Value = txtAllocationNbr.Text.ToString()
            Else
                'pnlResults.Visible = False
                ShowMsg("Following error(s) occured while processing your request!")
            End If
        Catch ex As Exception
            ShowMsg(ex.Message)
        End Try
    End Sub
    Private Function ValidateRequest() As Boolean
        Dim validReqFlag As Boolean = False
        If Page.IsValid AndAlso txtAllocationNbr.Text.Trim <> "" Then
            validReqFlag = True
        End If
        Return validReqFlag
    End Function
    Private Sub ShowMsg(ByVal msg As String)
        lblError.Text &= IIf(lblError.Text.Trim <> "", "<BR>", "") & msg
        'lblError.Text = msg
        lblError.Visible = True
    End Sub

    Protected Sub btnResetWLlines_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetWLlines.Click
        Try
            Dim strErrorMsg As String = String.Empty
            If (hdnAllocNbr.Value.Trim().Length > 0) Then
                ds = _workFlowManager.SetResetWLLineAllocs(hdnAllocNbr.Value.Trim(), Session("UserName"), strErrorMsg)
                ShowMsg(strErrorMsg)
                pnlResults.Visible = True
            Else
                ShowMsg("Invalid Allocation #" & hdnAllocNbr.Value & " submitted for Reset")
            End If
        Catch ex As Exception
            ShowMsg(ex.Message)
        End Try

    End Sub

    Protected Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Response.Redirect("./resetwlline.aspx")
    End Sub
End Class