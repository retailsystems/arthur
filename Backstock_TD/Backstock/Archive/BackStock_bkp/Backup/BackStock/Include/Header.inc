<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
<%
 IF IsNothing(session("UserName")) then
	response.redirect("Login.aspx")
 END IF
%>

	<title>Backstock Administration</title>
	
	<script language='javascript'>
			 function OpenPop(url,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=1"	 
			 window.open(url1,'cw',winProp);
		   }
		   	 function OpenPop1(url,cw,w, h)
		   {	
		    var url1 = url;
		  
			var winProp = "'menubar=no,resizable=yes,locationbar=no,toolbar=no,height=" + h +",width=" + w +",scrollbars=yes"	 
			 window.open(url1,cw,winProp);
		   }
		 function ChangeBgColor(obj,act){
				if(act == "in"){
					obj.style.background='#4ebfde';
				}else{
					obj.style.background='#246178';
				}
		   }
	</script>

<LINK href="include/Styles.css" type="text/css" rel="stylesheet">
</head>
<body bgcolor="black" text="white" vlink="white" alink="white" link="white" face="arial" style="margin: 5px 0px 0px 5px;">
<table border='0' cellspacing='0' cellpadding='0' width='770' ID="Table1">
<tr>
	<td width=200>	
		<A HREF='http://www.hottopic.com'> <img src='Images/hdr_main2.gif' border='0' alt='Hottopic' ></A>
	</td>
	<td  width=400 align='center' class="justifiedDkText">Welcome <%=session("UserName")%>,click here to <a href="Logout.aspx">log out</a></td>
	<td align=right>
		<A HREF='http://www.torrid.com'> <img src='Images/torridPink.jpg' border='0' alt='Torrid' ></A>
	</td>
</tr>


<tr  style="height:2px">
	<td colspan='3' style="height:2px" bgcolor="#990000"></td>	
</tr>
<tr  style="height:1px">
	<td colspan='3' style="height:1px" bgcolor="#000000"></td>	
</tr>
<!-- MENU -->

<tr >
	<td colspan=3 >		
		<table width='100%' bgcolor='#246178' cellspacing='0' cellpadding='0' border='0'  >
		<tr height=18px>
		<td class='cellvaluewhite' align=center width=120 onMouseOver="ChangeBgColor(this,'in')" onMouseOut="ChangeBgColor(this,'out')"   onClick="location.href='Home.aspx'"> 		
					&nbsp;<a href='Home.aspx'   onFocus='this.blur();' style='text-decoration: none; color: #FFFFFF'><b>Home</b></a>
		</td>
		
		<td bgcolor='#000000' width=1></td>	
		<td class='cellvaluewhite'  align=center width=120 onMouseOver="ChangeBgColor(this,'in')" onMouseOut="ChangeBgColor(this,'out')" onClick="location.href='Reports.aspx'"> 		
					&nbsp;<a href='Reports.aspx'   onFocus='this.blur();' style='text-decoration: none; color: #FFFFFF'><b>Reports</b></a>
		</td>		
		<td bgcolor='#000000' width=1></td>	
		<td class='cellvaluewhite'  align=center width=120 onMouseOver="ChangeBgColor(this,'in')" onMouseOut="ChangeBgColor(this,'out')" onClick="location.href='CancelStoreDistro.aspx'"> 		
					&nbsp;<a href='CancelStoreDistro.aspx'   onFocus='this.blur();' style='text-decoration: none; color: #FFFFFF'><b>Cancel Distro</b></a>
		</td>			
		<td bgcolor='#000000' width=1></td>	
		<td class='cellvaluewhite'  align=center width=120 onMouseOver="ChangeBgColor(this,'in')" onMouseOut="ChangeBgColor(this,'out')" onClick="location.href='ManageDistroPriority.aspx'"> 		
					&nbsp;<a href='ManageDistroPriority.aspx'   onFocus='this.blur();' style='text-decoration: none; color: #FFFFFF'><b>Distro Priority</b></a>
		</td>	
		<td bgcolor='#000000' width=1></td>	
		<td class='white79'>&nbsp;</td>
		<td align=right class=cellvaluewhite> <%=Application("AASchema")%>&nbsp;</td>
		</tr>
	</table>
	</td>
</tr>

<!-- END MENU-->
<tr><td colspan=3 height=5px></td></tr>
<tr >
<td colspan=3 valign=top >
<table border='0' cellspacing='0' cellpadding='0' width='100%' ID="Table3" >
<tr  height=250>
<td  colspan=2 valign=top  align=center>
<!--end header-->
<!-- begin here-->

