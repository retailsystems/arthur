Imports BackStock.Classes
Public Class WaveReport
    Inherits System.Web.UI.Page
    Protected WithEvents dgResults As System.Web.UI.WebControls.DataGrid
    Private _ds As DataSet
    Private _workFlowManager As New WorkFlowManager
    Private _fileName As String
    Private _warehouseNbr As Int16
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Response.ContentType = "Application/x-msexcel"
        _fileName = "WAVE_" & Request("waveNbr") & ".xls"
        _warehouseNbr = IIf(IsNumeric(Request("WarehouseNbr").Trim), Request("WarehouseNbr").Trim, 0)
        Response.AppendHeader("content-disposition", _
                "attachment; filename=" & _fileName)
        RunWaveReport()
        'Response.Flush()
        'Response.Close()
    End Sub
    Public Sub RunWaveReport()
        'Response.ContentType = "Application/x-msexcel"

        _ds = _workFlowManager.GetWaveCheckReport(Request("waveNbr"), _warehouseNbr)
        If _ds.Tables.Count > 0 AndAlso _ds.Tables(0).Rows.Count > 0 Then
            dgResults.DataSource = _ds.Tables(0)
            dgResults.DataBind()
        End If
    End Sub

End Class
