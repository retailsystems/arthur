Imports BackStock.Classes
Public Class ManageDistroPriority
    Inherits System.Web.UI.Page
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents txtNewMinLocnID As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNewMaxLocnID As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtNewDesc As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    Protected WithEvents txtNewPrtyCode As System.Web.UI.WebControls.TextBox
    Private _prtyGrp As PriorityGroup
    Protected WithEvents ValidationSummary1 As System.Web.UI.WebControls.ValidationSummary
    Private _workFlowManager As New WorkFlowManager
    Protected WithEvents ReqNewMinLocnID As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents ReqNewMaxLocnID As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents ReqNewPrtyCode As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents lstPrtyCode As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents lstStoresAll As System.Web.UI.WebControls.ListBox
    Protected WithEvents moveRight As System.Web.UI.WebControls.ImageButton
    Protected WithEvents moveLeft As System.Web.UI.WebControls.ImageButton
    Protected WithEvents lstStores As System.Web.UI.WebControls.ListBox
    Private _errMsg As String
    Private _ds As DataSet
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        lblError.Text = ""
        lblError.Visible = False
        If Not IsPostBack Then
            InitializeForm()
            ' BindData()        
        End If
    End Sub
    Private Sub InitializeForm()
        btnSubmit.Enabled = False
        lstStores.Items.Clear()
        lstStoresAll.Items.Clear()
        _ds = _workFlowManager.ListPriorityCodes
        If _ds.Tables.Count > 0 Then
            lstPrtyCode.DataSource = _ds.Tables(0)
            lstPrtyCode.DataValueField = "Priority_code"
            lstPrtyCode.DataTextField = "Priority_Desc"
            lstPrtyCode.DataBind()
        End If
        lstPrtyCode.Items.Insert(0, New ListItem("Select One", ""))
        ''txtNewGrpName.Text = ""
        'txtNewMinLocnID.Text = ""
        'txtNewMaxLocnID.Text = ""
        'txtNewDesc.Text = ""
        'txtNewPrtyCode.Text = ""
    End Sub
    Private Sub BindData()
        'dgPrtyGroup.DataSource = _workFlowManager.ListPriorityGroups
        'dgPrtyGroup.DataBind()
    End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'pnlNew.Visible = True
        InitializeForm()
    End Sub
    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'pnlNew.Visible = False
        'BindData()
    End Sub
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim storeStr As String = ""
            Dim li As ListItem
            For Each li In lstStores.Items
                storeStr &= IIf(storeStr.Length > 0, ",", "") & li.Value
            Next
            _workFlowManager.SaveStorePriority(lstPrtyCode.SelectedItem.Value, storeStr)

            PopulateStoreList()
            ShowMsg("Selected stores are successfully updated with priority code " & lstPrtyCode.SelectedItem.Value.PadLeft(2, "0"))
            'ShowMsg(storeStr)
            ''If Not ValidateRequest(txtNewGrpName.Text, txtNewMinLocnID.Text, txtNewMaxLocnID.Text, txtNewPrtyCode.Text) Then
            ''    Throw New BackStockAppException(_errMsg)
            ''End If
            'With _prtyGrp
            '    .grpId = 0
            '    '.grpName = txtNewGrpName.Text.Trim
            '    .grpDesc = txtNewDesc.Text.Trim
            '    .minLocnID = txtNewMinLocnID.Text.Trim
            '    .maxLocnId = txtNewMaxLocnID.Text.Trim
            '    .priorityCode = txtNewPrtyCode.Text.Trim
            'End With
            '_workFlowManager.SavePriorityGroup(_prtyGrp)
            'BindData()
            ''pnlNew.Visible = False
        Catch ex As Exception
            ShowMsg(ex.Message)
        End Try
    End Sub
    Private Sub ShowMsg(ByVal msg As String)
        lblError.Text &= IIf(lblError.Text.Trim <> "", "<BR>", "") & msg
        'lblError.Text = msg
        lblError.Visible = True
    End Sub

    'Private Sub dgPrtyGroup_EditCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
    '    dgPrtyGroup.EditItemIndex = e.Item.ItemIndex
    '    pnlNew.Visible = False
    '    BindData()
    'End Sub

    'Private Sub dgPrtyGroup_CancelCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
    '    dgPrtyGroup.EditItemIndex = -1
    '    pnlNew.Visible = False
    '    BindData()
    'End Sub

    'Private Sub dgPrtyGroup_DeleteCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
    '    Dim grpId As Integer
    '    Try
    '        grpId = e.Item.Cells(0).Text
    '        _workFlowManager.DeletePriorityGroupByID(grpId)
    '        dgPrtyGroup.EditItemIndex = -1
    '        pnlNew.Visible = False
    '        BindData()
    '    Catch ex As Exception
    '        ShowMsg(ex.Message)
    '    End Try
    'End Sub

    'Private Sub dgPrtyGroup_UpdateCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
    '    Try
    '        Dim grpId As Integer
    '        Dim tbGrpName, tbGrpDesc, tbMinLocn, tbMaxLocn, tbPrtyCode As TextBox
    '        grpId = e.Item.Cells(0).Text
    '        tbGrpName = e.Item.FindControl("txtGrpName")
    '        tbGrpDesc = e.Item.FindControl("txtDesc")
    '        tbMinLocn = e.Item.FindControl("txtMinLocnId")
    '        tbMaxLocn = e.Item.FindControl("txtMaxLocnId")
    '        tbPrtyCode = e.Item.FindControl("txtPrtyCode")
    '        If Not ValidateRequest(tbGrpName.Text, tbMinLocn.Text, tbMaxLocn.Text, tbPrtyCode.Text) Then
    '            Throw New BackStockAppException(_errMsg)
    '        End If
    '        With _prtyGrp
    '            .grpId = grpId
    '            .grpName = tbGrpName.Text.Trim
    '            .grpDesc = tbGrpDesc.Text.Trim
    '            .minLocnID = tbMinLocn.Text.Trim
    '            .maxLocnId = tbMaxLocn.Text.Trim
    '            .priorityCode = tbPrtyCode.Text.Trim
    '        End With
    '        _workFlowManager.SavePriorityGroup(_prtyGrp)
    '        dgPrtyGroup.EditItemIndex = -1
    '        pnlNew.Visible = False
    '        BindData()
    '    Catch ex As Exception
    '        ShowMsg(ex.Message)
    '    End Try
    'End Sub

    'Private Sub dgPrtyGroup_ItemCreated(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
    '    Dim myDeleteButton As TableCell
    '    myDeleteButton = e.Item.Cells(7)

    '    If dgPrtyGroup.EditItemIndex = e.Item.ItemIndex Then
    '        myDeleteButton.Text = ""
    '    Else
    '        myDeleteButton.Attributes.Add("onclick", "return confirm('Are you Sure you want to delete this group?');")
    '    End If
    'End Sub
    Private Function ValidateRequest(ByVal grpName As String, ByVal minLocnID As String, ByVal maxLocnID As String, ByVal prtyCode As String) As Boolean
        _errMsg = ""
        If (Not grpName.Trim.Length > 0) Then
            _errMsg = "Group name is required."
            Return False
        ElseIf (Not minLocnID.Trim.Length > 0) OrElse (Not IsNumeric(minLocnID)) OrElse CInt(minLocnID) < 1 Then
            _errMsg = "Invalid Min store#."
            Return False
        ElseIf (Not maxLocnID.Trim.Length > 0) OrElse (Not IsNumeric(maxLocnID)) OrElse CInt(maxLocnID) < 1 Then
            _errMsg = "Invalid Max store#."
            Return False
        ElseIf CInt(maxLocnID) < CInt(minLocnID) Then
            _errMsg = "Max store# should be greater than min store#."
            Return False
        ElseIf (Not prtyCode.Trim.Length > 0) OrElse (Not IsNumeric(prtyCode)) OrElse CInt(prtyCode) < 1 OrElse CInt(prtyCode) > 99 Then
            _errMsg = "Please enter a valid priority code[01-99]."
            Return False
        Else
            Return True
        End If

    End Function

    Private Sub lstPrtyCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstPrtyCode.SelectedIndexChanged
        If lstPrtyCode.SelectedIndex > 0 Then
            btnSubmit.Enabled = True
            PopulateStoreList()
        Else
            'btnSubmit.Enabled = False
            InitializeForm()
        End If
    End Sub

    Private Sub PopulateStoreList()
        Dim ds As DataSet
        ds = _workFlowManager.ListStoresByPriority(lstPrtyCode.SelectedItem.Value)
        If ds.Tables.Count > 0 Then
            lstStores.DataSource = ds.Tables(0)
            lstStores.DataValueField = "Location_Id"
            lstStores.DataTextField = "Location_desc"
            lstStores.DataBind()
            lstStoresAll.DataSource = ds.Tables(1)
            lstStoresAll.DataValueField = "Location_Id"
            lstStoresAll.DataTextField = "Location_desc"
            lstStoresAll.DataBind()
        End If
    End Sub

    Private Sub moveRight_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles moveRight.Click
        Dim li As ListItem
        Dim i As Integer
        Dim indArr As ArrayList
        indArr = New ArrayList
        For Each li In lstStoresAll.Items
            If li.Selected Then
                If lstStores.Items.IndexOf(li) = -1 Then
                    lstStores.Items.Add(li)
                    'lstUsers.Items.Remove(li)
                    indArr.Add(li)
                End If
            End If
        Next
        If indArr.Count > 0 Then
            For i = 0 To indArr.Count - 1
                lstStoresAll.Items.Remove(indArr(i))
            Next
        End If
        lstStoresAll.SelectedIndex = -1
        lstStores.SelectedIndex = -1
    End Sub

    Private Sub moveLeft_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles moveLeft.Click
        Dim li As ListItem
        Dim i As Integer
        Dim indArr As ArrayList
        indArr = New ArrayList
        For Each li In lstStores.Items
            If li.Selected Then
                If lstStoresAll.Items.IndexOf(li) = -1 Then
                    lstStoresAll.Items.Add(li)
                    'lstUsers.Items.Remove(li)
                    indArr.Add(li)
                End If
            End If
        Next
        If indArr.Count > 0 Then
            For i = 0 To indArr.Count - 1
                lstStores.Items.Remove(indArr(i))
            Next
        End If
        lstStoresAll.SelectedIndex = -1
        lstStores.SelectedIndex = -1

    End Sub
End Class
