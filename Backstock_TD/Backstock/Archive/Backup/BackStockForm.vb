Imports Backstock.Classes
Imports System.IO
Public Class App
    ' Entry point
    Public Shared Sub Main(ByVal args() As String)
        ' If the args parse in known way then run the app
        Dim sAction As String = ""
        If args.Length > 0 Then
            Dim _backStockForm As BackStockForm = New BackStockForm()
            'sAction = args(0).ToUpper
            'Select Case sAction
            '    Case "RESERVE_CLEAR"
            '        _backStockForm.ReserveClear()
            '    Case "RESERVE_UPDATE"
            '        _backStockForm.ReserveUpdate()
            'End Select
            sAction = args(0).ToString
            If IsNumeric(sAction) Then
                _backStockForm.CheckExportStatus(sAction)
                _backStockForm.Close()
                _backStockForm.Dispose()
            End If
        End If
    End Sub 'Main
End Class
Class BackStockForm
    Inherits System.Windows.Forms.Form
    Public _appSettings As AppSetting
    Public _workFlowManager As WorkFlowManager
    Private _debugMode As String
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
        InitializeForm()
        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        '
        'BackStockForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Name = "BackStockForm"
        Me.Text = "BackStock"

    End Sub

#End Region
    Public Sub CheckExportStatus(ByVal whseStr As String)
        Try
            ' 01\06\06 - Sri Bajjuri- loop thru the comma delimited whseStr          
            Dim whseArr As String() = whseStr.Split(",")
            Dim i As Int16
            For i = 0 To UBound(whseArr)
                If IsNumeric(whseArr(i)) Then
                    'LogException(whseArr(i), "CheckExportStatus")
                    _workFlowManager.CheckExportStatus(whseArr(i))
                End If
            Next
        Catch ex As Exception
            'Throw ex
            LogException(ex.ToString, "CheckExportStatus")
        End Try
    End Sub

    'Public Sub ReserveClear()
    '    Try
    '        _workFlowManager.ReserveClear()
    '    Catch ex As Exception
    '        'Throw ex
    '        LogException(ex.ToString)
    '    End Try
    'End Sub
    'Public Sub ReserveUpdate()
    '    Try
    '        Dim fileName As String
    '        fileName = _workFlowManager.ReserverUpdateNew()
    '        'MessageBox.Show(fileName)
    '    Catch ex As Exception
    '        LogException(ex.ToString)
    '        'Throw ex
    '    End Try
    'End Sub
    Private Sub InitializeForm()
        Dim settings As New System.Configuration.AppSettingsReader()
        _appSettings = New AppSetting()
        With _appSettings
            .AAConnectionString = settings.GetValue("AAConnectionString", GetType(String))
            .WMSConnectionString = settings.GetValue("WMSConnectionString", GetType(String))
            '01/06/06 - SRI - TNDC connection str
            .TnDcWMSConnectionString = settings.GetValue("TNDC_WMSConnectionString", GetType(String))

            .GERSConnectionString = settings.GetValue("GERSConnectionString", GetType(String))
            .EMailReceivers = settings.GetValue("NotificationEmailReceivers", GetType(String))
            .CacheDependencyFile = settings.GetValue("CacheDependency", GetType(String))
            .SmtpServer = settings.GetValue("SmtpServer", GetType(String))
            .SmtpSender = settings.GetValue("SmtpSender", GetType(String))
            .WebmasterEmail = settings.GetValue("WebmasterEmail", GetType(String))
            .ProjectName = settings.GetValue("ProjectName", GetType(String))
            .ReserveClearEmailBody = settings.GetValue("ReserveClearEmailBody", GetType(String))
            .ReserveUpdateEmailBody = settings.GetValue("ReserveUpdateEmailBody", GetType(String))
            .ReserveBatchEmailBody = settings.GetValue("ReserveBatchEmailBody", GetType(String))

            .WmsLocalFileDir = settings.GetValue("WmsLocalFileDir", GetType(String))
            .FtpUser = settings.GetValue("FtpUser", GetType(String))
            .FtpPwd = settings.GetValue("FtpPwd", GetType(String))
            .FtpHost = settings.GetValue("FtpHost", GetType(String))
            .FtpFolder = settings.GetValue("FtpFolder", GetType(String))
            .AABackstockPkg = settings.GetValue("AABackstockPkg", GetType(String))
            .WMBackstockPkg = settings.GetValue("WMBackstockPkg", GetType(String))
            .ErrorLogFile = settings.GetValue("ErrorLog", GetType(String))
        End With
        _debugMode = settings.GetValue("DebugMode", GetType(String))
        _workFlowManager = New WorkFlowManager()
    End Sub

    Private Sub LogException(ByVal msg As String, Optional ByVal msgHeader As String = "")
        Try
            If _debugMode.ToUpper = "YES" Then
                msg = msgHeader & IIf(msgHeader = "", "", ":") & msg
                MessageBox.Show(msg.Trim)
            Else
                Dim objStreamWriter As StreamWriter
                'Open the file.
                objStreamWriter = New StreamWriter(_appSettings.ErrorLogFile, True)
                objStreamWriter.WriteLine("")
                objStreamWriter.WriteLine(Now & ":" & msgHeader)
                objStreamWriter.WriteLine(msg)
                'Close the file.
                objStreamWriter.Close()
            End If
        Catch ex As Exception
            'Throw ex
        End Try
    End Sub
End Class
