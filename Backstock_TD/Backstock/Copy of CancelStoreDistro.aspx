<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CancelStoreDistro.aspx.vb" Inherits="BackStock.CancelStoreDistro"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<!-- #include file="include/header.inc" -->
		<script language="javascript">
			function CancelDistro(distroNbr){
				//alert(distroNbr);
				if (confirm('Are you sure to cancel distro #' + distroNbr + '?')){
					document.Form1.hdnDistroNbr.value = distroNbr;
					showWait();
					document.Form1.btnCancelDistro.click();					
				}
			}
			function CancelAndUpdateArthur(distroNbr){
				//alert(distroNbr);
				if (confirm('Are you sure to cancel distro #' + distroNbr + '?')){
					document.Form1.hdnDistroNbr.value = distroNbr;
					showWait();
					document.Form1.btnCancelAndUpdateArthur.click();					
				}
			}
			function ValidateRadioButton(Type)
			{
			  if(Type == 'shipment')
			  {
			    document.Form1.rbBackstock.checked = false;
			  }
			  else
			  {
			    document.Form1.rbShipment.checked = false;
			  }
			  return true;
			}
		</script>
		<script language="vb" runat="server">
		    Sub ValidateDistroType_Index_Changed(ByVal sender As Object, ByVal e As EventArgs)
		        If (rbDistroType.SelectedItem.Text.ToLower().Trim() = "shipment") Then
		            txtShipmentNbr.Enabled = True
		        Else
		            txtShipmentNbr.Enabled = False
		        End If
		    End Sub
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<br>
			<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
				cellSpacing="0" cellPadding="2" width="750" align="center" border="0">
				<tr><td><a href="CancelClosingStoreDistro.aspx" class="cellvaluecaption">Cancel Closing Store Distro</a></td></tr>
				<TR height="15">
					<TD class="cellvaluecaption" align="center" bgColor="#990000">Search Distro's
					</TD>
				</TR>
				<TR height="5">
					<TD></TD>
				</TR>
				<TR>
					<TD vAlign="top" align="center">
						<TABLE borderColor="#990000" height="100%" cellSpacing="0" cellPadding="0" width="100%"
							border="0">
							<TR height="3">
								<TD align="center" colspan="2"><asp:label id="lblError" Runat="server" CssClass="errStyle" Visible="False"></asp:label><asp:validationsummary id="ValidationSummary1" Runat="server" CssClass="errStyle" EnableClientScript="false"
										DisplayMode="BulletList" ShowMessageBox="False"></asp:validationsummary></TD>
							</TR>
							<tr>
							<td class="cellvaluecaption" style="height: 110px">								
                                <asp:RadioButtonList ID="rbDistroType" runat="server" CssClass="cellvaluecaption" OnSelectedIndexChanged="ValidateDistroType_Index_Changed" AutoPostBack ="true">
                                    <asp:ListItem Value="shipment" Selected="True">Shipment</asp:ListItem>
                                    <asp:ListItem Value="backstock">Backstock</asp:ListItem>
                                </asp:RadioButtonList></td>
								<td class="cellvaluecaption" align="left" style="height: 110px">Warehouse:
									<asp:dropdownlist id="lstWhse" Runat="server" CssClass="cellvalueleft">
										<asp:ListItem Value="">--Select One--</asp:ListItem>
										<asp:ListItem Value="999">DC-Industry,CA</asp:ListItem>
										<asp:ListItem Value="997">DC-LaVergne,TN</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="reqValidator1" Runat="server" CssClass="reqStyle" EnableClientScript="false"
										ControlToValidate="lstWhse" Display="None" ErrorMessage="Please select a Warehouse." InitialValue=""></asp:requiredfieldvalidator>&nbsp;&nbsp;Shipment 
									#:
									<asp:textbox id="txtShipmentNbr" Runat="server" CssClass="cellvalueleft" Width="170px" MaxLength="20"></asp:textbox><asp:comparevalidator id="validateShipmentNbr" Runat="server" EnableClientScript="false" ControlToValidate="txtShipmentNbr"
										Display="None" ErrorMessage="Invalid Shipment #." Type="Double" Operator="DataTypeCheck"></asp:comparevalidator><asp:requiredfieldvalidator id="reqShpmtNbr" Runat="server" CssClass="reqStyle" EnableClientScript="false" ControlToValidate="txtShipmentNbr"
										Display="None" ErrorMessage="Please enter Shipment #." InitialValue=""></asp:requiredfieldvalidator>
										<br /><br />&nbsp;&nbsp;Distro 
									#:
									<asp:textbox id="txtDistroNbr" Runat="server" CssClass="cellvalueleft" Width="100px" MaxLength="10"></asp:textbox><asp:comparevalidator id="validateDistroNbr" Runat="server" EnableClientScript="false" ControlToValidate="txtDistroNbr"
										Display="None" ErrorMessage="Invalid Distro #." Type="Double" Operator="DataTypeCheck" Enabled="True"></asp:comparevalidator><asp:requiredfieldvalidator id="reqDistroNbr" Runat="server" CssClass="reqStyle" EnableClientScript="false"
										ControlToValidate="txtDistroNbr" Display="None" ErrorMessage="Please enter Distro #." InitialValue=""></asp:requiredfieldvalidator>&nbsp;&nbsp;
									<asp:button id="btnSubmit" Runat="server" CssClass="btnSmall" Text="Submit"></asp:button>&nbsp;&nbsp;<input class="btnSmall" id="btnClear" onclick="location.href=location.href" type="button"
										value="Clear" name="btnClear">
								</td>
							</tr>
							<TR height="5">
								<TD class="cellvaluecaption" align="center"><asp:checkbox id="chkCopyToArthur" Visible="False" Runat="server" Text="Populate this distro to Arthur worklist"
										Checked="True"></asp:checkbox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR height="20">
					<TD></TD>
				</TR>
			</TABLE>
			<!--#include file="include/wait.inc"--><br>
			<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
				cellSpacing="0" cellPadding="2" width="750" align="center" border="0">
				<TR height="15">
					<TD class="cellvaluecaption" align="center" bgColor="#990000">Search Results
					</TD>
				</TR>
				<TR height="5">
					<TD></TD>
				</TR>
				<TR>
					<TD vAlign="top" align="center">
						<TABLE borderColor="#990000" height="100%" cellSpacing="0" cellPadding="0" width="100%"
							border="0">
							<TR height="3">
								<TD align="center"></TD>
							</TR>
							<tr>
								<td align="center"><asp:panel id="pnlResults" Runat="server" Visible="False" EnableViewState="False">
										<DIV id="divCancelDistro" style="DISPLAY: none"><INPUT id="hdnShpmtNbr" type="hidden" value="0" name="hdnShpmtNbr" runat="server">
											<INPUT id="hdnDistroNbr" type="hidden" value="0" name="hdnDistroNbr" runat="server">
											<INPUT id="hdnWhse" type="hidden" value="0" name="hdnWhse" runat="server">
											<asp:Button id="btnCancelDistro" Runat="server" Width="0px" Text="" Height="0px"></asp:Button>
											<asp:Button id="btnCancelAndUpdateArthur" Runat="server" Width="0px" Text="" Height="0px"></asp:Button></DIV>
										<%IF ds.tables.count > 0 ANDAlso ds.tables(0).rows.count > 0 then%>
										<TABLE class="DatagridSm" id="Table6" style="BORDER-COLLAPSE: collapse" cellSpacing="0"
											cellPadding="2" width="550" align="center" border="1">
											<TR class="DATAGRID_HeaderSm">
												<TD width="150">Distro #</TD>
												<TD width="100">Status</TD>
												<TD width="75">Count</TD>
												<TD width="75">Alloc Units</TD>
												<TD width="150"></TD>
											</TR>
											<%
												dim rowStyle as string = "BC333333sm"
												dim prevDistroNbr as string = "0"
												'dim cancelBtnStatus as string = "DISABLED"	
												dim cancelBtnStatus as string 
												dim cancelArthurBtnStatus as string
												dim populateToArthur as string = "False"
												Dim i as int16 = 0	
												Dim cancelButton as string = "</TD><TD valign=middle height='100%' align=center ><input type=button class=btnSmallgrey value=Cancel " & cancelBtnStatus & " onClick=""CancelDistro('" & prevDistroNbr & "')""></TD></TR>"
																				
											for each dr in ds.tables(0).rows
												' group all distro records
												IF prevDistroNbr <> dr("DISTRO_NBR") THEN
													rowStyle = IIF(rowStyle="BC333333sm","BC111111sm","BC333333sm")
													IF prevDistroNbr <> "0" THEN
														'response.write ("</TD><TD valign=middle height='100%' align=center ><input type=button class=btnSmallgrey value=Cancel " & cancelBtnStatus & " onClick=""CancelDistro('" & prevDistroNbr & "','N')"">&nbsp;&nbsp;&nbsp;&nbsp;<input type=button class=btnSmallgrey value=Cancel&UpdateArthur " & cancelArthurBtnStatus & " onClick=""CancelAndUpdateArthur('" & prevDistroNbr & "','Y')""></TD></TR>")
														'response.write ("</TD><TD valign=middle height='100%' align=center ><input type=button class=btnSmallgrey value=Cancel " & cancelBtnStatus & " onClick=""CancelDistro('" & prevDistroNbr & "')""></TD></TR>")
														'response.write (cancelButton)
													END IF	
													prevDistroNbr = dr("DISTRO_NBR")
													i = 0	
													cancelBtnStatus= "DISABLED"	
													cancelArthurBtnStatus = "DISABLED"
												else
													i += 1										
												END IF	
												'Enable cancel button if stat_code = 0
												'SELECT CASE dr("STAT_CODE")
												'	CASE 0
												'		cancelBtnStatus = "ENABLED"
												'		if  dr("UNIT_RCVD") > 0 then
												'			cancelArthurBtnStatus = "ENABLED"
												'		else
												'			cancelArthurBtnStatus = "DISABLED"
												'		end if
												'		populateToArthur = "True"
												'	CASE 95 
												'		' do nothing
												'	CASE ELSE
												'		cancelBtnStatus = "DISABLED"
												'		cancelArthurBtnStatus = "DISABLED"
												'END SELECT	
												
												' Enable cancel button if stat_code = 0
												IF (dr("STAT_CODE") = 0 OR dr("STAT_CODE") = 99) THEN
													cancelBtnStatus = "ENABLED"
													if  dr("UNIT_RCVD") > 0 then
														cancelArthurBtnStatus = "ENABLED"
													else
														cancelArthurBtnStatus = "DISABLED"
													end if
												ELSE
													cancelBtnStatus = "DISABLED"
														cancelArthurBtnStatus = "DISABLED"
												END IF					
											%>
											<%IF i = 0 THEN%>
											<TR class="<%=rowStyle%>">
												<TD vAlign="middle"><%=dr("DISTRO_NBR")%></TD>
												<TD colSpan="3"><%END IF%>
													<TABLE width="100%">
														<TR class="<%=rowStyle%>">
															<TD width="100"><%=dr("CODE_DESC")%></TD>
															<TD width="75"><%=dr("CNT")%></TD>
															<TD width="75"><%=dr("ALLOC_UNITS")%></TD>
														</TR>
													</TABLE>
													<%											
											next
											'response.write ("</TD><TD valign=middle height='100%' align=center ><input type=button class=btnSmallgrey value=Cancel " & cancelBtnStatus & " onClick=""CancelDistro('" & prevDistroNbr & "')""></TD></TR>")
											response.write ("</TD><TD valign=middle height='100%' align=center ><input type=button class=btnSmallgrey value=Cancel " & cancelBtnStatus & " onClick=""CancelDistro('" & prevDistroNbr & "','N')"">&nbsp;&nbsp;&nbsp;&nbsp;<input type=button class=btnSmallgrey value=Cancel&UpdateArthur " & cancelArthurBtnStatus & " onClick=""CancelAndUpdateArthur('" & prevDistroNbr & "','Y')""></TD></TR>")
											'response.write (cancelButton)
											%>
												</TD>
											</TR>
										</TABLE>
										<%ElseIf (strErrorMsg.Length > 0) Then%>
										<FONT class="errStyle"><%Response.Write(strErrorMsg)%></FONT>
										<%ELSE%>
										<FONT class="errStyle">No records found.</FONT>
										<%END IF%>
									</asp:panel></td>
							</tr>
						</TABLE>
					</TD>
				</TR>
				<TR height="5">
					<TD></TD>
				</TR>
			</TABLE>
		</form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
