<%@ Page Language="vb" AutoEventWireup="false" Codebehind="CancelStoreDistro.aspx.vb" Inherits="BackStock.CancelStoreDistro"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<!-- #include file="include/header.inc" -->
		<script language="javascript">
			function CancelDistro(distroNbr){
				//alert(distroNbr);
				if (confirm('Are you sure to cancel distro #' + distroNbr + '?')){
					document.Form1.hdnDistroNbr.value = distroNbr;
					showWait();
					document.Form1.btnCancelDistro.click();					
				}
			}
			function CancelAndUpdateArthur(distroNbr){
				//alert(distroNbr);
				if (confirm('Are you sure to cancel distro #' + distroNbr + '?')){
					document.Form1.hdnDistroNbr.value = distroNbr;
					showWait();
					document.Form1.btnCancelAndUpdateArthur.click();					
				}
			}
			function ValidateRadioButton(Type)
			{
			  if(Type == 'shipment')
			  {
			    document.Form1.rbBackstock.checked = false;
			  }
			  else
			  {
			    document.Form1.rbShipment.checked = false;
			  }
			  return true;
			}
		</script>
		<script language="vb" runat="server">
		    Sub ValidateDistroType_Index_Changed(ByVal sender As Object, ByVal e As EventArgs)
		        If (rbDistroType.SelectedItem.Text.ToLower().Trim() = "shipment") Then
		            txtShipmentNbr.Enabled = True
		            reqShpmtNbr.Enabled = True
		        Else
		            txtShipmentNbr.Text = String.Empty
		            hdnShpmtNbr.Value = String.Empty
		            txtShipmentNbr.Enabled = False
		            reqShpmtNbr.Enabled = False
		        End If
		    End Sub
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<br>
			<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
				cellSpacing="0" cellPadding="2" width="750" align="center" border="0">
				<tr><td></td></tr>
				<TR height="15">
					<TD class="cellvaluecaption" align="center" bgColor="#990000">Search Distro's
					</TD>
				</TR>
				<TR height="5">
					<TD></TD>
				</TR>
				<TR>
					<TD vAlign="top" align="center">
						<TABLE borderColor="#990000" height="100%" cellSpacing="0" cellPadding="0" width="100%"
							border="0">
							<TR height="3">
								<TD align="center" colspan="2"><asp:label id="lblError" Runat="server" CssClass="errStyle" Visible="False"></asp:label><asp:validationsummary id="ValidationSummary1" Runat="server" CssClass="errStyle" EnableClientScript="false"
										DisplayMode="BulletList" ShowMessageBox="False"></asp:validationsummary></TD>
							</TR>
							<tr>
							<td class="cellvaluecaption" style="height: 110px">								
                                <asp:RadioButtonList ID="rbDistroType" runat="server" CssClass="cellvaluecaption" OnSelectedIndexChanged="ValidateDistroType_Index_Changed" AutoPostBack ="true">
                                    <asp:ListItem Value="shipment" Selected="True">Shipment</asp:ListItem>
                                    <asp:ListItem Value="backstock">Backstock</asp:ListItem>
                                </asp:RadioButtonList></td>
								<td class="cellvaluecaption" align="left" style="height: 110px">Warehouse:
									<asp:dropdownlist id="lstWhse" Runat="server" CssClass="cellvalueleft">
										<asp:ListItem Value="">--Select One--</asp:ListItem>
										<asp:ListItem Value="985">DC-WestJefferson,OH</asp:ListItem>
										<asp:ListItem Value="997">DC-LaVergne,TN</asp:ListItem>
									</asp:dropdownlist><asp:requiredfieldvalidator id="reqValidator1" Runat="server" CssClass="reqStyle" EnableClientScript="false"
										ControlToValidate="lstWhse" Display="None" ErrorMessage="Please select a Warehouse." InitialValue=""></asp:requiredfieldvalidator>&nbsp;&nbsp;Shipment 
									#:
									<asp:textbox id="txtShipmentNbr" Runat="server" CssClass="cellvalueleft" Width="170px" MaxLength="20"></asp:textbox><asp:comparevalidator id="validateShipmentNbr" Runat="server" EnableClientScript="false" ControlToValidate="txtShipmentNbr"
										Display="None" ErrorMessage="Invalid Shipment #." Type="Double" Operator="DataTypeCheck"></asp:comparevalidator><asp:requiredfieldvalidator id="reqShpmtNbr" Runat="server" CssClass="reqStyle" EnableClientScript="false" ControlToValidate="txtShipmentNbr"
										Display="None" ErrorMessage="Please enter Shipment #." InitialValue=""></asp:requiredfieldvalidator>
										<br /><br />&nbsp;&nbsp;Distro 
									#:
									<asp:textbox id="txtDistroNbr" Runat="server" CssClass="cellvalueleft" Width="100px" MaxLength="10"></asp:textbox><asp:comparevalidator id="validateDistroNbr" Runat="server" EnableClientScript="false" ControlToValidate="txtDistroNbr"
										Display="None" ErrorMessage="Invalid Distro #." Type="Double" Operator="DataTypeCheck" Enabled="True"></asp:comparevalidator><asp:requiredfieldvalidator id="reqDistroNbr" Runat="server" CssClass="reqStyle" EnableClientScript="false"
										ControlToValidate="txtDistroNbr" Display="None" ErrorMessage="Please enter Distro #." InitialValue=""></asp:requiredfieldvalidator>&nbsp;&nbsp;
									<asp:button id="btnSubmit" Runat="server" CssClass="btnSmall" Text="Submit"></asp:button>&nbsp;&nbsp;<input class="btnSmall" id="btnClear" onclick="location.href=location.href" type="button"
										value="Clear" name="btnClear">
								</td>
							</tr>
							<TR height="5">
								<TD class="cellvaluecaption" align="center"><asp:checkbox id="chkCopyToArthur" Visible="False" Runat="server" Text="Populate this distro to Arthur worklist"
										Checked="True"></asp:checkbox></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<TR height="20">
					<TD></TD>
				</TR>
			</TABLE>
			<!--#include file="include/wait.inc"--><br>
			<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
				cellSpacing="0" cellPadding="2" width="750" align="center" border="0">
				<TR height="15">
					<TD class="cellvaluecaption" align="center" bgColor="#990000">Search Results
					</TD>
				</TR>
				<TR height="5">
					<TD></TD>
				</TR>
				<TR>
					<TD vAlign="top" align="center">
						<TABLE borderColor="#990000" height="100%" cellSpacing="0" cellPadding="0" width="100%"
							border="0">
							<TR height="3">
								<TD align="center"></TD>
							</TR>
							<tr>
								<td align="center"><asp:panel id="pnlResults" Runat="server" Visible="False" EnableViewState="False">
										<DIV id="divCancelDistro" style="DISPLAY: none"><INPUT id="hdnShpmtNbr" type="hidden" value="0" name="hdnShpmtNbr" runat="server">
											<INPUT id="hdnDistroNbr" type="hidden" value="0" name="hdnDistroNbr" runat="server">
											<INPUT id="hdnWhse" type="hidden" value="0" name="hdnWhse" runat="server">
											<asp:Button id="btnCancelDistro" Runat="server" Width="0px" Text="" Height="0px"></asp:Button>
											<asp:Button id="btnCancelAndUpdateArthur" Runat="server" Width="0px" Text="" Height="0px"></asp:Button></DIV>
											<%
											    'response.Write("Radio Button Value: " & rbDistroType.SelectedItem.Text.ToLower().Trim() & "<br>")
											    'response.Write("Security Group Value" & htSecurityGrp("CURRENT_USER_GRP").ToString() & "<br>")
											    'response.Write("Arthur: " & htSecurityGrp("ARTHUR").ToString() & "<br>")
											    'response.Write("WMS: " & htSecurityGrp("WMS").ToString() & "<br>")											    
											    
											    Dim rowStyle As String = "BC333333sm"
											    'dim prevDistroNbr as string = "0"												
											    Dim cancelBtnStatus As String = "DISABLED"
											    Dim cancelArthurBtnStatus As String = "DISABLED"
											    Dim displayResults As Boolean = False
											    Dim strError As String = String.Empty
											    'dim populateToArthur as string = "False"
											    'Dim i as int16 = 0	
											    'Dim cancelButton As String = "</TD><TD valign=middle height='100%' align=center ><input type=button class=btnSmallgrey value=Cancel " & cancelBtnStatus & " onClick=""CancelDistro('" & dr("distro_nbr") & "')""></TD></TR>"
											    
											    If ((rbDistroType.SelectedItem.Text.ToLower().Trim() = "shipment" Or rbDistroType.SelectedItem.Text.ToLower().Trim() = "backstock") And strErrorMsg = "WORKED_DISTROS" And htSecurityGrp("CURRENT_USER_GRP").ToString() = "ARTHUR") Then
											        'Error screen as unauthorized											        
											        strError = "Unauthorized to makes any changes to the worked distros"
											        displayResults = False
											    ElseIf ((rbDistroType.SelectedItem.Text.ToLower().Trim() = "shipment" Or rbDistroType.SelectedItem.Text.ToLower().Trim() = "backstock") And strErrorMsg = "WORKED_DISTROS" And htSecurityGrp("CURRENT_USER_GRP").ToString() = "WMS") Then
											        'Warning confirmation screen
											        cancelBtnStatus = "ENABLED"
											        cancelArthurBtnStatus = "DISABLED"
											        Response.Write("<FONT class='errStyle'>Warning: You are about to cancel distros on a partially worked set of distros</FONT>")
											        displayResults = True
											    ElseIf (rbDistroType.SelectedItem.Text.ToLower().Trim() = "shipment" And strErrorMsg = "NO_WORKED_DISTROS" And htSecurityGrp("CURRENT_USER_GRP").ToString() = "ARTHUR") Then
											        'Display screen with both the buttons along with results
											        cancelBtnStatus = "ENABLED"
											        cancelArthurBtnStatus = "ENABLED"
											        displayResults = True
											    ElseIf (rbDistroType.SelectedItem.Text.ToLower().Trim() = "backstock" And strErrorMsg = "NO_WORKED_DISTROS" And htSecurityGrp("CURRENT_USER_GRP").ToString() = "ARTHUR") Then
											        'Display screen with  the cancel button along with results
											        cancelBtnStatus = "ENABLED"
											        cancelArthurBtnStatus = "DISABLED"
											        displayResults = True
											    ElseIf ((rbDistroType.SelectedItem.Text.ToLower().Trim() = "shipment" Or rbDistroType.SelectedItem.Text.ToLower().Trim() = "backstock") And strErrorMsg = "NO_WORKED_DISTROS" And htSecurityGrp("CURRENT_USER_GRP").ToString() = "WMS") Then
											        'Display results only with Cancel button
											        cancelBtnStatus = "ENABLED"
											        cancelArthurBtnStatus = "DISABLED"
											        displayResults = True
											    Else
											        strError = strErrorMsg
											        displayResults = False
											    End If
											%>
										<%If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso displayResults = True Then%>
										<TABLE class="DatagridSm" id="Table6" style="BORDER-COLLAPSE: collapse" cellSpacing="0"
											cellPadding="2" width="550" align="center" border="1">
											<TR class="DATAGRID_HeaderSm">
												<TD width="75">Distro #</TD>
												<TD width="75">SKU</TD>
												<TD width="75">Status</TD>
												<TD width="75">Number of Distros</TD>
												<TD width="75">Sum of Qty</TD>
												<TD width="75"></TD>
											</TR>
											<%
												
											    
											    For Each dr In ds.Tables(0).Rows
												
											        ' Enable cancel button if stat_code = 0
											        'IF (dr("STAT_CODE") = 0 OR dr("STAT_CODE") = 99) THEN
											        '	cancelBtnStatus = "ENABLED"
											        '	if  dr("UNIT_RCVD") > 0 then
											        '		cancelArthurBtnStatus = "ENABLED"
											        '	else
											        '		cancelArthurBtnStatus = "DISABLED"
											        '	end if
											        'ELSE
											        '	cancelBtnStatus = "DISABLED"
											        '		cancelArthurBtnStatus = "DISABLED"
											        'END IF		
											        											        											        
											%>
											
											<TR class="<%=rowStyle%>">
												<TD vAlign="middle"><%=dr("distro_nbr")%></TD>
												<TD vAlign="middle"><%=dr("SKU_NBR")%></TD>
												<TD vAlign="middle"><%=dr("STAT_CODE")%></TD>
												<TD vAlign="middle"><%=dr("NO_OF_Distros")%></TD>
												<TD vAlign="middle"><%=dr("Sum_of_Qty")%></TD>												
												
											<%											
											Next
																					
											'response.write ("</TD><TD valign=middle height='100%' align=center ><input type=button class=btnSmallgrey value=Cancel " & cancelBtnStatus & " onClick=""CancelDistro('" & prevDistroNbr & "')""></TD></TR>")
											Response.Write("</TD><TD valign=middle height='100%' align=center ><input type=button class=btnSmallgrey value=Cancel " & cancelBtnStatus & " onClick=""CancelDistro('" & dr("distro_nbr") & "','N')"">&nbsp;&nbsp;&nbsp;&nbsp;<input type=button class=btnSmallgrey value=Cancel&UpdateArthur " & cancelArthurBtnStatus & " onClick=""CancelAndUpdateArthur('" & dr("Distro_nbr") & "','Y')""></TD></TR>")
											'response.write (cancelButton)
											%>
												<TD></TD>
											</TR>
										</TABLE>
										<%ElseIf (strError.Length > 0) Then%>
										<FONT class="errStyle"><%Response.Write(strError)%></FONT>
										<%ELSE%>
										<FONT class="errStyle">No records found.</FONT>
										<%END IF%>
									</asp:panel></td>
							</tr>
						</TABLE>
					</TD>
				</TR>
				<TR height="5">
					<TD></TD>
				</TR>
			</TABLE>
		</form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
