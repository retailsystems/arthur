﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using Microsoft.SqlServer.Dts.Runtime;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Text;
using System.Web.Mail;
namespace HotTopic.SSIS.PkgInitiator
{
    /// <summary>
    /// Summary description for WebService
    /// </summary>
    [WebService(Namespace = "http://HotTopic.SSIS.PkgInitiator/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WebService : System.Web.Services.WebService
    {
        Package ssisPackage;

        [WebMethod]
        public string LaunchPackage(string sourceType, string sourceLocation, string packageName, string packageConfigName, string deptList, string warehouseNum)
        {         
            string packagePath, packageconfigpath;

            try
            {

                //EventLog.WriteEntry("HotTopic.SSIS.PkgInitiator", "LaunchPackage method begin", System.Diagnostics.EventLogEntryType.Information);

                Application integrationServices = new Application();

                Variables var1 = null;
                Variables var2 = null;

                // Combine path and file name.
                packagePath = Path.Combine(sourceLocation, packageName);
                packageconfigpath = Path.Combine(sourceLocation, packageConfigName);

                switch (sourceType)
                {
                    case "file":
                        // Package is stored as a file.
                        // Add extension if not present.
                        if (String.IsNullOrEmpty(Path.GetExtension(packagePath)))
                        {
                            packagePath = String.Concat(packagePath, ".dtsx");
                        }

                        if (File.Exists(packagePath)) //Server.MapPath(packagePath)
                        {
                            ssisPackage = integrationServices.LoadPackage(packagePath, null);
                            ssisPackage.ImportConfigurationFile(packageconfigpath);

                            ssisPackage.VariableDispenser.LockOneForWrite("User::DeptList", ref var1);
                            ssisPackage.VariableDispenser.LockOneForWrite("User::WarehouseNum", ref var2);
                            //ssisPackage.VariableDispenser.GetVariables(ref vars);                        
                            var1["User::DeptList"].Value = deptList;
                            var2["User::WarehouseNum"].Value = warehouseNum;
                            //System.Diagnostics.Debug.WriteLine("Updated Eno: " + vars["User::var_Eno"].Value.ToString());
                            var1.Unlock();
                            var2.Unlock();

                            //ConnectionManager loggingConnection = ssisPackage.Connections.Add("FILE");
                            //loggingConnection.ConnectionString = sourceLocation + @"\SSISPackageLog.txt";

                            //LogProvider provider = ssisPackage.LogProviders.Add("DTS.LogProviderTextFile.2");
                            //provider.ConfigString = loggingConnection.Name;
                            //ssisPackage.LoggingOptions.SelectedLogProviders.Add(provider);
                            //ssisPackage.LoggingOptions.EventFilterKind = DTSEventFilterKind.Inclusion;
                            //ssisPackage.LoggingOptions.EventFilter = new String[] { "OnPreExecute", "OnPostExecute", "OnError", "OnWarning", "OnInformation" };
                            //ssisPackage.LoggingMode = DTSLoggingMode.Enabled;
                        }
                        else
                        {
                            throw new ApplicationException("Invalid file location: " + packagePath);
                        }
                        break;
                    case "sql":
                        // Package is stored in MSDB.
                        // Combine logical path and package name.
                        if (integrationServices.ExistsOnSqlServer(packagePath, ".", String.Empty, String.Empty))
                        {
                            ssisPackage = integrationServices.LoadFromSqlServer(packageName, "(local)", String.Empty, String.Empty, null);
                        }
                        else
                        {
                            throw new ApplicationException("Invalid package name or location: " + packagePath);
                        }
                        break;
                    case "dts":
                        // Package is managed by SSIS Package Store.
                        // Default logical paths are File System and MSDB.
                        if (integrationServices.ExistsOnDtsServer(packagePath, "."))
                        {
                            ssisPackage = integrationServices.LoadFromDtsServer(packagePath, "localhost", null);
                        }
                        else
                        {
                            throw new ApplicationException("Invalid package name or location: " + packagePath);
                        }
                        break;
                    default:
                        // throw new ApplicationException("Invalid sourceType argument: valid values are 'file', 'sql', and 'dts'.");
                        throw new ApplicationException("Invalid sourceType argument: valid values are 'file'.");
                }

                Thread newThread = new Thread(StartSSISPkg);

                newThread.Start();
                
                //DTSExecResult results = ssisPackage.Execute();      
                
                //return (Int32)ssisPackage.Execute();
                 //return results.ToString();
                return "Success";
            }
            catch (Exception ex)
            {
                return ex.ToString();                
            }

        }

        private void StartSSISPkg()
        {
            try
            {
                DTSExecResult results = ssisPackage.Execute();
                EventLog.WriteEntry("HotTopic.SSIS.PkgInitiator", results.ToString(), System.Diagnostics.EventLogEntryType.Information);
               // EventLog.WriteEntry("HotTopic.SSIS.PkgInitiator", "LaunchPackage method end", System.Diagnostics.EventLogEntryType.Information);
            }
            catch (Exception ex)
            {

                EventLog.WriteEntry("HotTopic.SSIS.PkgInitiator", ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
            }
        }

        public bool Send(string SmtpServer, string ToEmail,string SenderEmail,string body)
        {
            string errorMsg = string.Empty;
            try
            {
                

                return true;
            }
            catch (Exception ex)
            {
               errorMsg = ex.Message;
                return false;
            }
        }
    }    
}
