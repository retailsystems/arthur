create or replace
PROCEDURE     AAM.PROC_GET_APPROVED_ALLOCS
/*--------------------------------------------------------------------+
| PURPOSE
| ----------------
| Get Approved allocations with status code of 30 (approved),
| which has not been exported and not associated with any error codes for the particular allocation.
|
| If there are any allocations, the procedure will get the Details lines, and FTP that data to BizTalk.
|
| HISTORY
| ----------------
| 12/2/2003	Cris Tran  	   	  Initial version
| 1/22/2004	Hogan Lei         Version 1.1 (Update to insert into Temp table)
| 5/12/2004 Hogan Lei          Added TEMP_ALLOC_HISTORY for allocation modification
| 6/08/2004 Hogan Lei         Changed to always '001' as the Div_Nbr
| 6/10/2004 Hogan Lei         Changed to pass '001' for the modified records as well
| 9/2/2004  Hogan Lei          Changed the logic that updated the backstock allocation at the end.
| 9/24/2004 Hogan Lei          Changed to move most of the elements from Alloc Hdr to Alloc Dtl
| 10/27/2004 Sri Bajjuri      IF alloc work list lines belong to multiple whse create seperate files
|                             per each WHSE.
| 01/24/2005 Sri Bajjuri      Added Batch_Nbr(required for backstock allocs) field to ALLOC_HDR section
| 03/21/2005 Sanda Ma          Limited to 40 text files per each execution
| 08/29/2006 Jenny Castaneda  Added modifications for GERS 9.2 upgrade
| 10/23/2006 Sri Bajjuri      GERS 9.2 upgrade changes
| 11/28/2007  Hogan Lei          Change the logic to bridge when the worklist is "Released"
| 02/04/2010 Jackie Ogroski   Removed use of the TEMP_ALLOC_HISTORY table

| 07/21/2010  Karl Zeutizus   WMS Upgrade-2010: Changed logic to send allocations that are
|                             slated for CADC BackStock (9999) to be treated like a regular Store allocation
|                             and send the allocation to WMS. Also did general formatting of existing code.
|                             for better readability
| 2/21/2011  Hogan Lei        Modified by Hogan for TN WMS Upgrade.  Changed logic to send allocations that are
|                             slated for TNDC BackStock (9997) to be treated like a regular Store allocation
|                             and send the allocation to WMS.
| 8/10/2011  Hogan Lei        Added Dept_Nbr to the extraction.
| 9/20/2012  DRai       Got Production copy.
/ 9/25/2012  DRai       Added Style field to TEMP_APPROVED_ALLOC table, Send Parent Style with the component SKU number
/ 01/09/2012 DRai   Change Code to send correct allocated qty for multiple items . IF Pack ID = SKU_NBR  then  qty_per_pack * Results_QTY
| 04/08/2014 Vijay Naidu replaced UTL_FILE.PUT_LINE with UTL_FILE.PUT and also added CHR(13), CHR(10) as a newline delimiter and added UTL_FILE.FFLUSH(v_file_handle);
--------------------------------------------------------------------*/
IS
    v_errornum NUMBER;
    v_errormess CHAR(70);

    v_file_name VARCHAR2(50) := '';
    v_file_handle UTL_FILE.FILE_TYPE;

    v_alloc_nbr NUMBER := 0;
    v_whse_nbr VARCHAR2(4) := '';
    v_company_cd CHAR(3) := '001';

    v_sequence_nbr NUMBER;

    v_max_file_cntr NUMBER;

    v_deleted_location VARCHAR2(4);

    v_po_alloc_seq_nbr NUMBER; --/***/--
     v_cnt NUMBER;


    /*generate file name*/
    --CURSOR file_name_cursor(temp_alloc_nbr IN VARCHAR2, temp_warehouse_nbr IN VARCHAR2) IS
    --    SELECT 'alloc'||'_'||temp_alloc_nbr ||'_'|| temp_warehouse_nbr ||'_'||TO_CHAR(SYSDATE,'MMDDYY_HH24MISSSS')||'.txt' AS v_cur_file_name FROM DUAL;

    --Drai 10/04/12 change file name to alloc
    CURSOR file_name_cursor(temp_alloc_nbr IN VARCHAR2, temp_warehouse_nbr IN VARCHAR2) IS
    SELECT 'alloc'||'_'||temp_alloc_nbr ||'_'|| temp_warehouse_nbr ||'_'||TO_CHAR(SYSDATE,'MMDDYY_HH24MISSSS')||'.txt' AS v_cur_file_name FROM DUAL;

    -- approved allocations with asn --
    -- KJZ WMS-2010 upgrade: Comment out Warehouse_Flag and filter out 9997 location as this is the backstock "store" .
    -- We do want the CADC 9999 backstock lines to flow through to WMS as part of the upgrade.

    -- Filter out any TNDC backstock items.
    -- From WL_BULK --
    CURSOR alloc_cursor IS
     SELECT DISTINCT
         W.ALLOC_NBR,
         W.WL_KEY,
         W.PO_NBR,
         W.ASN,
         W.rec_nbr,
         W.COST_PRICE,
         W.RETAIL_PRICE,
         '001' AS DIV_NBR,
         W.merch_id_type,
         WB.PO_LN_SEQ_NUM,
         RD.LOCATION_ID,
        CASE WHEN WB.PACK_ID = WB.SKU_NBR AND WB.QTY_PER_PACK > 1 THEN WB.QTY_PER_PACK * RD.RESULT_QTY ELSE RD.RESULT_QTY END AS RESULT_QTY ,
        CASE WHEN WB.PACK_ID <> WB.SKU_NBR THEN WB.PACK_ID ELSE WB.SKU_NBR END AS SKU_NBR,
         H.WMS_WHSE,
         L.PTL_PRIORITY,
         W.DEPT_NBR,
         CASE WHEN WB.PACK_ID <> WB.SKU_NBR THEN WB.PACK_ID ELSE W.ITM_NBR END AS STYLE
    FROM
         WORKLIST W, WL_PACK WB, RESULTS_DETAIL RD, LOCATIONS L, HT_DC_XREF H
    WHERE
         NOT EXISTS (SELECT *
                     FROM WORKLIST L
                     WHERE L.ASN = W.ASN
                     AND L.status_code <> 40
                     AND L.avail_qty > 0) AND
         NOT EXISTS (SELECT *
                     FROM TEMP_APPROVED_ALLOCS T
                     WHERE T.ALLOC_NBR=W.ALLOC_NBR) AND
         W.WL_KEY   = WB.WL_KEY AND
         WB.WL_KEY  = RD.WL_KEY AND
      --  WB.SIZE_NBR = RD.LEVEL1_DESC AND
      WB.PACK_ID = SUBSTR(RD.unique_mer_key, INSTR(RD.unique_mer_key, '.') +1 , LENGTH(RD.unique_mer_key)) AND
         W.ALLOC_NBR = RD.ALLOCATION_NBR AND
         RD.LOCATION_ID = L.LOCATION_ID AND
         W.SOURCE_DC    = H.GERS_WHSE AND
         -- L.WAREHOUSE_FLAG <> 'Y' AND      -- KJZ WMS UPGRADE-2010
         W.status_code  = 40 AND
         W.exported IS NULL AND
         W.trouble_code IS NULL AND
         W.PO_NBR <> 0 AND
         W.ASN IS NOT NULL AND
         W.DOCUMENT_TYPE <> 'RESERVE'
         group by  W.ALLOC_NBR,
         W.WL_KEY,
         W.PO_NBR,
         W.ASN,
         W.rec_nbr,
         W.COST_PRICE,
         W.RETAIL_PRICE,
         W.merch_id_type,
         WB.PO_LN_SEQ_NUM,
         RD.LOCATION_ID,
         RD.RESULT_QTY,
         H.WMS_WHSE,
         L.PTL_PRIORITY,
		 WB.QTY_PER_PACK,
         W.DEPT_NBR, Pack_ID, ITM_NBR, sku_nbr ;

--   DRai 9/26/2012 This is not required for Prepacks
--    UNION
--
--    -- From WL_MULTIPLE --
--    SELECT
--         W.ALLOC_NBR,
--         W.WL_KEY,
--         W.PO_NBR,
--         W.ASN,
--         W.rec_nbr,
--         W.COST_PRICE,
--         W.RETAIL_PRICE,
--         '001' AS DIV_NBR,
--         W.merch_id_type,
--         WM.PO_LN_SEQ_NUM,
--         RD.LOCATION_ID,
--         RD.RESULT_QTY,
--         WM.SKU_NBR,
--         H.WMS_WHSE,
--         L.PTL_PRIORITY,
--         WM.SIZE_NBR,
--         W.DEPT_NBR
--    FROM
--         WORKLIST W, WL_MULTIPLE WM, RESULTS_DETAIL RD, LOCATIONS L, HT_DC_XREF H
--    WHERE
--         NOT EXISTS (SELECT *
--                     FROM WORKLIST L
--                     WHERE L.ASN = W.ASN
--                     AND L.status_code <> 40
--                     AND L.avail_qty > 0) AND
--         NOT EXISTS (SELECT *
--                     FROM TEMP_APPROVED_ALLOCS T
--                     WHERE T.ALLOC_NBR = W.ALLOC_NBR) AND
--         W.WL_KEY   = WM.WL_KEY AND
--         WM.WL_KEY  = RD.WL_KEY AND
--         WM.SIZE_NBR = RD.LEVEL1_DESC AND
--         W.ALLOC_NBR = RD.ALLOCATION_NBR AND
--         RD.LOCATION_ID = L.LOCATION_ID AND
--         W.SOURCE_DC = H.GERS_WHSE AND
--        -- L.WAREHOUSE_FLAG <> 'Y' AND      -- KJZ WMS UPGRADE-2010
--         W.status_code = 40 AND
--         W.exported IS NULL AND
--         W.trouble_code IS NULL AND
--         W.PO_NBR <> 0 AND
--         W.ASN IS NOT NULL AND
--         W.DOCUMENT_TYPE <> 'RESERVE';
         -- AND RD.LOCATION_ID <> '9997' ;         -- HOGAN LEI (TN WMS UPGRADE MODIFICATION).

    alloc_record alloc_cursor%ROWTYPE; --/***/--

    -- Temp_Approved_Alloc -- --/***/--
    CURSOR approved_alloc_cursor IS
        SELECT
         TAA.ALLOC_NBR, TAA.WL_KEY, TAA.PO_NBR, TAA.ASN,
         TAA.REC_NBR, TAA.COST_PRICE, TAA.RETAIL_PRICE, TAA.DIV_NBR,
         TAA.MERCH_ID_TYPE, TAA.PO_LN_SEQ_NUM, TAA.LOCATION_ID,
         TAA.RESULT_QTY, TAA.SKU_NBR, TAA.WAREHOUSE_NBR, TAA.PTL_PRIORITY, TAA.SIZE_NBR, SN.SEQ_NBR, TAA.DEPT_NBR, TAA.STYLE
         FROM TEMP_APPROVED_ALLOCS TAA,
              HT_DISTRO_SEQ_NBR SN
         WHERE TAA.PROCESSED = 0
         AND TAA.PO_NBR      = SN.PO_NUM
         AND TAA.ASN         = SN.SHPMT_NBR
         AND TAA.ALLOC_NBR   = SN.DISTRO_NBR
         AND TAA.LOCATION_ID = SN.LOCATION_ID
         AND TAA.STYLE = SN.STYLE
         AND  SUBSTR(TAA.SKU_NBR,instr(TAA.SKU_NBR,'-')+1,length(TAA.SKU_NBR)) = SN.STYLE_SUFFIX
       --  AND SUBSTR(TAA.SKU_NBR, 1,6) = SN.STYLE
        -- AND SUBSTR(TAA.SKU_NBR, 8,3) = SN.STYLE_SUFFIX
         ORDER BY TAA.ALLOC_NBR, TAA.WAREHOUSE_NBR;

    approved_alloc_record approved_alloc_cursor%ROWTYPE;

BEGIN

    OPEN alloc_cursor;
    -- Loop through the cursor and insert into the temp table until it's done --
    LOOP
        FETCH alloc_cursor INTO alloc_record;
        EXIT WHEN alloc_cursor%NOTFOUND;

        INSERT INTO TEMP_APPROVED_ALLOCS(
            ALLOC_NBR,
            WL_KEY,
            PO_NBR,
            ASN,
            REC_NBR,
            COST_PRICE,
            RETAIL_PRICE,
            DIV_NBR,
            MERCH_ID_TYPE,
            PO_LN_SEQ_NUM,
            LOCATION_ID,
            RESULT_QTY,
            SKU_NBR,
            WAREHOUSE_NBR,
            PTL_PRIORITY,
            --SIZE_NBR,
            DEPT_NBR,
            STYLE ---DRAI Added for Prepack to WMS
        )
        VALUES(
            alloc_record.ALLOC_NBR,
            alloc_record.WL_KEY,
            alloc_record.PO_NBR,
            alloc_record.ASN,
            alloc_record.REC_NBR,
            alloc_record.COST_PRICE,
            alloc_record.RETAIL_PRICE,
            alloc_record.DIV_NBR,
            alloc_record.MERCH_ID_TYPE,
            alloc_record.PO_LN_SEQ_NUM,
            alloc_record.LOCATION_ID,
            alloc_record.RESULT_QTY,
            alloc_record.SKU_NBR,
            alloc_record.WMS_WHSE,
            alloc_record.PTL_PRIORITY,
            --alloc_record.SIZE_NBR,
            alloc_record.DEPT_NBR,
            alloc_record.STYLE
        );

        SELECT po_alloc_seq.NEXTVAL
             INTO v_po_alloc_seq_nbr
             FROM DUAL;
        v_cnt := 0;

        -- sri - 10/23/2006 - check if record already exists.
        SELECT COUNT(*) INTO v_cnt
          FROM HT_DISTRO_SEQ_NBR
          WHERE PO_NUM = alloc_record.po_nbr
          AND STYLE = alloc_record.style---SUBSTR((alloc_record.sku_nbr),1,6)
          AND STYLE_SUFFIX = SUBSTR(alloc_record.sku_nbr,instr(alloc_record.sku_nbr,'-')+1,length(alloc_record.sku_nbr))
          AND LOCATION_ID = alloc_record.location_id
          AND SHPMT_NBR = alloc_record.asn
          AND DISTRO_NBR = alloc_record.alloc_nbr;

        -- insert if record not found in HT_DISTRO_SEQ_NBR table
        IF v_cnt <= 0 THEN
             INSERT INTO HT_DISTRO_SEQ_NBR(
                          SEQ_NBR,
                         PO_NUM,
                         STYLE,
                         STYLE_SUFFIX,
                         LOCATION_ID,
                         SHPMT_NBR,
                         DISTRO_NBR,
                         CREATE_DATE)
             VALUES (v_po_alloc_seq_nbr,
                      alloc_record.po_nbr,
                     --SUBSTR((alloc_record.sku_nbr),1,6),
                     --SUBSTR((alloc_record.sku_nbr),8,3),
                     alloc_record.style,
                     SUBSTR(alloc_record.sku_nbr,instr(alloc_record.sku_nbr,'-')+1,length(alloc_record.sku_nbr)),
                     alloc_record.location_id,
                     alloc_record.asn,
                     alloc_record.alloc_nbr,
                     SYSDATE);
        END IF;

    END LOOP;
    CLOSE alloc_cursor;

    COMMIT;

    -- Generate the txt file --
    OPEN approved_alloc_cursor;
    FETCH approved_alloc_cursor INTO approved_alloc_record;

    v_max_file_cntr := 1;

    WHILE (approved_alloc_cursor%FOUND AND v_max_file_cntr <= 40)
    LOOP

     /*
        KJZ WMS-2010 Upgrade: Business logic change is needed to incorporate Backstock Allocations
        in the standard "store allocation" file that is sent to WMS.  This will only be implemented
        for the CADC.  Any allocations for the TNDC backstock will be handeled with the current logic.

        The new logic is as follows:  Add CADC only backstock allocations to the Allocation_cursor
        (We'll filter out TNDC backstock lines.)  Process the dataset as normal including the CADC
        Backstock lines. If the Allocation is a 9997 backstock only allocation we'll set the EXPORTED
        flag at the end of the procedure as done in the current pre-upgrade process.
     */

            IF (approved_alloc_record.ALLOC_NBR <> v_alloc_nbr OR approved_alloc_record.WAREHOUSE_NBR <>  v_whse_nbr) THEN
                IF v_file_name <> '' THEN
                    UTL_FILE.FCLOSE(v_file_handle);
                    COMMIT;
                END IF;

                v_alloc_nbr := approved_alloc_record.ALLOC_NBR;
                v_whse_nbr  := approved_alloc_record.WAREHOUSE_NBR;

                -- Increment the file counter
                v_max_file_cntr := v_max_file_cntr + 1;

                -- CREATE NEW FILE --
                OPEN file_name_cursor(v_alloc_nbr, v_whse_nbr);
                    FETCH file_name_cursor
                    INTO  v_file_name;
                CLOSE file_name_cursor;


                v_file_handle := UTL_FILE.FOPEN('ALLOCATIONS',v_file_name,'w',32767);
                v_sequence_nbr := 1;

                -- OUTPUT THE HEADER LINE --
                UTL_FILE.PUT(v_file_handle, 'ALLOC_HDR:'||
                        approved_alloc_record.ALLOC_NBR ||'|'||
                        approved_alloc_record.WAREHOUSE_NBR || '|'||CHR(13)||CHR(10)
                        );
						
						UTL_FILE.FFLUSH(v_file_handle);

                -- OUTPUT THE DETAIL LINE --
                UTL_FILE.PUT(v_file_handle, 'ALLOC_DTL:' ||
                        approved_alloc_record.WL_KEY ||'|'||
                        approved_alloc_record.PO_NBR ||'|'||
                        approved_alloc_record.PO_LN_SEQ_NUM ||'|'||
                        approved_alloc_record.SEQ_NBR ||'|'||
                        approved_alloc_record.ASN ||'|'||
                        approved_alloc_record.REC_NBR||'|'||
                        approved_alloc_record.COST_PRICE||'|'||
                        approved_alloc_record.RETAIL_PRICE||'|'||
                        TRIM(TO_CHAR(TO_NUMBER(approved_alloc_record.DIV_NBR, '999'),'009'))||'|'||
                        approved_alloc_record.MERCH_ID_TYPE||'|'||
                        v_company_cd||'|'||
                        v_sequence_nbr||'|'||
                        approved_alloc_record.LOCATION_ID||'|'||
                        approved_alloc_record.PTL_PRIORITY||'|'||
                        approved_alloc_record.RESULT_QTY||'|'||
                        approved_alloc_record.SKU_NBR||'|'||
                        approved_alloc_record.DEPT_NBR||'|'||
                        approved_alloc_record.STYLE||CHR(13)||CHR(10));

						UTL_FILE.FFLUSH(v_file_handle);
						
                -- Update the record as "processing"
                UPDATE TEMP_APPROVED_ALLOCS
                SET PROCESSED   = 1
                WHERE PO_NBR    = approved_alloc_record.PO_NBR
                AND ALLOC_NBR   = approved_alloc_record.ALLOC_NBR
                AND WL_KEY      = approved_alloc_record.WL_KEY
                AND ASN         = approved_alloc_record.ASN
                AND PO_LN_SEQ_NUM = approved_alloc_record.PO_LN_SEQ_NUM
                AND LOCATION_ID = approved_alloc_record.LOCATION_ID;
            ELSE  -- OUTPUT THE DETAIL LINE --
                UTL_FILE.PUT(v_file_handle, 'ALLOC_DTL:' ||
                        approved_alloc_record.WL_KEY ||'|'||
                        approved_alloc_record.PO_NBR ||'|'||
                        approved_alloc_record.PO_LN_SEQ_NUM ||'|'||
                        approved_alloc_record.SEQ_NBR ||'|'||
                        approved_alloc_record.ASN ||'|'||
                        approved_alloc_record.REC_NBR||'|'||
                        approved_alloc_record.COST_PRICE||'|'||
                        approved_alloc_record.RETAIL_PRICE||'|'||
                        TRIM(TO_CHAR(TO_NUMBER(approved_alloc_record.DIV_NBR, '999'),'009'))||'|'||
                        approved_alloc_record.MERCH_ID_TYPE||'|'||
                        v_company_cd||'|'||
                        v_sequence_nbr||'|'||
                        approved_alloc_record.LOCATION_ID||'|'||
                        approved_alloc_record.PTL_PRIORITY||'|'||
                        approved_alloc_record.RESULT_QTY||'|'||
                        approved_alloc_record.SKU_NBR||'|'||
                        approved_alloc_record.DEPT_NBR||'|'||
                        approved_alloc_record.STYLE||CHR(13)||CHR(10));
						UTL_FILE.FFLUSH(v_file_handle);
                -- Update the record as "processing"
                UPDATE TEMP_APPROVED_ALLOCS
                SET PROCESSED   = 1
                WHERE PO_NBR    = approved_alloc_record.PO_NBR
                AND ALLOC_NBR   = approved_alloc_record.ALLOC_NBR
                AND WL_KEY      = approved_alloc_record.WL_KEY
                AND ASN         = approved_alloc_record.ASN
                AND PO_LN_SEQ_NUM = approved_alloc_record.PO_LN_SEQ_NUM
                AND LOCATION_ID = approved_alloc_record.LOCATION_ID;
             END IF;

              v_sequence_nbr := v_sequence_nbr + 1;

        FETCH approved_alloc_cursor INTO approved_alloc_record;

    END LOOP;

    CLOSE approved_alloc_cursor;

    UTL_FILE.FCLOSE(v_file_handle);
    COMMIT;

    EXCEPTION
        WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR(-20100,SQLERRM);
END;