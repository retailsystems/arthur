CREATE OR REPLACE PACKAGE BODY HTPROG."RIA_BASE_TO_STAGE" IS
/******************************************************************************
   NAME:       RIA_BASE_TO_STAGE
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        12/19/2011      DKaurRai       1. Created this package.
   1.1        11/27/2012      ATsoi            1. change substr(OH.SKU_NUM,1,6) to use itm_cd
   1.2         04/12/2013     DKaurRai      Bug 6005- Neg WIP Fix
   1.3        6/7/2014        VNaidu        updated the htprog to upper case
******************************************************************************/
-- Variables used to handle errors and logging.
   vcLoggingMessage               VARCHAR2(1000) := NULL;
   vcSystemUser                      RIA_LOGGING.SYS_USER%TYPE := 'htprog';
   vcAppName                         RIA_LOGGING.APP_NAME%TYPE := 'ORACLE';
   vcJobProcess                       RIA_LOGGING.JOB_PROCESS%TYPE := 'RIA_BASE_TO_STAGE';
   vcServerName                     RIA_LOGGING.SERVER_NAME%TYPE := 'DEVTEST';
   vcAvailibility                         RIA_LOGGING.AVAILABILITY%TYPE := NULL;
   vcFunctionName                   RIA_LOGGING.FUNCTION_NAME%TYPE := NULL;
   vcJobAttempt                       RIA_LOGGING.JOB_ATTEMPT%TYPE := NULL;
   vcJobStatusCD                     RIA_LOGGING.JOB_STATUS_CD%TYPE := NULL;
   vcLogMessage                     RIA_LOGGING.LOG_MESSAGE%TYPE := NULL;
   vcNativeErrorCD                  RIA_LOGGING.NATIVE_ERROR_CD%TYPE := NULL;

PROCEDURE Drop_Indexes (p_tbl_name varchar2)  as


 CURSOR IDXCUR IS

  select index_name from user_indexes where table_name = p_tbl_name AND upper(TABLE_OWNER) = 'HTPROG';

begin

   FOR idx_rec IN IDXCUR

   LOOP

      EXECUTE IMMEDIATE 'DROP INDEX ' || idx_rec.index_name ;

   END LOOP;

LogSuccess('Indexes Dropped');

END Drop_Indexes;

PROCEDURE Drop_Constraint (p_tbl_name varchar2)  as

 itemExists NUMBER;
begin
itemExists := 0;
 SELECT COUNT(CONSTRAINT_NAME) INTO itemExists FROM All_Constraints where table_name = p_tbl_name AND Upper(OWNER) = 'HTPROG' AND Constraint_type = 'P';

IF itemExists > 0 THEN
EXECUTE IMMEDIATE 'ALTER TABLE ' || p_tbl_name ||' DROP PRIMARY KEY CASCADE';
END IF;

LogSuccess('Table constraints dropped');
END Drop_Constraint;

/* Error handling will need to pass to precedure                         */
/* Error will go here                                                    */
/*                                                                       */
/*-----------------------------------------------------------------------*/
PROCEDURE RIA_Error_Logging
IS
BEGIN

     -- If the vairable is NULL apply the default value to load into logging

     vcSystemUser := NVL(vcSystemUser, 'htprog');
     vcAppName := NVL(vcAppName, 'ORACLE');
     vcJobProcess := NVL(vcJobProcess, 'Building Stage');
     vcServerName := NVL(vcServerName, 'DEVTEST');

     --Send error to for logging
     htprog.RIA_LOGGING_PROC(vcSystemUser, vcAppName, vcJobProcess, vcServerName,
                             vcAvailibility, vcFunctionName, vcJobAttempt,
                            vcJobStatusCD, vcLogMessage, vcNativeErrorCD);

    COMMIT;

    -- Reset error variables for the next load
    vcLoggingMessage := NULL;
    vcSystemUser := NULL;
    vcAppName := NULL;
    vcJobProcess := NULL;
    vcServerName := NULL;
    vcAvailibility := NULL;
    --vcFunctionName := NULL;
    vcJobAttempt := NULL;
    vcJobStatusCD := NULL;
    vcLogMessage := NULL;
    vcNativeErrorCD := NULL;

END RIA_Error_Logging;
/*---------------------------------------------------------------*/
/*     Log All sucess to the tables through this function        */
/*     This function takes only a message parameter              */
/*---------------------------------------------------------------*/

PROCEDURE LogSuccess (vcLoggingMessage IN VARCHAR2)
IS
BEGIN

     vcJobStatusCD := 0;
     vcAvailibility := 1;

     --SEND_MAIL(veMailRecipient,veMailSubject || 'Success', 'Success : ' || vcLoggingMessage);

     vcLogMessage := vcLoggingMessage;

     RIA_Error_Logging;

END LogSuccess;
/*---------------------------------------------------------------*/
/*     LLog all Erros and Exceptions that will be emailed        */
/*---------------------------------------------------------------*/
PROCEDURE LogFailure (vcErrorCD IN VARCHAR2,
                      vcLoggingMessage IN VARCHAR2)
IS
  vFormatMessage      VARCHAR2(1000);
BEGIN

     vcJobStatusCD := 1;
     vcAvailibility := 1;

     vFormatMessage := 'Failure Time: ' || TO_CHAR(SYSDATE, 'mm/dd/yyyy hh:mm:ss') || utl_tcp.crlf ||
                          'Error Code: ' || vcErrorCD || utl_tcp.crlf ||
                       'Failed at ' || vcJobProcess || utl_tcp.crlf ||
                       'Message: ' || vcLoggingMessage;

     --vcNativeErrorCD := SUBSTR(vcErrorCD,1,9);
     vcNativeErrorCD := vcErrorCD;
     vcLogMessage := 'Error: ' || vcLoggingMessage || ' ' || vcNativeErrorCD;

     --SEND_MAIL(veMailRecipient, veMailSubject || ' Failed!', vFormatMessage);

     RIA_Error_Logging;

         RAISE_APPLICATION_ERROR (-20100, 'Error: '|| vcErrorCD || ' - ' || vcLoggingMessage  );

END LogFailure;

PROCEDURE Populate_RIA_Calendar AS

v_WkBegDt date;

begin

EXECUTE IMMEDIATE 'Truncate table Ria_Cal_Mapping';

Select DT into v_WkBegDt from Calendar
Where ( Merch_Year, Week_Num_In_Merch_Year) in (
Select Merch_Year, Week_Num_In_Merch_Year from Calendar where DT =trunc( SYSDATE))
and Day_Num_in_merch_week = 1;

Insert into RIA_CAL_Mapping (CalWeek, RiaWeek, CalYear, Wk_Beg_Dt)
(
 select CalWeek, rownum -1 as RiaWeek, CalYear, Wk_Beg_dt

 from (

 Select week_num_in_merch_year as CalWeek,Merch_Year as CalYear, min(DT) as Wk_Beg_dt

  from htprog.Calendar where DT between  (v_WkBegDt + 64 * -7  ) and v_WkBegDt

GROUP BY week_num_in_merch_year, Merch_Year

order by Merch_Year desc  , week_num_in_merch_year desc
)
);
commit;

 LogSuccess('Populate_RIA_Calendar Successfully Proccessed');

End Populate_RIA_Calendar;

PROCEDURE Populate_RIA_Summary_Tables  as
  v_WkNbr NUMBER(2,0);
  v_increment NUMBER(2,0) := 5;


begin

EXECUTE IMMEDIATE 'TRUNCATE TABLE htprog.STAGE_SKU';
EXECUTE IMMEDIATE 'TRUNCATE TABLE htprog.STAGE_SUBCLASS';

--EXECUTE IMMEDIATE 'ALTER TABLE htprog.STAGE_SKU DROP PRIMARY KEY CASCADE';
--EXECUTE IMMEDIATE 'ALTER TABLE htprog.STAGE_SUBCLASS DROP PRIMARY KEY CASCADE';
Drop_Constraint('STAGE_SKU');

Drop_Indexes ('STAGE_SKU');
Drop_Indexes ('STAGE_SUBCLASS');




  --LOAD TY data (WK 01-12)
    v_wkNbr := 1;
    LOOP
       INSERT /*+ APPEND*/  INTO htprog.STAGE_SKU (DIV_CD,DEPT_CD,CLASS_CD,SUBCLASS_CD, sku_num, itm_cd, STORE_CD, WEEK_NBR
       ,TOT_SLS, REG_SLS, TOT_EI,SKU_LST_RCV_DT,ITM_LST_RCV_DT,SIZE_CODE, PRC_STAT)
      SELECT /*+ ALL_ROWS*/ D.DIV_CD, D.DEPT_CD, D.CLASS_CD, D.SUBCLASS_CD,D.SKU_NUM, D.ITM_CD, F.STORE_CD, t.riaweek AS WEEK_NBR
       ,TOT_SLS, REG_SLS, TOT_EI, L.LST_RCV_DT AS SKU_LST_RCV_DT, D.LST_RCV_DT AS  ITM_LST_RCV_DT,D.SIZE_CD,D.PRC_STAT
      FROM htprog.RIA_stage_sku F
      JOIN htprog.sku_desc D  ON F.SKU_NUM = D.SKU_NUM
      JOIN htprog.ria_cal_mapping T ON F.wk_beg_dt = T.wk_beg_dt
      LEFT OUTER JOIN htprog.SKULSTDT L ON F.STORE_CD = L.STORE_CD and F.SKU_NUM = L.SKU_NUM
      WHERE T.riaweek BETWEEN v_wkNbr and v_wkNbr + v_increment  ;
      COMMIT;

      INSERT /*+ APPEND */ INTO htprog.STAGE_SUBCLASS (div_cd, dept_cd, class_cd, subclass_cd, size_cd, store_cd, week_nbr, prc_stat, sls_qty, ei_qty, itm_lst_rcv_dt, sku_lst_rcv_dt)
      (
          SELECT /*+ ALL_ROWS*/ div_cd, dept_cd, class_cd, subclass_cd, size_code, store_cd, week_nbr, PRC_STAT, SUM(tot_sls), SUM(tot_ei), MAX(itm_lst_rcv_dt), MAX(sku_lst_rcv_dt)
            FROM htprog.STAGE_SKU A
           WHERE week_nbr BETWEEN v_wkNbr and v_wkNbr + v_increment
           -- AND A.SKU_NUM = B.SKU_NUM AND B.DIV_CD IS NOT NULL AND B.PRC_STAT IN ('REG', 'MD')
        GROUP BY div_cd,dept_cd, class_cd,subclass_cd,size_code, store_cd, week_nbr, PRC_STAT
      );
       COMMIT;

      v_wkNbr := v_wkNbr + v_increment + 1;
      EXIT WHEN v_wkNbr > 12;
    END LOOP;

    --LOAD LY data (WK 38-64)
    v_wkNbr := 38;
    LOOP
       INSERT /*+ APPEND*/  INTO htprog.STAGE_SKU (DIV_CD,DEPT_CD,CLASS_CD,SUBCLASS_CD, sku_num, itm_cd, STORE_CD, WEEK_NBR
       ,TOT_SLS, REG_SLS, TOT_EI,ITM_LST_RCV_DT,SIZE_CODE, PRC_STAT)
      SELECT /*+ ALL_ROWS*/ D.DIV_CD, D.DEPT_CD, D.CLASS_CD, D.SUBCLASS_CD,D.SKU_NUM, D.ITM_CD, F.STORE_CD, t.riaweek AS WEEK_NBR
       ,TOT_SLS, REG_SLS, REG_EI as TOT_EI , D.LST_RCV_DT AS  ITM_LST_RCV_DT,D.SIZE_CD,D.PRC_STAT
      FROM htprog.RIA_stage_sku F
      JOIN htprog.sku_desc D  ON F.SKU_NUM = D.SKU_NUM
      JOIN htprog.ria_cal_mapping T ON F.wk_beg_dt = T.wk_beg_dt
      WHERE T.riaweek BETWEEN v_wkNbr and v_wkNbr + v_increment
      and (TOT_SLS is not null or REG_EI is not null );
      COMMIT;

      INSERT /*+ APPEND */ INTO htprog.STAGE_SUBCLASS (div_cd, dept_cd, class_cd, subclass_cd, size_cd, store_cd, week_nbr, prc_stat, sls_qty, ei_qty, itm_lst_rcv_dt, sku_lst_rcv_dt)
      (
          SELECT /*+ ALL_ROWS*/ div_cd, dept_cd, class_cd, subclass_cd, size_code, store_cd, week_nbr, PRC_STAT, SUM(reg_sls), SUM(tot_ei), MAX(itm_lst_rcv_dt), MAX(sku_lst_rcv_dt)
            FROM htprog.STAGE_SKU A
           WHERE week_nbr BETWEEN v_wkNbr and v_wkNbr + v_increment
           -- AND A.SKU_NUM = B.SKU_NUM AND B.DIV_CD IS NOT NULL AND B.PRC_STAT IN ('REG', 'MD')
        GROUP BY div_cd,dept_cd, class_cd,subclass_cd,size_code, store_cd, week_nbr, PRC_STAT
      );
       COMMIT;

      v_wkNbr := v_wkNbr + v_increment + 1;
      EXIT WHEN v_wkNbr > 64;
    END LOOP;

     LogSuccess('Stage Summary tables populated with weekly data');

--  EXECUTE IMMEDIATE  'CREATE UNIQUE INDEX htprog.PK_STAGE_SKU ON htprog.STAGE_SKU
--(DIV_CD, DEPT_CD, CLASS_CD, SKU_NUM, STORE_CD,
--WEEK_NBR)
--LOGGING
--TABLESPACE REPORT_INDEX_BIG
--PCTFREE    5
--INITRANS   8
--MAXTRANS   255
--STORAGE    (
--            INITIAL          64M
--            NEXT             64M
--            MINEXTENTS       1
--            MAXEXTENTS       UNLIMITED
--            PCTINCREASE      0
--            FREELISTS        8
--            FREELIST GROUPS  2
--            BUFFER_POOL      DEFAULT
--           )
--NOPARALLEL';
--
----PARALLEL ( DEGREE 4 INSTANCES DEFAULT )
--
--
--EXECUTE IMMEDIATE  'CREATE BITMAP INDEX htprog.BM_STAGE_SKU_ITMCD ON htprog.STAGE_SKU
--(ITM_CD)
--LOGGING
--TABLESPACE REPORT_INDEX2
--PCTFREE    5
--INITRANS   2
--MAXTRANS   255
--STORAGE    (
--            INITIAL          512K
--            NEXT             16M
--            MINEXTENTS       1
--            MAXEXTENTS       UNLIMITED
--            PCTINCREASE      0
--            FREELISTS        4
--            FREELIST GROUPS  2
--            BUFFER_POOL      DEFAULT
--           )
--NOPARALLEL';
--
--
--EXECUTE IMMEDIATE  'ALTER TABLE htprog.STAGE_SKU ADD (
--  CONSTRAINT PK_STAGE_SKU
-- PRIMARY KEY
-- (DIV_CD, DEPT_CD, CLASS_CD, SKU_NUM, STORE_CD, WEEK_NBR)
--    USING INDEX
--    TABLESPACE REPORT_INDEX_BIG
--    PCTFREE    5
--    INITRANS   8
--    MAXTRANS   255
--    STORAGE    (
--                INITIAL          64M
--                NEXT             64M
--                MINEXTENTS       1
--                MAXEXTENTS       UNLIMITED
--                PCTINCREASE      0
--                FREELISTS        8
--                FREELIST GROUPS  2
--               ))';
--
--EXECUTE IMMEDIATE  'CREATE UNIQUE INDEX htprog.STAGE_SUBCLASS_INDEX ON htprog.STAGE_SUBCLASS
--(DIV_CD, DEPT_CD, CLASS_CD, SUBCLASS_CD, SIZE_CD,
--STORE_CD, WEEK_NBR, PRC_STAT)
--LOGGING
--TABLESPACE REPORT_INDEX2
--PCTFREE    5
--INITRANS   8
--MAXTRANS   255
--STORAGE    (
--            INITIAL          16M
--            NEXT             16M
--            MINEXTENTS       1
--            MAXEXTENTS       UNLIMITED
--            PCTINCREASE      0
--            FREELISTS        8
--            FREELIST GROUPS  2
--            BUFFER_POOL      DEFAULT
--           )
--NOPARALLEL';
----PARALLEL ( DEGREE 2 INSTANCES 1 )
--
--EXECUTE IMMEDIATE  'CREATE BITMAP INDEX htprog.BM_STAGE_SUBCLASS_DIV ON htprog.STAGE_SUBCLASS
--(DIV_CD)
--LOGGING
--TABLESPACE REPORT_INDEX2
--PCTFREE    5
--INITRANS   2
--MAXTRANS   255
--STORAGE    (
--            INITIAL          512K
--            NEXT             16M
--            MINEXTENTS       1
--            MAXEXTENTS       UNLIMITED
--            PCTINCREASE      0
--            FREELISTS        4
--            FREELIST GROUPS  2
--            BUFFER_POOL      DEFAULT
--           )
--NOPARALLEL';
--
--
--EXECUTE IMMEDIATE  'CREATE BITMAP INDEX htprog.BM_STAGE_SUBCLASS_DEPT ON htprog.STAGE_SUBCLASS
--(DEPT_CD)
--LOGGING
--TABLESPACE REPORT_INDEX2
--PCTFREE    5
--INITRANS   2
--MAXTRANS   255
--STORAGE    (
--            INITIAL          512K
--            NEXT             16M
--            MINEXTENTS       1
--            MAXEXTENTS       UNLIMITED
--            PCTINCREASE      0
--            FREELISTS        4
--            FREELIST GROUPS  2
--            BUFFER_POOL      DEFAULT
--           )
--NOPARALLEL';
--
--
--EXECUTE IMMEDIATE  'CREATE BITMAP INDEX htprog.BM_STAGE_SUBCLASS_CLASS ON htprog.STAGE_SUBCLASS
--(CLASS_CD)
--LOGGING
--TABLESPACE REPORT_INDEX2
--PCTFREE    5
--INITRANS   2
--MAXTRANS   255
--STORAGE    (
--            INITIAL          512K
--            NEXT             16M
--            MINEXTENTS       1
--            MAXEXTENTS       UNLIMITED
--            PCTINCREASE      0
--            FREELISTS        4
--            FREELIST GROUPS  2
--            BUFFER_POOL      DEFAULT
--           )
--NOPARALLEL';
--
--
--EXECUTE IMMEDIATE  'CREATE BITMAP INDEX htprog.BM_STAGE_SUBCLASS_SUBCLASS ON htprog.STAGE_SUBCLASS
--(SUBCLASS_CD)
--LOGGING
--TABLESPACE REPORT_INDEX2
--PCTFREE    5
--INITRANS   2
--MAXTRANS   255
--STORAGE    (
--            INITIAL          512K
--            NEXT             16M
--            MINEXTENTS       1
--            MAXEXTENTS       UNLIMITED
--            PCTINCREASE      0
--            FREELISTS        4
--            FREELIST GROUPS  2
--            BUFFER_POOL      DEFAULT
--           )
--NOPARALLEL';
--
--  LogSuccess('Stage SummaryTables Indexing Completed');

End Populate_RIA_Summary_Tables;

PROCEDURE Populate_RIA_STAGE_Tables  as

v_isNewWK number;


 CURSOR WKCUR IS
  select distinct wk_beg_dt from BASE_GERS_EI
  UNION
  select distinct wk_beg_dt from BASE_GERS_SLS
         ORDER BY 1;
begin


-- step1: identify non-zero inventory duplicate records and set tot_qty = 0 for 'REG' inventory

MERGE INTO /*ALL_ROWS*/ base_gers_ei TGT
  USING (
        SELECT /*ALL_ROWS*/ A.* FROM base_gers_ei A
        JOIN (
        SELECT  /*ALL_ROWS*/ SKU_NUM, STORE_CODE,WK_BEG_DT
                FROM  base_gers_ei
                WHERE TOT_QTY <> 0
                GROUP BY SKU_NUM, store_code,WK_BEG_DT
                HAVING COUNT(*) > 1

          ) B ON A.SKU_NUM = B.SKU_NUM AND A.STORE_CODE = B.STORE_CODE AND A.WK_BEG_DT = B.WK_BEG_DT
          WHERE A.PRC_STAT = 'REG'
        ) SRC
    ON (TGT.SKU_NUM = SRC.SKU_NUM AND TGT.STORE_CODE = SRC.STORE_CODE AND TGT.WK_BEG_DT = SRC.WK_BEG_DT AND TGT.PRC_STAT = SRC.PRC_STAT)
    WHEN MATCHED THEN
      UPDATE SET TGT.TOT_QTY = 0
    WHEN NOT MATCHED THEN
      INSERT (SKU_NUM) VALUES (SRC.SKU_NUM);

COMMIT;

---step2: delete all non-zero inventory records
DELETE /*ALL_ROWS*/ FROM base_gers_ei  WHERE tot_qty = 0;
COMMIT;

 FOR wk_rec IN WKCUR
   LOOP
    --v_isNewWK := 0;
   -- select case when exists (select 1 from BASE_GERS_EI where wk_beg_dt = wk_rec.wk_beg_dt) then 1 else 0 end into v_isNewWK from dual;
    DELETE  htprog.RIA_stage_sku WHERE wk_beg_dt = wk_rec.wk_beg_dt;
    COMMIT;
    INSERT /*+ APPEND*/  INTO htprog.RIA_stage_sku (store_cd, sku_num, itm_cd, Wk_Beg_Dt, tot_ei, reg_ei)

     -- SELECT /*+ ALL_ROWS FULL(OH)PARALLEL(OH,3) */ OH.STORE_CODE,OH.SKU_NUM,SUBSTR(OH.SKU_NUM,1,6) AS ITM_CD, OH.WK_BEG_DT
     -- ,OH.TOT_QTY AS EI, DECODE(P.PRC_STAT,'REG',TOT_QTY,NULL) as REG_EI
     -- FROM BASE_GERS_EI OH JOIN BASE_GERS_PRCSTAT P ON OH.WK_BEG_DT = P.WK_BEG_DT and Substr(OH.SKU_NUM,1,6) = P.ITM_CD
     -- WHERE OH.wk_beg_dt = wk_rec.wk_beg_dt;

      --11/28/2012 - change join  SUBSTR(OH.SKU_NUM,1,6)

    SELECT /*+ ALL_ROWS FULL(OH)PARALLEL(OH,3) */ OH.STORE_CODE,OH.SKU_NUM
      , sd.ITM_CD AS ITM_CD
      , OH.WK_BEG_DT
      , OH.TOT_QTY AS EI, DECODE(OH.PRC_STAT,'REG',TOT_QTY,NULL) as REG_EI
      FROM BASE_GERS_EI OH
      LEFT JOIN
      sku_desc sd
      ON  sd.sku_num = OH.SKU_NUM
      WHERE OH.wk_beg_dt = wk_rec.wk_beg_dt ;

      COMMIT;

     -- MERGE INTO   htprog.RIA_stage_sku S
     -- USING (  SELECT /*+ ALL_ROWS FULL(SLS)PARALLEL(SLS,3) */ SLS.STORE_CODE,SLS.SKU_NUM,SUBSTR(SLS.SKU_NUM,1,6) AS ITM_CD, SLS.WK_BEG_DT
     --    ,TOT_QTY AS TOT_SLS, Reg_Qty AS REG_SLS
     --    FROM  BASE_GERS_SLS SLS WHERE SLS.wk_beg_dt = wk_rec.wk_beg_dt
     --  ) SLS
     -- ON (S.SKU_NUM = SLS.SKU_NUM AND S.STORE_CD = SLS.STORE_CODE AND S.WK_BEG_DT = SLS.WK_BEG_DT)
     -- WHEN MATCHED THEN
     -- UPDATE SET TOT_SLS = SLS.TOT_SLS , REG_SLS = SLS.REG_SLS
     -- WHEN NOT MATCHED THEN
     -- INSERT (sku_num, itm_cd, store_cd, Wk_Beg_Dt, TOT_SLS, REG_SLS)
     -- VALUES(SLS.SKU_NUM, SLS.ITM_CD, SLS.STORE_CODE, SLS.WK_BEG_DT ,SLS.TOT_SLS, SLS.REG_SLS);


       --11/28/2012 - change join  SUBSTR(SLS.SKU_NUM,1,6)
       MERGE INTO   htprog.RIA_stage_sku S
       USING ( SELECT /*+ ALL_ROWS FULL(SLS)PARALLEL(SLS,3) */ SLS.STORE_CODE,SLS.SKU_NUM
          , sd.ITM_CD AS ITM_CD , SLS.WK_BEG_DT
          , TOT_QTY AS TOT_SLS, Reg_Qty AS REG_SLS
          FROM  BASE_GERS_SLS SLS
         left  JOIN
          sku_desc sd on sd.sku_num = SLS.SKU_NUM
            WHERE SLS.wk_beg_dt = wk_rec.wk_beg_dt
         ) SLS
        ON (S.SKU_NUM = SLS.SKU_NUM AND S.STORE_CD = SLS.STORE_CODE AND S.WK_BEG_DT = SLS.WK_BEG_DT)
        WHEN MATCHED THEN
        UPDATE SET TOT_SLS = SLS.TOT_SLS , REG_SLS = SLS.REG_SLS
        WHEN NOT MATCHED THEN
        INSERT (sku_num, itm_cd, store_cd, Wk_Beg_Dt, TOT_SLS, REG_SLS)
        VALUES(SLS.SKU_NUM, SLS.ITM_CD, SLS.STORE_CODE, SLS.WK_BEG_DT ,SLS.TOT_SLS, SLS.REG_SLS);
    COMMIT;

   END LOOP;
---Get SLS STD Data from RIA_Stage_Sku
/*
 CREATE MATERIALIZED VIEW htprog.MVW_STAGE_SKU
  REFRESH ON DEMAND
  AS SELECT  ALL_ROWS D.DIV_CD, D.DEPT_CD, D.CLASS_CD, D.SUBCLASS_CD,D.SKU_NUM, D.ITM_CD, F.STORE_CD, t.riaweek AS WEEK_NBR
   ,TOT_SLS, REG_SLS, TOT_EI, L.LST_RCV_DT AS SKU_LST_RCV_DT, D.LST_RCV_DT AS  ITM_LST_RCV_DT
  FROM htprog.RIA_stage_sku F
  JOIN htprog.sku_desc D  ON F.SKU_NUM = D.SKU_NUM
  JOIN ria_cal_mapping T ON F.wk_beg_dt = T.wk_beg_dt
  LEFT OUTER JOIN htprog.SKULSTDT L ON F.STORE_CD = L.STORE_CD and F.SKU_NUM = L.SKU_NUM;
*/
-- INSERT /*+ APPEND */ INTO htprog.RIA_stage_sku (div_cd, dept_cd, class_cd, subclass_cd, sku_num, itm_cd, store_cd, Wk_Beg_Dt, tot_ei, SKU_LST_RCV_DT, ITM_LST_RCV_DT)
--   (
--    SELECT D.DIV_CD, D.DEPT_CD, D.CLASS_CD, D.SUBCLASS_CD,B.SKU_NUM, D.ITM_CD, B.STORE_CODE AS STORE_CD, B.WK_BEG_DT
--    ,B.TOT_QTY AS EI, L.LST_RCV_DT AS SKU_LST_RCV_DT, D.LST_RCV_DT AS  ITM_LST_RCV_DT
--    FROM  BASE_GERS_EI B
--    LEFT OUTER JOIN htprog.SKULSTDT L ON b.STORE_CODE = L.STORE_CD and B.SKU_NUM = L.STORE_CD
--    JOIN  htprog.sku_desc D  ON b.SKU_NUM = D.SKU_NUM
--    );
--    commit;
--
--   --Merge or insert Sales data in RIA_Stage_SKU
--MERGE INTO   htprog.RIA_stage_sku S
--USING (  SELECT D.DIV_CD, D.DEPT_CD, D.CLASS_CD, D.SUBCLASS_CD,B.SKU_NUM, D.ITM_CD, B.STORE_CODE AS STORE_CD, B.WK_BEG_DT
--    ,TOT_QTY AS TOT_SLS, Reg_Qty AS REG_SLS, L.LST_RCV_DT AS SKU_LST_RCV_DT, D.LST_RCV_DT AS  ITM_LST_RCV_DT
--    FROM  BASE_GERS_SLS B
--    LEFT OUTER JOIN htprog.SKULSTDT L ON b.STORE_CODE = L.STORE_CD and B.SKU_NUM = L.STORE_CD
--    JOIN  htprog.sku_desc D  ON b.SKU_NUM = D.SKU_NUM) SLS
--  ON (S.SKU_NUM = SLS.SKU_NUM AND S.STORE_CD = SLS.STORE_CD AND S.WK_BEG_DT = SLS.WK_BEG_DT)
--  WHEN MATCHED THEN
--  UPDATE SET TOT_SLS = SLS.TOT_SLS , REG_SLS = SLS.REG_SLS
--  WHEN NOT MATCHED THEN
--  INSERT (div_cd, dept_cd, class_cd, subclass_cd, sku_num, itm_cd, store_cd, Wk_Beg_Dt, TOT_SLS, REG_SLS, SKU_LST_RCV_DT, ITM_LST_RCV_DT)
--  VALUES(SLS.DIV_CD, SLS.DEPT_CD, SLS.CLASS_CD, SLS.SUBCLASS_CD,SLS.SKU_NUM, SLS.ITM_CD, SLS.STORE_CD, SLS.WK_BEG_DT
--    ,SLS.TOT_SLS, SLS.REG_SLS, SLS.SKU_LST_RCV_DT, SLS.ITM_LST_RCV_DT)
--
/*
 INSERT + APPEND  INTO htprog.RIA_stage_sku (div_cd, dept_cd, class_cd, subclass_cd, sku_num, itm_cd, store_cd, Wk_Beg_Dt, tot_sls, reg_sls, tot_ei, SKU_LST_RCV_DT, ITM_LST_RCV_DT)
  (
  SELECT + ALL_ROWS ORDERED D.DIV_CD, D.DEPT_CD, D.CLASS_CD, D.SUBCLASS_CD,B.SKU_NUM, D.ITM_CD, B.STORE_CODE AS STORE_CD, B.WK_BEG_DT
   ,TOT_SLS, REG_SLS, EI, L.LST_RCV_DT AS SKU_LST_RCV_DT, D.LST_RCV_DT AS  ITM_LST_RCV_DT
    FROM
    (
    SELECT + ALL_ROWS FULL(OH) FULL(SLS) PARALLEL(OH,3)  NVL(OH.STORE_CODE,SLS.STORE_CODE) AS STORE_CODE,NVL(OH.SKU_NUM,SLS.SKU_NUM) AS SKU_NUM, NVL(OH.WK_BEG_DT,SLS.WK_BEG_DT) AS WK_BEG_DT
    ,OH.TOT_QTY AS EI,SLS.TOT_QTY AS TOT_SLS, SLS.Reg_Qty AS REG_SLS
    FROM BASE_GERS_EI OH
    FULL OUTER JOIN BASE_GERS_SLS SLS ON OH.STORE_CODE = SLS.STORE_CODE AND OH.SKU_NUM = SLS.SKU_NUM AND OH.WK_BEG_DT = SLS.WK_BEG_DT
    ) B
    JOIN  htprog.sku_desc D  ON b.SKU_NUM = D.SKU_NUM
    LEFT OUTER JOIN htprog.SKULSTDT L ON b.STORE_CODE = L.STORE_CD and B.SKU_NUM = L.STORE_CD
);
*/

EXECUTE IMMEDIATE 'analyze table htprog.RIA_STAGE_SKU estimate statistics';

 LogSuccess('RIA_STAGE_SKU populated with Weekly Data');

End Populate_RIA_STAGE_Tables;




--PROCEDURE Populate_SLSSTD as
--begin
--
-----Get SLS STD Data from RIA_Stage_Sku
--EXECUTE IMMEDIATE 'Truncate table SLSSTD';
--
--Select Sku_Num, Store_Cd, TOT_SLS as Tot_Qty, (TOT_SLS - REG_SLS) as Md_qty
--from htprog.RIA_stage_sku F
-- JOIN htprog.ria_cal_mapping T ON F.wk_beg_dt = T.wk_beg_dt
--      WHERE T.riaweek BETWEEN 1 and 26
--
--
--
--end Populate_SLSSTD;

Procedure Populate_WTD_Summary_Tables as
Begin
EXECUTE IMMEDIATE 'TRUNCATE TABLE htprog.WTD_SKU';
EXECUTE IMMEDIATE 'TRUNCATE TABLE htprog.WTD_SUBCLASS';

---EXECUTE IMMEDIATE 'ALTER TABLE htprog.WTD_SKU DROP PRIMARY KEY CASCADE';
Drop_Constraint('WTD_SKU');

Drop_Indexes ('WTD_SKU');
Drop_Indexes ('WTD_SUBCLASS');




    INSERT /*+ APPEND*/  INTO htprog.WTD_SKU(DIV_CD,DEPT_CD,CLASS_CD,SUBCLASS_CD, sku_num, itm_cd, STORE_CD, WEEK_NBR
                                              ,TOT_WTD_SLS, REG_WTD_SLS ,TOT_STD_SLS, REG_STD_SLS , CURRENT_OH,SKU_LST_RCV_DT
                                              ,ITM_LST_RCV_DT,SIZE_CODE, PRC_STAT, WIP_OH)

--              SELECT /*+ ALL_ROWS*/ D.DIV_CD, D.DEPT_CD, D.CLASS_CD, D.SUBCLASS_CD,D.SKU_NUM, D.ITM_CD, E.STORE_CD, WEEK_NBR
--               ,Sum(TOT_SLS) as TOT_WTD_SLS , Sum(REG_SLS) as REG_WTD_SLS , Sum(TOT_STD_SLS) as TOT_STD_SLS, Sum(REG_STD_SLS) as REG_STD_SLS
--               ,Sum(TOT_EI) as CURRENT_OH, L.LST_RCV_DT AS SKU_LST_RCV_DT, D.LST_RCV_DT AS  ITM_LST_RCV_DT,D.SIZE_CD,D.PRC_STAT
--              FROM
--              (SELECT  /*+ ALL_ROWS*/  F.SKU_NUM, F.STORE_CD, t. riaweek AS WEEK_NBR,TOT_SLS , REG_SLS , 0 as TOT_STD_SLS, 0 as REG_STD_SLS,TOT_EI
--              FROM htprog.RIA_stage_sku F
--              JOIN htprog.ria_cal_mapping T ON F.wk_beg_dt = T.wk_beg_dt
--              WHERE T.riaweek = 0
--
--              UNION
--
--              SELECT /*+ ALL_ROWS*/ F.SKU_NUM, F.STORE_CD, 0 AS WEEK_NBR ,0 AS TOT_SLS, 0 AS REG_SLS ,Sum(TOT_SLS) as TOT_STD_SLS , Sum(REG_SLS) as REG_STD_SLS ,0 as TOT_EI
--              FROM htprog.RIA_stage_sku F
--              JOIN htprog.ria_cal_mapping T ON F.wk_beg_dt = T.wk_beg_dt
--              WHERE  T.riaweek BETWEEN 1 AND 26 and TOT_SLS is not null
--              group by  F.SKU_NUM, F.STORE_CD
--              ) E
--               JOIN htprog.sku_desc D  ON E.SKU_NUM = D.SKU_NUM
--              LEFT OUTER JOIN htprog.SKULSTDT L ON E.STORE_CD = L.STORE_CD and E.SKU_NUM = L.SKU_NUM
--              GROUP BY D.DIV_CD, D.DEPT_CD, D.CLASS_CD, D.SUBCLASS_CD,D.SKU_NUM, D.ITM_CD, E.STORE_CD, WEEK_NBR,L.LST_RCV_DT , D.LST_RCV_DT ,D.SIZE_CD,D.PRC_STAT;

     SELECT /*+ ALL_ROWS*/ D.DIV_CD, D.DEPT_CD, D.CLASS_CD, D.SUBCLASS_CD,D.SKU_NUM, D.ITM_CD, E.STORE_CD, WEEK_NBR
               ,Sum(TOT_SLS) as TOT_WTD_SLS , Sum(REG_SLS) as REG_WTD_SLS , Sum(TOT_STD_SLS) as TOT_STD_SLS, Sum(REG_STD_SLS) as REG_STD_SLS
               ,Sum(TOT_EI) as CURRENT_OH, L.LST_RCV_DT AS SKU_LST_RCV_DT, D.LST_RCV_DT AS  ITM_LST_RCV_DT,D.SIZE_CD,D.PRC_STAT, SUM(E.WIP_OH_QTY) AS WIP_OH
              FROM
              (SELECT  /*+ ALL_ROWS*/  F.SKU_NUM, F.STORE_CODE as STORE_CD, t. riaweek AS WEEK_NBR,0 as TOT_SLS , 0 as REG_SLS , 0 as TOT_STD_SLS, 0 as REG_STD_SLS,TOT_QTY as TOT_EI ,0 AS WIP_OH_QTY
              FROM htprog.BASE_GERS_EI F
              JOIN htprog.ria_cal_mapping T ON F.wk_beg_dt = T.wk_beg_dt
              WHERE T.riaweek = 0

              UNION

              SELECT  /*+ ALL_ROWS*/  F.SKU_NUM, F.STORE_CODE as STORE_CD, t. riaweek AS WEEK_NBR,TOT_QTY as TOT_SLS , REG_QTY as REG_SLS , 0 as TOT_STD_SLS, 0 as REG_STD_SLS,0 as TOT_EI, 0 AS WIP_OH_QTY
              FROM htprog.BASE_GERS_SLS F
              JOIN htprog.ria_cal_mapping T ON F.wk_beg_dt = T.wk_beg_dt
              WHERE T.riaweek = 0

              UNION

              SELECT /*+ ALL_ROWS*/ F.SKU_NUM, F.STORE_CD, 0 AS WEEK_NBR ,0 AS TOT_SLS, 0 AS REG_SLS ,(TOT_Qty) as TOT_STD_SLS , (REG_Qty) as REG_STD_SLS ,0 as TOT_EI,0 AS WIP_OH_QTY
              FROM htprog.SLSSTD_New F

              UNION
              ---WIP union
              SELECT  /*+ ALL_ROWS*/  F.SKU_NUM, F.STORE_NBR as STORE_CD, 0 AS WEEK_NBR,0 as TOT_SLS , 0 as REG_SLS , 0 as TOT_STD_SLS, 0 as REG_STD_SLS,0 as TOT_EI ,WIP_OH_QTY
              FROM
            --PACKS
               ( SELECT  A.ALLOC_NBR,A.STORE_NBR,P.ITM_NBR, P.PACK_ID,P.SKU_NUMBER as SKU_NUM, (A.WIP_OH_QTY * P.QTY_PER_PACK) AS WIP_OH_QTY
                FROM AAM.HT_WIP_STAGE A
                JOIN  AAM.HT_BASE_RMS_PACKS P  ON  A.ITM_NBR = P.PACK_ID
                JOIN AAM.HT_BASE_GERS_UDF B ON P.sku_number = B.SKU_NUM
                WHERE A.WIP_OH_QTY >= 0
                GROUP BY A.ALLOC_NBR,A.STORE_NBR,P.ITM_NBR,P.PACK_ID, P.SKU_NUMBER , A.WIP_OH_QTY, P.QTY_PER_PACK

                UNION
                ---NON- PACKS
                SELECT  A.ALLOC_NBR,A.STORE_NBR,A.ITM_NBR, NUll as PACK_ID,A.SKU_NUM , A.WIP_OH_QTY
                FROM AAM.HT_WIP_STAGE A
                JOIN AAM.HT_BASE_GERS_UDF B  ON A.ITM_NBR = B.ITM_CD AND A.SKU_NUM = B.SKU_NUM AND B.PACK_IND = 'N'
                WHERE A.WIP_OH_QTY >= 0 )F
               --  FROM AAM.HT_WIP_STAGE GROUP BY SKU_NUM,STORE_NBR)F
--              SELECT  /*+ ALL_ROWS*/  F.SKU_NUM, F.STORE_CD as STORE_CD, 0 AS WEEK_NBR,0 as TOT_SLS , 0 as REG_SLS , 0 as TOT_STD_SLS, 0 as REG_STD_SLS,0 as TOT_EI ,WIP_OH_QTY
--              FROM (SELECT SKU_NUM,STORE_NBR as STORE_CD, SUM(WIP_OH_QTY) AS WIP_OH_QTY
--                  --  FROM AAM.HT_WIP_STAGE GROUP BY SKU_NUM,STORE_NBR)F
--                       FROM AAM.HT_WIP_STAGE GROUP BY SKU_NUM,STORE_NBR)F

              ) E
               JOIN htprog.sku_desc D  ON E.SKU_NUM = D.SKU_NUM
              LEFT OUTER JOIN htprog.SKULSTDT L ON E.STORE_CD = L.STORE_CD and E.SKU_NUM = L.SKU_NUM
             -- LEFT OUTER JOIN (SELECT SKU_NUM,STORE_NBR as STORE_CD, SUM(WIP_OH_QTY) AS WIP_OH_QTY FROM AAM.HT_WIP_STAGE GROUP BY SKU_NUM,STORE_NBR) W ON E.SKU_NUM = W.SKU_NUM AND E.STORE_CD = W.STORE_CD
             -- WHERE   D.Div_CD <> 8
              GROUP BY D.DIV_CD, D.DEPT_CD, D.CLASS_CD, D.SUBCLASS_CD,D.SKU_NUM, D.ITM_CD, E.STORE_CD, WEEK_NBR,L.LST_RCV_DT , D.LST_RCV_DT ,D.SIZE_CD,D.PRC_STAT;
     COMMIT;

      INSERT /*+ APPEND */ INTO htprog.WTD_SUBCLASS (div_cd, dept_cd, class_cd, subclass_cd, size_cd, store_cd, week_nbr, prc_stat, tot_wtd_sls, reg_wtd_sls,tot_std_sls,reg_std_sls,current_oh, itm_lst_rcv_dt, sku_lst_rcv_dt, WIP_OH)
      (
          SELECT /*+ ALL_ROWS*/ div_cd, dept_cd, class_cd, subclass_cd, size_code, store_cd, week_nbr, PRC_STAT, SUM(tot_wtd_sls), SUM(reg_wtd_sls), SUM(tot_std_sls), SUM(reg_std_sls), Sum(current_oh),MAX(itm_lst_rcv_dt), MAX(sku_lst_rcv_dt), SUM(WIP_OH)
            FROM htprog.WTD_SKU A
        GROUP BY div_cd,dept_cd, class_cd,subclass_cd,size_code, store_cd, week_nbr, PRC_STAT
      );
       COMMIT;

        LogSuccess('WTD Summary tables populated with WTD Data');

-- EXECUTE IMMEDIATE  'CREATE UNIQUE INDEX htprog.PK_WTD_SKU ON htprog.WTD_SKU
--(DIV_CD, DEPT_CD, CLASS_CD, SKU_NUM, STORE_CD,
--WEEK_NBR)
--LOGGING
--TABLESPACE REPORT_INDEX2
--PCTFREE    5
--INITRANS   8
--MAXTRANS   255
--STORAGE    (
--            INITIAL          16M
--            NEXT             16M
--            MINEXTENTS       1
--            MAXEXTENTS       UNLIMITED
--            PCTINCREASE      0
--            FREELISTS        8
--            FREELIST GROUPS  2
--            BUFFER_POOL      DEFAULT
--           )
--NOPARALLEL';
--
--
--EXECUTE IMMEDIATE  'CREATE BITMAP INDEX htprog.BM_WTD_SKU_ITMCD ON htprog.WTD_SKU
--(ITM_CD)
--LOGGING
--TABLESPACE REPORT_INDEX2
--PCTFREE    5
--INITRANS   2
--MAXTRANS   255
--STORAGE    (
--            INITIAL          512K
--            NEXT             16M
--            MINEXTENTS       1
--            MAXEXTENTS       UNLIMITED
--            PCTINCREASE      0
--            FREELISTS        4
--            FREELIST GROUPS  2
--            BUFFER_POOL      DEFAULT
--           )
--NOPARALLEL';
--
--
--EXECUTE IMMEDIATE  'ALTER TABLE htprog.WTD_SKU ADD (
--  CONSTRAINT PK_WTD_SKU
-- PRIMARY KEY
-- (DIV_CD, DEPT_CD, CLASS_CD, SKU_NUM, STORE_CD, WEEK_NBR)
--    USING INDEX
--    TABLESPACE REPORT_INDEX2
--    PCTFREE    5
--    INITRANS   8
--    MAXTRANS   255
--    STORAGE    (
--                INITIAL          16M
--                NEXT             16M
--                MINEXTENTS       1
--                MAXEXTENTS       UNLIMITED
--                PCTINCREASE      0
--                FREELISTS        8
--                FREELIST GROUPS  2
--               ))';
--
-- EXECUTE IMMEDIATE  'CREATE UNIQUE INDEX htprog.WTD_SUBCLASS_INDEX ON htprog.WTD_SUBCLASS
--(DIV_CD, DEPT_CD, CLASS_CD, SUBCLASS_CD, SIZE_CD,
--STORE_CD, WEEK_NBR, PRC_STAT)
--LOGGING
--TABLESPACE REPORT_INDEX2
--PCTFREE    5
--INITRANS   8
--MAXTRANS   255
--STORAGE    (
--            INITIAL          16M
--            NEXT             16M
--            MINEXTENTS       1
--            MAXEXTENTS       UNLIMITED
--            PCTINCREASE      0
--            FREELISTS        8
--            FREELIST GROUPS  2
--            BUFFER_POOL      DEFAULT
--           )
--NOPARALLEL';
--
--
--EXECUTE IMMEDIATE  'CREATE BITMAP INDEX htprog.BM_WTD_SUBCLASS_DIV ON htprog.WTD_SUBCLASS
--(DIV_CD)
--LOGGING
--TABLESPACE REPORT_INDEX2
--PCTFREE    5
--INITRANS   2
--MAXTRANS   255
--STORAGE    (
--            INITIAL          512K
--            NEXT             16M
--            MINEXTENTS       1
--            MAXEXTENTS       UNLIMITED
--            PCTINCREASE      0
--            FREELISTS        4
--            FREELIST GROUPS  2
--            BUFFER_POOL      DEFAULT
--           )
--NOPARALLEL';
--
--
--EXECUTE IMMEDIATE  'CREATE BITMAP INDEX htprog.BM_WTD_SUBCLASS_DEPT ON htprog.WTD_SUBCLASS
--(DEPT_CD)
--LOGGING
--TABLESPACE REPORT_INDEX2
--PCTFREE    5
--INITRANS   2
--MAXTRANS   255
--STORAGE    (
--            INITIAL          512K
--            NEXT             16M
--            MINEXTENTS       1
--            MAXEXTENTS       UNLIMITED
--            PCTINCREASE      0
--            FREELISTS        4
--            FREELIST GROUPS  2
--            BUFFER_POOL      DEFAULT
--           )
--NOPARALLEL';
--
--
--EXECUTE IMMEDIATE  'CREATE BITMAP INDEX htprog.BM_WTD_SUBCLASS_CLASS ON htprog.WTD_SUBCLASS
--(CLASS_CD)
--LOGGING
--TABLESPACE REPORT_INDEX2
--PCTFREE    5
--INITRANS   2
--MAXTRANS   255
--STORAGE    (
--            INITIAL          512K
--            NEXT             16M
--            MINEXTENTS       1
--            MAXEXTENTS       UNLIMITED
--            PCTINCREASE      0
--            FREELISTS        4
--            FREELIST GROUPS  2
--            BUFFER_POOL      DEFAULT
--           )
--NOPARALLEL';
--
--
--EXECUTE IMMEDIATE  'CREATE BITMAP INDEX htprog.BM_WTD_SUBCLASS_SUBCLASS ON htprog.WTD_SUBCLASS
--(SUBCLASS_CD)
--LOGGING
--TABLESPACE REPORT_INDEX2
--PCTFREE    5
--INITRANS   2
--MAXTRANS   255
--STORAGE    (
--            INITIAL          512K
--            NEXT             16M
--            MINEXTENTS       1
--            MAXEXTENTS       UNLIMITED
--            PCTINCREASE      0
--            FREELISTS        4
--            FREELIST GROUPS  2
--            BUFFER_POOL      DEFAULT
--           )
--NOPARALLEL';
--
-- LogSuccess('WTD Summary Tables indexing Successfully Proccessed');

End  Populate_WTD_Summary_Tables;

Procedure Populate_SLSSTD is
begin

EXECUTE IMMEDIATE 'TRUNCATE TABLE htprog.SLSSTD_New';

INSERT INTO htprog.SLSSTD_New(sku_num,store_cd, tot_qty,reg_qty)
(
 SELECT /*+ ALL_ROWS*/ F.SKU_NUM, F.STORE_CD,Sum(TOT_SLS) as TOT_STD_SLS , Sum(REG_SLS) as REG_STD_SLS
              FROM htprog.RIA_stage_sku F
              JOIN htprog.ria_cal_mapping T ON F.wk_beg_dt = T.wk_beg_dt
              WHERE  T.riaweek BETWEEN 1 AND 26 and TOT_SLS is not null
              group by  F.SKU_NUM, F.STORE_CD
 );
 COMMIT;

EXECUTE IMMEDIATE 'analyze table htprog.SLSSTD_New estimate statistics';

 LogSuccess('SLSSTD Successfully Proccessed');

End Populate_SLSSTD;


Procedure Weekly_Maintenance as

v_CyWkDt date;
v_LyWkDt date;

Begin

SELECT  wk_beg_dt into v_CyWkDt FROM  htprog.ria_cal_mapping WHERE riaweek = 27;
SELECT  wk_beg_dt -7 into v_LyWkDt  FROM  htprog.ria_cal_mapping WHERE riaweek = 64;

/* Remove week 27 after updating Ria_Cal_Mapping Table */
DELETE  htprog.RIA_stage_sku WHERE wk_beg_dt = v_CyWkDt ;
COMMIT;
/* Remove week 65 after updating Ria_Cal_Mapping Table */
DELETE  htprog.RIA_stage_sku WHERE wk_beg_dt = v_LyWkDt ;
COMMIT;

LogSuccess('Weekly Maintenance Completed');

END  Weekly_Maintenance;


Procedure Process_Weekly is

Begin

    --- Analyze table statistics
    EXECUTE IMMEDIATE 'analyze table htprog.SKULSTDT estimate statistics' ;
    EXECUTE IMMEDIATE 'analyze table htprog.SKU_DESC estimate statistics' ;
    EXECUTE IMMEDIATE 'analyze table htprog.BASE_GERS_SLS estimate statistics' ;
    EXECUTE IMMEDIATE 'analyze table htprog.BASE_GERS_EI estimate statistics' ;
  --  EXECUTE IMMEDIATE 'analyze table htprog.BASE_GERS_PRCSTAT estimate statistics' ;


  -- Build the RIA week mapping table
    Populate_RIA_Calendar;

    --Delete unrequired weeks from Stage on Sunday run
    Weekly_Maintenance;

    --Load last 7 weeks and week 64 of LY
    Populate_RIA_STAGE_Tables;

    --Populate Stage_sku and Stage_subclass Summary tables
    Populate_RIA_Summary_Tables;
      -- Analyze table
--     EXECUTE IMMEDIATE 'analyze table htprog.STAGE_SKU estimate statistics sample 3 percent' ;
--     EXECUTE IMMEDIATE 'analyze table htprog.STAGE_SUBCLASS estimate statistics' ;
--     EXECUTE IMMEDIATE 'analyze index htprog.PK_STAGE_SKU estimate statistics sample 3 percent' ;
--     EXECUTE IMMEDIATE 'analyze index htprog.BM_STAGE_SKU_ITMCD estimate statistics sample 3 percent' ;
--     EXECUTE IMMEDIATE 'ANALYZE INDEX htprog.STAGE_SUBCLASS_INDEX estimate statistics sample 3 percent' ;
--     EXECUTE IMMEDIATE 'ANALYZE INDEX htprog.BM_STAGE_SUBCLASS_DIV estimate statistics sample 3 percent' ;
--     EXECUTE IMMEDIATE 'ANALYZE INDEX htprog.BM_STAGE_SUBCLASS_DEPT estimate statistics sample 3 percent' ;
--     EXECUTE IMMEDIATE 'ANALYZE INDEX htprog.BM_STAGE_SUBCLASS_CLASS estimate statistics sample 3 percent' ;
--     EXECUTE IMMEDIATE 'ANALYZE INDEX htprog.BM_STAGE_SUBCLASS_SUBCLASS estimate statistics sample 3 percent' ;

    --Populate SLSSTD_New Table
    Populate_SLSSTD;

End Process_Weekly;



Procedure Process_WTD is


Begin

    --Analyze table statistics
    --EXECUTE IMMEDIATE 'analyze table htprog.SLSSTD_New estimate statistics' ;
    --EXECUTE IMMEDIATE 'analyze table htprog.SKU_DESC estimate statistics' ;
    EXECUTE IMMEDIATE 'analyze table htprog.BASE_GERS_SLS estimate statistics' ;
    EXECUTE IMMEDIATE 'analyze table htprog.BASE_GERS_EI estimate statistics' ;

    --Populate WTD_Sku and WTD_Subclass Summary tables
     Populate_WTD_Summary_Tables;

     -- Analyze table
--     EXECUTE IMMEDIATE 'analyze table htprog.WTD_SKU estimate statistics sample 3 percent' ;
--     EXECUTE IMMEDIATE 'analyze table htprog.WTD_SUBCLASS estimate statistics sample 3 percent' ;
--     EXECUTE IMMEDIATE 'analyze index htprog.BM_WTD_SUBCLASS_DIV estimate statistics sample 3 percent' ;
--     EXECUTE IMMEDIATE 'analyze index htprog.BM_WTD_SUBCLASS_DEPT estimate statistics sample 3 percent' ;
--     EXECUTE IMMEDIATE 'analyze index htprog.BM_WTD_SUBCLASS_CLASS estimate statistics sample 3 percent' ;
--     EXECUTE IMMEDIATE 'analyze index htprog.BM_WTD_SUBCLASS_SUBCLASS estimate statistics sample 3 percent' ;

End Process_WTD;

END RIA_BASE_TO_STAGE;
/
