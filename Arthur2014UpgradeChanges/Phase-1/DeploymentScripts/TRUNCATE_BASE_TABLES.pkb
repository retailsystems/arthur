create or replace
PACKAGE  BODY                                AAM.TRUNCATE_BASE_TABLES IS
/******************************************************************************
   NAME:       TRUNCATE_BASE_TABLES
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        4/8/2014      DKaurRai       1. Created this package.
******************************************************************************/


  PROCEDURE HT_BASE_WMS_BACKSTOCK as 
  
  BEGIN 
  
  Execute Immediate 'Truncate table HT_BASE_WMS_BACKSTOCK Drop storage';  
  
 END  HT_BASE_WMS_BACKSTOCK ;
  
 

  PROCEDURE   HT_BASE_WMS_SHPD_WAVED as 
  
  BEGIN 
  
  Execute Immediate 'Truncate table  HT_BASE_WMS_SHPD_WAVED Drop storage';  
  
  end HT_BASE_WMS_SHPD_WAVED  ;
  

PROCEDURE   HT_BASE_RMS_PACKS as 
  
  BEGIN 
  
  Execute Immediate 'Truncate table  HT_BASE_RMS_PACKS Drop storage';  
  
  end HT_BASE_RMS_PACKS  ;
  

PROCEDURE   HT_BASE_GERS_UDF as 
  
  BEGIN 
  
  Execute Immediate 'Truncate table  HT_BASE_GERS_UDF Drop storage';  
  
  end HT_BASE_GERS_UDF  ;  
  

 PROCEDURE  FORMAT_HOLD_GOOD_ONES as 
 Begin
 
  Execute Immediate 'Truncate table FORMAT_HOLD_GOOD_ONES Drop storage';  
 
 END FORMAT_HOLD_GOOD_ONES;
  


 PROCEDURE  FORMATMULILEVEL_HOLD_GOOD_ONES as 
 Begin
 
  Execute Immediate 'Truncate table FORMATMULILEVEL_HOLD_GOOD_ONES Drop storage';  
 
 END FORMATMULILEVEL_HOLD_GOOD_ONES;
  

  
 PROCEDURE  FORMAT as 
 Begin
 
  Execute Immediate 'Truncate table FORMAT Drop storage';  
 
 END FORMAT;
 
 
PROCEDURE   FORMATMULILEVEL as 
 Begin
 
  Execute Immediate 'Truncate table  FORMATMULILEVEL Drop storage';  
 
 END FORMATMULILEVEL;


PROCEDURE GRADE as 
Begin 

Execute Immediate 'Truncate table  GRADE Drop storage';  

END GRADE;


PROCEDURE HT_UPDATE_MODELS_STAGE as 
Begin 

Execute Immediate 'Truncate table  HT_UPDATE_MODELS_STAGE Drop storage';  

END HT_UPDATE_MODELS_STAGE;


PROCEDURE C$_LS_CLASS as 
Begin 

Execute Immediate 'Truncate table  C$_LS_CLASS Drop storage';  

END C$_LS_CLASS;

PROCEDURE C$_LS_DEPT as 
Begin 

Execute Immediate 'Truncate table  C$_LS_DEPT Drop storage';  

END C$_LS_DEPT;

PROCEDURE C$_LS_DIV as 
Begin 

Execute Immediate 'Truncate table  C$_LS_DIV Drop storage';  

END C$_LS_DIV;

PROCEDURE C$_LS_ITEM as 
Begin 

Execute Immediate 'Truncate table  C$_LS_ITEM Drop storage';  

END C$_LS_ITEM;

PROCEDURE C$_LS_SIZE as 
Begin 

Execute Immediate 'Truncate table  C$_LS_SIZE Drop storage';  

END C$_LS_SIZE;

PROCEDURE C$_LS_SUBCLASS as 
Begin 

Execute Immediate 'Truncate table C$_LS_SUBCLASS Drop storage';  

END C$_LS_SUBCLASS;


PROCEDURE GATHERTBL_STATS_FORMAT AS
Begin

   dbms_stats.gather_table_stats('AAM','FORMAT'); 

END GATHERTBL_STATS_FORMAT;

PROCEDURE GATHERTBL_STATS_FORMATMULILVL AS
Begin

dbms_stats.gather_table_stats('AAM','FORMATMULILEVEL'); 

END GATHERTBL_STATS_FORMATMULILVL;

END TRUNCATE_BASE_TABLES;
/

