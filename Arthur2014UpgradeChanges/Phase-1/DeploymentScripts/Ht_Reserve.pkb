create or replace
PACKAGE BODY     AAM.Ht_Reserve IS
-- from production
PROCEDURE PROC_WORKLIST_RESERVE_BATCH (
  p_warehouse_nbr VARCHAR2
  , p_biztalkfile_path VARCHAR2
  ,  p_dept_str VARCHAR2 DEFAULT ''
  , p_rec_cnt OUT NUMBER
  , p_batch_id OUT NUMBER
  )
 IS

/*-- --------------------------------------------------------------------------------------
-- Name         : PROC_WORKLIST_RESERVE_BATCH
-- Author       : Sri Bajjuri
-- Description  : Get Backstock Approved allocations with status code of 30 (approved).
--                If there are any allocations, the procedure will get the Details lines,
--                 and FTP that data to BizTalk
-- Requirements :
-- Ammedments   :
--   When         Who           What
--   ===========  ========      =================================================
--   17-MAR-2004  Sri Bajjuri   Initial Creation
--   24-MAR-2004  Sri Bajjuri   Pass 0 as PO_NBR for all backstock allocations
--   13-MAY-2004  Sri Bajjuri   Get the WHSE # from HT_DC_XREF table
--   06-Jun-2004  Sri Bajjuri    Hard code the DIV_NBR to '001'
--     11/24/2004   Sri Bajjuri   IF alloc work list lines belong to multiple whse create seperate files
--                               per each WHSE.
--   11/24/2004   Sri Bajjuri    Changed to move most of the elements from Alloc Hdr to Alloc Dtl
--   11/29/2004   Sri Bajjuri   Allocation files will be created for only passed in WHSE #
--   25-DEC-2004  Sri Bajjuri   A batch ID is generated and stored for each reserve batch run
--   18-JUL-2005  Sri Bajjuri   Loading from temp_alloc_history table is not required for
                                backstock allocations.
--   10-DEC-2007  Hogan Lei     Modify to process during "Release" instead of "Approve"
--   10-Dec-2008  HG            Added new fields :udf30,udf29', and udf72 to udf99
--   28-JUL-2009  Karl Zeutzius Changed file name creation prefix for BT06 upgrade
--   10-AUG-2011  Hogan Lei     Modify to pull DEPT_NBR from worklist table.
--03 - Oct -2012 Added prepack changes, Removed logic for WL_Bulk and WL_Multilple
-- 08 NOV 2012 Drai Fixed the issue of packs showing as Itm_nbr in worklist
---15-July-2013 DRai Put a flag for Update status date when status change to descrepancy by backstock nightly job
-- 08-APR-2014 Vijay Naidu replaced UTL_FILE.PUT_LINE with UTL_FILE.PUT and also added CHR(13), CHR(10) as a newline delimiter and added UTL_FILE.FFLUSH(v_file_handle);
-- ----------------------------------------------------------------------------------------------------------------------------------*/


    v_errornum NUMBER;
    v_errormess CHAR(70);

    v_file_name VARCHAR2(50) := NULL;
    v_file_handle UTL_FILE.FILE_TYPE;

    v_alloc_nbr NUMBER := 0;
    v_batch_id NUMBER := 0;
    v_company_cd CHAR(3) := '001';
v_whse_nbr VARCHAR2(4) := '';
    v_sequence_nbr NUMBER := 0;
    v_po_alloc_seq_nbr NUMBER;

  v_dept_filter VARCHAR2(5000);

    v_rec_cnt NUMBER := 0;
    v_gers_whse HT_DC_XREF.GERS_WHSE%TYPE ;
    --const_file_dir varchar2(40) := '\\biztalkdev\allocation';

     -- Temp_Approved_Alloc --
    CURSOR approved_alloc_cursor IS
        SELECT
         ALLOC_NBR, WL_KEY, PO_NBR, ASN,
         rec_nbr, COST_PRICE, RETAIL_PRICE, DIV_NBR,
         merch_id_type, PO_LN_SEQ_NUM, LOCATION_ID,
         RESULT_QTY, SKU_NBR, BATCH_SEQ_ID, WAREHOUSE_NBR,PTL_PRIORITY, DEPT_NBR, STYLE
         FROM TEMP_APPROVED_ALLOCS WHERE PROCESSED = 0 AND PO_NBR = 0
         ORDER BY ALLOC_NBR;
    approved_alloc_record approved_alloc_cursor%ROWTYPE;

BEGIN

  v_gers_whse := Gers_Whse_Nbr_Lookup(p_warehouse_nbr);

  -- insert backstock approved allocations into temp table --
   IF LENGTH(p_dept_str) > 0 THEN
        INSERT INTO TEMP_APPROVED_ALLOCS(
                ALLOC_NBR,WL_KEY,    PO_NBR,
                ASN,REC_NBR,COST_PRICE,
                RETAIL_PRICE,    DIV_NBR,
                MERCH_ID_TYPE,PO_LN_SEQ_NUM,
                LOCATION_ID,    RESULT_QTY,SKU_NBR,  WAREHOUSE_NBR
                ,PTL_PRIORITY--, SIZE_NBR
                ,DEPT_NBR, STYLE
            )
        SELECT DISTINCT
            W.ALLOC_NBR, W.WL_KEY, 0, NVL(W.ASN,0),
            W.rec_nbr, W.COST_PRICE, W.RETAIL_PRICE, '001' AS DIV_NBR,
            W.merch_id_type, WB.PO_LN_SEQ_NUM,
            RD.LOCATION_ID,
            CASE WHEN WB.PACK_ID = WB.SKU_NBR AND WB.QTY_PER_PACK > 1 THEN RD.RESULT_QTY* WB.QTY_PER_PACK ELSE  RD.RESULT_QTY END AS RESULT_QTY ,
             ---WB.SKU_NBR,
             CASE WHEN WB.PACK_ID <> WB.SKU_NBR THEN WB.PACK_ID ELSE WB.SKU_NBR END AS SKU_NBR,
              H.WMS_WHSE, L.PTL_PRIORITY,
           -- WB.SIZE_NBR,
            W.DEPT_NBR,
             CASE WHEN WB.PACK_ID <> WB.SKU_NBR THEN WB.PACK_ID ELSE W.ITM_NBR END AS STYLE
        FROM
            WORKLIST W, WL_PACK WB, RESULTS_DETAIL RD, LOCATIONS L, HT_DC_XREF H
        WHERE
            NOT EXISTS (SELECT * FROM TEMP_APPROVED_ALLOCS T WHERE T.ALLOC_NBR=W.ALLOC_NBR AND T.WL_KEY=W.WL_KEY ) AND
            W.WL_KEY = WB.WL_KEY AND
            WB.WL_KEY = RD.WL_KEY AND
           --- WB.SIZE_NBR = RD.LEVEL1_DESC AND
           WB.PACK_ID = SUBSTR(RD.unique_mer_key, INSTR(RD.unique_mer_key, '.') +1 , LENGTH(RD.unique_mer_key)) AND
            W.ALLOC_NBR = RD.ALLOCATION_NBR AND
            RD.LOCATION_ID = L.LOCATION_ID AND
            W.SOURCE_DC = H.GERS_WHSE AND
            L.WAREHOUSE_FLAG <> 'Y' AND
            W.status_code = 40 AND
            W.exported IS NULL AND
            W.trouble_code IS NULL AND
            W.document_type = 'RESERVE' AND
            W.SOURCE_DC = v_gers_whse AND
           LPAD(W.DEPT_NBR,4,'0') IN ( SELECT *  FROM THE
                                   ( SELECT CAST( Str2tbl( p_dept_str ) AS tbl_DeptNbr)
                                   FROM dual ) );
    ELSE
        INSERT INTO TEMP_APPROVED_ALLOCS(
                ALLOC_NBR,WL_KEY,    PO_NBR,
                ASN,REC_NBR,COST_PRICE,
                RETAIL_PRICE,    DIV_NBR,
                MERCH_ID_TYPE, PO_LN_SEQ_NUM,
                LOCATION_ID,    RESULT_QTY,
               SKU_NBR, WAREHOUSE_NBR ,PTL_PRIORITY,-- SIZE_NBR
               DEPT_NBR, STYLE
            )
        SELECT DISTINCT
            W.ALLOC_NBR, W.WL_KEY, 0, NVL(W.ASN,0),
            W.rec_nbr, W.COST_PRICE, W.RETAIL_PRICE, '001' AS DIV_NBR,
            W.merch_id_type, WB.PO_LN_SEQ_NUM,
            RD.LOCATION_ID,
            CASE WHEN WB.PACK_ID = WB.SKU_NBR AND WB.QTY_PER_PACK > 1 THEN RD.RESULT_QTY* WB.QTY_PER_PACK ELSE  RD.RESULT_QTY END AS RESULT_QTY,
            CASE WHEN WB.PACK_ID <> WB.SKU_NBR THEN WB.PACK_ID ELSE WB.SKU_NBR END AS SKU_NBR,-- WB.SKU_NBR,
             H.WMS_WHSE, L.PTL_PRIORITY ,-- WB.SIZE_NBR,
             W.DEPT_NBR, CASE WHEN WB.PACK_ID <> WB.SKU_NBR THEN WB.PACK_ID ELSE W.ITM_NBR END AS STYLE
        FROM
            WORKLIST W, WL_PACK WB, RESULTS_DETAIL RD, LOCATIONS L, HT_DC_XREF H
        WHERE
            NOT EXISTS (SELECT * FROM TEMP_APPROVED_ALLOCS T WHERE T.ALLOC_NBR=W.ALLOC_NBR AND T.WL_KEY=W.WL_KEY) AND
            W.WL_KEY = WB.WL_KEY AND
            WB.WL_KEY = RD.WL_KEY AND
           -- WB.SIZE_NBR = RD.LEVEL1_DESC AND
           WB.PACK_ID = SUBSTR(RD.unique_mer_key, INSTR(RD.unique_mer_key, '.') +1 , LENGTH(RD.unique_mer_key)) AND
            W.ALLOC_NBR = RD.ALLOCATION_NBR AND
            RD.LOCATION_ID = L.LOCATION_ID AND
            W.SOURCE_DC = H.GERS_WHSE AND
            L.WAREHOUSE_FLAG <> 'Y' AND
            W.status_code = 40 AND
            W.exported IS NULL AND
            W.trouble_code IS NULL AND
            W.document_type = 'RESERVE' AND
      W.SOURCE_DC = v_gers_whse ;

    END IF;

 COMMIT;

    -- Generate the txt file --
    OPEN approved_alloc_cursor;
    FETCH approved_alloc_cursor INTO approved_alloc_record;

    WHILE approved_alloc_cursor%FOUND LOOP
       v_rec_cnt := v_rec_cnt + 1;
        IF (approved_alloc_record.ALLOC_NBR <> v_alloc_nbr  OR approved_alloc_record.WAREHOUSE_NBR <>  v_whse_nbr) THEN
            IF v_file_name IS NOT NULL THEN
                UTL_FILE.FCLOSE(v_file_handle);
            ELSE
               SELECT HT_RESERVE_BATCH_SEQ.NEXTVAL INTO v_batch_id FROM dual;
            END IF;


            -- CREATE NEW FILE --
            v_alloc_nbr := approved_alloc_record.ALLOC_NBR;
            v_whse_nbr := approved_alloc_record.WAREHOUSE_NBR;
            --v_file_name := 'rbatch_'||v_whse_nbr || '_' || v_alloc_nbr || '_' || TO_CHAR(SYSDATE,'MMDDYY_HH24MISSSS') || '.txt';  -- KJZ 7-28-09 BT06 upgrade
            v_file_name := 'alloc_'||v_whse_nbr || '_' || v_alloc_nbr || '_' || TO_CHAR(SYSDATE,'MMDDYY_HH24MISSSS') || '.txt';     -- KJZ 7-28-09 BT06 upgrade
            v_file_handle := UTL_FILE.FOPEN(p_biztalkfile_path,v_file_name,'w',32767);

            v_sequence_nbr := 1;
            -- insert a record in HT_RESERVE_BATCH table
        PROC_UPDATE_HT_RESERVE_BATCH(v_batch_id,v_alloc_nbr,approved_alloc_record.WAREHOUSE_NBR,0,0,NULL);

            /*
            -- OUTPUT THE HEADER LINE --
            UTL_FILE.PUT_LINE(v_file_handle, 'ALLOC_HDR:'||
                approved_alloc_record.ALLOC_NBR ||'|'||
                approved_alloc_record.WL_KEY ||'|'||
                approved_alloc_record.PO_NBR ||'|'||
                approved_alloc_record.ASN ||'|'||
                approved_alloc_record.REC_NBR||'|'||
                approved_alloc_record.COST_PRICE||'|'||
                approved_alloc_record.RETAIL_PRICE||'|'||
                --LPAD(approved_alloc_record.DIV_NBR,3,'0')||'|'||
                '001'||'|'|| --hard code DIV_NBR to 001
                approved_alloc_record.MERCH_ID_TYPE||'|'||
                v_company_cd||'|'||
                approved_alloc_record.WAREHOUSE_NBR
                );
            */
            -- OUTPUT THE HEADER LINE --
            UTL_FILE.PUT(v_file_handle, 'ALLOC_HDR:'||
                    approved_alloc_record.ALLOC_NBR ||'|'||
                    approved_alloc_record.WAREHOUSE_NBR||'|'||
                  v_batch_id||CHR(13)||CHR(10)
                );
            UTL_FILE.FFLUSH(v_file_handle);
        END IF;
            /*
            -- OUTPUT THE DETAIL LINE --
            UTL_FILE.PUT_LINE(v_file_handle, 'ALLOC_DTL:' ||
                    approved_alloc_record.PO_LN_SEQ_NUM ||'|'||
                    v_sequence_nbr||'|'||
                    approved_alloc_record.LOCATION_ID||'|'||
                    approved_alloc_record.PTL_PRIORITY||'|'||
                    approved_alloc_record.RESULT_QTY||'|'||
                    approved_alloc_record.SKU_NBR);
            --UTL_FILE.FFLUSH(v_file_handle);
            */

            -- Added by Hogan Mar-30-2007 for GERS 9.2 upgrade
           SELECT po_alloc_seq.NEXTVAL INTO v_po_alloc_seq_nbr  FROM DUAL;

            UTL_FILE.PUT(v_file_handle, 'ALLOC_DTL:' ||
                    approved_alloc_record.WL_KEY ||'|'||
                    approved_alloc_record.PO_NBR ||'|'||
                    approved_alloc_record.PO_LN_SEQ_NUM ||'|'||
                    v_po_alloc_seq_nbr ||'|'||
                    approved_alloc_record.ASN ||'|'||
                    approved_alloc_record.REC_NBR||'|'||
                    approved_alloc_record.COST_PRICE||'|'||
                    approved_alloc_record.RETAIL_PRICE||'|'||
                    TRIM(TO_CHAR(TO_NUMBER(approved_alloc_record.DIV_NBR, '999'),'009'))||'|'||
                    approved_alloc_record.MERCH_ID_TYPE||'|'||
                    v_company_cd||'|'||
                    v_sequence_nbr||'|'||
                    approved_alloc_record.LOCATION_ID||'|'||
                    approved_alloc_record.PTL_PRIORITY||'|'||
                    approved_alloc_record.RESULT_QTY||'|'||
                    approved_alloc_record.SKU_NBR||'|'||
                    approved_alloc_record.DEPT_NBR||'|'||
                    approved_alloc_record.STYLE||CHR(13)||CHR(10));
					
					UTL_FILE.FFLUSH(v_file_handle);
					
            -- Update the record as "processed"
            UPDATE TEMP_APPROVED_ALLOCS SET PROCESSED = 1
               WHERE ALLOC_NBR=approved_alloc_record.ALLOC_NBR
               AND WL_KEY=approved_alloc_record.WL_KEY
               AND LOCATION_ID = approved_alloc_record.LOCATION_ID
               AND SKU_NBR=approved_alloc_record.SKU_NBR ;

         v_sequence_nbr := v_sequence_nbr + 1;

        FETCH approved_alloc_cursor INTO approved_alloc_record;
    END LOOP;

    CLOSE approved_alloc_cursor;


    UTL_FILE.FCLOSE(v_file_handle);
  p_rec_cnt := v_rec_cnt;
  p_batch_id := v_batch_id;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_errornum := -20001;
            v_errormess := SQLERRM;
            RAISE_APPLICATION_ERROR(v_errornum,v_errormess);
        WHEN UTL_FILE.INVALID_MODE THEN
            v_errornum := -20002;
            v_errormess := SQLERRM;
            RAISE_APPLICATION_ERROR(v_errornum,v_errormess);
        WHEN UTL_FILE.INVALID_PATH THEN
            v_errornum := -20003;
            v_errormess := SQLERRM;
            RAISE_APPLICATION_ERROR(v_errornum,v_errormess);
        WHEN UTL_FILE.INVALID_FILEHANDLE THEN
            v_errornum := -20004;
            v_errormess := SQLERRM;
            RAISE_APPLICATION_ERROR(v_errornum,v_errormess);
        WHEN UTL_FILE.INVALID_OPERATION THEN
            v_errornum := -20005;
            v_errormess := SQLERRM;
            RAISE_APPLICATION_ERROR(v_errornum,v_errormess);
        WHEN UTL_FILE.READ_ERROR THEN
            v_errornum := -20006;
            v_errormess := SQLERRM;
            RAISE_APPLICATION_ERROR(v_errornum,v_errormess);
        WHEN UTL_FILE.WRITE_ERROR THEN
            v_errornum := -20007;
            v_errormess := SQLERRM;
            RAISE_APPLICATION_ERROR(v_errornum,v_errormess);
        WHEN UTL_FILE.INTERNAL_ERROR THEN
            v_errornum := -20008;
            v_errormess := SQLERRM;
            RAISE_APPLICATION_ERROR(v_errornum,v_errormess);
        WHEN OTHERS THEN
            v_errornum := -20009;
            v_errormess := SQLERRM;
            RAISE_APPLICATION_ERROR(v_errornum,v_errormess);

END PROC_WORKLIST_RESERVE_BATCH;

--PROCEDURE PROC_WORKLIST_RESERVE_UPDATE
--  (
--        p_warehouse_nbr     VARCHAR2
--        ,p_po_nbr            NUMBER
--        ,p_avail_qty        NUMBER
--        ,p_on_order_balance    NUMBER
--        ,p_itm_nbr            VARCHAR2
--        ,p_vendor_nbr        VARCHAR2
--        ,p_div_nbr            VARCHAR2
--        ,p_dept_nbr            VARCHAR2
--        ,p_class_nbr        VARCHAR2
--        ,p_subclass_nbr        VARCHAR2
--        ,p_itm_desc1        VARCHAR2
--        ,p_itm_desc2        VARCHAR2
--        ,p_uom_cd            VARCHAR2
--        ,p_merch_id_type    NUMBER
--        ,p_uom_qty            NUMBER
--        ,p_retail_price        NUMBER
--        ,p_cost_price        NUMBER
--        ,p_vendor_desc        VARCHAR2
--        ,p_div_desc            VARCHAR2
--        ,p_dept_desc        VARCHAR2
--        ,p_class_desc        VARCHAR2
--        ,p_subclass_desc    VARCHAR2
--        ,p_color_nbr        VARCHAR2
--        ,p_color_desc        VARCHAR2
--        ,p_document_status    VARCHAR2
--        ,p_sku_nbr            VARCHAR2
--        ,p_size_nbr            VARCHAR2
--        ,p_size_of_multiple    VARCHAR2
--        ,p_sku_seq_num     NUMBER
--        ,p_sendmail_flag VARCHAR2
--        ,p_udf1 VARCHAR2
--        ,p_udf2 VARCHAR2
--        ,p_udf3 VARCHAR2
--        ,p_udf4 VARCHAR2
--        ,p_udf5 NUMBER
--        ,p_udf6 NUMBER
--        ,p_udf29                           VARCHAR2
--        ,p_udf30                           VARCHAR2
--        ,p_udf72                        VARCHAR2
--        ,p_udf73                         VARCHAR2
--        ,p_udf74                         VARCHAR2
--        ,p_udf75                          VARCHAR2
--        ,p_udf76                         VARCHAR2
--        ,p_udf77                         VARCHAR2
--        ,p_udf78                          VARCHAR2
--        ,p_udf79                          VARCHAR2
--        ,p_udf80                          VARCHAR2
--        ,p_udf81                          VARCHAR2
--        ,p_udf82                           VARCHAR2
--        ,p_udf83                           VARCHAR2
--        ,p_udf84                           VARCHAR2
--        ,p_udf85                           VARCHAR2
--        ,p_udf86                           VARCHAR2
--        ,p_udf87                           VARCHAR2
--        ,p_udf88                           VARCHAR2
--        ,p_udf89                            VARCHAR2
--        ,p_udf90                            VARCHAR2
--        ,p_udf91                            VARCHAR2
--        ,p_udf92                              VARCHAR2
--        ,p_udf93                             VARCHAR2
--        ,p_udf94                             VARCHAR2
--        ,p_udf95                             VARCHAR2
--        ,p_udf96                             VARCHAR2
--        ,p_udf97                            VARCHAR2
--        ,p_udf98                            VARCHAR2
--        ,p_udf99                             VARCHAR2
--    )
--    IS
--/*-- --------------------------------------------------------------------------
---- Name         : PROC_WORKLIST_RESERVE_UPDATE
---- Author       : Sri Bajjuri
---- Description  : Add/Update WORKLIST and WL_MULTIPLE/WL_BULK records based on backstock inventory from WMS.
---- Requirements :
---- Ammedments   :
----   When         Who       What
----   ===========  ========  =================================================
----   27-FEB-2004  Sri Bajjuri  Initial Creation
----   13-MAY-2004  Sri Bajjuri   Get the WHSE # from HT_DC_XREF table
----   03-JUN-2004  Sri Bajjuri  p_sku_seq_num is mapped to Wl_BULK,WL_MULTIPLE po_ln_seq_num
----     11-09-2004      Sri Bajjjuri    xml special character handling code added
----   20-JAN-2006  Sri Bajjuri   Passing div_nbr to function GET_WHSENBR_FROM_SOURCE_DC
----   02-MAY-2006  Sri Bajjuri  For Backstock worklist lines map WAREHOUSE_NBR = SOURCE_DC, don't call
----                              function GET_WHSENBR_FROM_SOURCE_DC
---- --------------------------------------------------------------------------*/
--
--    -- for WORKLIST
--    v_wl_key NUMBER;
--    v_wl_item_key NUMBER := 0;
--    v_alloc_nbr NUMBER := 0;
--    v_status_code NUMBER := 10; --default for new WORKLIST
--    v_status_description VARCHAR2(50) := 'Available'; --default for new WORKLIST
--    v_rec_nbr NUMBER := 0;
--    v_document_type VARCHAR2(50) := 'RESERVE';
--    v_trouble_code CHAR(1);
--    v_trouble_description VARCHAR2(50);
--    v_old_merch_id_type NUMBER;
--
--    v_old_order_qty NUMBER := 0;
--    v_new_order_qty NUMBER := 0;
--
--    v_Total_Avail_QTY NUMBER;
--
--    v_po_ln_seq_num NUMBER := 0;
--
--  v_email_sent BOOLEAN := FALSE;
--  v_avail_qty NUMBER := 0;
--  v_gers_whse HT_DC_XREF.GERS_WHSE%TYPE ;
--  v_source_dc WORKLIST.SOURCE_DC%TYPE;
--    CURSOR cr_total_avail_qty_multiple(temp_wl_key IN NUMBER) IS
--        SELECT SUM(wm.QTY_AVAIL) AS TOTAL_AVAIL_QTY FROM  WL_MULTIPLE wm WHERE wm.WL_KEY = temp_wl_key;
--
--    CURSOR cr_total_avail_qty_bulk(temp_wl_key IN NUMBER) IS
--        SELECT SUM(wm.QTY_AVAIL) AS TOTAL_AVAIL_QTY FROM  WL_BULK wm WHERE wm.WL_KEY = temp_wl_key;
--
--    BEGIN
--        -- GET the GERS WHSE#
--
--        BEGIN
--        v_source_dc := Gers_Whse_Nbr_Lookup(p_warehouse_nbr);
--        --v_gers_whse := GET_WHSENBR_FROM_SOURCE_DC(v_source_dc,p_div_nbr);
--        -- 05/02/2006
--        v_gers_whse := v_source_dc;
--
--        EXCEPTION
--            WHEN NO_DATA_FOUND THEN
--                RAISE_APPLICATION_ERROR(-20100,'Unable to find WHSE number in HT_DC_XREF');
--        END;
--        --look for existing WL_KEY
--        BEGIN
--            --look in WORKLIST
--            SELECT W.WL_KEY
--            INTO v_wl_key
--            FROM WORKLIST W
--            WHERE W.DOCUMENT_TYPE = 'RESERVE'
--                AND WAREHOUSE_NBR =  v_gers_whse
--                AND ITM_NBR = p_itm_nbr
--                AND STATUS_CODE = 10
--                AND COLOR_NBR = p_color_nbr;
--
--        EXCEPTION
--            WHEN NO_DATA_FOUND THEN
--                BEGIN
--                                v_wl_key := 0;
--
--                END;
--        END;
--
--        IF p_merch_id_type = 1 THEN
--            v_avail_qty := p_avail_qty ;
--        ELSIF p_merch_id_type = 2 THEN
--            v_avail_qty := FLOOR(p_avail_qty/p_size_of_multiple) * p_size_of_multiple;
--        END IF;
--
--            --create new WORKLIST
--            IF v_wl_key = 0 THEN
--                --get next WL_KEY
--                SELECT WORKLISTSEQ.NEXTVAL
--                INTO v_wl_key
--                FROM DUAL;
--
--                INSERT INTO WORKLIST(
--                    ALLOC_NBR --has default
--                    ,WL_KEY
--                    ,STATUS_CODE --has default
--                    ,STATUS_DESCRIPTION --has default
--                    ,CREATE_DATE
--                    ,MERCH_ID_TYPE
--                    ,AVAIL_QTY
--                    ,WAREHOUSE_NBR
--                    ,REC_NBR
--                    ,PO_NBR
--                    ,ORDER_QTY
--                    ,ON_ORDER_BALANCE
--                    ,ITM_NBR
--                    ,ITM_DESC1
--                    ,ITM_DESC2
--                    ,UOM_CD
--                    ,UOM_QTY
--                    ,VENDOR_NBR
--                    ,VENDOR_DESC
--                    ,PO_CANCEL_DATE
--                    ,DIV_NBR
--                    ,DIV_DESC
--                    ,DEPT_NBR
--                    ,DEPT_DESC
--                    ,CLASS_NBR
--                    ,CLASS_DESC
--                    ,SUBCLASS_NBR
--                    ,SUBCLASS_DESC
--                    ,COLOR_NBR
--                    ,COLOR_DESC
--                    ,RETAIL_PRICE
--                    ,COST_PRICE
--                    ,DOCUMENT_TYPE
--                    ,DOCUMENT_STATUS
--                    ,UDF1
--                    ,UDF2
--                    ,UDF3
--                    ,UDF4
--                    ,UDF5
--                    ,UDF6
--                    ,SOURCE_DC
--                    ,BTS4  --new field added 05/01/2007
--                    -- new fields added 12/12/2008
--                    ,UDF29
--                    ,UDF30
--                    ,UDF72
--                    ,UDF73
--                    ,UDF74
--                    ,UDF75
--                    ,UDF76
--                    ,UDF77
--                    ,UDF78
--                    ,UDF79
--                    ,UDF80
--                    ,UDF81
--                    ,UDF82
--                    ,UDF83
--                    ,UDF84
--                    ,UDF85
--                    ,UDF86
--                    ,UDF87
--                    ,UDF88
--                    ,UDF89
--                    ,UDF90
--                    ,UDF91
--                    ,UDF92
--                    ,UDF93
--                    ,UDF94
--                    ,UDF95
--                    ,UDF96
--                    ,UDF97
--                    ,UDF98
--                    ,UDF99
--                    )
--                VALUES (v_alloc_nbr --has default
--                    ,v_wl_key
--                    ,v_status_code --has default
--                    ,v_status_description --has default
--                    ,SYSDATE
--                    ,p_merch_id_type
--                    ,v_avail_qty
--                    ,v_gers_whse
--                    ,v_rec_nbr
--                    ,p_po_nbr
--                    ,v_avail_qty
--                    ,v_avail_qty
--                    ,p_itm_nbr
--                    ,ConvertXmlSpecialChar(p_itm_desc1)
--                    ,ConvertXmlSpecialChar(p_itm_desc2)
--                    ,p_uom_cd
--                    ,p_uom_qty
--                    ,p_vendor_nbr
--                    ,p_vendor_desc
--                    ,SYSDATE
--                    ,p_div_nbr
--                    ,p_div_desc
--                    ,p_dept_nbr
--                    ,p_dept_desc
--                    ,p_class_nbr
--                    ,p_class_desc
--                    ,p_subclass_nbr
--                    ,p_subclass_desc
--                    ,p_color_nbr
--                    ,p_color_desc
--                    ,p_retail_price
--                    ,p_cost_price
--                    ,v_document_type
--                    ,p_document_status
--                    ,ConvertXmlSpecialChar(p_udf1)
--                    ,ConvertXmlSpecialChar(p_udf2)
--                    ,ConvertXmlSpecialChar(p_udf3)
--                    ,ConvertXmlSpecialChar(p_udf4)
--                    ,p_udf5
--                    ,p_udf6
--                    ,v_source_dc
--                    ,p_div_nbr  --new field added 05/01/2007
--                    ,ConvertXmlSpecialChar(p_udf29)    --new field added 09/09/2008
--                    ,ConvertXmlSpecialChar(p_udf30)  --new field added 09/09/2008
--                    ,ConvertXmlSpecialChar(p_udf72)  --new field added 09/09/2008
--                    ,ConvertXmlSpecialChar(p_udf73)  --new field added 09/09/2008
--                    ,ConvertXmlSpecialChar(p_udf74)  --new field added 09/09/2008
--                    ,ConvertXmlSpecialChar(p_udf75) --new field added 09/09/2008
--                    ,ConvertXmlSpecialChar(p_udf76) --new field added 09/09/2008
--                    ,ConvertXmlSpecialChar(p_udf77) --new field added 09/09/2008
--                    ,ConvertXmlSpecialChar(p_udf78) --new field added 09/09/2008
--                    ,ConvertXmlSpecialChar(p_udf79) --new field added 09/09/2008
--                    ,ConvertXmlSpecialChar(p_udf80) --new field added 09/09/2008
--                    ,ConvertXmlSpecialChar(p_udf81) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf82) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf83) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf84) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf85) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf86) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf87) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf88) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf89) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf90) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf91) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf92) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf93) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf94) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf95) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf96) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf97) --new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf98)--new field added 12/12/2008
--                    ,ConvertXmlSpecialChar(p_udf99) --new field added 12/12/2008
--                    );
--            ELSE
--               UPDATE WORKLIST
--               SET ALLOC_NBR = v_alloc_nbr
--                    ,STATUS_CODE = v_status_code
--                    ,STATUS_DESCRIPTION = v_status_description
--                    ,CREATE_DATE = SYSDATE
--                    ,MERCH_ID_TYPE = p_merch_id_type
--                    --,WAREHOUSE_NBR = v_gers_whse
--                    ,REC_NBR = v_rec_nbr
--                    ,PO_NBR = p_po_nbr
--                    ,UOM_CD = p_uom_cd
--                    ,UOM_QTY = p_uom_qty
--                    ,VENDOR_NBR = p_vendor_nbr
--                    ,VENDOR_DESC = p_vendor_desc
--                    ,DIV_NBR = p_div_nbr
--                    ,DIV_DESC = p_div_desc
--                    ,DEPT_NBR = p_dept_nbr
--                    ,DEPT_DESC = p_dept_desc
--                    ,CLASS_NBR = p_class_nbr
--                    ,CLASS_DESC = p_class_desc
--                    ,SUBCLASS_NBR = p_subclass_nbr
--                    ,SUBCLASS_DESC = p_subclass_desc
--                    ,COLOR_NBR = p_color_nbr
--                    ,COLOR_DESC = p_color_desc
--                    ,RETAIL_PRICE = p_retail_price
--                    ,COST_PRICE        = p_cost_price
--                    ,DOCUMENT_TYPE = v_document_type
--                    ,DOCUMENT_STATUS = p_document_status
--                    ,UDF1 = ConvertXmlSpecialChar(p_udf1)
--                    ,UDF2 = ConvertXmlSpecialChar(p_udf2)
--                    ,UDF3 = ConvertXmlSpecialChar(p_udf3)
--                    ,UDF4 = ConvertXmlSpecialChar(p_udf4)
--                    ,UDF5 = p_udf5
--                    ,UDF6 = p_udf6
--                    ,BTS4 = p_div_nbr  --new field added 05/01/2007
--                    -- new fields added 12/12/2008
--                    ,UDF29 = ConvertXmlSpecialChar(p_udf29)
--                    ,UDF30 =ConvertXmlSpecialChar(p_udf30)
--                    ,UDF72 =ConvertXmlSpecialChar(p_udf72)
--                    ,UDF73 =ConvertXmlSpecialChar(p_udf73)
--                    ,UDF74 =ConvertXmlSpecialChar(p_udf74)
--                    ,UDF75 =ConvertXmlSpecialChar(p_udf75)
--                    ,UDF76 =ConvertXmlSpecialChar(p_udf76)
--                    ,UDF77 =ConvertXmlSpecialChar(p_udf77)
--                    ,UDF78 =ConvertXmlSpecialChar(p_udf78)
--                    ,UDF79 =ConvertXmlSpecialChar(p_udf79)
--                    ,UDF80 =ConvertXmlSpecialChar(p_udf80)
--                    ,UDF81 =ConvertXmlSpecialChar(p_udf81)
--                    ,UDF82 =ConvertXmlSpecialChar(p_udf82)
--                    ,UDF83 =ConvertXmlSpecialChar(p_udf83)
--                    ,UDF84 =ConvertXmlSpecialChar(p_udf84)
--                    ,UDF85 =ConvertXmlSpecialChar(p_udf85)
--                    ,UDF86 =ConvertXmlSpecialChar(p_udf86)
--                    ,UDF87 =ConvertXmlSpecialChar(p_udf87)
--                    ,UDF88 =ConvertXmlSpecialChar(p_udf88)
--                    ,UDF89 =ConvertXmlSpecialChar(p_udf89)
--                    ,UDF90 =ConvertXmlSpecialChar(p_udf90)
--                    ,UDF91 =ConvertXmlSpecialChar(p_udf91)
--                    ,UDF92 =ConvertXmlSpecialChar(p_udf92)
--                    ,UDF93 =ConvertXmlSpecialChar(p_udf93)
--                    ,UDF94 =ConvertXmlSpecialChar(p_udf94)
--                    ,UDF95 =ConvertXmlSpecialChar(p_udf95)
--                    ,UDF96 =ConvertXmlSpecialChar(p_udf96)
--                    ,UDF97 =ConvertXmlSpecialChar(p_udf97)
--                    ,UDF98 =ConvertXmlSpecialChar(p_udf98)
--                    ,UDF99 =ConvertXmlSpecialChar(p_udf99)
--                WHERE WL_KEY = v_wl_key;
--
--            END IF;
--
--            IF p_merch_id_type = 1 THEN
--
--             BEGIN
--               --look in WL_BULK
--               SELECT WL_KEY
--               INTO v_wl_item_key
--               FROM WL_BULK
--                 WHERE WL_KEY = v_wl_key
--                    AND SKU_NBR =  p_sku_nbr
--                    AND COLOR_NBR = p_color_nbr
--                    AND SIZE_NBR = p_size_nbr;
--
--            EXCEPTION
--               WHEN NO_DATA_FOUND THEN
--                  BEGIN
--                    v_wl_item_key := 0;
--                  END;
--                END;
--                  IF v_wl_item_key = 0 THEN
--                   --create WL_BULK
--                    INSERT INTO WL_BULK(
--                         WL_KEY
--                         ,SKU_NBR
--                         ,COLOR_NBR
--                         ,SIZE_NBR
--                         ,QTY_AVAIL
--                         ,PO_LN_SEQ_NUM
--                         ,ORDER_QTY)
--                      VALUES (v_wl_key
--                         ,p_sku_nbr
--                         ,p_color_nbr
--                         ,p_size_nbr
--                         ,p_avail_qty
--                         ,p_sku_seq_num
--                         ,v_avail_qty);
--                     ELSE
--                       UPDATE WL_BULK
--                         SET  QTY_AVAIL = v_avail_qty
--                         ,PO_LN_SEQ_NUM = p_sku_seq_num
--                         , ORDER_QTY = p_avail_qty
--                         WHERE WL_KEY = v_wl_key
--                     AND SKU_NBR =  p_sku_nbr
--                     AND COLOR_NBR = p_color_nbr
--                     AND SIZE_NBR = p_size_nbr;
--                END IF;
--
--          OPEN cr_total_avail_qty_bulk(v_wl_key);
--                    FETCH cr_total_avail_qty_bulk INTO v_Total_Avail_QTY;
--
--                    IF v_Total_Avail_QTY IS NULL THEN
--                        v_Total_Avail_QTY := 0;
--                    END IF;
--
--                    CLOSE cr_total_avail_qty_bulk;
--            ELSIF p_merch_id_type = 2 THEN
--             BEGIN
--               -- WL_MULTIPLE
--               SELECT WL_KEY
--               INTO v_wl_item_key
--               FROM WL_MULTIPLE
--                 WHERE WL_KEY = v_wl_key
--                    AND SKU_NBR =  p_sku_nbr
--                    AND COLOR_NBR = p_color_nbr
--                    AND SIZE_NBR = p_size_nbr;
--
--            EXCEPTION
--               WHEN NO_DATA_FOUND THEN
--                  BEGIN
--                    v_wl_item_key := 0;
--                  END;
--                END;
--                  IF v_wl_item_key = 0 THEN
--                    --create WL_MULTIPLE
--                    INSERT INTO WL_MULTIPLE(
--                         WL_KEY
--                         ,SKU_NBR
--                         ,COLOR_NBR
--                         ,SIZE_NBR
--                         ,QTY_AVAIL
--                         ,SIZE_OF_MULTIPLE
--                         ,PO_LN_SEQ_NUM
--                         ,ORDER_QTY)
--                     VALUES (v_wl_key
--                         ,p_sku_nbr
--                         ,p_color_nbr
--                         ,p_size_nbr
--                         ,v_avail_qty
--                         ,p_size_of_multiple
--                         ,p_sku_seq_num
--                         ,v_avail_qty);
--                     ELSE
--                       UPDATE WL_MULTIPLE
--                         SET  QTY_AVAIL = v_avail_qty
--                         ,PO_LN_SEQ_NUM = p_sku_seq_num
--                         ,SIZE_OF_MULTIPLE = p_size_of_multiple
--                         , ORDER_QTY = v_avail_qty
--                         WHERE WL_KEY = v_wl_key
--                     AND SKU_NBR =  p_sku_nbr
--                     AND COLOR_NBR = p_color_nbr
--                     AND SIZE_NBR = p_size_nbr;
--                  END IF;
--
--                  OPEN cr_total_avail_qty_multiple(v_wl_key);
--                    FETCH cr_total_avail_qty_multiple INTO v_Total_Avail_QTY;
--
--                    IF v_Total_Avail_QTY IS NULL THEN
--                        v_Total_Avail_QTY := 0;
--                    END IF;
--
--                    CLOSE cr_total_avail_qty_multiple;
--            END IF;
--
--            UPDATE WORKLIST SET ORDER_QTY = v_Total_Avail_QTY, ON_ORDER_BALANCE = v_Total_Avail_QTY, AVAIL_QTY = v_Total_Avail_QTY WHERE WL_KEY = v_wl_key;
--            /*
--            IF p_sendmail_flag = 'TRUE' THEN
--               BEGIN
--                 -- send email to ReserveBatch@hottopic.com
--                 SEND_MAIL('ReserveBatch@hottopic.com','Reserve Update','Reserve update completed');
--                 v_email_sent := TRUE;
--                 EXCEPTION
--              WHEN OTHERS THEN
--                    -- Do nothing, ignore email faiures
--                    v_email_sent := FALSE;
--                   --RAISE_APPLICATION_ERROR(-20002,'ERROR IN EMAIL => '||SQLCODE||':'||SQLERRM);
--               END;
--            END IF;
--            */
--    EXCEPTION
--      WHEN OTHERS THEN
--           RAISE_APPLICATION_ERROR(-20100,'ERROR IN RESERVE UPDATE => '||SQLCODE||':'||SQLERRM);
--
--    END PROC_WORKLIST_RESERVE_UPDATE;

---Removed functionality Drai 9/27/2012
--    PROCEDURE PROC_WORKLIST_CLEAR_BY_DEPT
--       (
--       p_warehouse_nbr VARCHAR2
--       ,p_dept_str VARCHAR2 DEFAULT ''
--       )
--    IS
--    -- --------------------------------------------------------------------------
---- Name         : PROC_WORKLIST_RESERVE_CLEAR
---- Author       : Tuong Nguyen
---- Description  : Clears unallocated reserve data by department.
---- Requirements :
---- Ammedments   :
----   When         Who       What
----   ===========  ========  =================================================
----   14-Jan-2008  Tuong Nguyen  Initial Creation
---- --------------------------------------------------------------------------
--   v_gers_whse HT_DC_XREF.GERS_WHSE%TYPE ;
--    v_commit_interval NUMBER := 5 ;
-- v_prev_commit_dt DATE;
--  BEGIN
--        -- Delete all backstock data with status_code = 10
--        v_gers_whse := Gers_Whse_Nbr_Lookup(p_warehouse_nbr);
--        --v_gers_whse := GET_WHSENBR_FROM_SOURCE_DC(v_gers_whse);
--        DELETE FROM WL_BULK W WHERE W.WL_KEY IN (SELECT WL_KEY FROM WORKLIST WHERE document_type = 'RESERVE' AND STATUS_CODE= 10 AND SOURCE_DC = v_gers_whse AND DEPT_NBR IN ( SELECT *  FROM THE ( SELECT CAST( Str2tbl( p_dept_str ) AS tbl_DeptNbr ) FROM dual )));
--        DELETE FROM WL_MULTIPLE W WHERE W.WL_KEY IN (SELECT WL_KEY FROM WORKLIST WHERE document_type = 'RESERVE' AND STATUS_CODE= 10 AND SOURCE_DC = v_gers_whse AND DEPT_NBR IN ( SELECT *  FROM THE ( SELECT CAST( Str2tbl( p_dept_str ) AS tbl_DeptNbr ) FROM dual )));
--        DELETE FROM WL_DETAILS W WHERE  W.WL_KEY IN (SELECT WL_KEY FROM WORKLIST WHERE document_type = 'RESERVE' AND  STATUS_CODE= 10  AND SOURCE_DC = v_gers_whse AND DEPT_NBR IN ( SELECT *  FROM THE ( SELECT CAST( Str2tbl( p_dept_str ) AS tbl_DeptNbr ) FROM dual )));
--        DELETE FROM WORKLIST W WHERE W.document_type = 'RESERVE' AND STATUS_CODE= 10  AND SOURCE_DC = v_gers_whse AND DEPT_NBR IN ( SELECT *  FROM THE ( SELECT CAST( Str2tbl( p_dept_str ) AS tbl_DeptNbr ) FROM dual ) );
--        COMMIT;
--         -- CALLing HT-Release
--         v_prev_commit_dt := SYSDATE;
--
--        DECLARE
--        CURSOR c_Wlst  IS
--          SELECT WL_KEY  FROM WORKLIST W
--            WHERE W.document_type = 'RESERVE'
--            AND EXPORTED='Exported' AND STATUS_CODE=40  AND SOURCE_DC = v_gers_whse
--            AND DEPT_NBR IN ( SELECT *  FROM THE ( SELECT CAST( Str2tbl( p_dept_str ) AS tbl_DeptNbr ) FROM dual ) );
--          c_Rec c_Wlst%ROWTYPE;
--         BEGIN
--          OPEN c_Wlst;
--          FETCH c_Wlst INTO c_Rec;
--          IF c_Wlst%FOUND THEN
--            LOOP
--                Ht_Release.Release_Worklistline('WL_KEY', c_Rec.WL_KEY);
--                 IF (ROUND(TO_NUMBER(SYSDATE - v_prev_commit_dt)*1440) >= v_commit_interval) THEN
--                    v_prev_commit_dt := SYSDATE;
--                    COMMIT;
--                 END IF;
--              FETCH c_Wlst INTO c_Rec;
--              EXIT WHEN c_Wlst%NOTFOUND;
--            END LOOP;
--          END IF;
--          CLOSE c_Wlst;
--          COMMIT;
--         END;
--
--  EXCEPTION
--      WHEN OTHERS THEN
--           RAISE_APPLICATION_ERROR(-20100,'ERROR IN RESERVE CLEAR => '||SQLCODE||':'||SQLERRM);
--END PROC_WORKLIST_CLEAR_BY_DEPT;

PROCEDURE PROC_WORKLIST_RESERVE_CLEAR

IS
    -- --------------------------------------------------------------------------
-- Name         : PROC_WORKLIST_RESERVE_CLEAR
-- Author       : Sri Bajjuri
-- Description  : Clears unallocated reserve data.
-- Requirements :
-- Ammedments   :
--   When         Who       What
--   ===========  ========  =================================================
--   27-FEB-2004  Sri Bajjuri  Initial Creation
--   27-SEP-2004  Sri Bajjui Multi DC enhancements adding new input parameter p_WMS_WhseNum
--  11-NOV-2004 Sri Bajjuri Using SOURCE_DC column to cleare WORKLIST lines by WHSE#
-- 11-25-2012  Davendar Rai Updated the logic to delete any worklist lines that are in RESERVE Location and in Available status
-- ----------------------------------------------------------------------------------------------------------------------------------------------
     --v_gers_whse HT_DC_XREF.GERS_WHSE%TYPE ;
     -- v_commit_interval NUMBER := 5 ;
     --v_prev_commit_dt DATE;
  BEGIN
        -- Delete all backstock data with status_code = 10
     --   v_gers_whse := Gers_Whse_Nbr_Lookup(p_warehouse_nbr);
        --v_gers_whse := GET_WHSENBR_FROM_SOURCE_DC(v_gers_whse);
        DELETE FROM WL_PACK W WHERE W.WL_KEY IN (SELECT WL_KEY FROM WORKLIST WHERE document_type = 'RESERVE' AND STATUS_CODE= 10 );
        DELETE FROM WL_DETAILS W WHERE  W.WL_KEY IN (SELECT WL_KEY FROM WORKLIST WHERE document_type = 'RESERVE' AND  STATUS_CODE= 10);
        DELETE FROM WORKLIST W WHERE W.document_type = 'RESERVE' AND STATUS_CODE= 10 ;
        COMMIT;
END PROC_WORKLIST_RESERVE_CLEAR;


---Removed functionality Drai 9/27/2012
--    PROCEDURE PROC_WORKLIST_RESERVE_CLEAR
--       (
--       p_warehouse_nbr VARCHAR2
--       )
--    IS
--    -- --------------------------------------------------------------------------
---- Name         : PROC_WORKLIST_RESERVE_CLEAR
---- Author       : Sri Bajjuri
---- Description  : Clears unallocated reserve data.
---- Requirements :
---- Ammedments   :
----   When         Who       What
----   ===========  ========  =================================================
----   27-FEB-2004  Sri Bajjuri  Initial Creation
----   27-SEP-2004  Sri Bajjui Multi DC enhancements adding new input parameter p_WMS_WhseNum
----    11-NOV-2004 Sri Bajjuri Using SOURCE_DC column to cleare WORKLIST lines by WHSE#
---- --------------------------------------------------------------------------
--   v_gers_whse HT_DC_XREF.GERS_WHSE%TYPE ;
--    v_commit_interval NUMBER := 5 ;
-- v_prev_commit_dt DATE;
--  BEGIN
--        -- Delete all backstock data with status_code = 10
--        v_gers_whse := Gers_Whse_Nbr_Lookup(p_warehouse_nbr);
--        --v_gers_whse := GET_WHSENBR_FROM_SOURCE_DC(v_gers_whse);
--        DELETE FROM WL_BULK W WHERE W.WL_KEY IN (SELECT WL_KEY FROM WORKLIST WHERE document_type = 'RESERVE' AND STATUS_CODE= 10 AND SOURCE_DC = v_gers_whse );
--        DELETE FROM WL_MULTIPLE W WHERE W.WL_KEY IN (SELECT WL_KEY FROM WORKLIST WHERE document_type = 'RESERVE' AND STATUS_CODE= 10 AND SOURCE_DC = v_gers_whse   );
--        DELETE FROM WL_DETAILS W WHERE  W.WL_KEY IN (SELECT WL_KEY FROM WORKLIST WHERE document_type = 'RESERVE' AND  STATUS_CODE= 10  AND SOURCE_DC = v_gers_whse  );
--        DELETE FROM WORKLIST W WHERE W.document_type = 'RESERVE' AND STATUS_CODE= 10  AND SOURCE_DC = v_gers_whse  ;
--        COMMIT;
--         -- CALLing HT-Release
--         v_prev_commit_dt := SYSDATE;
--
--        DECLARE
--        CURSOR c_Wlst  IS
--          SELECT WL_KEY  FROM WORKLIST W
--            WHERE W.document_type = 'RESERVE'
--            AND EXPORTED='Exported' AND STATUS_CODE=40  AND SOURCE_DC = v_gers_whse ;
--          c_Rec c_Wlst%ROWTYPE;
--         BEGIN
--          OPEN c_Wlst;
--          FETCH c_Wlst INTO c_Rec;
--          IF c_Wlst%FOUND THEN
--            LOOP
--                Ht_Release.Release_Worklistline('WL_KEY', c_Rec.WL_KEY);
--                 IF (ROUND(TO_NUMBER(SYSDATE - v_prev_commit_dt)*1440) >= v_commit_interval) THEN
--                    v_prev_commit_dt := SYSDATE;
--                    COMMIT;
--                 END IF;
--              FETCH c_Wlst INTO c_Rec;
--              EXIT WHEN c_Wlst%NOTFOUND;
--            END LOOP;
--          END IF;
--          CLOSE c_Wlst;
--          COMMIT;
--         END;
--
--  EXCEPTION
--      WHEN OTHERS THEN
--           RAISE_APPLICATION_ERROR(-20100,'ERROR IN RESERVE CLEAR => '||SQLCODE||':'||SQLERRM);
--
--    END PROC_WORKLIST_RESERVE_CLEAR;

PROCEDURE SEND_MAIL(p_mailto IN VARCHAR2,
p_subj IN VARCHAR2,
p_message IN VARCHAR2) IS

--PURPOSE: SEND p_message TO p_target VIA EMAIL.

v_eol VARCHAR2(2) := CHR(13)||CHR(10); -- EOL CHARACTERS
v_sender VARCHAR2(50) := 'Backstock@hottopic.com';
mailhost VARCHAR2(35) := 'filter';
mail_connection utl_smtp.connection;

BEGIN
-- ESTABLISH CONNECTION AND PERFORM HANDSHAKING
mail_connection := utl_smtp.open_connection(mailhost,25);
utl_smtp.helo(mail_connection,mailhost);
utl_smtp.mail(mail_connection,v_sender);
utl_smtp.rcpt(mail_connection,p_mailto);

-- BUILD THE MAIL MESSAGE AND SEND IT OUT
utl_smtp.DATA(mail_connection,'From:' || v_sender || v_eol ||
'To:'|| p_mailto || v_eol ||
'Subject:'||p_subj||v_eol||v_eol||p_message||v_eol);

-- SEVER THE CONNECTION
utl_smtp.quit(mail_connection);

EXCEPTION
WHEN OTHERS THEN
RAISE_APPLICATION_ERROR(-20002,'ERROR IN EMAIL => '||SQLCODE||':
'||SQLERRM);
END SEND_MAIL;

FUNCTION Gers_Whse_Nbr_Lookup
   (
       p_warehouse_nbr IN    VARCHAR2
     ) RETURN HT_DC_XREF.GERS_WHSE%TYPE
     IS
   v_gers_whse      HT_DC_XREF.GERS_WHSE%TYPE;
  -- GET the GERS WHSE#
        BEGIN
            SELECT GERS_WHSE
                INTO v_gers_whse FROM HT_DC_XREF
                WHERE WMS_WHSE = p_warehouse_nbr;

          RETURN v_gers_whse;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20100,'Unable to find WHSE number in HT_DC_XREF');
END Gers_Whse_Nbr_Lookup;

FUNCTION ConvertXmlSpecialChar
   (
       p_in_str IN    VARCHAR2
     ) RETURN VARCHAR2
     IS
  v_out_str VARCHAR2(255);

        BEGIN
            v_out_str := p_in_str;
            v_out_str := REPLACE(v_out_str, '&amp;', '&');
            v_out_str := REPLACE(v_out_str, '&lt;', '<');
            v_out_str := REPLACE(v_out_str, '&gt;', '>');
            v_out_str := REPLACE(v_out_str, '&quot;', '"');
            v_out_str := REPLACE(v_out_str, '&#39;', '''');
          RETURN v_out_str;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20100,'Error in ConvertXmlSpecialChar');
END ConvertXmlSpecialChar;
PROCEDURE PROC_UPDATE_HT_RESERVE_BATCH
  (
  p_batch_id HT_RESERVE_BATCH_HISTORY.BATCH_ID%TYPE
  ,p_alloc_nbr HT_RESERVE_BATCH_HISTORY.ALLOC_NBR%TYPE
  ,p_warehouse_nbr HT_RESERVE_BATCH_HISTORY.WAREHOUSE_NBR%TYPE
  ,p_export_flag HT_RESERVE_BATCH_HISTORY.EXPORT_FLAG%TYPE
  ,p_email_sent_flag HT_RESERVE_BATCH_HISTORY.EMAIL_SENT_FLAG%TYPE
  ,p_user_id HT_RESERVE_BATCH_HISTORY.USER_ID%TYPE
  )
IS
v_rec_cnt NUMBER := -1;
BEGIN
  UPDATE HT_RESERVE_BATCH_HISTORY
    SET EXPORT_FLAG = p_export_flag
    ,EMAIL_SENT_FLAG = p_email_sent_flag
    ,MOD_DATE_TIME = SYSDATE
    WHERE BATCH_ID = p_batch_id
    AND ALLOC_NBR = p_alloc_nbr;
  IF (SQL%ROWCOUNT = 0) THEN
    INSERT INTO HT_RESERVE_BATCH_HISTORY
      (
      BATCH_ID
      ,ALLOC_NBR
      ,WAREHOUSE_NBR
      ,USER_ID
      )
    VALUES
      (
      p_batch_id
      ,p_alloc_nbr
      ,p_warehouse_nbr
      ,p_user_id
      );
  ELSE
    COMMIT;
    SELECT COUNT(*) INTO v_rec_cnt FROM HT_RESERVE_BATCH_HISTORY
      WHERE BATCH_ID = p_batch_id AND EXPORT_FLAG = 0;
    IF v_rec_cnt = 0 THEN
        UPDATE HT_RESERVE_BATCH_HISTORY
          SET EMAIL_SENT_FLAG = 1
          WHERE BATCH_ID = p_batch_id;
        COMMIT;
        -- in production send e-mails to ReserveBatch@hottopic.com
        --SEND_MAIL('ITInterfaceGroup@hottopic.com', 'Back Stock Alert','Reserve batch ID # ' || p_batch_id ||' exported to Warehouse ' || p_warehouse_nbr ||' successfully');
		SEND_MAIL('ReserveBatch@hottopic.com', 'Back Stock Alert','Reserve batch ID # ' || p_batch_id ||' exported to Warehouse ' || p_warehouse_nbr ||' successfully');
    END IF;
  END IF;
 COMMIT;
END PROC_UPDATE_HT_RESERVE_BATCH;

PROCEDURE LIST_EXPORT_PENDING_ALLOCS
  (
  p_warehouse_nbr HT_RESERVE_BATCH_HISTORY.WAREHOUSE_NBR%TYPE
  ,p_cur_Allocs OUT T_CURSOR
  )
IS

BEGIN
  OPEN p_cur_Allocs FOR
  SELECT ALLOC_NBR,BATCH_ID FROM HT_RESERVE_BATCH_HISTORY
  WHERE EXPORT_FLAG = 0 AND WAREHOUSE_NBR = p_warehouse_nbr
  ORDER BY BATCH_ID, ALLOC_NBR;

END LIST_EXPORT_PENDING_ALLOCS;

PROCEDURE EXPORT_STATUS_BY_BATCH
  (
  p_batch_id HT_RESERVE_BATCH_HISTORY.BATCH_ID%TYPE
  ,p_cur_Allocs OUT T_CURSOR
  )
IS

BEGIN
  OPEN p_cur_Allocs FOR
  SELECT ALLOC_NBR,EXPORT_FLAG, DECODE(EXPORT_FLAG, 1, 'YES','PENDING') AS EXPORT_STATUS
  FROM HT_RESERVE_BATCH_HISTORY
  WHERE BATCH_ID = p_batch_id
  ORDER BY EXPORT_FLAG, ALLOC_NBR;

END EXPORT_STATUS_BY_BATCH;


PROCEDURE LIST_ALLOCS_BY_BATCH
  (
  p_batch_id HT_RESERVE_BATCH_HISTORY.BATCH_ID%TYPE
  ,p_cur_Allocs OUT T_CURSOR
  )
IS

BEGIN

---This Query is used by Backstock Console and has to be revisited/fixed DRai 9/27/2012
  OPEN p_cur_Allocs FOR
  SELECT
   W.DEPT_NBR,W.ALLOC_NBR,W.ITM_DESC1,WB.SKU_NBR, SUM(RD.RESULT_QTY ) AS QTY, W.RETAIL_PRICE
 ,W.UOM_QTY, (SUM(RD.RESULT_QTY ) * W.RETAIL_PRICE ) AS TOTAL_PRICE
  FROM
   AAM.WORKLIST W,  AAM.WL_PACK WB,  AAM.RESULTS_DETAIL RD
   ,  AAM.LOCATIONS L,  AAM.HT_DC_XREF H, AAM.HT_RESERVE_BATCH_HISTORY B
  WHERE
   W.WL_KEY = WB.WL_KEY AND
   WB.WL_KEY = RD.WL_KEY AND
  -- WB.SIZE_NBR = RD.LEVEL1_DESC AND
   W.ALLOC_NBR = RD.ALLOCATION_NBR AND
   RD.LOCATION_ID = L.LOCATION_ID AND
   W.SOURCE_DC = H.GERS_WHSE AND
   W.ALLOC_NBR = B.ALLOC_NBR AND
   L.WAREHOUSE_FLAG <> 'Y' AND
   W.status_code = 40 AND
   W.exported = 'Exported'AND
   W.document_type = 'RESERVE'
   AND B.BATCH_ID = p_batch_id
GROUP BY  W.DEPT_NBR,W.ALLOC_NBR,W.ITM_DESC1,WB.SKU_NBR,W.RETAIL_PRICE,W.UOM_QTY

---Removed Drai 9/27 Prepack Mod
--  UNION
--  SELECT
--   W.DEPT_NBR,W.ALLOC_NBR,W.ITM_DESC1, WM.SKU_NBR, SUM(RD.RESULT_QTY ) AS QTY, W.RETAIL_PRICE
-- ,W.UOM_QTY, (SUM(RD.RESULT_QTY ) * W.RETAIL_PRICE ) AS TOTAL_PRICE
--  FROM
--    AAM.WORKLIST W,  AAM.WL_MULTIPLE WM,  AAM.RESULTS_DETAIL RD
--    ,  AAM.LOCATIONS L,  AAM.HT_DC_XREF H, AAM.HT_RESERVE_BATCH_HISTORY B
--  WHERE
--   W.WL_KEY = WM.WL_KEY AND
--   WM.WL_KEY = RD.WL_KEY AND
--   WM.SIZE_NBR = RD.LEVEL1_DESC AND
--   W.ALLOC_NBR = RD.ALLOCATION_NBR AND
--   RD.LOCATION_ID = L.LOCATION_ID AND
--   W.SOURCE_DC = H.GERS_WHSE AND
--   W.ALLOC_NBR = B.ALLOC_NBR AND
--   L.WAREHOUSE_FLAG <> 'Y' AND
--   W.status_code = 40 AND
--   W.exported = 'Exported'AND
--   W.document_type = 'RESERVE'
--   AND B.BATCH_ID = p_batch_id
-- GROUP BY    W.DEPT_NBR,W.ALLOC_NBR,W.ITM_DESC1,WM.SKU_NBR,W.RETAIL_PRICE,W.UOM_QTY
ORDER BY 1,2,3;


END LIST_ALLOCS_BY_BATCH;

---Removed during Phase 0 changes 9/27/2012
--PROCEDURE PROC_RESERVE_UPDATE_PROCESS
--(
--p_warehouse_nbr HT_BACKSTOCK_TEMP.warehouse_nbr%TYPE
--)
--AS
--
--/*-- --------------------------------------------------------------------------
---- Name         : PROC_RESERVE_UPDATE_PROCESS
---- Author       : Sri Bajjuri
---- Description  :
---- Requirements :
---- Ammedments   :
----   When         Who       What
----   ===========  ========  =================================================
----     14-FEB-2005      Sri Bajjjuri    Include total allocatble units in the email.
----   09-JAN-2006    Sri Bajjuri   Include WHSE# in e-mail.
---- --------------------------------------------------------------------------*/
--
--  CURSOR cur_backstock IS
--        SELECT warehouse_nbr,avail_qty,itm_nbr,vendor_nbr
--    ,div_nbr,dept_nbr,class_nbr,subclass_nbr,sku_desc
--    ,uom_cd,uom_qty,retail_price,cost_price
--    ,sku_nbr,size_of_multiple,size_nbr,color_nbr
--    ,udf1,udf2 ,udf3 ,udf4 ,udf5 ,udf6, udf29
--    ,udf30,udf72,udf73,udf74,udf75,udf76,udf77,udf78,udf79
--    ,udf80,udf81,udf82,udf83,udf84,udf85,udf86,udf87,udf88,udf89
--   ,udf90,udf91,udf92,udf93,udf94,udf95,udf96,udf97,udf98,udf99
--        FROM  HT_BACKSTOCK_TEMP
--        WHERE warehouse_nbr = p_warehouse_nbr
--        ORDER BY sku_nbr;
--
--    CURSOR cur_availqtyByDept(t_source_dc IN WORKLIST.SOURCE_DC %TYPE)  IS
--      SELECT  DEPT_NBR, SUM(AVAIL_QTY) AS TOTAL_UNITS  FROM WORKLIST
--    WHERE STATUS_CODE = 10 AND DOCUMENT_TYPE = 'RESERVE'
--    AND SOURCE_DC = t_source_dc
--    GROUP BY DEPT_NBR ORDER BY DEPT_NBR;
--
--    cur_backstock_record cur_backstock%ROWTYPE;
--    cur_availqtyByDept_record cur_availqtyByDept%ROWTYPE;
--
--    v_ind NUMBER := 0;
--    v_merch_id_type NUMBER;
--    v_item_desc1 VARCHAR2(20);
--    v_item_desc2 VARCHAR2(20);
--    v_err_msg VARCHAR2(250);
--    v_sku_nbr VARCHAR2(20) := '';
--    v_total_units NUMBER := 0;
--    v_gers_whse HT_DC_XREF.GERS_WHSE%TYPE ;
--    v_email_body VARCHAR2(32767) := ' ';
--BEGIN
--
--  v_gers_whse := Gers_Whse_Nbr_Lookup(p_warehouse_nbr);
--
--  FOR cur_backstock_record IN cur_backstock LOOP
--      v_ind := v_ind + 1;
--      v_sku_nbr := cur_backstock_record.sku_nbr;
--      IF (cur_backstock_record.uom_cd = 'EA') THEN
--        v_merch_id_type := 1;
--      ELSE
--        v_merch_id_type := 2;
--      END IF;
--      IF (LENGTH(NVL(cur_backstock_record.sku_desc,'')) > 20) THEN
--            v_item_desc1 := SUBSTR(cur_backstock_record.sku_desc,1,20) ;
--            v_item_desc2 := SUBSTR(cur_backstock_record.sku_desc,21) ;
--      ELSE
--              v_item_desc1 := cur_backstock_record.sku_desc;
--      END IF;
--      --CALLING PROC_WORKLIST_RESERVE_UPDATE
--      PROC_WORKLIST_RESERVE_UPDATE
--        (
--          cur_backstock_record.warehouse_nbr
--          ,0
--          ,cur_backstock_record.avail_qty
--          ,cur_backstock_record.avail_qty
--          ,cur_backstock_record.itm_nbr
--          ,NVL(cur_backstock_record.vendor_nbr,0)
--          ,NVL(cur_backstock_record.div_nbr,0)
--          ,NVL(cur_backstock_record.dept_nbr,0)
--          ,NVL(cur_backstock_record.class_nbr,0)
--          ,NVL(cur_backstock_record.subclass_nbr,0)
--          ,v_item_desc1 --cur_backstock_record.sku_desc
--          ,v_item_desc2  --cur_backstock_record.sku_desc
--          ,cur_backstock_record.uom_cd
--          ,v_merch_id_type -- merch_id_type
--          ,NVL(cur_backstock_record.size_of_multiple,1) --cur_backstock_record.uom_qty
--          ,cur_backstock_record.retail_price
--          ,cur_backstock_record.cost_price
--          ,NULL -- p_vendor_desc
--          ,NULL -- p_div_desc
--              ,NULL --  p_dept_desc
--              ,NULL --  p_class_desc
--              ,NULL --  p_subclass_desc
--              ,NVL(cur_backstock_record.color_nbr,'NA')
--              ,NULL --  p_color_desc
--              ,'RES'--  p_document_status
--              ,cur_backstock_record.sku_nbr
--              ,NVL(cur_backstock_record.size_nbr,'NONE')
--              ,NVL(cur_backstock_record.size_of_multiple,1)
--              ,v_ind -- p_sku_seq_num
--              ,'FALSE'
--              ,cur_backstock_record.udf1
--              ,cur_backstock_record.udf2
--              ,cur_backstock_record.udf3
--              ,cur_backstock_record.udf4
--              ,cur_backstock_record.udf5
--              ,cur_backstock_record.udf6
--              ,cur_backstock_record.udf29
--              ,cur_backstock_record.udf30
--              ,cur_backstock_record.udf72
--              ,cur_backstock_record.udf73
--              ,cur_backstock_record.udf74
--              ,cur_backstock_record.udf75
--              ,cur_backstock_record.udf76
--              ,cur_backstock_record.udf77
--              ,cur_backstock_record.udf78
--              ,cur_backstock_record.udf79
--              ,cur_backstock_record.udf80
--              ,cur_backstock_record.udf81
--              ,cur_backstock_record.udf82
--              ,cur_backstock_record.udf83
--              ,cur_backstock_record.udf84
--              ,cur_backstock_record.udf85
--              ,cur_backstock_record.udf86
--              ,cur_backstock_record.udf87
--              ,cur_backstock_record.udf88
--              ,cur_backstock_record.udf89
--              ,cur_backstock_record.udf90
--              ,cur_backstock_record.udf91
--              ,cur_backstock_record.udf92
--              ,cur_backstock_record.udf93
--              ,cur_backstock_record.udf94
--              ,cur_backstock_record.udf95
--              ,cur_backstock_record.udf96
--              ,cur_backstock_record.udf97
--              ,cur_backstock_record.udf98
--              ,cur_backstock_record.udf99
--        ) ;
--  END LOOP;
--  COMMIT;
--  --DELETE FROM HT_BACKSTOCK_TEMP WHERE warehouse_nbr = p_warehouse_nbr;
--  v_email_body := 'Reserve update completed,' || v_ind || ' records processed.';
--  IF (v_ind > 0) THEN
--  -- 14-FEB-2005 Include total allocatble units in the email.
--    /*
--    SELECT SUM(AVAIL_QTY) INTO v_total_units FROM WORKLIST
--      WHERE STATUS_CODE = 10 and DOCUMENT_TYPE = 'RESERVE'
--      AND SOURCE_DC = v_gers_whse;
--     */
--
--     v_email_body := v_email_body || '<br>Warehouse #: ' || p_warehouse_nbr ;
--      v_email_body := v_email_body || '<br><br>' ||
--                      '<style>.mailBody{FONT-SIZE: 9pt;COLOR: black; FONT-FAMILY:Arial;}</style>' ||
--                      '<TABLE border=1 class=mailBody cellpadding=0 cellspacing=0><TR><TH width = 100>DEPT #</TH><TH>Available Units</TH></TR>';
--     FOR cur_availqtyByDept_record IN cur_availqtyByDept(v_gers_whse) LOOP
--        v_total_units := v_total_units + NVL(cur_availqtyByDept_record.TOTAL_UNITS,0);
--        v_email_body := v_email_body || '<TR><TD>' ||
--                      NVL(cur_availqtyByDept_record.DEPT_NBR,'') || '</TD><TD align=right>' ||
--                      NVL(cur_availqtyByDept_record.TOTAL_UNITS,0) || '</TD></TR>' ;
--     END LOOP;
--
--      v_email_body := v_email_body || '<TR><TD align=right><B>TOTAL</B></TD>' ||
--                      '<TD align=right>' || v_total_units || '</TD></TR>' ||
--                      '</TABLE><br>' ||
--                      'Please do not reply to this e-mail because this is an unmonitored alias.' ;
--
--  END IF;
--  IF (LENGTH(v_email_body) > 0) THEN
--    -- in production send e-mails to ReserveBatch@hottopic.com
--  	Html_Email('ReserveBatch@hottopic.com','Backstock@hottopic.com','Reserve Update',' ',v_email_body);
--  END IF;
--  --SEND_MAIL('ReserveBatch@hottopic.com','Reserve Update','Reserve update completed,' || v_ind || ' records(' || nvl(v_total_units,0) || ' units) processed');
--  EXCEPTION
--    WHEN OTHERS THEN
--      v_err_msg := SQLERRM;
--      INSERT INTO WMS_ERROR_LOG
--            (PROCEDURE_NAME, PARAMETER_LIST, WL_COMMENT, ERROR_MSG, CREATED_DATE)
--          VALUES
--            ('HT_RESERVE.PROC_RESERVE_UPDATE_PROCESS', NULL, 'SKU:' || v_sku_nbr, v_err_msg, SYSDATE);
--      COMMIT;
--       RAISE_APPLICATION_ERROR(-20006, SQLERRM);
--END PROC_RESERVE_UPDATE_PROCESS;

PROCEDURE PROC_RESERVE_UPDATE_LOADFILE
  (
  p_filename VARCHAR2
  ,p_filepath VARCHAR2
  ,p_warehouse_nbr HT_BACKSTOCK_TEMP.warehouse_nbr%TYPE
  )
AS
  v_cnt NUMBER := 0;
  v_job NUMBER;
  create_date_time DATE:=SYSDATE;
    const_columnStr VARCHAR2(14000) := 'warehouse_nbr,avail_qty,itm_nbr,vendor_nbr' ||
       ',div_nbr,dept_nbr,class_nbr,subclass_nbr,sku_desc' ||
       ',uom_cd,uom_qty,retail_price,cost_price' ||
       ',sku_nbr,size_of_multiple,size_nbr,color_nbr' ||
       ',udf1,udf2,udf3,udf4,udf5,udf6,create_date_time'||
        ',udf30,udf72,udf73,udf74,udf75,udf76,udf77,udf78,udf79'||
       ',udf80,udf81,udf82,udf83,udf84,udf85,udf86,udf87,udf88,udf89'||
       ',udf90,udf91,udf92,udf93,udf94,udf95,udf96,udf97,udf98,udf99,udf29' ;
BEGIN
    DELETE FROM HT_BACKSTOCK_TEMP WHERE warehouse_nbr = p_warehouse_nbr;
  v_cnt := Load_Data( 'HT_BACKSTOCK_TEMP',const_columnStr,p_filepath,p_filename,'|' );
 -- v_cnt := load_data( 'HT_BACKSTOCK_TEMP',const_columnStr,'EXTERNAL_TABLES',p_filename,'|' );
  IF (v_cnt > 0) THEN
     /* kick off reserve update process asynchronously */
      DBMS_JOB.SUBMIT(v_job,'HT_RESERVE.PROC_RESERVE_UPDATE_PROCESS(' || p_warehouse_nbr || ');');
  END IF;
   EXCEPTION
     WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR(-20006, SQLERRM);


END PROC_RESERVE_UPDATE_LOADFILE;

PROCEDURE PROC_RESERVE_UPDATE_LOAD AS

BEGIN
DELETE FROM HT_BACKSTOCK_TEMP;
COMMIT;


---UPDATE/INSERT TOTAL WAVED
MERGE INTO HT_STAGE_TOTAL_WAVED WV
USING (
      SELECT ALLOC_NBR, ITM_NBR,SKU_NUM, SUM(CANCEL_QTY) AS CANCEL_QTY, SUM(WAVE_QTY) As WAVE_QTY, SUM(CREATED_QTY) As CREATED_QTY, SHIP_TO
      FROM(
      SELECT W.DISTRO_NBR AS ALLOC_NBR, W.ITM_CD as ITM_NBR,W.SKU_NUM, 0 AS CANCEL_QTY,(SUM(W.ALLOC_UNITS) + SUM(WAVE_UNITS)) AS WAVE_QTY, 0 AS CREATED_QTY,W.SHIP_TO
          FROM HT_BASE_WMS_SHPD_WAVED W
          WHERE CARTON_NBR IS NULL AND STAT_CODE IN ('90','30') and w.SHIP_TO is NOT NULL
          GROUP BY  W.DISTRO_NBR,W.ITM_CD, W.SKU_NUM, W.CARTON_NBR, W.SHIP_TO
          UNION
          SELECT W.DISTRO_NBR AS ALLOC_NBR,W.ITM_CD as ITM_NBR,W.SKU_NUM,SUM(W.ORIG_REQ_UNITS) AS CANCEL_QTY,0 AS WAVE_QTY,0 AS CREATED_QTY, W.SHIP_TO
            FROM HT_BASE_WMS_SHPD_WAVED W
            WHERE CARTON_NBR IS NULL AND STAT_CODE = '95' and w.SHIP_TO is NOT NULL
            GROUP BY  W.DISTRO_NBR,W.ITM_CD, W.SKU_NUM, W.CARTON_NBR, W.SHIP_TO
          UNION
          SELECT W.DISTRO_NBR AS ALLOC_NBR,W.ITM_CD as ITM_NBR, W.SKU_NUM,0 AS CANCEL_QTY,0 AS WAVE_QTY,SUM(w.req_units) - SUM(W.ALLOC_UNITS) AS CREATED_QTY, W.SHIP_TO
            FROM HT_BASE_WMS_SHPD_WAVED W
            WHERE CARTON_NBR IS NULL AND STAT_CODE in (0,30)  and w.SHIP_TO is NOT NULL
            GROUP BY  W.DISTRO_NBR, W.ITM_CD,W.SKU_NUM, W.CARTON_NBR, W.SHIP_TO
      ) A GROUP BY  ALLOC_NBR, ITM_NBR, SKU_NUM, SHIP_TO
      ) BS
           ON (WV.ALLOC_NBR = BS.ALLOC_NBR AND WV.SKU_NUM = BS.SKU_NUM AND WV.STORE_NBR = BS.SHIP_TO AND WV.ITM_NBR = BS.ITM_NBR)
WHEN MATCHED THEN
UPDATE SET WV.QTY_WAVED = BS.WAVE_QTY , WV.CANCEL_QTY = BS.CANCEL_QTY ,  WV.CREATED_QTY= BS.CREATED_QTY
WHEN NOT MATCHED THEN
INSERT (ALLOC_NBR, ITM_NBR,SKU_NUM, STORE_NBR, QTY_WAVED, CANCEL_QTY, CREATED_QTY)
VALUES (BS.ALLOC_NBR,BS.ITM_NBR, BS.SKU_NUM, BS.SHIP_TO, BS.WAVE_QTY,BS.CANCEL_QTY, BS.CREATED_QTY);
COMMIT;

---UPDATE/INSERT  TOTAL SHIPPED
MERGE INTO HT_STAGE_TOTAL_SHIPPED SHP
USING (SELECT W.DISTRO_NBR AS ALLOC_NBR,W.ITM_CD AS ITM_NBR, W.SKU_NUM, W.CARTON_NBR, SUM(W.UNITS_PAKD) AS SHIP_QTY, SHIP_TO
            FROM HT_BASE_WMS_SHPD_WAVED W
            WHERE CODE_DESC = 'Shipped/Invoiced' AND CARTON_NBR IS NOT NULL
            GROUP BY  W.DISTRO_NBR, W.ITM_CD, W.SKU_NUM, W.CARTON_NBR,W.SHIP_TO) BS
       ON (SHP.ALLOC_NBR = BS.ALLOC_NBR AND SHP.ITM_NBR = BS.ITM_NBR AND SHP.SKU_NUM = BS.SKU_NUM AND SHP.CARTON_NBR = BS.CARTON_NBR AND SHP.LOCATION = BS.SHIP_TO)
WHEN MATCHED THEN
UPDATE SET SHP.QTY_SHIPPED = BS.SHIP_QTY
WHEN NOT MATCHED THEN
INSERT (ALLOC_NBR,ITM_NBR, SKU_NUM,CARTON_NBR,QTY_SHIPPED, LOCATION)
VALUES (BS.ALLOC_NBR, BS.ITM_NBR, BS.SKU_NUM, BS.CARTON_NBR, BS.SHIP_QTY, BS.SHIP_TO);
COMMIT;


---Updated for prepack changes
INSERT INTO HT_BACKSTOCK_TEMP (WAREHOUSE_NBR,AVAIL_QTY,QTY_PER_PACK,ITM_NBR,VENDOR_NBR,PACK_ID,DIV_NBR,DEPT_NBR,CLASS_NBR,SUBCLASS_NBR
    ,SKU_DESC,UOM_CD,UOM_QTY,RETAIL_PRICE,COST_PRICE,SKU_NBR,SIZE_OF_MULTIPLE,SIZE_NBR,COLOR_NBR,UDF1,UDF2,UDF3,UDF4,UDF5,UDF6
    ,CREATE_DATE_TIME, UDF30,UDF72 ,UDF73, UDF74, UDF75, UDF76, UDF77, UDF78, UDF79 , UDF80, UDF81, UDF82, UDF83, UDF84
    ,UDF85, UDF86, UDF87, UDF88, UDF89, UDF90, UDF91 , UDF92, UDF93, UDF94, UDF95, UDF96, UDF97, UDF98, UDF99, UDF29)

       SELECT /*+ALL_ROWS*/ G.WHSE AS WAREHOUSE_NBR , G.AVAIL_QTY, TO_CHAR(P.QTY_PER_PACK) AS QTY_PER_PACK,
            P.ITM_NBR AS ITM_NBR,substr(G.VENDOR_NBR,1,6) as VENDOR_NBR ,  P.PACK_ID as PACK_ID, G.DIV_NBR, G.DEPT_NBR,G.CLASS_NBR
       ,G.SUBCLASS_NBR, substr(B.ITEM_DESC,1,20) AS SKU_DESC, G.UOM_CD, G.UOM_QTY
       ,G.RETAIL_PRICE,G.COST_PRICE ,P.SKU_NUMBER AS SKU_NBR
       ,G.SIZE_OF_MULTIPLE , substr(B.SIZE_CD,1,6)  AS SIZE_NBR , substr(B.COLOR_CD,1,6) as COLOR_NBR
       ,B.UDF1,B.UDF2,B.UDF3,B.UDF4,B.UDF5,B.UDF6,TRUNC(SYSDATE) AS CREATE_DATE_TIME
       ,substr(B.UDF030,1,8) AS UDF30,B.UDF072 AS UDF72,B.UDF073 AS UDF73,B.UDF074 AS UDF74,B.UDF075 AS UDF75,B.UDF076 AS UDF76,substr(B.UDF077,1,8) AS UDF77
       ,substr(B.UDF078,1,8) AS UDF78,B.UDF079 AS UDF79 ,substr(B.UDF080,1,8) AS UDF80,B.UDF081 AS UDF81,B.UDF082 AS UDF82,substr(B.UDF083,1,8) AS UDF83,B.UDF084 AS UDF84
       ,B.UDF085 AS UDF85,B.UDF086 AS UDF86,substr(B.UDF087,1,8) AS UDF87,B.UDF088 AS UDF88,B.UDF089 AS UDF89,substr(B.UDF090,1,8) AS UDF90,B.UDF091 AS UDF91
       ,B.UDF092 AS UDF92,B.UDF093 AS UDF93,B.UDF094 AS UDF94,substr(B.UDF095,1,8) AS UDF95,B.UDF096 AS UDF96,B.UDF097 AS UDF97,B.UDF098 AS UDF98
       ,B.UDF099 AS UDF99,substr(B.UDF029,1,8) AS UDF29
       FROM

        (SELECT/*+ALL_ROWS*/ A.WHSE, A.QTY_AVAIL - NVL(C.BKS_WIP_OH,0) AS AVAIL_QTY
                ,A.STYLE AS ITM_NBR,A.DFLT_VENDOR AS VENDOR_NBR
                ,A.STORE_DEPT AS DIV_NBR, A.MISC_ALPHA_1 AS DEPT_NBR,LPAD(A.MISC_NUMERIC_1,4, 0) AS CLASS_NBR
               ,A.SALE_GRP AS SUBCLASS_NBR, A.SKU_DESC AS SKU_DESC, A.PROD_GROUP AS UOM_CD, A.PROD_SUB_GRP AS UOM_QTY
               ,A.RETAIL_PRICE AS RETAIL_PRICE,A.UNIT_PRICE AS COST_PRICE ,DECODE(LENGTH(A.STYLE_SFX),3,(A.STYLE||'-'||A.STYLE_SFX),A.STYLE_SFX) AS SKU_NBR
               ,A.PROD_SUB_GRP AS SIZE_OF_MULTIPLE, A.MISC_ALPHA_3 AS SIZE_NBR,(A.MISC_ALPHA_2 ||A.COLOR_SFX) AS COLOR_NBR
        FROM HT_BASE_WMS_BACKSTOCK A
        LEFT OUTER JOIN (
                SELECT  ITM_NBR, SKU_NUM, PACK_ID, SOURCE_DC, SUM(CREATED_QTY) AS BKS_WIP_OH
                FROM(
                    ---PACKS
                    SELECT distinct A.ITM_NBR, A.SKU_NUM, P.PACK_ID,A.STORE_NBR,wl.source_dc, A.CREATED_QTY
                    FROM HT_STAGE_TOTAL_WAVED A
                    JOIN  HT_BASE_RMS_PACKS P ON A.ITM_NBR = P.PACK_ID
                    JOIN WORKLIST WL ON A.ALLOC_NBR = WL.ALLOC_NBR AND P.ITM_NBR = WL.ITM_NBR
                    JOIN WL_PACK PK ON WL.WL_KEY = PK.WL_KEY
                    WHERE wl.document_type = 'RESERVE' AND UPPER(trim(WL.EXPORTED)) = 'EXPORTED' and A.CREATED_QTY > 0

                    UNION
                    ---NON- PACKS
                    SELECT distinct A.ITM_NBR, A.SKU_NUM, A.SKU_NUM AS PACK_ID,A.STORE_NBR,wl.source_dc, A.CREATED_QTY
                    FROM HT_STAGE_TOTAL_WAVED A
                    JOIN HT_BASE_GERS_UDF B  ON A.ITM_NBR = B.ITM_CD AND A.SKU_NUM = B.SKU_NUM AND B.PACK_IND = 'N'
                    JOIN WORKLIST WL ON A.ALLOC_NBR = WL.ALLOC_NBR AND B.ITM_CD = WL.ITM_NBR
                    JOIN WL_PACK PK ON WL.WL_KEY = PK.WL_KEY
                    WHERE wl.document_type = 'RESERVE' AND UPPER(trim(WL.EXPORTED)) = 'EXPORTED' and A.CREATED_QTY > 0
                    ) GROUP BY  ITM_NBR, SKU_NUM, PACK_ID, SOURCE_DC
         ) C ON A.STYLE = C.ITM_NBR AND DECODE(LENGTH(A.STYLE_SFX),3,(A.STYLE||'-'||A.STYLE_SFX),A.STYLE_SFX) = C.SKU_NUM
         AND LPAD(a.whse,4,'9') = C.source_dc where  A.QTY_AVAIL - NVL(C.BKS_WIP_OH,0) > 0
        ) G
       JOIN  HT_BASE_RMS_PACKS P  ON  G.ITM_NBR = P.PACK_ID
       JOIN HT_BASE_GERS_UDF B ON P.sku_number = B.SKU_NUM

UNION

 SELECT /*+ALL_ROWS*/ G.WHSE AS WAREHOUSE_NBR , FLOOR(G.AVAIL_QTY/G. UOM_QTY) as AVAIL_QTY, G. UOM_QTY AS QTY_PER_PACK,
            B.ITM_CD AS ITM_NBR,substr(G.VENDOR_NBR,1,6) as VENDOR_NBR ,  NULL as PACK_ID, G.DIV_NBR, G.DEPT_NBR,G.CLASS_NBR
       ,G.SUBCLASS_NBR, substr(B.ITEM_DESC,1,20) AS SKU_DESC, G.UOM_CD, G.UOM_QTY
       ,G.RETAIL_PRICE,G.COST_PRICE ,B.SKU_NUM AS SKU_NBR
       ,G.SIZE_OF_MULTIPLE , substr(B.SIZE_CD,1,6)  AS SIZE_NBR ,substr(B.COLOR_CD,1,6) as COLOR_NBR
       ,B.UDF1,B.UDF2,B.UDF3,B.UDF4,B.UDF5,B.UDF6,TRUNC(SYSDATE) AS CREATE_DATE_TIME
       ,substr(B.UDF030,1,8) AS UDF30,B.UDF072 AS UDF72,B.UDF073 AS UDF73,B.UDF074 AS UDF74,B.UDF075 AS UDF75,B.UDF076 AS UDF76,substr(B.UDF077,1,8) AS UDF77
       ,substr(B.UDF078,1,8) AS UDF78,B.UDF079 AS UDF79 ,substr(B.UDF080,1,8) AS UDF80,B.UDF081 AS UDF81,B.UDF082 AS UDF82,substr(B.UDF083,1,8) AS UDF83,B.UDF084 AS UDF84
       ,B.UDF085 AS UDF85,B.UDF086 AS UDF86,substr(B.UDF087,1,8) AS UDF87,B.UDF088 AS UDF88,B.UDF089 AS UDF89,substr(B.UDF090,1,8) AS UDF90,B.UDF091 AS UDF91
       ,B.UDF092 AS UDF92,B.UDF093 AS UDF93,B.UDF094 AS UDF94, substr(B.UDF095,1,8) AS UDF95,B.UDF096 AS UDF96,B.UDF097 AS UDF97,B.UDF098 AS UDF98
       ,B.UDF099 AS UDF99,substr(B.UDF029,1,8) AS UDF29
       FROM

        (SELECT/*+ALL_ROWS*/ A.WHSE, A.QTY_AVAIL - NVL(C.BKS_WIP_OH,0) AS AVAIL_QTY
                ,A.STYLE AS ITM_NBR,A.DFLT_VENDOR AS VENDOR_NBR
                ,A.STORE_DEPT AS DIV_NBR, A.MISC_ALPHA_1 AS DEPT_NBR,LPAD(A.MISC_NUMERIC_1,4, 0) AS CLASS_NBR
               ,A.SALE_GRP AS SUBCLASS_NBR, A.SKU_DESC AS SKU_DESC, A.PROD_GROUP AS UOM_CD, A.PROD_SUB_GRP AS UOM_QTY
               ,A.RETAIL_PRICE AS RETAIL_PRICE,A.UNIT_PRICE AS COST_PRICE ,DECODE(LENGTH(A.STYLE_SFX),3,(A.STYLE||'-'||A.STYLE_SFX),A.STYLE_SFX) AS SKU_NBR
               ,A.PROD_SUB_GRP AS SIZE_OF_MULTIPLE, A.MISC_ALPHA_3 AS SIZE_NBR,(A.MISC_ALPHA_2 ||A.COLOR_SFX) AS COLOR_NBR
        FROM HT_BASE_WMS_BACKSTOCK A
        LEFT OUTER JOIN (
                SELECT  ITM_NBR, SKU_NUM, PACK_ID, SOURCE_DC, SUM(CREATED_QTY) AS BKS_WIP_OH
                FROM(
                    ---PACKS
                    SELECT distinct A.ITM_NBR, A.SKU_NUM, P.PACK_ID,A.STORE_NBR,wl.source_dc, A.CREATED_QTY
                    FROM HT_STAGE_TOTAL_WAVED A
                    JOIN  HT_BASE_RMS_PACKS P ON A.ITM_NBR = P.PACK_ID
                    JOIN WORKLIST WL ON A.ALLOC_NBR = WL.ALLOC_NBR AND P.ITM_NBR = WL.ITM_NBR
                    JOIN WL_PACK PK ON WL.WL_KEY = PK.WL_KEY
                    WHERE wl.document_type = 'RESERVE' AND UPPER(trim(WL.EXPORTED)) = 'EXPORTED' and A.CREATED_QTY > 0

                    UNION
                    ---NON- PACKS
                    SELECT distinct A.ITM_NBR, A.SKU_NUM, A.SKU_NUM AS PACK_ID,A.STORE_NBR,wl.source_dc, A.CREATED_QTY
                    FROM HT_STAGE_TOTAL_WAVED A
                    JOIN HT_BASE_GERS_UDF B  ON A.ITM_NBR = B.ITM_CD AND A.SKU_NUM = B.SKU_NUM AND B.PACK_IND = 'N'
                    JOIN WORKLIST WL ON A.ALLOC_NBR = WL.ALLOC_NBR AND B.ITM_CD = WL.ITM_NBR
                    JOIN WL_PACK PK ON WL.WL_KEY = PK.WL_KEY
                    WHERE wl.document_type = 'RESERVE' AND UPPER(trim(WL.EXPORTED)) = 'EXPORTED' and A.CREATED_QTY > 0
                    ) GROUP BY  ITM_NBR, SKU_NUM, PACK_ID, SOURCE_DC
         ) C ON A.STYLE = C.ITM_NBR AND DECODE(LENGTH(A.STYLE_SFX),3,(A.STYLE||'-'||A.STYLE_SFX),A.STYLE_SFX) = C.SKU_NUM
         AND LPAD(a.whse,4,'9') = C.source_dc where  A.QTY_AVAIL - NVL(C.BKS_WIP_OH,0) > 0
        ) G
       JOIN HT_BASE_GERS_UDF B ON G.ITM_NBR = B.ITM_CD AND G.SKU_NBR = B.SKU_NUM  AND B.PACK_IND = 'N';

COMMIT;

END  PROC_RESERVE_UPDATE_LOAD;

PROCEDURE PROC_RESERVE_UPDATE_NIGHTLY

AS

CURSOR cur_backstock IS
        SELECT warehouse_nbr,avail_qty as nbr_packs,qty_per_pack,itm_nbr, --decode(pack_id, NULL,sku_nbr,pack_id) as pack_id ,vendor_nbr
        pack_id as pack_id ,vendor_nbr
    ,div_nbr,dept_nbr,class_nbr,subclass_nbr,sku_desc, DECODE(PACK_ID, NULL,NULL, PACK_ID) as ITEM_DES2
    ,uom_cd,uom_qty,retail_price,cost_price
    ,sku_nbr,size_of_multiple,size_nbr,color_nbr
    ,udf1,udf2 ,udf3 ,udf4 ,udf5 ,udf6, udf29
    ,udf30,udf72,udf73,udf74,udf75,udf76,udf77,udf78,udf79
    ,udf80,udf81,udf82,udf83,udf84,udf85,udf86,udf87,udf88,udf89
   ,udf90,udf91,udf92,udf93,udf94,udf95,udf96,udf97,udf98,udf99
        FROM  HT_BACKSTOCK_TEMP
        ORDER BY sku_nbr;

  cur_backstock_record cur_backstock%ROWTYPE;
    --cur_availqtyByDept_record cur_availqtyByDept%ROWTYPE;

    v_ind NUMBER := 0;
    v_merch_id_type NUMBER;
    v_item_desc1 VARCHAR2(20);
    v_item_desc2 VARCHAR2(20);
    v_err_msg VARCHAR2(250);
    v_sku_nbr VARCHAR2(20) := '';
    v_total_units NUMBER := 0;
  --  v_gers_whse HT_DC_XREF.GERS_WHSE%TYPE ;


BEGIN
 --v_gers_whse := Gers_Whse_Nbr_Lookup(p_warehouse_nbr);

 ----Call Procedure to Clear Backstock reserve
 PROC_WORKLIST_RESERVE_CLEAR;
 -- SBAJJURI - 2/10/13 - Update legacy backstock lines with ORMS merch hierarchy3- Added package ID un Using join
 --DRai 1101201 added package id
  MERGE INTO HT_BACKSTOCK_TEMP TGT
USING
(
  select bks.warehouse_nbr, bks.sku_nbr, NVL(bks.PACK_ID,bks.sku_nbr) as Pack_ID  ,i.itm_nbr, sc.subclass_cd, sc.des as subclass_desc, c.class_cd, c.des as class_desc, d.dept_cd, d.des as dept_desc
  from HT_BACKSTOCK_TEMP bks
  join ls_item i on bks.itm_nbr = i.itm_nbr
  join ls_subclass sc on i.subclass_cd = sc.subclass_cd
  join ls_class c on sc.class_cd = c.class_cd
  join ls_dept d on c.dept_cd = d.dept_cd
  where (bks.subclass_nbr <> sc.subclass_cd OR bks.class_nbr <> c.class_cd OR bks.dept_nbr <> d.dept_cd)
) SRC
ON (TGT.warehouse_nbr = SRC.warehouse_nbr AND TGT.sku_nbr = SRC.sku_nbr and NVL(TGT.Pack_ID,TGT.SKU_NBR) = SRC.PACK_ID)
WHEN MATCHED THEN
UPDATE SET DEPT_NBR = SRC.dept_cd, CLASS_NBR = SRC.class_cd,subclass_nbr = SRC.subclass_cd
WHEN NOT MATCHED THEN
      INSERT (warehouse_nbr)
      VALUES (SRC.warehouse_nbr);

  COMMIT;

  FOR cur_backstock_record IN cur_backstock LOOP

  IF cur_backstock_record.PACK_ID is null THEN
      v_ind := v_ind + 1;
  ELSE
      v_ind := 0;
  END IF;

      v_sku_nbr := cur_backstock_record.sku_nbr;

      --Drai 10/3/12 All packs will be default to 2
        v_merch_id_type := 2;

    --      IF (cur_backstock_record.uom_cd = 'EA') THEN
    --        v_merch_id_type := 1;
    --      ELSE
    --        v_merch_id_type := 2;
    --      END IF;
      IF (LENGTH(NVL(cur_backstock_record.sku_desc,'')) > 20) THEN
            v_item_desc1 := SUBSTR(cur_backstock_record.sku_desc,1,20) ;
            --v_item_desc2 := SUBSTR(cur_backstock_record.sku_desc,21) ;
          ---  v_item_desc2 := cur_backstock_record.ITEM_DES2;
      ELSE
              v_item_desc1 := cur_backstock_record.sku_desc;
      END IF;
      --CALLING PROC_WORKLIST_RESERVE_UPDATE

      PROC_WL_RES_UPDATE_NIGHTLY
        (
          cur_backstock_record.warehouse_nbr
          ,0
          ,cur_backstock_record.nbr_packs
          ,0
          ,cur_backstock_record.itm_nbr
          ,NVL(cur_backstock_record.vendor_nbr,0)
          ,NVL(cur_backstock_record.div_nbr,0)
          ,NVL(cur_backstock_record.dept_nbr,0)
          ,NVL(cur_backstock_record.class_nbr,0)
          ,NVL(cur_backstock_record.subclass_nbr,0)
          ,v_item_desc1 --cur_backstock_record.sku_desc
          ,cur_backstock_record.ITEM_DES2  --cur_backstock_record.sku_desc
          ,cur_backstock_record.uom_cd
          ,v_merch_id_type -- merch_id_type
          ,NVL(cur_backstock_record.size_of_multiple,1) --cur_backstock_record.uom_qty
          ,cur_backstock_record.retail_price
          ,cur_backstock_record.cost_price
          ,NULL -- p_vendor_desc
          ,NULL -- p_div_desc
              ,NULL --  p_dept_desc
              ,NULL --  p_class_desc
              ,NULL --  p_subclass_desc
              ,NVL(cur_backstock_record.color_nbr,'NA')
              ,NULL --  p_color_desc
              ,'RES'--  p_document_status
              ,cur_backstock_record.sku_nbr
              ,NVL(cur_backstock_record.size_nbr,'NONE')
              ,NVL(cur_backstock_record.size_of_multiple,1)
              ,v_ind -- p_sku_seq_num
              ,'FALSE'
              ,cur_backstock_record.udf1
              ,cur_backstock_record.udf2
              ,cur_backstock_record.udf3
              ,cur_backstock_record.udf4
              ,cur_backstock_record.udf5
              ,cur_backstock_record.udf6
              ,cur_backstock_record.udf29
              ,cur_backstock_record.udf30
              ,cur_backstock_record.udf72
              ,cur_backstock_record.udf73
              ,cur_backstock_record.udf74
              ,cur_backstock_record.udf75
              ,cur_backstock_record.udf76
              ,cur_backstock_record.udf77
              ,cur_backstock_record.udf78
              ,cur_backstock_record.udf79
              ,cur_backstock_record.udf80
              ,cur_backstock_record.udf81
              ,cur_backstock_record.udf82
              ,cur_backstock_record.udf83
              ,cur_backstock_record.udf84
              ,cur_backstock_record.udf85
              ,cur_backstock_record.udf86
              ,cur_backstock_record.udf87
              ,cur_backstock_record.udf88
              ,cur_backstock_record.udf89
              ,cur_backstock_record.udf90
              ,cur_backstock_record.udf91
              ,cur_backstock_record.udf92
              ,cur_backstock_record.udf93
              ,cur_backstock_record.udf94
              ,cur_backstock_record.udf95
              ,cur_backstock_record.udf96
              ,cur_backstock_record.udf97
              ,cur_backstock_record.udf98
              ,cur_backstock_record.udf99
              ,cur_backstock_record.qty_per_pack ---drai 10/3/12 New addded
              ,cur_backstock_record.pack_id         ---drai 10/3/12 New addded
        ) ;

  ---      DBMS_OUTPUT.PUT_LINE(cur_backstock_record.warehouse_nbr||' &'||cur_backstock_record.itm_nbr||' &'||NVL(cur_backstock_record.color_nbr,'NA'));
  END LOOP;
  COMMIT;


END PROC_RESERVE_UPDATE_NIGHTLY;

PROCEDURE PROC_WL_RES_UPDATE_NIGHTLY
(       p_warehouse_nbr     VARCHAR2
        ,p_po_nbr            NUMBER
        ,p_nbr_packs        NUMBER
        ,p_on_order_balance    NUMBER
        ,p_itm_nbr            VARCHAR2
        ,p_vendor_nbr        VARCHAR2
        ,p_div_nbr            VARCHAR2
        ,p_dept_nbr            VARCHAR2
        ,p_class_nbr        VARCHAR2
        ,p_subclass_nbr        VARCHAR2
        ,p_itm_desc1        VARCHAR2
        ,p_itm_desc2        VARCHAR2
        ,p_uom_cd            VARCHAR2
        ,p_merch_id_type    NUMBER
        ,p_uom_qty            NUMBER
        ,p_retail_price        NUMBER
        ,p_cost_price        NUMBER
        ,p_vendor_desc        VARCHAR2
        ,p_div_desc            VARCHAR2
        ,p_dept_desc        VARCHAR2
        ,p_class_desc        VARCHAR2
        ,p_subclass_desc    VARCHAR2
        ,p_color_nbr        VARCHAR2
        ,p_color_desc        VARCHAR2
        ,p_document_status    VARCHAR2
        ,p_sku_nbr            VARCHAR2
        ,p_size_nbr            VARCHAR2
        ,p_size_of_multiple    VARCHAR2
        ,p_sku_seq_num     NUMBER
        ,p_sendmail_flag VARCHAR2
        ,p_udf1 VARCHAR2
        ,p_udf2 VARCHAR2
        ,p_udf3 VARCHAR2
        ,p_udf4 VARCHAR2
        ,p_udf5 NUMBER
        ,p_udf6 NUMBER
        ,p_udf29                           VARCHAR2
        ,p_udf30                           VARCHAR2
        ,p_udf72                        VARCHAR2
        ,p_udf73                         VARCHAR2
        ,p_udf74                         VARCHAR2
        ,p_udf75                          VARCHAR2
        ,p_udf76                         VARCHAR2
        ,p_udf77                         VARCHAR2
        ,p_udf78                          VARCHAR2
        ,p_udf79                          VARCHAR2
        ,p_udf80                          VARCHAR2
        ,p_udf81                          VARCHAR2
        ,p_udf82                           VARCHAR2
        ,p_udf83                           VARCHAR2
        ,p_udf84                           VARCHAR2
        ,p_udf85                           VARCHAR2
        ,p_udf86                           VARCHAR2
        ,p_udf87                           VARCHAR2
        ,p_udf88                           VARCHAR2
        ,p_udf89                            VARCHAR2
        ,p_udf90                            VARCHAR2
        ,p_udf91                            VARCHAR2
        ,p_udf92                              VARCHAR2
        ,p_udf93                             VARCHAR2
        ,p_udf94                             VARCHAR2
        ,p_udf95                             VARCHAR2
        ,p_udf96                             VARCHAR2
        ,p_udf97                            VARCHAR2
        ,p_udf98                            VARCHAR2
        ,p_udf99                             VARCHAR2
        ,p_qty_per_pack                   NUMBER
        ,p_pack_id                           VARCHAR2
    )
    IS
/*-- --------------------------------------------------------------------------
-- Name         : PROC_WL_RES_UPDATE_NIGHTLY
-- Author       : Davendar Rai
-- Description  : Add/Update WORKLIST and WL_MULTIPLE/WL_BULK records based on backstock inventory from WMS.
-- Requirements : For WL Items with Available = 10 status then update avail_qty with backstock qty
---                      For  WL Items with Descrepancy = 25 status the update avail_qty with backstock qty
---                      For WL Items with Approved/Unapproved= 30/50 if avail_qty <> backstock qty then update avail_qty and change to descrepancy
---                     If WL exists in  Released = 40 status for the Items then create a new WL line with the
-- Ammedments   :
--   When         Who       What
--   ===========  ========  =================================================
--   27-Jul-2012  Davendar Rai  Initial Creation
         Description: Update Backstock worklist lines in the Worklist and Shape tables (WL_BULK, WL_DETAILS, WL_MULTIPLE)
-- --------------------------------------------------------------------------*/

    -- for WORKLIST
    v_wl_key NUMBER;
    v_wl_item_key NUMBER := 0;
    v_alloc_nbr NUMBER := 0;
    v_status_code NUMBER := 10; --default for new WORKLIST
    v_status_description VARCHAR2(50) := 'Available'; --default for new WORKLIST
    v_rec_nbr NUMBER := 0;
    v_document_type VARCHAR2(50) := 'RESERVE';
    v_trouble_code CHAR(1);
    v_trouble_description VARCHAR2(50);
    v_old_merch_id_type NUMBER;

    v_nbr_packs WL_PACK.NBR_PACKS%TYPE;
    v_pack_id WL_PACK.PACK_ID%TYPE;

    v_UpdateStatus VARCHAR2(1) := 'N';

    v_wlavail_qty NUMBER;
    v_old_order_qty NUMBER := 0;
    v_new_order_qty NUMBER := 0;

    v_Total_Avail_QTY NUMBER;

    v_po_ln_seq_num NUMBER := 0;

  v_email_sent BOOLEAN := FALSE;
  v_avail_qty NUMBER := 0;
  v_gers_whse HT_DC_XREF.GERS_WHSE%TYPE ;
  v_source_dc WORKLIST.SOURCE_DC%TYPE;
--    CURSOR cr_total_avail_qty_multiple(temp_wl_key IN NUMBER) IS
--        SELECT SUM(wm.QTY_AVAIL) AS TOTAL_AVAIL_QTY FROM  WL_MULTIPLE wm WHERE wm.WL_KEY = temp_wl_key;

--    CURSOR cr_total_avail_qty_bulk(temp_wl_key IN NUMBER) IS
--        SELECT SUM(wm.QTY_AVAIL) AS TOTAL_AVAIL_QTY FROM  WL_BULK wm WHERE wm.WL_KEY = temp_wl_key;


--added for Prepack
    CURSOR cr_total_avail_qty_pack(temp_wl_key IN NUMBER) IS
        SELECT SUM(wm.QTY_AVAIL) AS TOTAL_AVAIL_QTY FROM  WL_PACK wm WHERE wm.WL_KEY = temp_wl_key;

    BEGIN
        -- GET the GERS WHSE#

         v_Total_Avail_QTY :=0;

        BEGIN
        v_source_dc := Gers_Whse_Nbr_Lookup(p_warehouse_nbr);
        --v_gers_whse := GET_WHSENBR_FROM_SOURCE_DC(v_source_dc,p_div_nbr);
        -- 05/02/2006
        v_gers_whse := v_source_dc;

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(-20100,'Unable to find WHSE number in HT_DC_XREF');
        END;
        --look for existing WL_KEY


        Begin
--
--            IF p_pack_id is null and p_qty_per_pack > 1 THEN
--
--              v_nbr_packs := (FLOOR(p_nbr_packs/p_qty_per_pack)* p_qty_per_pack);
--              v_pack_id := p_sku_nbr;
--
--             ELSE
                v_nbr_packs := p_nbr_packs;
                v_pack_id := NVL(p_pack_id,p_sku_nbr);

--            END IF;

        end;



        BEGIN
            --look in WORKLIST/WL_PACK
       SELECT /*+ALL_ROWS*/ distinct WL_KEY into v_wl_key from(
          SELECT/*+ALL_ROWS*/ distinct  W.WL_KEY
          FROM WORKLIST W, WL_PACK PK
          WHERE W.DOCUMENT_TYPE = 'RESERVE'
                AND W.WL_KEY = PK.WL_KEY
                AND WAREHOUSE_NBR =  v_gers_whse
                AND ITM_NBR = p_itm_nbr
                AND STATUS_CODE <> 40
                --AND COLOR_NBR = p_color_nbr
                and( PK.Pack_id = v_pack_id and v_pack_id <> p_sku_nbr)
            UNION --non-pack
            SELECT/*+ALL_ROWS*/  distinct  W.WL_KEY
            FROM WORKLIST W, WL_PACK PK
            WHERE W.DOCUMENT_TYPE = 'RESERVE'
                AND W.WL_KEY = PK.WL_KEY
                AND WAREHOUSE_NBR =  v_gers_whse
                AND ITM_NBR = p_itm_nbr
                AND PK.PACK_ID = PK.SKU_NBR
                AND STATUS_CODE <> 40
                AND (v_pack_id = p_sku_nbr));


        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                BEGIN
                                v_wl_key := 0;

                END;
        END;


       -- Get the Status Code, WL_PACK avail qty for that Worklist
        IF v_wl_key <> 0 THEN
            BEGIN
                SELECT/*+ALL_ROWS*/  W.STATUS_CODE,SUM(PK.QTY_AVAIL) AS AVAIL_QTY-- W.AVAIL_QTY
                ,DECODE(W.STATUS_CODE,25, W.STATUS_DESCRIPTION,v_status_description)
                INTO  v_status_code,  v_wlavail_qty, v_status_description
                FROM WORKLIST W , WL_PACK PK
                WHERE W.WL_KEY = PK.WL_KEY
                AND W.WL_KEY =  v_wl_key
                AND PK.SKU_NBR = p_sku_nbr
                 and PK.PACK_ID = v_pack_id
                 GROUP BY W.STATUS_CODE,W.STATUS_DESCRIPTION;

                EXCEPTION
            WHEN NO_DATA_FOUND THEN
                BEGIN
                               -- v_wl_key := 0;
                                v_status_code := 10;
                                v_wlavail_qty  :=0;
                END;
             END;
         END IF;


        ---if the status is Approved/UnApproved and there is change to qty then
        ---change the status to Descrepancy =25 with new quantity
        IF  v_status_code = 30 or  v_status_code = 50 THEN
          IF v_wlavail_qty <> (v_nbr_packs * p_qty_per_pack)   THEN ---v_avail_qty THEN
            v_status_code := 25;
            v_status_description := 'Discrepancy';
            --7/15 DRai Status flag when changing Status Code
            v_UpdateStatus := 'Y';

            ---07/17/2013 DRai added to get alloc nbr for discrepancy line and update all lines with that alloc_nbr
            BEGIN
            SELECT alloc_nbr into v_alloc_nbr  FROM WORKLIST where wl_key =  v_wl_key;
                 EXCEPTION
            WHEN NO_DATA_FOUND THEN
                BEGIN
                               v_alloc_nbr := 0;
                END;
             END;
          END IF;
        END IF;

            IF v_wl_key = 0 THEN
            --create new WORKLIST
                --get next WL_KEY
                SELECT WORKLISTSEQ.NEXTVAL
                INTO v_wl_key
                FROM DUAL;

                INSERT INTO WORKLIST(
                    ALLOC_NBR --has default
                    ,WL_KEY
                    ,STATUS_CODE --has default
                    ,STATUS_DESCRIPTION --has default
                    ,CREATE_DATE
                    ,MERCH_ID_TYPE
                    ,AVAIL_QTY
                    ,WAREHOUSE_NBR
                    ,REC_NBR
                    ,PO_NBR
                    ,ORDER_QTY
                    ,ON_ORDER_BALANCE
                    ,ITM_NBR
                    ,ITM_DESC1
                    ,ITM_DESC2
                    ,UOM_CD
                    ,UOM_QTY
                    ,VENDOR_NBR
                    ,VENDOR_DESC
                    ,PO_CANCEL_DATE
                    ,DIV_NBR
                    ,DIV_DESC
                    ,DEPT_NBR
                    ,DEPT_DESC
                    ,CLASS_NBR
                    ,CLASS_DESC
                    ,SUBCLASS_NBR
                    ,SUBCLASS_DESC
                    ,COLOR_NBR
                    ,COLOR_DESC
                    ,RETAIL_PRICE
                    ,COST_PRICE
                    ,DOCUMENT_TYPE
                    ,DOCUMENT_STATUS
                    ,UDF1
                    ,UDF2
                    ,UDF3
                    ,UDF4
                    ,UDF5
                    ,UDF6
                    ,SOURCE_DC
                    ,BTS4  --new field added 05/01/2007
                    -- new fields added 12/12/2008
                    ,UDF29
                    ,UDF30
                    ,UDF72
                    ,UDF73
                    ,UDF74
                    ,UDF75
                    ,UDF76
                    ,UDF77
                    ,UDF78
                    ,UDF79
                    ,UDF80
                    ,UDF81
                    ,UDF82
                    ,UDF83
                    ,UDF84
                    ,UDF85
                    ,UDF86
                    ,UDF87
                    ,UDF88
                    ,UDF89
                    ,UDF90
                    ,UDF91
                    ,UDF92
                    ,UDF93
                    ,UDF94
                    ,UDF95
                    ,UDF96
                    ,UDF97
                    ,UDF98
                    ,UDF99
                    )
                VALUES (v_alloc_nbr --has default
                    ,v_wl_key
                    ,v_status_code --has default "10"
                    ,v_status_description --has default "Available"
                    ,SYSDATE
                    ,p_merch_id_type
                    ,v_nbr_packs * p_qty_per_pack--v_avail_qty
                    ,v_gers_whse
                    ,v_rec_nbr
                    ,p_po_nbr
                    ,v_nbr_packs * p_qty_per_pack
                    ,v_nbr_packs * p_qty_per_pack
                    ,p_itm_nbr
                    ,ConvertXmlSpecialChar(p_itm_desc1)
                    ,ConvertXmlSpecialChar(p_itm_desc2)
                    ,p_uom_cd
                    ,p_uom_qty
                    ,p_vendor_nbr
                    ,p_vendor_desc
                    ,SYSDATE
                    ,p_div_nbr
                    ,p_div_desc
                    ,p_dept_nbr
                    ,p_dept_desc
                    ,p_class_nbr
                    ,p_class_desc
                    ,p_subclass_nbr
                    ,p_subclass_desc
                    ,p_color_nbr
                    ,p_color_desc
                    ,p_retail_price
                    ,p_cost_price
                    ,v_document_type
                    ,p_document_status
                    ,ConvertXmlSpecialChar(p_udf1)
                    ,ConvertXmlSpecialChar(p_udf2)
                    ,ConvertXmlSpecialChar(p_udf3)
                    ,ConvertXmlSpecialChar(p_udf4)
                    ,p_udf5
                    ,p_udf6
                    ,v_source_dc
                    ,p_div_nbr  --new field added 05/01/2007
                    ,ConvertXmlSpecialChar(p_udf29)    --new field added 09/09/2008
                    ,ConvertXmlSpecialChar(p_udf30)  --new field added 09/09/2008
                    ,ConvertXmlSpecialChar(p_udf72)  --new field added 09/09/2008
                    ,ConvertXmlSpecialChar(p_udf73)  --new field added 09/09/2008
                    ,ConvertXmlSpecialChar(p_udf74)  --new field added 09/09/2008
                    ,ConvertXmlSpecialChar(p_udf75) --new field added 09/09/2008
                    ,ConvertXmlSpecialChar(p_udf76) --new field added 09/09/2008
                    ,ConvertXmlSpecialChar(p_udf77) --new field added 09/09/2008
                    ,ConvertXmlSpecialChar(p_udf78) --new field added 09/09/2008
                    ,ConvertXmlSpecialChar(p_udf79) --new field added 09/09/2008
                    ,ConvertXmlSpecialChar(p_udf80) --new field added 09/09/2008
                    ,ConvertXmlSpecialChar(p_udf81) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf82) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf83) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf84) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf85) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf86) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf87) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf88) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf89) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf90) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf91) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf92) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf93) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf94) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf95) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf96) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf97) --new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf98)--new field added 12/12/2008
                    ,ConvertXmlSpecialChar(p_udf99) --new field added 12/12/2008
                    );


                ELSE
              ----If the status is Available, then update the qauntity with backstock quantity
                ---if the status is Descrepancy then update the quantity with backstock quanity
                   IF  v_status_code <> 20 THEN


                       UPDATE WORKLIST
                       SET --ALLOC_NBR = v_alloc_nbr,
                            STATUS_CODE = v_status_code
                            ,STATUS_DESCRIPTION = DECODE(v_status_code,10,'Available',
                                                                                30,'Approved',
                                                                                25,'Discrepancy',
                                                                                50,'Unapproved' ,
                                                                                40,'Released',NULL )--Updated Status
                           -- ,CREATE_DATE = SYSDATE
                           ,UPDATE_STATUS_DATE = DECODE( v_UpdateStatus,'Y', SYSDATE,UPDATE_STATUS_DATE)
                            ,MERCH_ID_TYPE = p_merch_id_type
                            --,WAREHOUSE_NBR = v_gers_whse
                            ,REC_NBR = v_rec_nbr
                            ,PO_NBR = p_po_nbr
                            ,UOM_CD = p_uom_cd
                            ,UOM_QTY = p_uom_qty
                            ,VENDOR_NBR = p_vendor_nbr
                            ,VENDOR_DESC = p_vendor_desc
                            ,DIV_NBR = p_div_nbr
                            ,DIV_DESC = p_div_desc
                            ,DEPT_NBR = p_dept_nbr
                            ,DEPT_DESC = p_dept_desc
                            ,CLASS_NBR = p_class_nbr
                            ,CLASS_DESC = p_class_desc
                            ,SUBCLASS_NBR = p_subclass_nbr
                            ,SUBCLASS_DESC = p_subclass_desc
                            ,COLOR_NBR = p_color_nbr
                            ,COLOR_DESC = p_color_desc
                            ,RETAIL_PRICE = p_retail_price
                            ,COST_PRICE        = p_cost_price
                            ,DOCUMENT_TYPE = v_document_type
                            ,DOCUMENT_STATUS = p_document_status
                            ,UDF1 = ConvertXmlSpecialChar(p_udf1)
                            ,UDF2 = ConvertXmlSpecialChar(p_udf2)
                            ,UDF3 = ConvertXmlSpecialChar(p_udf3)
                            ,UDF4 = ConvertXmlSpecialChar(p_udf4)
                            ,UDF5 = p_udf5
                            ,UDF6 = p_udf6
                            ,BTS4 = p_div_nbr  --new field added 05/01/2007
                            -- new fields added 12/12/2008
                            ,UDF29 = ConvertXmlSpecialChar(p_udf29)
                            ,UDF30 =ConvertXmlSpecialChar(p_udf30)
                            ,UDF72 =ConvertXmlSpecialChar(p_udf72)
                            ,UDF73 =ConvertXmlSpecialChar(p_udf73)
                            ,UDF74 =ConvertXmlSpecialChar(p_udf74)
                            ,UDF75 =ConvertXmlSpecialChar(p_udf75)
                            ,UDF76 =ConvertXmlSpecialChar(p_udf76)
                            ,UDF77 =ConvertXmlSpecialChar(p_udf77)
                            ,UDF78 =ConvertXmlSpecialChar(p_udf78)
                            ,UDF79 =ConvertXmlSpecialChar(p_udf79)
                            ,UDF80 =ConvertXmlSpecialChar(p_udf80)
                            ,UDF81 =ConvertXmlSpecialChar(p_udf81)
                            ,UDF82 =ConvertXmlSpecialChar(p_udf82)
                            ,UDF83 =ConvertXmlSpecialChar(p_udf83)
                            ,UDF84 =ConvertXmlSpecialChar(p_udf84)
                            ,UDF85 =ConvertXmlSpecialChar(p_udf85)
                            ,UDF86 =ConvertXmlSpecialChar(p_udf86)
                            ,UDF87 =ConvertXmlSpecialChar(p_udf87)
                            ,UDF88 =ConvertXmlSpecialChar(p_udf88)
                            ,UDF89 =ConvertXmlSpecialChar(p_udf89)
                            ,UDF90 =ConvertXmlSpecialChar(p_udf90)
                            ,UDF91 =ConvertXmlSpecialChar(p_udf91)
                            ,UDF92 =ConvertXmlSpecialChar(p_udf92)
                            ,UDF93 =ConvertXmlSpecialChar(p_udf93)
                            ,UDF94 =ConvertXmlSpecialChar(p_udf94)
                            ,UDF95 =ConvertXmlSpecialChar(p_udf95)
                            ,UDF96 =ConvertXmlSpecialChar(p_udf96)
                            ,UDF97 =ConvertXmlSpecialChar(p_udf97)
                            ,UDF98 =ConvertXmlSpecialChar(p_udf98)
                            ,UDF99 =ConvertXmlSpecialChar(p_udf99)
                        WHERE WL_KEY = v_wl_key;

                        ---7/17/13 Change backstock WL line status to `Discrepancy? for all approved WL lines with that allocation number
                        ---if AVAIL_QTY is changed for any single item.
                        UPDATE WORKLIST
                        SET STATUS_CODE = 25, STATUS_DESCRIPTION = 'Discrepancy', UPDATE_STATUS_DATE = SYSDATE
                        Where alloc_nbr = v_alloc_nbr
                        and alloc_nbr <> 0;



                   END IF;
            END IF;


        BEGIN

              SELECT/*+ALL_ROWS*/  PK.WL_KEY
               INTO v_wl_item_key
               FROM  WL_PACK PK
                 WHERE PK.WL_KEY = v_wl_key
                    AND PK.SKU_NBR =  p_sku_nbr
                    AND PK.COLOR_NBR = p_color_nbr
                    AND PK.SIZE_NBR = p_size_nbr;
                    --AND PK.PACK_ID = p_pack_id;

            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  BEGIN
                    v_wl_item_key := 0;
                  END;
            END;

            IF v_wl_item_key = 0 THEN



            --Create WL_PACK
                        INSERT INTO WL_PACK(WL_KEY,
                                                 PACK_ID,
                                                 SKU_NBR,
                                                 COLOR_NBR,
                                                 SIZE_NBR,
                                                 QTY_PER_PACK,
                                                 NBR_PACKS,
                                                 QTY_AVAIL,
                                                 PO_LN_SEQ_NUM,
                                                 ORDER_QTY)
                                VALUES(v_wl_key ,
                                            v_pack_id,p_sku_nbr,p_color_nbr,
                                            p_size_nbr, p_qty_per_pack,
                                            v_nbr_packs,(p_qty_per_pack * v_nbr_packs), p_sku_seq_num,(p_qty_per_pack *v_nbr_packs));

            ELSE
                 IF v_status_code <> 20 THEN

                   UPDATE WL_PACK
                    SET  QTY_AVAIL = (p_qty_per_pack *v_nbr_packs)
                           ,NBR_PACKS = v_nbr_packs
                           ,PO_LN_SEQ_NUM = p_sku_seq_num
                            , ORDER_QTY = (p_qty_per_pack *v_nbr_packs)
                   WHERE WL_KEY = v_wl_item_key--v_wl_key
                       AND SKU_NBR =  p_sku_nbr
                       AND COLOR_NBR = p_color_nbr
                       AND SIZE_NBR = p_size_nbr;
                 END IF;

            END IF;


                    OPEN cr_total_avail_qty_pack(v_wl_key);
                    FETCH cr_total_avail_qty_pack INTO v_Total_Avail_QTY;
                    IF v_Total_Avail_QTY IS NULL THEN
                        v_Total_Avail_QTY := 0;
                    END IF;
                    CLOSE cr_total_avail_qty_pack;

            UPDATE WORKLIST SET ORDER_QTY = v_Total_Avail_QTY, ON_ORDER_BALANCE = v_Total_Avail_QTY, AVAIL_QTY = v_Total_Avail_QTY WHERE WL_KEY = v_wl_key;


    EXCEPTION
      WHEN OTHERS THEN
       -- DBMS_OUTPUT.PUT_LINE(v_gers_whse||' &'||p_itm_nbr||' &'||p_color_nbr||' &'|| v_wl_key);
      --  DBMS_OUTPUT.PUT_LINE(v_gers_whse||' &'||p_itm_nbr||' &'||p_size_nbr||' &'|| p_sku_nbr);
           RAISE_APPLICATION_ERROR(-20100,'ERROR IN RESERVE UPDATE => '||SQLCODE||':'||SQLERRM);

commit;
END PROC_WL_RES_UPDATE_NIGHTLY;

END Ht_Reserve;
/