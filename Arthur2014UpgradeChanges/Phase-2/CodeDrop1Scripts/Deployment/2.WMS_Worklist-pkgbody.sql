create or replace
PACKAGE BODY         AAM.WMS_WORKLIST IS

/*
|| Function  : FN_FIX_STRING
||
|| Description: This funcation replaces the ';' string with '&'||
||
|| Parameters : p_input_string VARCHAR2
||
|| Return     : VARCHAR2
*/
FUNCTION FN_FIX_STRING
(
	p_input_string VARCHAR2
) RETURN VARCHAR2
AS

	v_return_string VARCHAR2(54);

BEGIN

	v_return_string := REPLACE(p_input_string, ';', '&');
	RETURN v_return_string;

EXCEPTION
	WHEN OTHERS THEN
		RAISE_APPLICATION_ERROR(-20100,SQLERRM);

END;

/*
|| Procedure  : PROC_UPDATE_HT_DEC_XREF
||
|| Description: This procedure updates the HT_DC_XREF table.
||
|| Parameters : p_GERS_WHSE_CD VARCHAR2
||              p_WM_WHSE_CD VARCHAR2
||
|| Return     : NONE
*/
PROCEDURE PROC_UPDATE_HT_DC_XREF
(
	p_GERS_WHSE_CD VARCHAR2,
	p_WM_WHSE_CD VARCHAR2
)
IS

	v_existing_wms_whse_cd VARCHAR2(3);
	v_ht_dc_xref_id NUMBER;

	CURSOR cr_HT_DC_XREF(temp_gers_whse_cd IN VARCHAR2) IS
		SELECT WMS_WHSE FROM HT_DC_XREF WHERE GERS_WHSE=temp_gers_whse_cd;

BEGIN
	OPEN cr_HT_DC_XREF(p_GERS_WHSE_CD);
	FETCH cr_HT_DC_XREF INTO v_existing_wms_whse_cd;

	IF (v_existing_wms_whse_cd IS NULL) THEN
		-- GERS warehouse does not exist in reference table --
		-- Create new record in reference table --
		SELECT HT_DC_XREF_ID.NEXTVAL INTO v_ht_dc_xref_id FROM DUAL;

		INSERT INTO HT_DC_XREF(
			ID,
			GERS_WHSE,
			WMS_WHSE
		)
		VALUES(
			v_ht_dc_xref_id,
			p_GERS_WHSE_CD,
			p_WM_WHSE_CD
		);
	ELSE
		IF (v_existing_wms_whse_cd <> p_WM_WHSE_CD) THEN
			-- Update the reference code if the WMS warehouse cd does not match the new value passed in --
			UPDATE HT_DC_XREF SET WMS_WHSE=p_WM_WHSE_CD WHERE GERS_WHSE=p_GERS_WHSE_CD;
		END IF;
	END IF;

	CLOSE cr_HT_DC_XREF;

EXCEPTION
	WHEN OTHERS THEN
		RAISE_APPLICATION_ERROR(-20100,SQLERRM);
END;

/*
|| Procedure  : PROC_WL_DETAILS_UPDATE
||
|| Description: This procedure insert the comment into the WL_Details table.
||
|| Parameters : p_wl_key NUMBER
||              p_wl_details_xml LONG
||  DRai 11/05/2012 - Add New logic for PO Comments, Changed Text field size on WL_Detail table to 250
||                    Since there will be one PO Comment and One comment line  associated to PO it will be updated
||                      with line number 0 and sysdate for commant date. There are no comment dates associated with PO.
||
|| Return     : NONE
*/
PROCEDURE PROC_WL_DETAILS_UPDATE
(
p_wl_key NUMBER,
p_wl_details_xml VARCHAR2
)
AS

v_cmntTxt VARCHAR2(250);
v_wl_key WL_DETAILS.WL_KEY%TYPE;


CURSOR cr_WL_DETAIL(temp_wl_key IN NUMBER) IS
        SELECT * FROM WL_DETAILS WHERE WL_KEY = temp_wl_key;


BEGIN

    BEGIN
      SELECT WL_KEY, TEXT INTO v_wl_key ,v_cmntTxt
      FROM WL_DETAILS
      WHERE WL_KEY = p_wl_key and rownum = 1;
    EXCEPTION
                WHEN NO_DATA_FOUND THEN
                BEGIN
                    v_wl_key :=  0;
                END;
     END;

    IF v_wl_key <> 0 THEN

            IF p_wl_details_xml IS NULL AND (v_cmntTxt is null OR v_cmntTxt is not null) THEN

                DELETE WL_DETAILS WHERE WL_KEY = v_wl_key;

            ELSIF  (p_wl_details_xml IS NOT NULL AND v_cmntTxt IS NULL) OR (upper(p_wl_details_xml) <> upper(v_cmntTxt) ) THEN

                UPDATE WL_DETAILS
                SET TEXT = p_wl_details_xml, CMNT_DATE = SYSDATE
                WHERE WL_KEY = v_wl_key and rownum = 1;

            END IF;



    ELSE

        IF  p_wl_details_xml <> NULL THEN
            INSERT INTO WL_DETAILS(
                            WL_KEY,
                            CMNT_TP,
                            CMNT_DATE,
                            LINE_ORDER,
                            TEXT)
                            VALUES
                            ( p_wl_key,
                            'PO',
                            SYSDATE,
                            0,
                            p_wl_details_xml );
         END IF;
    END IF;


	EXCEPTION
	WHEN OTHERS THEN
		RAISE_APPLICATION_ERROR(-20100,SQLERRM);
END;

/*
|| Procedure  : PROC_WORKLIST_UPDATE
||
|| Description: This procedure inserts the PO information into the Worklist table.
||
|| Parameters :
||
|| MOD 11/19/2004 - Sri Bajjuri - Inserting WHSE# value in Source_DC column
||								WAREHOUSE_NBR will be set to 0 for TNDC.
|| MOD 11/5/2007 - Hogan Lei - Modify to update the new column START_TO_SHIP
|| MOD 11/25/2007 - Hogan Lei - Modify to change Worklist line to "Discrepancy"
|| 	   			  		  	  		   	  		 		  	   only if the quantity got changed.
|| MOD 1/4/2008 - Hogan Lei - Modify to bridge udf72 to udf99 from GERS to WORKLIST.
// MOD 11/16/2012  DRAI Prepack Changes
|| MOD 5/16/2014 - Vijay Naidu - Add new field PO_TYPE to insert/update statement and also added substr to UDF1, UDF2, UDF4
|| MOD 5/21/2014 - Vijay Naidu - Added logic to update the START_TO_SHIP, PO_CANCEL_DATE fields if the PO has been fully ASN
|| Return     : NONE
*/
PROCEDURE PROC_WORKLIST_UPDATE
(
p_internal_status       NUMBER      DEFAULT NULL, --WORKLIST table
p_po_nbr                NUMBER      DEFAULT NULL,
p_po_cancel_date        VARCHAR2    DEFAULT NULL,
p_avail_qty             NUMBER      DEFAULT NULL,
p_on_order_balance      NUMBER      DEFAULT NULL,
p_itm_nbr               VARCHAR2    DEFAULT NULL,
p_vendor_nbr            VARCHAR2    DEFAULT NULL,
p_div_nbr               VARCHAR2    DEFAULT NULL,
p_dept_nbr              VARCHAR2    DEFAULT NULL,
p_class_nbr             VARCHAR2    DEFAULT NULL,
p_subclass_nbr          VARCHAR2    DEFAULT NULL,
p_po_vendor_nbr         VARCHAR2    DEFAULT NULL,
p_itm_desc1             VARCHAR2    DEFAULT NULL,
p_itm_desc2             VARCHAR2    DEFAULT NULL,
p_uom_cd                VARCHAR2    DEFAULT NULL,
p_merch_id_type         NUMBER      DEFAULT NULL,
p_uom_qty               NUMBER      DEFAULT NULL,
p_retail_price          NUMBER      DEFAULT NULL,
p_cost_price            NUMBER      DEFAULT NULL,
p_udf1                  VARCHAR2    DEFAULT NULL,
p_udf2                  VARCHAR2    DEFAULT NULL,
p_udf3                  VARCHAR2    DEFAULT NULL,
p_udf4                  VARCHAR2    DEFAULT NULL,
p_udf5                  VARCHAR2    DEFAULT NULL,
p_udf6                  VARCHAR2    DEFAULT NULL,
p_vendor_desc           VARCHAR2    DEFAULT NULL,
p_div_desc              VARCHAR2    DEFAULT NULL,
p_dept_desc             VARCHAR2    DEFAULT NULL,
p_class_desc            VARCHAR2    DEFAULT NULL,
p_subclass_desc         VARCHAR2    DEFAULT NULL,
p_color_nbr             VARCHAR2    DEFAULT NULL,
p_color_desc            VARCHAR2    DEFAULT NULL,
p_document_status       VARCHAR2    DEFAULT NULL,
p_notes                 VARCHAR2    DEFAULT NULL,
p_report_group          VARCHAR2    DEFAULT NULL,
p_order_qty             NUMBER      DEFAULT NULL, --WL_MULTIPLE/WL_BULK table
p_qty_avail             NUMBER      DEFAULT NULL,
p_sku_nbr               VARCHAR2    DEFAULT NULL,
p_size_nbr              VARCHAR2    DEFAULT NULL,
p_po_ln_seq_num         NUMBER      DEFAULT NULL,
p_size_of_multiple      VARCHAR2    DEFAULT NULL, --WL_MULTIPLE table
p_worklist_details      VARCHAR2        DEFAULT NULL, --WL_DETAILS table --XML
p_WarehouseNumber       VARCHAR2    DEFAULT NULL,
p_WMS_WarehouseNumber   VARCHAR2    DEFAULT NULL,
p_Start_Ship_Date       VARCHAR2    DEFAULT NULL,
p_size_2                VARCHAR2    DEFAULT NULL,
p_grid_fld_1            VARCHAR2    DEFAULT NULL,
p_grid_fld_2            VARCHAR2    DEFAULT NULL,
p_grid_fld_3            VARCHAR2    DEFAULT NULL,
p_udf29                 VARCHAR2    DEFAULT NULL,
p_udf30                 VARCHAR2    DEFAULT NULL,
p_udf72                 VARCHAR2    DEFAULT NULL,
p_udf73                 VARCHAR2    DEFAULT NULL,
p_udf74                 VARCHAR2    DEFAULT NULL,
p_udf75                 VARCHAR2    DEFAULT NULL,
p_udf76                 VARCHAR2    DEFAULT NULL,
p_udf77                 VARCHAR2    DEFAULT NULL,
p_udf78                 VARCHAR2    DEFAULT NULL,
p_udf79                 VARCHAR2    DEFAULT NULL,
p_udf80                 VARCHAR2    DEFAULT NULL,
p_udf81                 VARCHAR2    DEFAULT NULL,
p_udf82                 VARCHAR2    DEFAULT NULL,
p_udf83                 VARCHAR2    DEFAULT NULL,
p_udf84                 VARCHAR2    DEFAULT NULL,
p_udf85                 VARCHAR2    DEFAULT NULL,
p_udf86                 VARCHAR2    DEFAULT NULL,
p_udf87                 VARCHAR2    DEFAULT NULL,
p_udf88                 VARCHAR2    DEFAULT NULL,
p_udf89                 VARCHAR2    DEFAULT NULL,
p_udf90                 VARCHAR2    DEFAULT NULL,
p_udf91                 VARCHAR2    DEFAULT NULL,
p_udf92                 VARCHAR2    DEFAULT NULL,
p_udf93                 VARCHAR2    DEFAULT NULL,
p_udf94                 VARCHAR2    DEFAULT NULL,
p_udf95                 VARCHAR2    DEFAULT NULL,
p_udf96                 VARCHAR2    DEFAULT NULL,
p_udf97                 VARCHAR2    DEFAULT NULL,
p_udf98                 VARCHAR2    DEFAULT NULL,
p_udf99                 VARCHAR2    DEFAULT NULL,
p_nbr_packs             NUMBER      DEFAULT NULL,
p_pack_id               VARCHAR2    DEFAULT NULL,
p_qty_per_pack          NUMBER      DEFAULT NULL,
p_po_type               VARCHAR2    DEFAULT NULL
)
IS

--Created 05/21/2014
V_ASN_REC_CNT NUMBER := 0;

---for multiple packs
    v_avail_qty Number;
    v_order_qty Number;
    v_on_order_balance Number;
    v_nbr_packs Number;
    v_qty_per_pack Number;
    v_pack_id Varchar2(25);

	-- for WORKLIST
	v_wl_key NUMBER;
	v_alloc_nbr NUMBER := 0;
	v_status_code NUMBER := 10; --default for new WORKLIST
	v_status_description VARCHAR2(50) := 'Available'; --default for new WORKLIST

	v_rec_nbr NUMBER := 0;
	v_document_type VARCHAR2(50) := 'PO';
	v_trouble_code CHAR(1);
	v_trouble_description VARCHAR2(50);
	v_old_merch_id_type NUMBER;

	v_old_order_qty NUMBER := 0;
    v_old_on_order_balance NUMBER := 0;
	v_new_order_qty NUMBER := 0;
	v_order_qty_diff NUMBER;
	v_old_qty_avail NUMBER;
	v_new_qty_avail NUMBER;


    --Created 02/20/2013
    v_temp_tot_avail_qty NUMBER;
    v_temp_tot_nbr_packs NUMBER;
    v_temp_tot_order_qty NUMBER;
    v_temp_tot_onorder_bal NUMBER;

     v_temp_totb_avail_qty NUMBER;
    v_temp_totb_nbr_packs NUMBER;
    v_temp_totb_order_qty NUMBER;
    v_temp_totb_onorder_bal NUMBER;

    v_avail_qty_diff  NUMBER;
    v_nbr_packs_diff NUMBER;
    v_onorder_diff Number;
    v_onorder_bal_diff Number;


	v_TotalOrderQty NUMBER;
	v_Total_Avail_QTY NUMBER;
    v_TotalOnOrderBal NUMBER;

	v_ParameterList VARCHAR2(4000);

	v_rec_cnt NUMBER;
	v_wl_key_cnt NUMBER;

	v_set_alloc_to_disc BOOLEAN := FALSE;


	v_location_id RESULTS_DETAIL.LOCATION_ID%TYPE;
	v_unique_id RESULTS_DETAIL.UNIQUE_MER_KEY%TYPE;
	v_qty_allocated RESULTS_DETAIL.RESULT_QTY%TYPE;

	WL_UNIQUE_CONSTRAIN EXCEPTION;

	CURSOR cr_old_wl_key(temp_wl_key IN NUMBER, temp_itm_nbr IN VARCHAR2, temp_PACK_ID in VARCHAR2) IS
		SELECT Distinct W.WL_KEY FROM WORKLIST W JOIN WL_PACK P ON W.WL_KEY = P.WL_KEY  WHERE W.WL_KEY = temp_wl_key AND ITM_NBR=temp_ITM_NBR
                                                                        AND P.PACK_ID = temp_PACK_ID ;

--	CURSOR cr_total_avail_qty_multiple(temp_wl_key IN NUMBER, temp_itm_nbr IN VARCHAR2, temp_po_nbr IN NUMBER) IS
--		SELECT SUM(wm.QTY_AVAIL) AS TOTAL_AVAIL_QTY FROM WORKLIST w, WL_MULTIPLE_OLD wm WHERE w.PO_NBR=temp_po_nbr AND w.ITM_NBR=temp_itm_nbr AND w.WL_KEY=wm.WL_KEY AND w.WL_KEY <> temp_wl_key;
--
--	CURSOR cr_total_avail_qty_bulk(temp_wl_key IN NUMBER, temp_itm_nbr IN VARCHAR2, temp_po_nbr IN NUMBER) IS
--		SELECT SUM(wm.QTY_AVAIL) AS TOTAL_AVAIL_QTY FROM WORKLIST w, WL_BULK_OLD wm WHERE w.PO_NBR=temp_po_nbr AND w.ITM_NBR=temp_itm_nbr AND w.WL_KEY=wm.WL_KEY AND w.WL_KEY <> temp_wl_key;
--
--	CURSOR cr_cur_wl_bulk_info(temp_wl_key IN NUMBER) IS
--		SELECT NVL(SUM(wm.QTY_AVAIL), 0) AS TOTAL_AVAIL_QTY, NVL(SUM(wm.ORDER_QTY), 0) AS TOTAL_ORDER_QTY FROM WORKLIST w, WL_BULK_OLD wm WHERE w.WL_KEY=wm.WL_KEY AND w.WL_KEY = temp_wl_key;
--
--	CURSOR cr_cur_wl_multiple_info(temp_wl_key IN NUMBER) IS
--		SELECT NVL(SUM(wm.QTY_AVAIL), 0) AS TOTAL_AVAIL_QTY, NVL(SUM(wm.ORDER_QTY), 0) AS TOTAL_ORDER_QTY FROM WORKLIST w, WL_MULTIPLE_OLD wm WHERE w.WL_KEY=wm.WL_KEY AND w.WL_KEY = temp_wl_key;
--
--	CURSOR cr_cur_old_wl_bulk_info(temp_wl_key IN NUMBER, temp_sku_nbr IN VARCHAR2) IS
--		SELECT QTY_AVAIL, ORDER_QTY FROM WL_BULK_OLD WHERE WL_KEY = temp_wl_key AND SKU_NBR = temp_sku_nbr;
--
--	CURSOR cr_cur_old_wl_multiple_info(temp_wl_key IN NUMBER, temp_sku_nbr IN VARCHAR2) IS
--		SELECT QTY_AVAIL, ORDER_QTY FROM WL_MULTIPLE_OLD WHERE WL_KEY = temp_wl_key AND SKU_NBR = temp_sku_nbr;

    CURSOR cr_WL_PACK(temp_wl_key IN NUMBER) IS
       SELECT * FROM WL_PACK WHERE WL_KEY = temp_wl_key
       FOR UPDATE OF QTY_AVAIl;

    CURSOR cr_cur_old_wl_pack_info(temp_wl_key IN NUMBER, temp_sku_nbr IN VARCHAR2, temp_pack_id IN VARCHAR2) IS
        SELECT QTY_AVAIL, ORDER_QTY, ON_ORDER_BALANCE FROM WL_PACK WHERE WL_KEY = temp_wl_key AND SKU_NBR = temp_sku_nbr AND PACK_ID = temp_pack_id ;

	CURSOR cr_Worklist(temp_wl_key IN NUMBER) IS
		SELECT * FROM WORKLIST WHERE WL_KEY=temp_wl_key;

	cr_Worklist_Record cr_Worklist%ROWTYPE;

	CURSOR cr_unique_constrain(temp_PO_NBR IN NUMBER, temp_ITM_NBR IN VARCHAR2, temp_PACK_ID IN VARCHAR2) IS
		SELECT COUNT(*) AS WL_COUNT FROM
        (SELECT Distinct W.* FROM WORKLIST W JOIN WL_PACK P ON W.WL_KEY = P.WL_KEY  WHERE PO_NBR=temp_PO_NBR AND ITM_NBR=temp_ITM_NBR
                                                                        AND P.PACK_ID = temp_PACK_ID AND ASN IS NOT NULL);

	-- Added by Hogan 03-12-2007 to cleanup Results_Detail and CAQ_HOST Where QTY_AVAIL = 0 --
	CURSOR cr_results_detail (temp_alloc_nbr IN NUMBER, temp_wl_key IN NUMBER, temp_size_nbr IN VARCHAR2) IS
		SELECT LOCATION_ID, UNIQUE_MER_KEY,  RESULT_QTY FROM RESULTS_DETAIL WHERE WL_KEY = temp_wl_key AND ALLOCATION_NBR = temp_alloc_nbr AND LEVEL1_DESC =  temp_size_nbr;



BEGIN


	--Look for existing WL_KEY
	BEGIN
		--Look in WL_PACK

        SELECT
        WL_KEY INTO v_wl_key  FROM (
            --Packs
            SELECT
                distinct W.WL_KEY
            FROM
                WL_PACK P,WORKLIST W
            WHERE
                P.WL_KEY = W.WL_KEY ---AND P.PO_LN_SEQ_NUM = p_po_ln_seq_num
            AND W.ITM_NBR = p_itm_nbr --AND P.COLOR_NBR = p_color_nbr AND P.SIZE_NBR = p_size_nbr
            AND W.PO_NBR = p_po_nbr AND W.ASN IS NULL
            AND ( P.Pack_id = p_pack_id and p_pack_id <> p_sku_nbr)

            UNION

            --Non-Packs
            SELECT
               distinct  W.WL_KEY
            FROM
                WL_PACK P,WORKLIST W
            WHERE
                P.WL_KEY = W.WL_KEY --AND P.PO_LN_SEQ_NUM = p_po_ln_seq_num
            AND  W.ITM_NBR = p_itm_nbr  --AND P.COLOR_NBR = p_color_nbr AND P.SIZE_NBR = p_size_nbr
            AND W.PO_NBR = p_po_nbr AND W.ASN IS NULL
            AND P.PACK_ID = P.SKU_NBR
            AND (p_pack_id = p_sku_nbr) ) ;

			EXCEPTION
				WHEN NO_DATA_FOUND THEN
				BEGIN
					IF (p_itm_nbr IS NULL) THEN
						SELECT MIN(WL_KEY) INTO v_wl_key FROM WORKLIST WHERE PO_NBR = p_po_nbr;

						IF (v_wl_key IS NULL) THEN
							v_wl_key := 0;
						END IF;
				  	ELSE
		        			v_wl_key := 0;
				  	END IF;
				END;
			END;


            Select decode(p_pack_id, NULL,p_sku_nbr,p_pack_id) into v_pack_id from dual;

             IF v_pack_id = p_sku_nbr  and p_qty_per_pack > 1 then

              SELECT decode(p_qty_per_pack,null,1,p_qty_per_pack) INTO v_qty_per_pack FROM dual;

                v_avail_qty := p_avail_qty;
                v_order_qty := p_order_qty;
                v_on_order_balance:= p_on_order_balance;
                v_nbr_packs :=  Floor(p_avail_qty/v_qty_per_pack);

             else
               SELECT decode(p_qty_per_pack,null,1,p_qty_per_pack) INTO v_qty_per_pack FROM dual;
                v_avail_qty := p_nbr_packs *v_qty_per_pack;
                v_order_qty := p_order_qty * v_qty_per_pack;
                v_on_order_balance:= p_on_order_balance * v_qty_per_pack;
                v_nbr_packs :=  p_nbr_packs;
            END IF;

  --Disabled on 6/12/2013 so no inserts into HT_DC_XREF happens causing duplicate records returned
	---PROC_UPDATE_HT_DC_XREF(p_WarehouseNumber, p_WMS_WarehouseNumber);

--Added below logic for updating the START_TO_SHIP, PO_CANCEL_DATE if po is fully ASN

SELECT COUNT(*) INTO V_ASN_REC_CNT FROM WORKLIST
 WHERE PO_NBR=p_po_nbr AND ITM_NBR=p_itm_nbr AND ASN IS NOT NULL; 

IF V_ASN_REC_CNT > 0 THEN

   UPDATE WORKLIST SET 
   PO_CANCEL_DATE = TO_DATE(P_PO_CANCEL_DATE,'mm/dd/yyyy'), 
   START_TO_SHIP = TO_DATE(P_START_SHIP_DATE, 'mm/dd/yyyy')
   WHERE PO_NBR = P_PO_NBR AND P_ITM_NBR = ITM_NBR;
   

END IF;

--

	IF v_wl_key = 0 THEN
		OPEN cr_unique_constrain(p_po_nbr, p_itm_nbr, p_pack_id);
		FETCH cr_unique_constrain INTO v_wl_key_cnt;
		CLOSE cr_unique_constrain;

		IF v_wl_key_cnt > 0 THEN
			RAISE WL_UNIQUE_CONSTRAIN;
		END IF;
	END IF;

    --02/20/2013 Get quantity diff during PO Update
   IF v_wl_key <> 0  THEN

    -- Added by DRai 02/20/2013 to get nbr packs and qty_avail
     ---- ASN Total line


        SELECT  SUM(NVL(p.QTY_AVAIL,0)) , SUM(NVL(p.NBR_PACKS,0)), Sum(NVL(p.ORDER_QTY,0)), SUM(NVL(P.ON_ORDER_BALANCE,0))
               INTO v_temp_tot_avail_qty, v_temp_tot_nbr_packs, v_temp_tot_order_qty, v_temp_tot_onorder_bal
        FROM WORKLIST w JOIN WL_PACK p ON W.WL_KEY = P.WL_KEY
        WHERE W.PO_NBR = p_po_nbr AND P.PACK_ID =p_pack_id and  W.ITM_NBR = p_itm_nbr and P.Sku_nbr =  p_sku_nbr and w.asn is not null;





        ---Non ASN total
        SELECT  SUM(NVL(p.QTY_AVAIL,0)) , SUM(NVL(p.NBR_PACKS,0)), Sum(NVL(p.ORDER_QTY,0)), SUM(NVL(P.ON_ORDER_BALANCE,0))
        INTO v_temp_totb_avail_qty, v_temp_totb_nbr_packs, v_temp_totb_order_qty, v_temp_totb_onorder_bal
        FROM WORKLIST w JOIN WL_PACK p ON W.WL_KEY = P.WL_KEY
           WHERE W.PO_NBR = p_po_nbr AND P.PACK_ID =p_pack_id  and W.ITM_NBR = p_itm_nbr and P.Sku_nbr =  p_sku_nbr and w.asn is null;

        v_avail_qty_diff :=    v_avail_qty - (NVL(v_temp_tot_avail_qty,0) + NVL(v_temp_totb_avail_qty,0)) ;
        v_nbr_packs_diff :=  v_nbr_packs - (NVL(v_temp_tot_nbr_packs,0) + NVL(v_temp_totb_nbr_packs,0));

        --02/20/2013  Update with difference
        --IF    v_nbr_packs_diff <> 0 THEN
        IF     v_nbr_packs <= NVL(v_temp_tot_nbr_packs,0) THEN

               v_nbr_packs:= 0;
               v_avail_qty := 0;
               v_on_order_balance := 0;

        ELSE
                v_nbr_packs := NVL(v_temp_totb_nbr_packs,0) + v_nbr_packs_diff ;
                v_avail_qty :=  NVL(v_temp_totb_avail_qty,0) +    v_avail_qty_diff ;
                v_on_order_balance:= v_avail_qty;


        END IF;

   END IF;



	IF v_wl_key = 0 AND p_internal_status <> 40 THEN
		--create new WORKLIST
		OPEN cr_old_wl_key(p_po_nbr, p_itm_nbr,p_pack_id);
		FETCH cr_old_wl_key INTO v_wl_key;
		CLOSE cr_old_wl_key;

        -- 9/25 If Pack other than POD then Insert
		---IF v_wl_key = 0 AND p_dept_nbr <> '35'  and  v_nbr_packs > 0 THEN
        IF v_wl_key = 0  and  v_nbr_packs > 0 THEN
			--Get next WL_KEY
			SELECT WORKLISTSEQ.NEXTVAL INTO v_wl_key FROM DUAL;

           			INSERT INTO WORKLIST(
				ALLOC_NBR, --has default
				WL_KEY,
				STATUS_CODE, --has default
				STATUS_DESCRIPTION, --has default
				CREATE_DATE,
				MERCH_ID_TYPE,
				AVAIL_QTY,
				WAREHOUSE_NBR,
				REC_NBR,
				PO_NBR,
				ORDER_QTY,
				ON_ORDER_BALANCE,
				ITM_NBR,
				ITM_DESC1,
				ITM_DESC2,
				UOM_CD,
				UOM_QTY,
				VENDOR_NBR,
				VENDOR_DESC,
				PO_CANCEL_DATE,
				DIV_NBR,
				DIV_DESC,
				DEPT_NBR,
				DEPT_DESC,
				CLASS_NBR,
				CLASS_DESC,
				SUBCLASS_NBR,
				SUBCLASS_DESC,
				COLOR_NBR,
				COLOR_DESC,
				RETAIL_PRICE,
				COST_PRICE,
				PO_VENDOR_NBR,
				UDF1,
				UDF2,
				UDF3,
				UDF4,
				UDF5,
				UDF6,
				OPEN_COL_1,---changed from Report_Group 9/25/2012
				DOCUMENT_TYPE,
				DOCUMENT_STATUS,
				NOTES,
				SOURCE_DC,
				BTS4,  --new field added on 05/02/2007
				START_TO_SHIP,
				UDF29,
				UDF30,
				UDF72,
				UDF73,
				UDF74,
				UDF75,
				UDF76,
				UDF77,
				UDF78,
				UDF79,
				UDF80,
				UDF81,
				UDF82,
				UDF83,
				UDF84,
				UDF85,
				UDF86,
				UDF87,
				UDF88,
				UDF89,
				UDF90,
				UDF91,
				UDF92,
				UDF93,
				UDF94,
				UDF95,
				UDF96,
				UDF97,
				UDF98,
				UDF99,
				PO_TYPE
			)
			VALUES (
				v_alloc_nbr, --has default
				v_wl_key,
				v_status_code, --has default
				v_status_description, --has default
				SYSDATE,
				p_merch_id_type,
                v_avail_qty,
                --p_avail_qty * p_qty_per_pack ,
				--FLOOR(p_avail_qty / p_uom_qty) * p_uom_qty,
				Get_Whsenbr_From_Source_Dc(p_WarehouseNumber, p_div_nbr),
				v_rec_nbr,
				p_po_nbr,
                v_order_qty,
				--p_order_qty * p_qty_per_pack,
                v_on_order_balance,
				--p_on_order_balance * p_qty_per_pack,
				p_itm_nbr,
				FN_FIX_STRING(p_itm_desc1),
				--FN_FIX_STRING(p_itm_desc2),
                DECODE(p_pack_id, p_sku_nbr,NULL,p_pack_id),
				p_uom_cd,
				p_uom_qty,
				p_vendor_nbr,
				FN_FIX_STRING(p_vendor_desc),
				TO_DATE(p_po_cancel_date,'mm/dd/yyyy'),   -- KJZ testing BT06 Old code
                --TO_DATE(p_po_cancel_date,'yyyy/mm/dd'),     -- KJZ testing BT06 New code
				p_div_nbr,
				FN_FIX_STRING(p_div_desc),
                p_dept_nbr,
				--LPAD(p_dept_nbr,4,'0'),
				FN_FIX_STRING(p_dept_desc),
				p_class_nbr,
				FN_FIX_STRING(p_class_desc),
				p_subclass_nbr,
				FN_FIX_STRING(p_subclass_desc),
				p_color_nbr,
				FN_FIX_STRING(p_color_desc),
				p_retail_price,
				p_cost_price,
				substr(p_po_vendor_nbr,1,20),
				substr(FN_FIX_STRING(p_udf1),1,8),
				substr(FN_FIX_STRING(p_udf2),1,8),
				FN_FIX_STRING(p_udf3),
				substr(FN_FIX_STRING(p_udf4),1,8),
				p_udf5,
				p_udf6,
				FN_FIX_STRING(p_report_group),
				v_document_type,
				p_document_status,
				FN_FIX_STRING(p_notes),
				p_WarehouseNumber,
				p_div_nbr,
				TO_DATE(p_Start_Ship_Date, 'mm/dd/yyyy'),   -- KJZ testing BT06 Old code
--                TO_DATE(p_Start_Ship_Date, 'yyyy/mm/dd'),   -- KJZ testing BT06 New code
				substr(FN_FIX_STRING(p_udf29),1,8),
				substr(FN_FIX_STRING(p_udf30),1,8),
				FN_FIX_STRING(p_udf72),
				FN_FIX_STRING(p_udf73),
				FN_FIX_STRING(p_udf74),
				FN_FIX_STRING(p_udf75),
				FN_FIX_STRING(p_udf76),
				substr(FN_FIX_STRING(p_udf77),1,8),
				substr(FN_FIX_STRING(p_udf78),1,8),
				substr(FN_FIX_STRING(p_udf79),1,8),
				substr(FN_FIX_STRING(p_udf80),1,8),
				substr(FN_FIX_STRING(p_udf81),1,8),
				FN_FIX_STRING(p_udf82),
				substr(FN_FIX_STRING(p_udf83),1,8),
				FN_FIX_STRING(p_udf84),
				FN_FIX_STRING(p_udf85),
				FN_FIX_STRING(p_udf86),
				substr(FN_FIX_STRING(p_udf87),1,8),
				FN_FIX_STRING(p_udf88),
				FN_FIX_STRING(p_udf89),
				FN_FIX_STRING(p_udf90),
				FN_FIX_STRING(p_udf91),
				FN_FIX_STRING(p_udf92),
				FN_FIX_STRING(p_udf93),
				FN_FIX_STRING(p_udf94),
				substr(FN_FIX_STRING(p_udf95),1,8),
				FN_FIX_STRING(p_udf96),
				FN_FIX_STRING(p_udf97),
				FN_FIX_STRING(p_udf98),
				FN_FIX_STRING(p_udf99),
				p_po_type
			);

			--create WL_DETAILS
			PROC_WL_DETAILS_UPDATE(v_wl_key, p_worklist_details);
		ELSE


        --UPDATE WORKLIST SET ORDER_QTY = ORDER_QTY + (p_order_qty * p_qty_per_pack) , ON_ORDER_BALANCE = ON_ORDER_BALANCE + (p_qty_per_pack * p_on_order_balance) , AVAIL_QTY = AVAIL_QTY + (p_qty_per_pack * p_nbr_packs) WHERE WL_KEY = v_wl_key ;
        UPDATE WORKLIST SET ORDER_QTY = ORDER_QTY + v_order_qty  , ON_ORDER_BALANCE = ON_ORDER_BALANCE +  v_on_order_balance , AVAIL_QTY = AVAIL_QTY + (v_qty_per_pack * v_nbr_packs) WHERE WL_KEY = v_wl_key ;
		END IF;

                 --pack
                   --IF v_wl_key <> 0 AND p_dept_nbr<> '35' THEN
       IF v_wl_key <> 0  THEN

                    OPEN cr_cur_old_wl_pack_info(v_wl_key, p_sku_nbr, p_pack_id);
                                    FETCH cr_cur_old_wl_pack_info INTO v_old_qty_avail, v_old_order_qty, v_old_on_order_balance;
                                    CLOSE cr_cur_old_wl_pack_info;

                                    v_order_qty_diff :=   ( p_order_qty * p_qty_per_pack) - v_old_order_qty;

                     if ( v_nbr_packs > 0) then --#012
                         MERGE INTO WL_PACK TGT
                          USING( SELECT v_wl_key AS wl_key, decode(p_pack_id, NULL,p_sku_nbr,p_pack_id)  as pack_id, p_sku_nbr as sku_nbr, p_color_nbr as color_nbr, p_size_nbr as size_nbr, v_qty_per_pack as qty_per_pack
                          ,v_qty_per_pack * v_nbr_packs as qty_avail,v_nbr_packs as nbr_packs, p_po_ln_seq_num as po_ln_seq_num, v_order_qty as order_qty
                          ,v_avail_qty as on_order_balance
                            FROM DUAL) SRC
                          ON (TGT.wl_key = SRC.wl_key AND TGT.pack_id = SRC.pack_id AND TGT.SKU_NBR = SRC.SKU_NBR)
                          --TGT.color_nbr = SRC.color_nbr AND TGT.size_nbr = SRC.size_nbr)
                        WHEN MATCHED THEN
                            UPDATE SET TGT.qty_avail = SRC.qty_avail , TGT.nbr_packs = SRC.nbr_packs, TGT.po_ln_seq_num = SRC.po_ln_seq_num, TGT.qty_per_pack = SRC.qty_per_pack, TGT.order_qty = SRC.order_qty, TGT.on_order_balance = SRC.on_order_balance,
                                                TGT.COLOR_NBR = SRC.COLOR_NBR, TGT.SIZE_NBR = SRC.SIZE_NBR
                        WHEN NOT MATCHED THEN
                          INSERT (WL_KEY,  PACK_ID,  SKU_NBR,  COLOR_NBR,  SIZE_NBR,  QTY_PER_PACK,  NBR_PACKS,  QTY_AVAIL,  PO_LN_SEQ_NUM,  ORDER_QTY, ON_ORDER_BALANCE)
                          VALUES(SRC.WL_KEY,  SRC.PACK_ID,  SRC.SKU_NBR,  SRC.COLOR_NBR,  SRC.SIZE_NBR,  SRC.QTY_PER_PACK,  SRC.NBR_PACKS,  SRC.QTY_AVAIL,  SRC.PO_LN_SEQ_NUM,  SRC.ORDER_QTY, SRC.ON_ORDER_BALANCE);
                      else
                                 UPDATE WL_PACK
                                 SET qty_avail = (v_qty_per_pack * v_nbr_packs) , nbr_packs = v_nbr_packs, po_ln_seq_num =  p_po_ln_seq_num, qty_per_pack = v_qty_per_pack, order_qty = v_order_qty, on_order_balance = Decode(NVL(v_on_order_balance,0),0,v_order_qty,v_on_order_balance),
                                        color_nbr = p_color_nbr , size_nbr = p_size_nbr
                                 Where WL_KEY = v_wl_key and pack_ID = decode(p_pack_id, NULL,p_sku_nbr,p_pack_id) and sku_nbr = p_sku_nbr and po_ln_seq_num= p_po_ln_seq_num ;
                      end if; --#012

          END IF;
              --  COMMIT;
	--ELSIF v_wl_key <> 0 AND p_internal_status <> 40 AND p_dept_nbr <> '35' THEN
    ELSIF v_wl_key <> 0 AND p_internal_status <> 40  THEN


		IF (p_itm_nbr IS NULL) OR (p_itm_nbr = 0) THEN --#006
 			UPDATE
 				WORKLIST
 			SET	VENDOR_NBR = p_vendor_nbr,
 				VENDOR_DESC = FN_FIX_STRING(p_vendor_desc),
 				PO_CANCEL_DATE = TO_DATE(p_po_cancel_date,'mm/dd/yyyy'),
 				DOCUMENT_STATUS = p_document_status,
 				NOTES = FN_FIX_STRING(p_notes),
				START_TO_SHIP = TO_DATE(p_Start_Ship_Date, 'mm/dd/yyyy')
 			WHERE WL_KEY = v_wl_key;

			-- Only update the comment.
			proc_WL_DETAILS_UPDATE(v_wl_key, p_worklist_details);
		ELSE --#006
			-- First, check current status if we can update WORKLIST
			SELECT
				ALLOC_NBR,
				STATUS_CODE,
				STATUS_DESCRIPTION,
				TROUBLE_CODE,
				TROUBLE_DESCRIPTION
			INTO
				v_alloc_nbr,
				v_status_code,
				v_status_description,
				v_trouble_code,
				v_trouble_description
			FROM WORKLIST
			WHERE WL_KEY = v_wl_key;

            ---IF Alloc In Progress
			IF v_status_code = 20 THEN
				v_trouble_code := 'T';
				v_trouble_description := 'PO Update Attempted';
			END IF;



           IF v_wl_key <> 0 THEN

            OPEN cr_cur_old_wl_pack_info(v_wl_key, p_sku_nbr,p_pack_id);
                    FETCH cr_cur_old_wl_pack_info INTO v_old_qty_avail, v_old_order_qty,v_old_on_order_balance;
                    CLOSE cr_cur_old_wl_pack_info;

                   -- v_order_qty_diff := p_order_qty - v_old_order_qty;

             SELECT COUNT(*) INTO v_rec_cnt FROM WL_PACK WHERE WL_KEY = v_wl_key ;
                    IF (v_rec_cnt > 0) THEN
                        Select  v_order_qty into v_new_qty_avail from dual;
                    ELSE
                       v_new_qty_avail := v_old_order_qty;
                    END IF;
                    IF (v_new_qty_avail < 0) THEN
                        v_new_qty_avail := 0;
                    END IF;



             IF (v_new_qty_avail <> v_old_qty_avail) AND (v_status_code = 30 OR v_status_code = 50 OR v_status_code = 60) THEN
                        v_status_code := 25;
                        v_status_description := 'Discrepancy';
                        v_set_alloc_to_disc := TRUE;
             END IF;

            END IF;


			OPEN cr_Worklist(v_wl_key);
 			FETCH cr_Worklist INTO cr_Worklist_Record;
 			CLOSE cr_Worklist;

        IF v_wl_key <> 0 THEN
             if ( v_nbr_packs > 0) then
                      MERGE INTO WL_PACK TGT
                      USING( SELECT v_wl_key AS wl_key, decode(p_pack_id, NULL,p_sku_nbr,p_pack_id)  as pack_id, p_sku_nbr as sku_nbr, p_color_nbr as color_nbr, p_size_nbr as size_nbr, v_qty_per_pack as qty_per_pack
                      ,v_qty_per_pack * v_nbr_packs as qty_avail,v_nbr_packs as nbr_packs, p_po_ln_seq_num as po_ln_seq_num, v_order_qty as order_qty
                      --,Decode(NVL(v_on_order_balance,0),0,v_order_qty,v_on_order_balance) as on_order_balance
                       ,v_avail_qty as on_order_balance
                        FROM DUAL) SRC
                      ON (TGT.wl_key = SRC.wl_key AND TGT.pack_id = SRC.pack_id AND TGT.SKU_NBR = SRC.SKU_NBR)
                      --Removed 02/19/2013
                      --TGT.color_nbr = SRC.color_nbr AND TGT.size_nbr = SRC.size_nbr)
                    WHEN MATCHED THEN
                        UPDATE SET TGT.qty_avail = SRC.qty_avail , TGT.nbr_packs = SRC.nbr_packs, TGT.po_ln_seq_num = SRC.po_ln_seq_num, TGT.qty_per_pack = SRC.qty_per_pack, TGT.order_qty = SRC.order_qty, TGT.on_order_balance = SRC.on_order_balance,
                                            TGT.COLOR_NBR = SRC.Color_nbr, TGT.Size_Nbr = SRC.Size_Nbr
                    WHEN NOT MATCHED THEN
                      INSERT (WL_KEY,  PACK_ID,  SKU_NBR,  COLOR_NBR,  SIZE_NBR,  QTY_PER_PACK,  NBR_PACKS,  QTY_AVAIL,  PO_LN_SEQ_NUM,  ORDER_QTY, ON_ORDER_BALANCE)
                      VALUES(SRC.WL_KEY,  SRC.PACK_ID,  SRC.SKU_NBR,  SRC.COLOR_NBR,  SRC.SIZE_NBR,  SRC.QTY_PER_PACK,  SRC.NBR_PACKS,  SRC.QTY_AVAIL,  SRC.PO_LN_SEQ_NUM,  SRC.ORDER_QTY, SRC.ON_ORDER_BALANCE);
            else
                 UPDATE WL_PACK
                 SET qty_avail = (v_qty_per_pack * v_nbr_packs) , nbr_packs = v_nbr_packs, po_ln_seq_num =  p_po_ln_seq_num, qty_per_pack = v_qty_per_pack, order_qty = v_order_qty, on_order_balance = v_avail_qty, --Decode(NVL(v_on_order_balance,0),0,v_order_qty,v_on_order_balance),
                        color_nbr = p_color_nbr , size_nbr = p_size_nbr
                 Where WL_KEY = v_wl_key and pack_ID = decode(p_pack_id, NULL,p_sku_nbr,p_pack_id) and sku_nbr = p_sku_nbr and po_ln_seq_num= p_po_ln_seq_num ;
            end if;

        END IF;

	    SELECT
			sum(qty_avail), sum(ORDER_QTY), SUM(ON_ORDER_BALANCE) INTO v_Total_Avail_QTY,  v_TotalOrderQty, v_TotalOnOrderBal
		FROM
			WL_PACK P
		WHERE
			p.wl_key = v_wl_key;


         ---Order diff at Worklist Key level
           v_order_qty_diff := v_TotalOrderQty - v_old_order_qty;

			UPDATE
 				WORKLIST
 			SET
 				MERCH_ID_TYPE = p_merch_id_type,
 				AVAIL_QTY = v_Total_Avail_QTY,
 				PO_NBR = p_po_nbr,
 				ORDER_QTY = v_TotalOrderQty,
 				ON_ORDER_BALANCE = v_TotalOnOrderBal,
 				ITM_NBR = p_itm_nbr,
 				ITM_DESC1 = FN_FIX_STRING(p_itm_desc1),
 				--ITM_DESC2 = FN_FIX_STRING(p_itm_desc2),
                ITM_DESC2 =    DECODE(p_pack_id, p_sku_nbr,NULL,p_pack_id),
 				UOM_CD = p_uom_cd,
 				UOM_QTY = p_uom_qty,
 				VENDOR_NBR = p_vendor_nbr,
 				VENDOR_DESC = FN_FIX_STRING(p_vendor_desc),
 				PO_CANCEL_DATE = TO_DATE(p_po_cancel_date,'mm/dd/yyyy'),
 				DIV_NBR = p_div_nbr,
 				DIV_DESC = FN_FIX_STRING(p_div_desc),
 				--DEPT_NBR =LPAD(p_dept_nbr,4,'0') ,
                DEPT_NBR =p_dept_nbr ,
 				DEPT_DESC = FN_FIX_STRING(p_dept_desc),
 				CLASS_NBR = p_class_nbr,
 				CLASS_DESC = FN_FIX_STRING(p_class_desc),
 				SUBCLASS_NBR = p_subclass_nbr,
 				SUBCLASS_DESC = FN_FIX_STRING(p_subclass_desc),
 				COLOR_NBR = p_color_nbr,
 				COLOR_DESC = FN_FIX_STRING(p_color_desc),
 				RETAIL_PRICE = p_retail_price,
 				COST_PRICE = p_cost_price,
 				PO_VENDOR_NBR = substr(p_po_vendor_nbr,1,20),
 				UDF1 = substr(FN_FIX_STRING(p_udf1),1,8),
 				UDF2 = substr(FN_FIX_STRING(p_udf2),1,8),
 				UDF3 = FN_FIX_STRING(p_udf3),
 				UDF4 = substr(FN_FIX_STRING(p_udf4),1,8),
 				UDF5 = p_udf5,
 				UDF6 = p_udf6,
 				---REPORT_GROUP = FN_FIX_STRING(p_report_group), --not required for RMS 9/25/2012
 				DOCUMENT_STATUS = p_document_status,
 				NOTES = FN_FIX_STRING(p_notes),
				TROUBLE_CODE = v_trouble_code,
				TROUBLE_DESCRIPTION = v_trouble_description,
				STATUS_CODE = v_status_code,
				STATUS_DESCRIPTION = v_status_description,
				BTS4 = p_div_nbr,  --new field added on 05/02/2007
				START_TO_SHIP = TO_DATE(p_Start_Ship_Date, 'mm/dd/yyyy'),
				UDF29 = substr(FN_FIX_STRING(p_udf29),1,8),
				UDF30 = substr(FN_FIX_STRING(p_udf30),1,8),
				UDF72 = FN_FIX_STRING(p_udf72),
				UDF73 = FN_FIX_STRING(p_udf73),
				UDF74 = FN_FIX_STRING(p_udf74),
				UDF75 = FN_FIX_STRING(p_udf75),
				UDF76 = FN_FIX_STRING(p_udf76),
				UDF77 = substr(FN_FIX_STRING(p_udf77),1,8),
				UDF78 = substr(FN_FIX_STRING(p_udf78),1,8),
				UDF79 = substr(FN_FIX_STRING(p_udf79),1,8),
				UDF80 = substr(FN_FIX_STRING(p_udf80),1,8),
				UDF81 = substr(FN_FIX_STRING(p_udf81),1,8),
				UDF82 = FN_FIX_STRING(p_udf82),
				UDF83 = substr(FN_FIX_STRING(p_udf83),1,8),
				UDF84 = FN_FIX_STRING(p_udf84),
				UDF85 = FN_FIX_STRING(p_udf85),
				UDF86 = FN_FIX_STRING(p_udf86),
				UDF87 = substr(FN_FIX_STRING(p_udf87),1,8),
				UDF88 = FN_FIX_STRING(p_udf88),
				UDF89 = FN_FIX_STRING(p_udf89),
				UDF90 = FN_FIX_STRING(p_udf90),
				UDF91 = FN_FIX_STRING(p_udf91),
				UDF92 = FN_FIX_STRING(p_udf92),
				UDF93 = FN_FIX_STRING(p_udf93),
				UDF94 = FN_FIX_STRING(p_udf94),
				UDF95 = substr(FN_FIX_STRING(p_udf95),1,8),
				UDF96 = FN_FIX_STRING(p_udf96),
				UDF97 = FN_FIX_STRING(p_udf97),
				UDF98 = FN_FIX_STRING(p_udf98),
				UDF99 = FN_FIX_STRING(p_udf99),
				PO_TYPE = p_po_type
 			WHERE WL_KEY = v_wl_key;

			-- Flag all the Worklist with the same allocation nbr to Discrepancy.
			IF v_alloc_nbr <> 0 AND v_set_alloc_to_disc THEN
			    UPDATE WORKLIST SET STATUS_CODE=25 ,STATUS_DESCRIPTION='Discrepancy' , UPDATE_STATUS_DATE = SYSDATE WHERE ALLOC_NBR = v_alloc_nbr;
			END IF;

            --DRai 10/3 Temporarly disabled
			PROC_WL_DETAILS_UPDATE(v_wl_key, p_worklist_details);

			-- Added by Hogan 03-12-2007 to cleanup Results_Detail and CAQ_HOST Where QTY_AVAIL = 0 --

         OPEN cr_Worklist(v_wl_key);
             FETCH cr_Worklist INTO cr_Worklist_Record;

             IF cr_Worklist_Record.STATUS_CODE <> 25 OR cr_Worklist_Record.STATUS_CODE <> 50 THEN


                    IF v_TotalOrderQty = 0 THEN

                       DELETE FROM WL_DETAILS WHERE WL_KEY = v_wl_key;
                       DELETE FROM WL_PACK WHERE WL_KEY = v_wl_key;
                       DELETE FROM WORKLIST WHERE WL_KEY = v_wl_key;

                    END IF ;

             END IF;

          CLOSE cr_Worklist;

           END IF;

			--END IF;
			-- End of Modification by Hogan 03-12-2007 --

			--CLOSE cr_Worklist;
		--END IF;

	ELSIF  p_internal_status = 40 THEN

        IF v_wl_key <> 0 THEN

            if ( v_nbr_packs > 0) then
                     MERGE INTO WL_PACK TGT
                      USING( SELECT v_wl_key AS wl_key, decode(p_pack_id, NULL,p_sku_nbr,p_pack_id)  as pack_id, p_sku_nbr as sku_nbr, p_color_nbr as color_nbr, p_size_nbr as size_nbr, v_qty_per_pack as qty_per_pack
                      ,v_qty_per_pack * v_nbr_packs as qty_avail,v_nbr_packs as nbr_packs, p_po_ln_seq_num as po_ln_seq_num, v_order_qty as order_qty
                      ,v_avail_qty as on_order_balance --Decode(NVL(v_on_order_balance,0),0,v_order_qty,v_on_order_balance) as on_order_balance
                        FROM DUAL) SRC
                      ON (TGT.wl_key = SRC.wl_key AND TGT.pack_id = SRC.pack_id AND TGT.SKU_NBR = SRC.SKU_NBR)
                      --Removed 02/19/2013
                      --AND TGT.color_nbr = SRC.color_nbr AND TGT.size_nbr = SRC.size_nbr)
                     WHEN MATCHED THEN
                        UPDATE SET TGT.qty_avail = SRC.qty_avail , TGT.nbr_packs = SRC.nbr_packs, TGT.po_ln_seq_num = SRC.po_ln_seq_num, TGT.qty_per_pack = SRC.qty_per_pack, TGT.order_qty = SRC.order_qty, TGT.on_order_balance = SRC.on_order_balance,
                                            TGT.COLOR_NBR = SRC.Color_nbr, TGT.Size_Nbr = SRC.Size_Nbr
                        WHEN NOT MATCHED THEN
                      INSERT (WL_KEY,  PACK_ID,  SKU_NBR,  COLOR_NBR,  SIZE_NBR,  QTY_PER_PACK,  NBR_PACKS,  QTY_AVAIL,  PO_LN_SEQ_NUM,  ORDER_QTY, ON_ORDER_BALANCE)
                      VALUES(SRC.WL_KEY,  SRC.PACK_ID,  SRC.SKU_NBR,  SRC.COLOR_NBR,  SRC.SIZE_NBR,  SRC.QTY_PER_PACK,  SRC.NBR_PACKS,  SRC.QTY_AVAIL,  SRC.PO_LN_SEQ_NUM,  SRC.ORDER_QTY, SRC.ON_ORDER_BALANCE);
        else
             UPDATE WL_PACK
             SET qty_avail = (v_qty_per_pack * v_nbr_packs) , nbr_packs = v_nbr_packs, po_ln_seq_num =  p_po_ln_seq_num, qty_per_pack = v_qty_per_pack, order_qty = v_order_qty, on_order_balance = v_avail_qty,
                color_nbr = p_color_nbr , size_nbr = p_size_nbr
             Where WL_KEY = v_wl_key and pack_ID = decode(p_pack_id, NULL,p_sku_nbr,p_pack_id) and sku_nbr = p_sku_nbr and po_ln_seq_num= p_po_ln_seq_num ;
         end if;

        END IF;

        SELECT
            sum(P.qty_avail), sum(P.ORDER_QTY), SUM(P.ON_ORDER_BALANCE) INTO v_Total_Avail_QTY,  v_TotalOrderQty, v_TotalOnOrderBal
        FROM
            WL_PACK P join WORKLIST K ON P.WL_KEY = K.WL_KEY
        WHERE
            p.wl_key = v_wl_key and K.PO_NBR = p_po_nbr ;

        UPDATE WORKLIST SET AVAIL_QTY = v_Total_Avail_QTY, ORDER_QTY =   v_TotalOrderQty, ON_ORDER_BALANCE = v_TotalOnOrderBal
          WHERE WL_KEY = v_wl_key and PO_NBR = p_po_nbr ;



        IF v_TotalOrderQty = 0 THEN
		-- Delete WORKLIST and related data	--
            DELETE FROM WL_PACK WHERE WL_KEY IN (SELECT WL_key FROM WORKLIST WHERE PO_NBR=p_po_nbr );
            DELETE FROM WL_DETAILS WHERE WL_KEY IN (SELECT WL_key FROM WORKLIST WHERE PO_NBR=p_po_nbr);
            DELETE FROM WORKLIST WHERE WL_KEY IN (SELECT WL_key FROM WORKLIST WHERE PO_NBR=p_po_nbr);

        END IF;

	END IF;



-- END IF;

EXCEPTION
	WHEN WL_UNIQUE_CONSTRAIN THEN
		-- Capture the unique key violation --
		v_ParameterList := '@p_internal_status=''' || p_internal_status || ',''';
		v_ParameterList := v_ParameterList || '@p_po_nbr=' || TO_CHAR(p_po_nbr) || ',';
		v_ParameterList := v_ParameterList || '@p_po_cancel_date=''' || p_po_cancel_date || ',''';
		v_ParameterList := v_ParameterList || '@p_avail_qty=' || TO_CHAR(p_avail_qty) || ',';
		v_ParameterList := v_ParameterList || '@p_on_order_balance=' || TO_CHAR(p_on_order_balance) || ',';
		v_ParameterList := v_ParameterList || '@p_itm_nbr=' || p_itm_nbr || ',''';
		v_ParameterList := v_ParameterList || '@p_vendor_nbr=' || p_vendor_nbr || ',''';
		v_ParameterList := v_ParameterList || '@p_div_nbr=' || p_div_nbr || ',''';
		--v_ParameterList := v_ParameterList || '@p_dept_nbr=' || LPAD(p_dept_nbr,4,'0') || ',''';
        v_ParameterList := v_ParameterList || '@p_dept_nbr=' ||p_dept_nbr || ',''';
		v_ParameterList := v_ParameterList || '@p_class_nbr=' || p_class_nbr || ',''';
		v_ParameterList := v_ParameterList || '@p_subclass_nbr=' || p_subclass_nbr || ',''';
		v_ParameterList := v_ParameterList || '@p_po_vendor_nbr=' || substr(p_po_vendor_nbr,1,20) || ',''';
		v_ParameterList := v_ParameterList || '@p_itm_desc1=' || p_itm_desc1 || ',''';
		v_ParameterList := v_ParameterList || '@p_itm_desc2=' || p_itm_desc2 || ',''';
		v_ParameterList := v_ParameterList || '@p_uom_cd=' || p_uom_cd || ',''';
		v_ParameterList := v_ParameterList || '@p_merch_id_type=' || TO_CHAR(p_merch_id_type) || ',';
		v_ParameterList := v_ParameterList || '@p_uom_qty=' || TO_CHAR(p_uom_qty) || ',';
		v_ParameterList := v_ParameterList || '@p_retail_price=' || TO_CHAR(p_retail_price) || ',';
		v_ParameterList := v_ParameterList || '@p_cost_price=' || TO_CHAR(p_cost_price) || ',';
		v_ParameterList := v_ParameterList || '@p_udf1=' || p_udf1 || ',''';
		v_ParameterList := v_ParameterList || '@p_udf2=' || p_udf2 || ',''';
		v_ParameterList := v_ParameterList || '@p_udf3=' || p_udf3 || ',''';
		v_ParameterList := v_ParameterList || '@p_udf4=' || p_udf4 || ',''';
		v_ParameterList := v_ParameterList || '@p_udf5=' || p_udf5 || ',''';
		v_ParameterList := v_ParameterList || '@p_udf6=' || p_udf6 || ',''';
		v_ParameterList := v_ParameterList || '@p_vendor_desc=' || p_vendor_desc || ',''';
		v_ParameterList := v_ParameterList || '@p_div_desc=' || p_div_desc || ',''';
		v_ParameterList := v_ParameterList || '@p_dept_desc=' || p_dept_desc || ',''';
		v_ParameterList := v_ParameterList || '@p_class_desc=' || p_class_desc || ',''';
		v_ParameterList := v_ParameterList || '@p_subclass_desc=' || p_subclass_desc || ',''';
		v_ParameterList := v_ParameterList || '@p_color_nbr=' || p_color_nbr || ',''';
		v_ParameterList := v_ParameterList || '@p_color_desc=' || p_color_desc || ',''';
		v_ParameterList := v_ParameterList || '@p_document_status=' || p_document_status || ',''';
		v_ParameterList := v_ParameterList || '@p_notes=' || p_notes || ',''';
		v_ParameterList := v_ParameterList || '@p_report_group=' || p_report_group || ',''';
		v_ParameterList := v_ParameterList || '@p_order_qty=' || TO_CHAR(p_order_qty) || ',';
		v_ParameterList := v_ParameterList || '@p_qty_avail=' || TO_CHAR(p_qty_avail) || ',';
		v_ParameterList := v_ParameterList || '@p_sku_nbr=''' || p_sku_nbr || ',''';
		v_ParameterList := v_ParameterList || '@p_size_nbr=' || p_size_nbr || ',''';
		v_ParameterList := v_ParameterList || '@p_po_ln_seq_num=' || TO_CHAR(p_po_ln_seq_num) || ',';
		v_ParameterList := v_ParameterList || '@p_size_of_multiple=' || p_size_of_multiple || ',''';
		v_ParameterList := v_ParameterList || '@p_WarehouseNumber=' || p_WarehouseNumber || ',''';
		v_ParameterList := v_ParameterList || '@p_WMS_WarehouseNumber=' || p_WMS_WarehouseNumber || '''';

		INSERT INTO WMS_ERROR_LOG
			(PROCEDURE_NAME, PARAMETER_LIST, WL_COMMENT, ERROR_MSG, CREATED_DATE)
		VALUES
			('WMS_WORKLIST.PROC_WORKLIST_UPDATE', v_ParameterList, p_worklist_details, 'User try to update PO that has been fully ASN.', SYSDATE);

	--WHEN OTHERS THEN
		--RAISE_APPLICATION_ERROR(-20100,SQLERRM);


END;


END Wms_Worklist;