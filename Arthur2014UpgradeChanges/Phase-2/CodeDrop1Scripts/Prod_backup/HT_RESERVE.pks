CREATE OR REPLACE PACKAGE AAM.Ht_Reserve IS
-- --------------------------------------------------------------------------------------
-- Name         : HT_RESERVE
-- Author       : Sri Bajjuri
-- Description  : This package contains reserve batch, reserve update, reserve clear
--                procedure calls
-- Requirements :
-- Ammedments   :
--   When         Who       What
--   ===========  ========  =================================================
--   17-MAR-2004  Sri Bajjuri  Initial Creation
--   8 Aug  2012  DRai Phase 0 changes added
--03 - Oct -2012 Added prepack changes, Removed logic for WL_Bulk and WL_Multilple
-- 12-Nov-2012 Fix issue of Multiples lines going to  WMS when Prepack Backstock line is allocated
--11-26-2012 Drai added reserve clear procedure
-- ---------------------------------------------------------------------------------------------------------------------------------

  TYPE T_CURSOR IS REF CURSOR;

  PROCEDURE PROC_WORKLIST_RESERVE_BATCH (
   p_warehouse_nbr VARCHAR2
   ,p_biztalkfile_path VARCHAR2
   , p_dept_str VARCHAR2 DEFAULT ''
   , p_rec_cnt OUT NUMBER
   , p_batch_id OUT NUMBER
   );

  ---Removed functionality Drai 9/27/2012
--PROCEDURE PROC_WORKLIST_CLEAR_BY_DEPT
--(
-- p_warehouse_nbr VARCHAR2
-- ,p_dept_str VARCHAR2 DEFAULT ''
--);
--  PROCEDURE PROC_WORKLIST_RESERVE_UPDATE
--    (
--        p_warehouse_nbr     VARCHAR2
--        ,p_po_nbr            NUMBER
--        ,p_avail_qty        NUMBER
--        ,p_on_order_balance    NUMBER
--        ,p_itm_nbr            VARCHAR2
--        ,p_vendor_nbr        VARCHAR2
--        ,p_div_nbr            VARCHAR2
--        ,p_dept_nbr            VARCHAR2
--        ,p_class_nbr        VARCHAR2
--        ,p_subclass_nbr        VARCHAR2
--        ,p_itm_desc1        VARCHAR2
--        ,p_itm_desc2        VARCHAR2
--        ,p_uom_cd            VARCHAR2
--        ,p_merch_id_type    NUMBER
--        ,p_uom_qty            NUMBER
--        ,p_retail_price        NUMBER
--        ,p_cost_price        NUMBER
--        ,p_vendor_desc        VARCHAR2
--        ,p_div_desc            VARCHAR2
--        ,p_dept_desc        VARCHAR2
--        ,p_class_desc        VARCHAR2
--        ,p_subclass_desc    VARCHAR2
--        ,p_color_nbr        VARCHAR2
--        ,p_color_desc        VARCHAR2
--        ,p_document_status    VARCHAR2
--        ,p_sku_nbr            VARCHAR2
--        ,p_size_nbr            VARCHAR2
--        ,p_size_of_multiple    VARCHAR2
--        ,p_sku_seq_num     NUMBER
--        ,p_sendmail_flag VARCHAR2
--        ,p_udf1 VARCHAR2
--        ,p_udf2 VARCHAR2
--        ,p_udf3 VARCHAR2
--        ,p_udf4 VARCHAR2
--        ,p_udf5 NUMBER
--        ,p_udf6 NUMBER
--        ,p_udf29                           VARCHAR2
--        ,p_udf30                           VARCHAR2
--        ,p_udf72                        VARCHAR2
--        ,p_udf73                         VARCHAR2
--        ,p_udf74                         VARCHAR2
--        ,p_udf75                          VARCHAR2
--        ,p_udf76                         VARCHAR2
--        ,p_udf77                         VARCHAR2
--        ,p_udf78                          VARCHAR2
--        ,p_udf79                          VARCHAR2
--        ,p_udf80                          VARCHAR2
--        ,p_udf81                          VARCHAR2
--        ,p_udf82                           VARCHAR2
--        ,p_udf83                           VARCHAR2
--        ,p_udf84                           VARCHAR2
--        ,p_udf85                           VARCHAR2
--        ,p_udf86                           VARCHAR2
--        ,p_udf87                           VARCHAR2
--        ,p_udf88                           VARCHAR2
--        ,p_udf89                            VARCHAR2
--        ,p_udf90                            VARCHAR2
--        ,p_udf91                            VARCHAR2
--        ,p_udf92                              VARCHAR2
--        ,p_udf93                             VARCHAR2
--        ,p_udf94                             VARCHAR2
--        ,p_udf95                             VARCHAR2
--        ,p_udf96                             VARCHAR2
--        ,p_udf97                            VARCHAR2
--        ,p_udf98                            VARCHAR2
--        ,p_udf99                             VARCHAR2
--    );

---Removed functionality Drai 9/27/2012
--  PROCEDURE PROC_WORKLIST_RESERVE_CLEAR(p_warehouse_nbr IN VARCHAR2);

PROCEDURE PROC_WORKLIST_RESERVE_CLEAR;

  PROCEDURE SEND_MAIL(p_mailto IN VARCHAR2, p_subj IN VARCHAR2, p_message IN VARCHAR2);

  FUNCTION Gers_Whse_Nbr_Lookup
   (
       p_warehouse_nbr IN    VARCHAR2
     ) RETURN HT_DC_XREF.GERS_WHSE%TYPE;

    FUNCTION ConvertXmlSpecialChar
   (
       p_in_str IN    VARCHAR2
     ) RETURN VARCHAR2;
     PROCEDURE PROC_UPDATE_HT_RESERVE_BATCH
  (
  p_batch_id HT_RESERVE_BATCH_HISTORY.BATCH_ID%TYPE
  ,p_alloc_nbr HT_RESERVE_BATCH_HISTORY.ALLOC_NBR%TYPE
  ,p_warehouse_nbr HT_RESERVE_BATCH_HISTORY.WAREHOUSE_NBR%TYPE
  ,p_export_flag HT_RESERVE_BATCH_HISTORY.EXPORT_FLAG%TYPE
  ,p_email_sent_flag HT_RESERVE_BATCH_HISTORY.EMAIL_SENT_FLAG%TYPE
  ,p_user_id HT_RESERVE_BATCH_HISTORY.USER_ID%TYPE
  );
  PROCEDURE LIST_EXPORT_PENDING_ALLOCS
  (
  p_warehouse_nbr HT_RESERVE_BATCH_HISTORY.WAREHOUSE_NBR%TYPE
  ,p_cur_Allocs OUT T_CURSOR
  );
  PROCEDURE EXPORT_STATUS_BY_BATCH
  (
  p_batch_id HT_RESERVE_BATCH_HISTORY.BATCH_ID%TYPE
  ,p_cur_Allocs OUT T_CURSOR
  );
  PROCEDURE LIST_ALLOCS_BY_BATCH
  (
  p_batch_id HT_RESERVE_BATCH_HISTORY.BATCH_ID%TYPE
  ,p_cur_Allocs OUT T_CURSOR
  );

 ---Removed during Phase 0 changes 9/27/2012 replaced by PROC_RESERVE_UPDATE_LOADFILE
-- PROCEDURE PROC_RESERVE_UPDATE_PROCESS
--  (
--  p_warehouse_nbr HT_BACKSTOCK_TEMP.warehouse_nbr%TYPE
--  );
  PROCEDURE PROC_RESERVE_UPDATE_LOADFILE
  (
  p_filename VARCHAR2
  ,p_filepath VARCHAR2
  ,p_warehouse_nbr HT_BACKSTOCK_TEMP.warehouse_nbr%TYPE
  );

 PROCEDURE  PROC_RESERVE_UPDATE_LOAD;
 PROCEDURE PROC_RESERVE_UPDATE_NIGHTLY ;
 PROCEDURE  PROC_WL_RES_UPDATE_NIGHTLY
 (
     p_warehouse_nbr     VARCHAR2
        ,p_po_nbr            NUMBER
        ,p_nbr_packs        NUMBER
        ,p_on_order_balance    NUMBER
        ,p_itm_nbr            VARCHAR2
        ,p_vendor_nbr        VARCHAR2
        ,p_div_nbr            VARCHAR2
        ,p_dept_nbr            VARCHAR2
        ,p_class_nbr        VARCHAR2
        ,p_subclass_nbr        VARCHAR2
        ,p_itm_desc1        VARCHAR2
        ,p_itm_desc2        VARCHAR2
        ,p_uom_cd            VARCHAR2
        ,p_merch_id_type    NUMBER
        ,p_uom_qty            NUMBER
        ,p_retail_price        NUMBER
        ,p_cost_price        NUMBER
        ,p_vendor_desc        VARCHAR2
        ,p_div_desc            VARCHAR2
        ,p_dept_desc        VARCHAR2
        ,p_class_desc        VARCHAR2
        ,p_subclass_desc    VARCHAR2
        ,p_color_nbr        VARCHAR2
        ,p_color_desc        VARCHAR2
        ,p_document_status    VARCHAR2
        ,p_sku_nbr            VARCHAR2
        ,p_size_nbr            VARCHAR2
        ,p_size_of_multiple    VARCHAR2
        ,p_sku_seq_num     NUMBER
        ,p_sendmail_flag VARCHAR2
        ,p_udf1     VARCHAR2
        ,p_udf2     VARCHAR2
        ,p_udf3     VARCHAR2
        ,p_udf4     VARCHAR2
        ,p_udf5     NUMBER
        ,p_udf6     NUMBER
        ,p_udf29                           VARCHAR2
        ,p_udf30                           VARCHAR2
        ,p_udf72                        VARCHAR2
        ,p_udf73                         VARCHAR2
        ,p_udf74                         VARCHAR2
        ,p_udf75                          VARCHAR2
        ,p_udf76                         VARCHAR2
        ,p_udf77                         VARCHAR2
        ,p_udf78                          VARCHAR2
        ,p_udf79                          VARCHAR2
        ,p_udf80                          VARCHAR2
        ,p_udf81                          VARCHAR2
        ,p_udf82                           VARCHAR2
        ,p_udf83                           VARCHAR2
        ,p_udf84                           VARCHAR2
        ,p_udf85                           VARCHAR2
        ,p_udf86                           VARCHAR2
        ,p_udf87                           VARCHAR2
        ,p_udf88                           VARCHAR2
        ,p_udf89                            VARCHAR2
        ,p_udf90                            VARCHAR2
        ,p_udf91                            VARCHAR2
        ,p_udf92                              VARCHAR2
        ,p_udf93                             VARCHAR2
        ,p_udf94                             VARCHAR2
        ,p_udf95                             VARCHAR2
        ,p_udf96                             VARCHAR2
        ,p_udf97                            VARCHAR2
        ,p_udf98                            VARCHAR2
        ,p_udf99                             VARCHAR2
        ,p_qty_per_pack                   NUMBER
        ,p_pack_id                           VARCHAR2
 );

END Ht_Reserve;
/