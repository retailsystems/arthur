CREATE OR REPLACE PACKAGE AAM."WMS_WORKLIST" IS

/*
|| Function  : FN_FIX_STRING
||
|| Description: This funcation replaces the 'CREATE SEQUENCE "AAM"."HT_DC_XREF_ID" INCREMENT BY 1 START WITH 1;' string with '&'||
||
|| Parameters : p_input_string VARCHAR2
||
|| Return     : VARCHAR2
*/
FUNCTION FN_FIX_STRING
(
	p_input_string VARCHAR2
) RETURN VARCHAR2;

/*
|| Procedure  : PROC_UPDATE_HT_DEC_XREF
||
|| Description: This procedure updates the HT_DC_XREF table.
||
|| Parameters : p_GERS_WHSE_CD VARCHAR2
||              p_WM_WHSE_CD VARCHAR2
||
|| Return     : NONE
*/
PROCEDURE PROC_UPDATE_HT_DC_XREF
(
	p_GERS_WHSE_CD VARCHAR2,
	p_WM_WHSE_CD VARCHAR2
);

/*
|| Procedure  : PROC_WL_DETAILS_UPDATE
||
|| Description: This procedure insert the comment into the WL_Details table.
||
|| Parameters : p_wl_key NUMBER
||              p_wl_details_xml LONG
||
|| Return     : NONE
*/
PROCEDURE PROC_WL_DETAILS_UPDATE
(
p_wl_key NUMBER,
p_wl_details_xml VARCHAR2
);

/*
|| Procedure  : PROC_WORKLIST_UPDATE
||
|| Description: This procedure inserts the PO information into the Worklist table.
||
|| Parameters :
||
|| Return     : NONE
*/
PROCEDURE PROC_WORKLIST_UPDATE
(
p_internal_status       NUMBER      DEFAULT NULL, --WORKLIST table
p_po_nbr                NUMBER      DEFAULT NULL,
p_po_cancel_date        VARCHAR2    DEFAULT NULL,
p_avail_qty             NUMBER      DEFAULT NULL,
p_on_order_balance      NUMBER      DEFAULT NULL,
p_itm_nbr               VARCHAR2    DEFAULT NULL,
p_vendor_nbr            VARCHAR2    DEFAULT NULL,
p_div_nbr               VARCHAR2    DEFAULT NULL,
p_dept_nbr              VARCHAR2    DEFAULT NULL,
p_class_nbr             VARCHAR2    DEFAULT NULL,
p_subclass_nbr          VARCHAR2    DEFAULT NULL,
p_po_vendor_nbr	        VARCHAR2    DEFAULT NULL,
p_itm_desc1             VARCHAR2    DEFAULT NULL,
p_itm_desc2             VARCHAR2    DEFAULT NULL,
p_uom_cd                VARCHAR2    DEFAULT NULL,
p_merch_id_type	        NUMBER      DEFAULT NULL,
p_uom_qty               NUMBER      DEFAULT NULL,
p_retail_price          NUMBER      DEFAULT NULL,
p_cost_price            NUMBER      DEFAULT NULL,
p_udf1                  VARCHAR2    DEFAULT NULL,
p_udf2                  VARCHAR2    DEFAULT NULL,
p_udf3                  VARCHAR2    DEFAULT NULL,
p_udf4                  VARCHAR2    DEFAULT NULL,
p_udf5                  VARCHAR2    DEFAULT NULL,
p_udf6                  VARCHAR2    DEFAULT NULL,
p_vendor_desc           VARCHAR2    DEFAULT NULL,
p_div_desc              VARCHAR2    DEFAULT NULL,
p_dept_desc             VARCHAR2    DEFAULT NULL,
p_class_desc            VARCHAR2    DEFAULT NULL,
p_subclass_desc	        VARCHAR2    DEFAULT NULL,
p_color_nbr             VARCHAR2    DEFAULT NULL,
p_color_desc            VARCHAR2    DEFAULT NULL,
p_document_status       VARCHAR2    DEFAULT NULL,
p_notes                 VARCHAR2    DEFAULT NULL,
p_report_group          VARCHAR2    DEFAULT NULL,
p_order_qty             NUMBER      DEFAULT NULL, --WL_MULTIPLE/WL_BULK table
p_qty_avail             NUMBER      DEFAULT NULL,
p_sku_nbr               VARCHAR2    DEFAULT NULL,
p_size_nbr              VARCHAR2    DEFAULT NULL,
p_po_ln_seq_num         NUMBER      DEFAULT NULL,
p_size_of_multiple      VARCHAR2    DEFAULT NULL, --WL_MULTIPLE table
p_worklist_details      VARCHAR2      DEFAULT NULL, --WL_DETAILS table --XML
p_WarehouseNumber       VARCHAR2    DEFAULT NULL,
p_WMS_WarehouseNumber   VARCHAR2    DEFAULT NULL,
p_Start_Ship_Date       VARCHAR2    DEFAULT NULL,
p_size_2                VARCHAR2    DEFAULT NULL,
p_grid_fld_1            VARCHAR2    DEFAULT NULL,
p_grid_fld_2            VARCHAR2    DEFAULT NULL,
p_grid_fld_3            VARCHAR2    DEFAULT NULL,
p_udf29                 VARCHAR2    DEFAULT NULL,
p_udf30                 VARCHAR2    DEFAULT NULL,
p_udf72                 VARCHAR2    DEFAULT NULL,
p_udf73                 VARCHAR2    DEFAULT NULL,
p_udf74                 VARCHAR2    DEFAULT NULL,
p_udf75                 VARCHAR2    DEFAULT NULL,
p_udf76                 VARCHAR2    DEFAULT NULL,
p_udf77                 VARCHAR2    DEFAULT NULL,
p_udf78                 VARCHAR2    DEFAULT NULL,
p_udf79                 VARCHAR2    DEFAULT NULL,
p_udf80                 VARCHAR2    DEFAULT NULL,
p_udf81                 VARCHAR2    DEFAULT NULL,
p_udf82                 VARCHAR2    DEFAULT NULL,
p_udf83                 VARCHAR2    DEFAULT NULL,
p_udf84                 VARCHAR2    DEFAULT NULL,
p_udf85                 VARCHAR2    DEFAULT NULL,
p_udf86                 VARCHAR2    DEFAULT NULL,
p_udf87                 VARCHAR2    DEFAULT NULL,
p_udf88                 VARCHAR2    DEFAULT NULL,
p_udf89                 VARCHAR2    DEFAULT NULL,
p_udf90                 VARCHAR2    DEFAULT NULL,
p_udf91                 VARCHAR2    DEFAULT NULL,
p_udf92                 VARCHAR2    DEFAULT NULL,
p_udf93                 VARCHAR2    DEFAULT NULL,
p_udf94                 VARCHAR2    DEFAULT NULL,
p_udf95                 VARCHAR2    DEFAULT NULL,
p_udf96                 VARCHAR2    DEFAULT NULL,
p_udf97                 VARCHAR2    DEFAULT NULL,
p_udf98                 VARCHAR2    DEFAULT NULL,
p_udf99                 VARCHAR2    DEFAULT NULL,
p_nbr_packs             NUMBER      DEFAULT NULL,
p_pack_id               VARCHAR2    DEFAULT NULL,
p_qty_per_pack          NUMBER      DEFAULT NULL
);

END;
/