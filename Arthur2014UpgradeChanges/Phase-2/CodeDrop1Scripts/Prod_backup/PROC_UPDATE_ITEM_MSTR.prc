CREATE OR REPLACE PROCEDURE AAM."PROC_UPDATE_ITEM_MSTR" (
 p_itm_nbr VARCHAR2 DEFAULT NULL,
 p_sku_nbr VARCHAR2 DEFAULT NULL,
 p_cost_price NUMBER DEFAULT NULL,
 p_retail_price NUMBER DEFAULT NULL,
 p_udf1 VARCHAR2 DEFAULT NULL,
 p_udf2 VARCHAR2 DEFAULT NULL,
 p_udf3 VARCHAR2 DEFAULT NULL,
 p_udf4 VARCHAR2 DEFAULT NULL,
 p_udf5 NUMBER DEFAULT NULL,
 p_udf6 NUMBER DEFAULT NULL,
 p_report_group VARCHAR2 DEFAULT NULL,
 p_itm_desc1 VARCHAR2 DEFAULT NULL,
 p_itm_desc2 VARCHAR2 DEFAULT NULL,
 p_class_nbr VARCHAR2 DEFAULT NULL,
 p_class_desc VARCHAR2 DEFAULT NULL,
 p_subclass_nbr VARCHAR2 DEFAULT NULL,
 p_subclass_desc VARCHAR2 DEFAULT NULL,
 p_dept_nbr VARCHAR2 DEFAULT NULL,
 p_dept_desc VARCHAR2 DEFAULT NULL,
 p_uom_cd VARCHAR2 DEFAULT NULL,
 p_uom_qty NUMBER DEFAULT NULL,
 p_new_item NUMBER DEFAULT NULL,
 p_color_cd VARCHAR2 DEFAULT NULL,
 p_size_cd VARCHAR2 DEFAULT NULL,
 p_udf29 VARCHAR2 DEFAULT NULL,
 p_udf30 VARCHAR2 DEFAULT NULL,
 p_udf72 VARCHAR2 DEFAULT NULL,
 p_udf73 VARCHAR2 DEFAULT NULL,
 p_udf74 VARCHAR2 DEFAULT NULL,
 p_udf75 VARCHAR2 DEFAULT NULL,
 p_udf76 VARCHAR2 DEFAULT NULL,
 p_udf77 VARCHAR2 DEFAULT NULL,
 p_udf78 VARCHAR2 DEFAULT NULL,
 p_udf79 VARCHAR2 DEFAULT NULL,
 p_udf80 VARCHAR2 DEFAULT NULL,
 p_udf81 VARCHAR2 DEFAULT NULL,
 p_udf82 VARCHAR2 DEFAULT NULL,
 p_udf83 VARCHAR2 DEFAULT NULL,
 p_udf84 VARCHAR2 DEFAULT NULL,
 p_udf85 VARCHAR2 DEFAULT NULL,
 p_udf86 VARCHAR2 DEFAULT NULL,
 p_udf87 VARCHAR2 DEFAULT NULL,
 p_udf88 VARCHAR2 DEFAULT NULL,
 p_udf89 VARCHAR2 DEFAULT NULL,
 p_udf90 VARCHAR2 DEFAULT NULL,
 p_udf91 VARCHAR2 DEFAULT NULL,
 p_udf92 VARCHAR2 DEFAULT NULL,
 p_udf93 VARCHAR2 DEFAULT NULL,
 p_udf94 VARCHAR2 DEFAULT NULL,
 p_udf95 VARCHAR2 DEFAULT NULL,
 p_udf96 VARCHAR2 DEFAULT NULL,
 p_udf97 VARCHAR2 DEFAULT NULL,
 p_udf98 VARCHAR2 DEFAULT NULL,
 p_udf99 VARCHAR2 DEFAULT NULL
 )
/*********************************************************************************************
*
* AAM.PROC_UPDATE_ITEM_MSTR
*
* PURPOSE
* ----------------
* Add/update record in WORKLIST table and WL_MULTIPLE/WL_BULK table according to the new info
* passed over from GERS.
*
* HISTORY
* ----------------
* 01/31/2004	Hogan Lei		Initial creation
* 06/14/2004  Hogan Lei   Replace Biztalk transposed ampersand [;]
*   			   		          with orignial ampersand [&] in itm_desc1, itm_desc2, udf1, udf2, udf3, udf4
*	03/22/2005  YHERYANTO   Bug #2513: QTY_AVAIL in AA get set to 0 when UOM in GERS changed
*                         Removed code that related with column ON_ORDER_BALANCE in table WORKLIST
* 1/4/2008 		Hogan Lei - Modify to bridge udf72 to udf99 from GERS to WORKLIST.
* 8/5/2012  Sri Bajjuri - Apply item changes to all work list lines except Released (stat code = 40) and In-progess (stat code =20),
*                       -   current logic is limited to open (stat code = 10) work list lines only.
* 10/02/2012  Logic to update Item Dimension data in worklist table only. Assumption is that UOM_Qty cannot be changed once Item is created
* 03/02/2013 Added substring of 8 for uda description values
*********************************************************************************************/
IS
--   v_old_uom_qty NUMBER;
   v_wl_key NUMBER;
--   v_temp_wl_key NUMBER;
--   v_merch_id_type NUMBER;
--   v_d_sku_nbr VARCHAR2(12);
--   v_d_color_nbr VARCHAR2(6);
--   v_d_size_nbr VARCHAR2(6);
--   v_d_qty_avail NUMBER;
--   v_d_po_ln_seq_num NUMBER;
--   v_d_order_qty NUMBER;
--   v_total_qty_avail NUMBER;
	 v_xml_amp	 CONSTANT VARCHAR2(5) := ';';
	 v_amp		 CONSTANT VARCHAR2(1) := '&';

   /*WORKLIST TABLE*/
   CURSOR cur_worklist(v_itm_nbr IN VARCHAR2) IS
      SELECT WL_KEY--, UOM_QTY
      FROM   WORKLIST
       --WHERE  STATUS_CODE = 10
      WHERE  STATUS_CODE not in(40,20)
      AND    ITM_NBR = v_itm_nbr;



--   /*WL_BULK TABLE*/
--   CURSOR cur_wl_bulk(v_wl_key IN NUMBER) IS
--      SELECT SKU_NBR, COLOR_NBR, SIZE_NBR, QTY_AVAIL, PO_LN_SEQ_NUM, ORDER_QTY
--      FROM   WL_BULK
--      WHERE  WL_KEY = v_wl_key;
--
--   /*WL_MULTIPLE TABLE*/
--	 CURSOR cur_wl_multiple(v_wl_key IN NUMBER) IS
--      SELECT SKU_NBR, COLOR_NBR, SIZE_NBR, QTY_AVAIL, PO_LN_SEQ_NUM, ORDER_QTY
--      FROM   WL_MULTIPLE
--      WHERE  WL_KEY = v_wl_key;

 --   /*Sum of all qty_avail from WL_BULK TABLE*/
--	 CURSOR cr_WL_BULK_TOTAL_QTY_AVAIL(temp_wl_key IN NUMBER) IS
--      SELECT SUM(QTY_AVAIL)
--      FROM   WL_BULK
--      WHERE  WL_KEY = temp_wl_key;
--
--   /*Sum of all qty_avail from WL_MULTIPLE TABLE*/
--   CURSOR cr_WL_MULTIPLE_TOTAL_QTY_AVAIL(temp_wl_key IN NUMBER) IS
--      SELECT SUM(QTY_AVAIL)
--      FROM   WL_MULTIPLE
--      WHERE  WL_KEY = temp_wl_key;
BEGIN
  FOR cur_rec IN cur_worklist(p_itm_nbr) LOOP
      v_wl_key := cur_rec.WL_KEY;
		  -- Update the worklist table --
		  UPDATE WORKLIST
      SET       -- MERCH_ID_TYPE = v_merch_id_type,
			      -- AVAIL_QTY = v_total_qty_avail,
			       COST_PRICE = p_cost_price,
			       RETAIL_PRICE = p_retail_price,
			      -- UDF1 = REPLACE(p_udf1, v_xml_amp, v_amp),
			      -- UDF2 = REPLACE(p_udf2, v_xml_amp, v_amp),
			      -- UDF3 = REPLACE(p_udf3, v_xml_amp, v_amp),
			      -- UDF4 = REPLACE(p_udf4, v_xml_amp, v_amp),
			      -- UDF5 = p_udf5,
			      -- UDF6 = p_udf6,
			      -- REPORT_GROUP = p_report_group,
			       ITM_DESC1 = REPLACE(p_itm_desc1, v_xml_amp, v_amp),
			       --ITM_DESC2 = REPLACE(p_itm_desc2, v_xml_amp, v_amp),
			       CLASS_NBR = p_class_nbr,
			       CLASS_DESC = p_class_desc,
			       SUBCLASS_NBR = p_subclass_nbr,
			       SUBCLASS_DESC = p_subclass_desc,
			       DEPT_NBR = p_dept_nbr,
			       DEPT_DESC = p_dept_desc,
			       UOM_CD = p_uom_cd,
			       UOM_QTY = p_uom_qty,
				   UDF29 = substr(REPLACE(p_udf29, v_xml_amp, v_amp),1,8),
				   UDF30 = substr(REPLACE(p_udf30, v_xml_amp, v_amp),1,8),
				 --  UDF72 = REPLACE(p_udf72, v_xml_amp, v_amp),
				  -- UDF73 = REPLACE(p_udf73, v_xml_amp, v_amp),
				   --UDF74 = REPLACE(p_udf74, v_xml_amp, v_amp),
				   --UDF75 = REPLACE(p_udf75, v_xml_amp, v_amp),
				  -- UDF76 = REPLACE(p_udf76, v_xml_amp, v_amp),
				   UDF77 = substr( REPLACE(p_udf77, v_xml_amp, v_amp),1,8),
				   UDF78 = substr(REPLACE(p_udf78, v_xml_amp, v_amp),1,8),
				   UDF79 = substr(REPLACE(p_udf79, v_xml_amp, v_amp),1,8),
				   UDF80 = substr(REPLACE(p_udf80, v_xml_amp, v_amp),1,8),
				   --UDF81 = REPLACE(p_udf81, v_xml_amp, v_amp),
				  -- UDF82 = REPLACE(p_udf82, v_xml_amp, v_amp),
				   UDF83 = substr(REPLACE(p_udf83, v_xml_amp, v_amp),1,8),
				   --UDF84 = REPLACE(p_udf84, v_xml_amp, v_amp),
				   --UDF85 = REPLACE(p_udf85, v_xml_amp, v_amp),
				   --UDF86 = REPLACE(p_udf86, v_xml_amp, v_amp),
				   UDF87 = substr(REPLACE(p_udf87, v_xml_amp, v_amp),1,8),
				   --UDF88 = REPLACE(p_udf88, v_xml_amp, v_amp),
				  -- UDF89 = REPLACE(p_udf89, v_xml_amp, v_amp),
				  -- UDF90 = REPLACE(p_udf90, v_xml_amp, v_amp),
				   --UDF91 = REPLACE(p_udf91, v_xml_amp, v_amp),
				  -- UDF92 = REPLACE(p_udf92, v_xml_amp, v_amp),
				   --UDF93 = REPLACE(p_udf93, v_xml_amp, v_amp),
				  -- UDF94 = REPLACE(p_udf94, v_xml_amp, v_amp),
				   UDF95 = substr(REPLACE(p_udf95, v_xml_amp, v_amp),1,8)--,
				  -- UDF96 = REPLACE(p_udf96, v_xml_amp, v_amp),
				   --UDF97 = REPLACE(p_udf97, v_xml_amp, v_amp),
				  -- UDF98 = REPLACE(p_udf98, v_xml_amp, v_amp),
				   --UDF99 = REPLACE(p_udf99, v_xml_amp, v_amp)
		  WHERE  WL_KEY = v_wl_key;
	END LOOP;

   -- Error handling --
	 EXCEPTION
	    WHEN OTHERS THEN
			   RAISE_APPLICATION_ERROR(-20100,SQLERRM);
END;
/