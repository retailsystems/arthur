using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class UserLogin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        txtUserName.Focus();
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {

            User user = new User();
            ProcessUserLogin processlogin = new ProcessUserLogin();

            user.UserName = txtUserName.Text.Trim();
            user.Password = txtPassword.Text.Trim();

            processlogin.User = user;
            
            try
            {
                processlogin.Invoke();

                if (processlogin.IsAuthenticated)
                {
                    Session["User"] = user;
                    Response.Redirect("Default.aspx");
                }
                else
                {
                    lblError.Text = "Invalid login. Please try again!";
                    lblError.Visible = true;
                }
            }
            catch(Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }
    }
}
