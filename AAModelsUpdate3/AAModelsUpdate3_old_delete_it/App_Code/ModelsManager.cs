using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using HotTopic.DCSS.Services;
using System.Data.OracleClient;
using HottopicUtilities;



/// <summary>
/// Summary description for ModelsManager
/// </summary>
public class ModelsManager
{
    private string _ArthurConStr = "AA_ConStr";
    private OracleConnection _oraCon;

    //public ModelsManager()
    //{
    //}

    public string ArthurConStr
    {
        get { return _ArthurConStr; }
    }

    public void UpdateModels(DataTable dTable)
    // This function uploads excel data passed via a data table and inserts
    // into a working temp table in Arthur.  It first issues a truncate table
    // command to clear out the temp table and then loops through each data row 
    // in the passed data table and inserts to the Oracle temp table in Arthur.
    // Finally, it calls an oracle package HT_MODEL_UPDATE.PROCESS_MODEL_DATA
    // to process the imported data and finalize the upload.  -- kjz 3/11/09
    {
        if (dTable != null)
        {
            //Build call for Oracle package
            StringBuilder ProcCallCmd = new StringBuilder();
            ProcCallCmd.Append(ConfigurationManager.AppSettings["OraclePkg"]);

            //Build second command object for truncate
            OracleCommand cmd2 = new OracleCommand();
            cmd2.CommandType = CommandType.Text;

            // Truncate temp table in Arthur to prep for new data upload
            StringBuilder TruncateCmd = new StringBuilder();            
            TruncateCmd.Append(ConfigurationManager.AppSettings["TruncCmd"]);
            cmd2.CommandText = TruncateCmd.ToString();
            ExecuteOracleSQL(cmd2);
            
            foreach (DataRow row in dTable.Rows)
            {
                // bypass first 8 rows. ADO.NET scans the first 8 rows of data, and based on that 
                // guesses the datatype for each column. Then it attempts to coerce all data from
                // that column to that datatype, returning NULL whenever the coercion fails!
                // Thus, we've loaded the excel sheet with 8 rows of string data to trick the scan
                // into setting the data type to string so we don't loose any data.  --kjz 
                if (Convert.ToString(row["Model_name"]) != Convert.ToString(ConfigurationManager.AppSettings["ConfigRow"]))                
                {
                    StringBuilder insertFields = new StringBuilder();
                    StringBuilder insertValues = new StringBuilder();
                    StringBuilder insertSQL = new StringBuilder();
                    StringBuilder querySQL = new StringBuilder();

                    //insertFields.Append("INSERT INTO HT_UPDATE_MODELS_TMP (MODEL_NAME");                  
                    insertFields.Append("INSERT INTO " + Convert.ToString(ConfigurationManager.AppSettings["StagingTable"])+ " (MODEL_NAME");
                    insertValues.Append(" VALUES ( :MODEL_NAME ");

                    OracleParameter modelParm = new OracleParameter("MODEL_NAME", OracleType.VarChar);
                    modelParm.Direction = ParameterDirection.Input;
                    modelParm.Size = 50;
                    modelParm.SourceVersion = DataRowVersion.Current;

                    OracleParameter level1Parm;
                    OracleParameter level2Parm;
                    OracleParameter level3Parm;
                    OracleParameter level4Parm;
                    OracleParameter level5Parm;
                    OracleParameter locationIDParm;
                    OracleParameter thresholdParm;
                    OracleParameter targetParm;

                    OracleCommand cmd = new OracleCommand();
                    cmd.CommandType = CommandType.Text;

                    modelParm.Value = row["Model_name"].ToString().Trim();

                    cmd.Parameters.Add(modelParm);

                    if (row["Level_1"] != Convert.DBNull)
                    {
                        level1Parm = new OracleParameter("LEVEL_1", OracleType.VarChar);
                        level1Parm.Direction = ParameterDirection.Input;
                        level1Parm.SourceVersion = DataRowVersion.Current;
                        level1Parm.Value = row["Level_1"].ToString().Trim();
                        cmd.Parameters.Add(level1Parm);
                        insertFields.Append(", LEVEL_1");
                        insertValues.Append(", :LEVEL_1");
                        querySQL.Append(" AND LEVEL_1 = :LEVEL_1");
                    }

                    if (row["Level_2"] != Convert.DBNull)
                    {
                        level2Parm = new OracleParameter("LEVEL_2", OracleType.VarChar);
                        level2Parm.Direction = ParameterDirection.Input;
                        level2Parm.SourceVersion = DataRowVersion.Current;
                        level2Parm.Value = row["Level_2"].ToString().Trim();
                        cmd.Parameters.Add(level2Parm);
                        insertFields.Append(", LEVEL_2");
                        insertValues.Append(", :LEVEL_2");
                        querySQL.Append(" AND LEVEL_2 = :LEVEL_2");
                    }

                    if (row["Level_3"] != Convert.DBNull)
                    {
                        level3Parm = new OracleParameter("LEVEL_3", OracleType.VarChar);
                        level3Parm.Direction = ParameterDirection.Input;
                        level3Parm.SourceVersion = DataRowVersion.Current;
                        level3Parm.Value = row["Level_3"].ToString().Trim();
                        cmd.Parameters.Add(level3Parm);
                        insertFields.Append(", LEVEL_3");
                        insertValues.Append(", :LEVEL_3");
                        querySQL.Append(" AND LEVEL_3 = :LEVEL_3");
                    }

                    if (row["Level_4"] != Convert.DBNull)
                    {
                        level4Parm = new OracleParameter("LEVEL_4", OracleType.VarChar);
                        level4Parm.Direction = ParameterDirection.Input;
                        level4Parm.SourceVersion = DataRowVersion.Current;
                        level4Parm.Value = row["Level_4"].ToString().Trim();
                        cmd.Parameters.Add(level4Parm);
                        insertFields.Append(", LEVEL_4");
                        insertValues.Append(", :LEVEL_4");
                        querySQL.Append(" AND LEVEL_4 = :LEVEL_4");
                    }

                    if (row["Level_5"] != Convert.DBNull)
                    {
                        level5Parm = new OracleParameter("LEVEL_5", OracleType.VarChar);
                        level5Parm.Direction = ParameterDirection.Input;
                        level5Parm.SourceVersion = DataRowVersion.Current;
                        level5Parm.Value = row["Level_5"].ToString().Trim();
                        cmd.Parameters.Add(level5Parm);
                        insertFields.Append(", LEVEL_5");
                        insertValues.Append(", :LEVEL_5");
                        querySQL.Append(" AND LEVEL_5 = :LEVEL_5");
                    }
                    if (row["Location_id"] != Convert.DBNull)
                    {
                        locationIDParm = new OracleParameter("LOCATION_ID", OracleType.Number);
                        locationIDParm.Direction = ParameterDirection.Input;
                        locationIDParm.SourceVersion = DataRowVersion.Current;
                        locationIDParm.Value = row["Location_id"].ToString();
                        cmd.Parameters.Add(locationIDParm);
                        insertFields.Append(", LOCATION_ID");
                        insertValues.Append(", :LOCATION_ID");
                        querySQL.Append(" AND LOCATION_ID = :LOCATION_ID");
                    }

                    if (row["Threshold"] != Convert.DBNull)
                    {
                        thresholdParm = new OracleParameter("THRESHOLD", OracleType.Number);
                        thresholdParm.Direction = ParameterDirection.Input;
                        thresholdParm.SourceVersion = DataRowVersion.Current;
                        thresholdParm.Value = row["Threshold"].ToString();
                        cmd.Parameters.Add(thresholdParm);
                        insertFields.Append(", THRESHOLD");
                        insertValues.Append(", :THRESHOLD");
                        querySQL.Append(" AND THRESHOLD = :THRESHOLD");
                    }

                    if (row["Target"] != Convert.DBNull)
                    {
                        targetParm = new OracleParameter("TARGET", OracleType.Number);
                        targetParm.Direction = ParameterDirection.Input;
                        targetParm.SourceVersion = DataRowVersion.Current;
                        targetParm.Value = row["Target"].ToString();
                        cmd.Parameters.Add(targetParm);
                        insertFields.Append(", TARGET");
                        insertValues.Append(", :TARGET");
                        querySQL.Append(" AND TARGET = :TARGET");
                    }

                    insertSQL.Append(insertFields.Append(" )"));
                    insertSQL.Append(insertValues.Append(" )"));

                    //Insert row to Oracle temp table
                    cmd.CommandText = insertSQL.ToString();
                    ExecuteOracleSQL(cmd);
                }
            } //** End of data loop **

            //Initiate Proc call
            cmd2.CommandText = ProcCallCmd.ToString();
            ExecuteOracleSQL(cmd2);
        }
    }



    // **************** 03-MAR-09 OLD CODE BEFORE UPDATE ***************
    //public void UpdateModels(DataTable dTable)
    //{
    //    bool hasNewRecord = false;
    //    if (dTable != null)
    //    {
    //        foreach (DataRow row in dTable.Rows)
    //        {
    //            StringBuilder insertFields = new StringBuilder();
    //            StringBuilder insertValues = new StringBuilder();
    //            StringBuilder insertSQL = new StringBuilder();
    //            StringBuilder querySQL = new StringBuilder();

    //            querySQL.Append("SELECT COUNT(*) FROM MD$IMPORT_DATA WHERE MODEL_NAME = :MODEL_NAME");
    //            insertFields.Append("INSERT INTO MD$IMPORT_DATA (MODEL_NAME");
    //            insertValues.Append(" VALUES ( :MODEL_NAME ");

    //            OracleParameter modelParm = new OracleParameter("MODEL_NAME", OracleType.VarChar);
    //            modelParm.Direction = ParameterDirection.Input;
    //            modelParm.Size = 50;
    //            modelParm.SourceVersion = DataRowVersion.Current;
                
    //            OracleParameter level1Parm;
    //            OracleParameter level2Parm;
    //            OracleParameter level3Parm;
    //            OracleParameter level4Parm;
    //            OracleParameter level5Parm;
    //            OracleParameter locationIDParm;
    //            OracleParameter thresholdParm;
    //            OracleParameter targetParm;

    //            OracleCommand cmd = new OracleCommand();
    //            cmd.CommandType = CommandType.Text;

    //            modelParm.Value = row["Model_name"].ToString().Trim();
               
    //            cmd.Parameters.Add(modelParm);

    //            if (row["Level_1"] != Convert.DBNull)
    //            {
    //                level1Parm = new OracleParameter("LEVEL_1", OracleType.VarChar);
    //                level1Parm.Direction = ParameterDirection.Input;
    //                level1Parm.SourceVersion = DataRowVersion.Current;
    //                level1Parm.Value = row["Level_1"].ToString().Trim();
    //                cmd.Parameters.Add(level1Parm);
    //                insertFields.Append(", LEVEL_1");
    //                insertValues.Append(", :LEVEL_1");

    //                querySQL.Append(" AND LEVEL_1 = :LEVEL_1");
    //            }

    //            if (row["Level_2"] != Convert.DBNull)
    //            {
    //                level2Parm = new OracleParameter("LEVEL_2", OracleType.VarChar);
    //                level2Parm.Direction = ParameterDirection.Input;
    //                level2Parm.SourceVersion = DataRowVersion.Current;
    //                level2Parm.Value = row["Level_2"].ToString().Trim();
    //                cmd.Parameters.Add(level2Parm);
    //                insertFields.Append(", LEVEL_2");
    //                insertValues.Append(", :LEVEL_2");

    //                querySQL.Append(" AND LEVEL_2 = :LEVEL_2");
    //            }

    //            if (row["Level_3"] != Convert.DBNull)
    //            {
    //                level3Parm = new OracleParameter("LEVEL_3", OracleType.VarChar);
    //                level3Parm.Direction = ParameterDirection.Input;
    //                level3Parm.SourceVersion = DataRowVersion.Current;
    //                level3Parm.Value = row["Level_3"].ToString().Trim();
    //                cmd.Parameters.Add(level3Parm);
    //                insertFields.Append(", LEVEL_3");
    //                insertValues.Append(", :LEVEL_3");

    //                querySQL.Append(" AND LEVEL_3 = :LEVEL_3");
    //            }

    //            if (row["Level_4"] != Convert.DBNull)
    //            {
    //                level4Parm = new OracleParameter("LEVEL_4", OracleType.VarChar);
    //                level4Parm.Direction = ParameterDirection.Input;
    //                level4Parm.SourceVersion = DataRowVersion.Current;
    //                level4Parm.Value = row["Level_4"].ToString().Trim();
    //                cmd.Parameters.Add(level4Parm);
    //                insertFields.Append(", LEVEL_4");
    //                insertValues.Append(", :LEVEL_4");

    //                querySQL.Append(" AND LEVEL_4 = :LEVEL_4");
    //            }

    //            if (row["Level_5"] != Convert.DBNull)
    //            {
    //                level5Parm = new OracleParameter("LEVEL_5", OracleType.VarChar);
    //                level5Parm.Direction = ParameterDirection.Input;
    //                level5Parm.SourceVersion = DataRowVersion.Current;
    //                level5Parm.Value = row["Level_5"].ToString().Trim();
    //                cmd.Parameters.Add(level5Parm);
    //                insertFields.Append(", LEVEL_5");
    //                insertValues.Append(", :LEVEL_5");

    //                querySQL.Append(" AND LEVEL_5 = :LEVEL_5");
    //            }
    //            if (row["Location_id"] != Convert.DBNull)
    //            {
    //                locationIDParm = new OracleParameter("LOCATION_ID", OracleType.Number);
    //                locationIDParm.Direction = ParameterDirection.Input;
    //                locationIDParm.SourceVersion = DataRowVersion.Current;
    //                locationIDParm.Value = row["Location_id"].ToString();
    //                cmd.Parameters.Add(locationIDParm);

    //                insertFields.Append(", LOCATION_ID");
    //                insertValues.Append(", :LOCATION_ID");

    //                querySQL.Append(" AND LOCATION_ID = :LOCATION_ID");
    //            }

    //            if (row["Threshold"] != Convert.DBNull)
    //            {
    //                thresholdParm = new OracleParameter("THRESHOLD", OracleType.Number);
    //                thresholdParm.Direction = ParameterDirection.Input;
    //                thresholdParm.SourceVersion = DataRowVersion.Current;
    //                thresholdParm.Value = row["Threshold"].ToString();
    //                cmd.Parameters.Add(thresholdParm);

    //                insertFields.Append(", THRESHOLD");
    //                insertValues.Append(", :THRESHOLD");

    //                querySQL.Append(" AND THRESHOLD = :THRESHOLD");
    //            }

    //            if (row["Target"] != Convert.DBNull)
    //            {
    //                targetParm = new OracleParameter("TARGET", OracleType.Number);
    //                targetParm.Direction = ParameterDirection.Input;
    //                targetParm.SourceVersion = DataRowVersion.Current;
    //                targetParm.Value = row["Target"].ToString();
    //                cmd.Parameters.Add(targetParm);

    //                insertFields.Append(", TARGET");
    //                insertValues.Append(", :TARGET");

    //                querySQL.Append(" AND TARGET = :TARGET");
    //            }

    //            insertSQL.Append(insertFields.Append(" )"));
    //            insertSQL.Append(insertValues.Append(" )"));

    //            cmd.CommandText = querySQL.ToString();

    //            if (!QueryModel(cmd))
    //            {
    //                cmd.CommandText = insertSQL.ToString();
    //                Update(cmd);
    //                hasNewRecord = true;
    //            }
    //        }//End for loop

    //        //If there is any new models to be updated, then create key(s)
    //        if(hasNewRecord)
    //        {
    //            GenerateModel_Key();
    //        }
    //    }
    //}
    //**************************** END OF OLD CODE COMMENT BLOCK *****************



    private void ExecuteOracleSQL(OracleCommand cmd)
    // Initiate Oracle connection and process SQL or package
    {
        _oraCon = DBConnectionManager.GetConnection(this.ArthurConStr);
        try
        {
            cmd.Connection = _oraCon;
            cmd.ExecuteNonQuery();
        }
        catch (Exception)
        {
           throw;
        }
        finally
        {
            _oraCon.Close();
        }
    }

    private void GenerateModel_Key()
    // This function calls Arthur Package that generates keys in the 
    // import table and assigns them to the new lines  --kjz
    {
        _oraCon = DBConnectionManager.GetConnection(this.ArthurConStr);
        OracleCommand cmd = _oraCon.CreateCommand();
        try
        {
            cmd.Connection = _oraCon;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "ALLOCMODELS.UPDATEIMPORTDATA";
            cmd.ExecuteNonQuery();
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            _oraCon.Close();
        }
    }


    public void DeleteModel(String modelName)
    {
        if (!String.IsNullOrEmpty(modelName))
        {
            string sql = "DELETE FROM MD$IMPORT_DATA WHERE MODEL_NAME = :MODEL_NAME";

            OracleParameter modelParm = new OracleParameter("MODEL_NAME", OracleType.VarChar);
            modelParm.Direction = ParameterDirection.Input;
            modelParm.Size = 50;
            modelParm.Value = modelName.Trim();
            modelParm.SourceVersion = DataRowVersion.Current;

            OracleCommand cmd = new OracleCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = sql;

            cmd.Parameters.Add(modelParm);

            try
            {
                ExecuteOracleSQL(cmd);
                GenerateModel_Key();
            }
            catch (Exception)
            {
                throw;
            }
           
        }
    }
    public bool QueryModel(OracleCommand cmd)
    {
        bool exist = false;
        int count = 0;
        _oraCon = DBConnectionManager.GetConnection(this.ArthurConStr);
        try
        {
            cmd.Connection = _oraCon;
            object result = cmd.ExecuteScalar();

            if (!(result is System.DBNull))
                count = int.Parse(result.ToString());
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            _oraCon.Close();
        }
        return exist = count > 0? true: false;
    }
    public DataSet QueryModelName()
    {
        string sql = "SELECT DISTINCT(MODEL_NAME) FROM MD$IMPORT_DATA ORDER BY MODEL_NAME DESC";
        _oraCon = DBConnectionManager.GetConnection("AA_ConStr");
        DataSet ds = new DataSet();
        OracleDataAdapter da;
        try
        {
            da = new OracleDataAdapter(sql, _oraCon);
            da.Fill(ds);
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            _oraCon.Close();
        }
        return ds;
    }
}
