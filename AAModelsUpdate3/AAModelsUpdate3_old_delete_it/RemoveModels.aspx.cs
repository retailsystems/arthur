using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using HottopicUtilities;

public partial class RemoveModels : System.Web.UI.Page
{
    private LogUtil fLog;
    protected void Page_Load(object sender, EventArgs e)
    {
        //Commenting the below user section not to use the screen - by Vijay
        //if (Session["User"] == null)
        //    Response.Redirect("UserLogin.aspx");
        if (!IsPostBack)
        {
            fLog = (LogUtil)Application["flLogFile"];
            LoadModelName();
            btnRemove.Attributes.Add("OnClick", "return confirm('Are you sure to delete this model?');");
        }
    }
    protected void btnRemove_Click(object sender, EventArgs e)
    {
        ModelsManager modelManager = new ModelsManager();
        if (lstModelName.SelectedItem != null)
        {
            string delModel = lstModelName.SelectedItem.Value;
            try
            {
                if (!String.IsNullOrEmpty(delModel))
                    modelManager.DeleteModel(delModel);

                LoadModelName();
            }
            catch (Exception ex)
            {
                lblError.Text = "Error deleting model: " + delModel + ", please try again!";
                fLog.WriteItem(ErrorHandler.GetErrorMessage(ex));
            }
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
    private void LoadModelName()
    {
        DataSet ds;
        ModelsManager modelManager = new ModelsManager();
        try
        {
            ds = modelManager.QueryModelName();
            if (ds.Tables.Count > 0)
            {
                lstModelName.DataSource = ds;
                lstModelName.DataTextField = "MODEL_NAME";
                lstModelName.DataValueField = "MODEL_NAME";
                lstModelName.DataBind();
            }
        }
        catch (Exception ex)
        {
            fLog.WriteItem(ErrorHandler.GetErrorMessage(ex));
            lblError.Text = "Error deleting models, please try again later!";
        }
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        Response.Write("<script language='javascript'> { window.close();}</script>");
    }
}
