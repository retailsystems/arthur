<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserLogin.aspx.cs" Inherits="UserLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>AA Models Update User Login</title>
    <link href="Styles2.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <!-- #include file="include/Header.inc" -->
    <form id="frmUserLogin" runat="server">
		    <table cellpadding="0" cellspacing="0" width="777" border="0">
		    <tr>
				<td style="height:20" colspan="2"></td>
			</tr>
			<tr>
				<td colspan="2">
					<table id="messageDisplay" style="WIDTH: 763px; HEIGHT: 30px">
						<tr>
							<td align="center" colspan="2">
								<!--<asp:validationsummary id="vldSummary" EnableClientScript="False" runat="server" width="266px"
									displaymode="List" CssClass="errStyle"></asp:validationsummary>-->
							</td>
						</tr>
						<tr>
							<td align="center" colspan="2">
								<asp:Label id="lblError" Runat="server" Visible="True" CssClass="errStyle" Height="20px"></asp:Label></td>
						</tr>
					</table>
				</td>
			</tr>
		    <tr>
		        <td align="right" class="cellvaluecaption" style="width:50%"></td>
		        <td style="height:40; text-align:left" align="left"><h4>Login</h4></td>
		    </tr>
		    <tr>
		        <td align="right" class="cellvaluecaption" style="width:50%">Username:</td>
		        <td><asp:TextBox ID="txtUserName" runat="server" Width="100"></asp:TextBox>
		            <asp:RequiredFieldValidator ID="rqvUserName" ControlToValidate="txtUserName" runat="server" Display="Dynamic" EnableClientScript="false" ErrorMessage="Please enter user name" ></asp:RequiredFieldValidator></td>
		    </tr>
		    <tr>
		        <td align="right" class="cellvaluecaption" style="width:50%">Password:</td>
		        <td><asp:TextBox ID="txtPassword" TextMode="Password" runat="server" Width="100"></asp:TextBox>
		            <asp:RequiredFieldValidator ID="rqvPassword" ControlToValidate="txtPassword" runat="server" Display="Dynamic"  EnableClientScript="false" ErrorMessage="Please enter password"></asp:RequiredFieldValidator></td>
		    </tr>
		    <tr>
		        <td align="right" class="cellvaluecaption" style="width:50%"></td>
		        <td align="left"><asp:Button ID="btnSubmit" runat="server" CssClass="btnSmall" Text="Login" OnClick="btnSubmit_Click" /></td>
		    </tr>
		</table>
    </form>
</body>
</html>
