using System;
using System.Data;
using System.Data.OracleClient;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for DBConnectionManager
/// </summary>
public class DBConnectionManager
{
	public DBConnectionManager()
	{
	}
    public static OracleConnection GetConnection(string strDB)
    {
        OracleConnection con = null;
        if(!String.IsNullOrEmpty(strDB))
        {
            try
            {
                string conStr = ConfigurationManager.ConnectionStrings[strDB.Trim()].ToString();
                con = new OracleConnection(conStr);
                con.Open();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        return con;
    }
}
