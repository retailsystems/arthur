using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Data.OracleClient;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for ProcessUserLogin
/// </summary>
public class ProcessUserLogin
{
    private User _user;

    public User User
    {
        get { return _user; }
        set { _user = value; }
    }
    private bool _isauthenticated;

    public bool IsAuthenticated
    {
        get { return _isauthenticated; }
        set { _isauthenticated = value; }
    }
    private DataSet _dataSet;

    public DataSet DataSet
    {
        get { return _dataSet; }
        set { _dataSet = value; }
    } 
	public ProcessUserLogin()
	{
	}
    public void Invoke()
    {
        OracleConnection oracleConnection = null;
        OracleCommand oracleCmd = null;
        try
        {
            oracleConnection = new OracleConnection(ConfigurationManager.ConnectionStrings["AA_ConStr"].ToString());
            oracleCmd = new OracleCommand("HT_VALIDATE_USER", oracleConnection);
            oracleCmd.CommandType = CommandType.StoredProcedure;
            OracleParameter[] oracleParameterArray = new OracleParameter[4];
            oracleParameterArray[0] = new OracleParameter("p_userid", OracleType.VarChar, 30);
            oracleParameterArray[0].Value = (object)this.User.UserName.ToUpper();
            oracleParameterArray[1] = new OracleParameter("p_password", OracleType.VarChar, 30);
            oracleParameterArray[1].Value = (object)this.User.Password.ToUpper();
            oracleParameterArray[2] = new OracleParameter("p_validflag", OracleType.VarChar, 5);
            oracleParameterArray[2].Direction = ParameterDirection.Output;
            oracleParameterArray[3] = new OracleParameter("p_fullname", OracleType.VarChar, 100);
            oracleParameterArray[3].Direction = ParameterDirection.Output;            
            oracleCmd.Parameters.Add(oracleParameterArray[0]);
            oracleCmd.Parameters.Add(oracleParameterArray[1]);
            oracleCmd.Parameters.Add(oracleParameterArray[2]);
            oracleCmd.Parameters.Add(oracleParameterArray[3]);
            oracleConnection.Open();
            oracleCmd.ExecuteNonQuery();
            if (oracleParameterArray[2].Value.ToString().ToUpper() != "TRUE")
                return;
            else
                IsAuthenticated = true;

            oracleCmd.Dispose();
            oracleConnection.Close();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        finally
        {
            if (oracleCmd != null)
            {
                oracleCmd.Dispose();
                oracleCmd = null;
            }
            if (oracleConnection != null)
            {
                oracleConnection.Close();
                oracleConnection = null;
            }
        }
        //string sql = "SELECT USER_ID FROM USERS WHERE UPPER(USER_ID) = :USER_ID AND UPPER(PASSWORD) = :PASSWORD AND SYSADMIN_FLAG = 'Y'";

        //OracleParameter userIDParm = new OracleParameter("USER_ID", OracleType.VarChar);
        //userIDParm.Direction = ParameterDirection.Input;
        //userIDParm.Size = 30;
        //userIDParm.Value = User.UserName.ToUpper();
        //userIDParm.SourceVersion = DataRowVersion.Current;

        //OracleParameter passwordParm = new OracleParameter("PASSWORD", OracleType.VarChar);
        //passwordParm.Direction = ParameterDirection.Input;
        //passwordParm.Size = 30;
        //passwordParm.Value = User.Password.ToUpper();
        //passwordParm.SourceVersion = DataRowVersion.Current;

        //using (OracleConnection con = DBConnectionManager.GetConnection("AA_ConStr"))
        //{
        //    using (OracleCommand cmd = new OracleCommand(sql.ToString(), con))
        //    {
        //        cmd.Parameters.Add(userIDParm);
        //        cmd.Parameters.Add(passwordParm);
        //        object result = cmd.ExecuteScalar();

        //        if (result != null)
        //            IsAuthenticated = true;
        //    }
        //}

    }
}
