﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Main</title>
    <link href="Styles2.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align:center">
        <!-- #include file="include/Header.inc" -->
        <table>
            <tbody>
                <tr>
                    <th style="color: Red; font-size:x-large;"><b>Arthur Models Update</b></th>
                </tr>
                <tr>
                    <td><b>Actions:</b>
                    <ul>
                        <li>Click on Upload Models button will take you to the updating model page</li>
                        <li>Click on Remove Models button will take you to the removing model page</li>
                    </ul>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Button ID="btnExit" runat="server" CssClass="btnSmall" OnClick="btnExit_Click" Text="Close" />
                        <asp:Button ID="btnUpload" runat="server" CssClass="btnSmall" OnClick="btnUpload_Click" Text="Upload Models" />
                        <asp:Button ID="btnDelete" runat="server" CssClass="btnSmall" OnClick="btnDelete_Click" Text="Remove Models" /></td>
                </tr>
            </tbody>
        </table>
      </div>
    </form>
</body>
</html>
