
/*
INSERT INTO KJZ_BACKUP_IMPORT_DATA_TABLE
SELECT *
from MD$IMPORT_DATA 
*/

select *
from KJZ_BACKUP_IMPORT_DATA_TABLE


select *
from MD$IMPORT_DATA
where model_key in ('48','49')



INSERT INTO HT_UPDATE_MODELS_TMP
select *
from MD$IMPORT_DATA
where model_key in ('4','48','49')


INSERT INTO HT_UPDATE_MODELS_TMP
values(48,'Clear_Saddle','AA','BB','CC','','',-1,5,'')

INSERT INTO HT_UPDATE_MODELS_TMP
values(4,'Low_Chuck','99','88','77','','',-1,5,'')

delete from HT_UPDATE_MODELS_TMP 
where model_key = 48
and model_name = 'Low_Chuck' 


select *
from HT_UPDATE_MODELS_TMP
order by model_key


update HT_UPDATE_MODELS_TMP
set LEVEL_3 = 'EXST'
where Model_key = 48
and LEVEL_1 = '2'
and location_id = -1
and threshold = 3


update MD$IMPORT_DATA
set LEVEL_3 = NULL
where Model_key = 48
and LEVEL_1 = '00'
and location_id = -1
and threshold = 2

--desc MD$IMPORT_DATA





select *
from HT_UPDATE_MODELS_TMP


select *
from MD$IMPORT_DATA
where model_key in ('4','48','49')




    SELECT  T2.MODEL_KEY,
            T2.MODEL_NAME,
            T2.LEVEL_1            
    --select t1.*, t2.*            
    FROM    MD$IMPORT_DATA t1,
            HT_UPDATE_MODELS_TMP t2

    WHERE   t1.MODEL_KEY(+)    = t2.MODEL_KEY
    AND     t1.MODEL_NAME(+)   = T2.MODEL_NAME
    AND     t1.LEVEL_1(+)      = T2.LEVEL_1
    --AND     t1.LEVEL_2      = T2.LEVEL_2
    --AND     t1.LEVEL_3      = T2.LEVEL_3
    --AND     t1.LEVEL_4      = T2.LEVEL_4
    --AND     t1.LEVEL_5      = T2.LEVEL_5
    AND     t1.LOCATION_ID(+)  = T2.LOCATION_ID
    AND     t1.THRESHOLD(+)    = T2.THRESHOLD
    AND     NVL(t1.TARGET(+),0) = NVL(T2.TARGET,0)
    order by T1.MODEL_KEY
    
    
    
    select t1.*, t2.*            
    FROM    MD$IMPORT_DATA t1,
            HT_UPDATE_MODELS_TMP t2
    WHERE   t1.MODEL_KEY    = t2.MODEL_KEY(+)
    AND     t1.MODEL_NAME   = T2.MODEL_NAME(+)
    AND     t1.LEVEL_1      = T2.LEVEL_1(+)
    --AND     t1.LEVEL_2      = T2.LEVEL_2
    --AND     t1.LEVEL_3      = T2.LEVEL_3
    --AND     t1.LEVEL_4      = T2.LEVEL_4
    --AND     t1.LEVEL_5      = T2.LEVEL_5
    AND     t1.LOCATION_ID  = T2.LOCATION_ID(+)
    AND     t1.THRESHOLD    = T2.THRESHOLD(+)
    AND     NVL(t1.TARGET,0) = NVL(T2.TARGET(+),0)
    order by T1.MODEL_KEY
    
     
    
    
    
    SELECT  T2.MODEL_KEY,
            T2.MODEL_NAME,
            T2.LEVEL_1             
    FROM    MD$IMPORT_DATA t1,
            HT_UPDATE_MODELS_TMP t2    
    WHERE   t1.MODEL_KEY    = t2.MODEL_KEY
    AND     t1.MODEL_NAME   = t2.MODEL_NAME
    AND     t1.LEVEL_1      = t2.LEVEL_1
    --AND     t1.LEVEL_2      = T2.LEVEL_2
    --AND     t1.LEVEL_3      = T2.LEVEL_3
    --AND     t1.LEVEL_4      = T2.LEVEL_4
    --AND     t1.LEVEL_5      = T2.LEVEL_5
    AND     t1.LOCATION_ID  = t2.LOCATION_ID
    AND     t1.THRESHOLD    = t2.THRESHOLD
    AND     NVL(t1.TARGET,0) = NVL(t2.TARGET,0)                            
    order by T1.MODEL_KEY
    
    
    
    
    
    
    
    
    
        INSERT INTO MD$IMPORT_DATA
        -- 1. Get all the new or updated records that exists in the tmp table
        --    but not in the import table 
            SELECT  NULL,
                    T2.MODEL_NAME,
                    T2.LEVEL_1,  
                    T2.LEVEL_2,
                    T2.LEVEL_3,
                    T2.LEVEL_4,
                    T2.LEVEL_5,
                    T2.LOCATION_ID,
                    T2.THRESHOLD,
                    T2.TARGET  
                    --T1.MODEL_NAME                      
            FROM    MD$IMPORT_DATA t1,
                    HT_UPDATE_MODELS_TMP t2
            --WHERE   t1.MODEL_KEY(+)      = t2.MODEL_KEY
            where      t1.MODEL_NAME(+)     = T2.MODEL_NAME
            --and      t1.MODEL_NAME(+)     = T2.MODEL_NAME
            AND     NVL(t1.LEVEL_1(+),0) = NVL(T2.LEVEL_1,0)
            AND     NVL(t1.LEVEL_2(+),0) = NVL(T2.LEVEL_2,0)
            AND     NVL(t1.LEVEL_3(+),0) = NVL(T2.LEVEL_3,0)
            AND     NVL(t1.LEVEL_4(+),0) = NVL(T2.LEVEL_4,0)
            AND     NVL(t1.LEVEL_5(+),0) = NVL(T2.LEVEL_5,0)
            AND     t1.LOCATION_ID(+)    = T2.LOCATION_ID
            AND     t1.THRESHOLD(+)      = T2.THRESHOLD
            AND     NVL(t1.TARGET(+),0)  = NVL(T2.TARGET,0)
            and T1.MODEL_NAME is null
            --and T1.MODEL_KEY is null
            
            
            -- Now Delete records from the $Import table
           select * 
            FROM  MD$IMPORT_DATA t3
            WHERE EXISTS
            (
            SELECT  --T1.MODEL_KEY,
                    T1.MODEL_NAME,
                    T1.LEVEL_1,  
                    T1.LEVEL_2,
                    T1.LEVEL_3,
                    T1.LEVEL_4,
                    T1.LEVEL_5,
                    T1.LOCATION_ID,
                    T1.THRESHOLD,
                    T1.TARGET        
            FROM    MD$IMPORT_DATA t1,
                    HT_UPDATE_MODELS_TMP t2
            --WHERE   t1.MODEL_KEY        = t2.MODEL_KEY(+)
            --AND     t1.MODEL_NAME       = T2.MODEL_NAME(+)
            WHERE    t1.MODEL_NAME       = T2.MODEL_NAME(+)
            AND     NVL(t1.LEVEL_1,0)   = NVL(T2.LEVEL_1(+),0)          
            AND     NVL(t1.LEVEL_2,0)   = NVL(T2.LEVEL_2(+),0)
            AND     NVL(t1.LEVEL_3,0)   = NVL(T2.LEVEL_3(+),0)
            AND     NVL(t1.LEVEL_4,0)   = NVL(T2.LEVEL_4(+),0)
            AND     NVL(t1.LEVEL_5,0)   = NVL(T2.LEVEL_5(+),0)            
            AND     t1.LOCATION_ID      = T2.LOCATION_ID(+)
            AND     t1.THRESHOLD        = T2.THRESHOLD(+)
            AND     NVL(t1.TARGET,0)    = NVL(T2.TARGET(+),0)
--            AND     t1.model_name       IN (SELECT DISTINCT Model_name
--                                           FROM   HT_UPDATE_MODELS_TMP) 
            AND     T2.MODEL_name       IS NULL     
/*
            AND     t1.model_key       IN (SELECT DISTINCT Model_key
                                           FROM   HT_UPDATE_MODELS_TMP) 
            AND     T2.MODEL_KEY       IS NULL     
*/
            /* Join back to t3 for delete */                   
            --AND     T1.MODEL_KEY        = t3.model_key
            AND     T1.MODEL_NAME       = t3.model_name
            AND     NVL(T1.LEVEL_1,0)   = NVL(t3.level_1,0)
            AND     NVL(T1.LEVEL_2,0)   = NVL(t3.level_2,0)                 
            AND     NVL(T1.LEVEL_3,0)   = NVL(t3.level_3,0)
            AND     NVL(T1.LEVEL_4,0)   = NVL(t3.level_4,0)
            AND     NVL(T1.LEVEL_5,0)   = NVL(t3.level_5,0)                        
            AND     T1.LOCATION_ID      = t3.location_id
            AND     T1.THRESHOLD        = t3.threshold
            AND NVL(T1.TARGET,0)        = NVL(t3.target,0)            
            )
            
            
            
            
           
            
            
            call HT_MODEL_UPDATE.PROCESS_MODEL_DATA();
            
            
            
            EXECUTE IMMEDIATE('TRUNCATE TABLE HT_UPDATE_MODELS_TMP;');
            
            
            
            
            
            
            select *
            FROM    MD$IMPORT_DATA t2
            WHERE  EXISTS
            (
            SELECT  T1.MODEL_KEY,
                    T1.MODEL_NAME,
                    T1.LEVEL_1,  
                      T1.LEVEL_2,
                      T1.LEVEL_3,
                      T1.LEVEL_4,
                      T1.LEVEL_5,
                    T1.LOCATION_ID,
                    T1.THRESHOLD,
                    T1.TARGET  
                    --T2.MODEL_KEY                      
            FROM    MD$IMPORT_DATA t1  
            WHERE   t1.MODEL_KEY = t2.MODEL_KEY
            AND     t1.MODEL_NAME = t2.MODEL_NAME
            AND     t1.LEVEL_1 = t2.LEVEL_1                                    
            and   t1.MODEL_KEY    = '48'
            AND t1.MODEL_NAME   = 'Clear_Saddle'
            AND     t1.LEVEL_1      = '0'            
            )
            
            
--delete from    HT_UPDATE_MODELS_TMP  where model_name = 'Bob'            
            
            select *
            -- select count(*)            
            from    HT_UPDATE_MODELS_TMP t2
            where model_name = 'Bob'
            
            select *
            -- select count(*)
            FROM    MD$IMPORT_DATA            
            where model_name in ('Fred','Bob','Bobby','Cilla','Elvis','Bart','Homer','John')
            
            
            select *
            FROM    MD$IMPORT_DATA
            where MODEL_NAME not in (SELECT MODEL_NAME
                                     from    HT_UPDATE_MODELS_TMP )  
            
            
            
select *
from KJZ_BACKUP_IMPORT_DATA_TABLE
where model_name = 'John'
