using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using HottopicUtilities;

public partial class UploadModels : System.Web.UI.Page
{
    private LogUtil fLog;

    protected void Page_Load(object sender, EventArgs e)
    {
        fLog = (LogUtil)Application["flLogFile"];
        Upload.Attributes.Add("onclick", "return ShowPopup();");

        //Commenting the below user section not to use the screen - by Vijay
        //if (Session["User"] == null)
        //    Response.Redirect("UserLogin.aspx");
      
    }
    protected void Cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
    protected void Upload_Click(object sender, EventArgs e)
    {
        Process();
    }
    protected void Close_Click(object sender, EventArgs e)
    {
        Response.Write("<script language='javascript'> { window.close();}</script>");
    }
    private void Process()
    {
        string sql = "SELECT * FROM [Sheet1$] WHERE Model_name IS NOT NULL";
        
        ExcelManager em = new ExcelManager(ConfigurationManager.AppSettings["MODELS_FILE"]);
        ModelsManager modelManager = new ModelsManager();
        try
        { 
            modelManager.UpdateModels(em.QueryExcel(sql));
            lblError.Text = "Models updated successful!";
        }
        catch (Exception ex)
        {
            fLog.WriteItem(ErrorHandler.GetErrorMessage(ex));                        
            lblError.Text = Convert.ToString(ex.Message);
            //lblError.Text = "Error uploading models.  Please try again later!"; 
          
        }
       
        
    }
}
