using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Commenting the below user section not to use the screen - by Vijay
        //if (Session["User"] == null)
        //    Response.Redirect("UserLogin.aspx");
    }
    protected void btnExit_Click(object sender, EventArgs e)
    {
        Response.Write("<script language='javascript'> { window.close();}</script>");
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        Response.Redirect("UploadModels.aspx");
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        Response.Redirect("RemoveModels.aspx");
    }
}
