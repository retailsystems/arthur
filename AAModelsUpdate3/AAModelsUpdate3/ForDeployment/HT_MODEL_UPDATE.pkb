CREATE OR REPLACE PACKAGE BODY "HT_MODEL_UPDATE" AS
       
/******************************************************************************
   NAME:       HT_MODEL_UPDATE
   PURPOSE:    This Procedure is called by the AA Models update .NET app 
               It updates the MD$IMPORT_DATA table with data from the 
               Models Excel application maintained by the Allocation Analysts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3/09/2009   Karl Zeutzius    Created this procedure. 
   1.1        3/10/2009   Karl Zeutzius    Comment out SELECT DISTINCT code
                                           in delete statement to allow data    
                                           to be in sync with Excel sheet and
                                           MD$IMPORT_DATA table 
                                            
******************************************************************************/
PROCEDURE PROCESS_MODEL_DATA
IS
            v_RecCount                     NUMBER := 0;
BEGIN
            -- Get all the new or updated records that exists in the staging table
            -- but not in the import table 
            INSERT INTO MD$IMPORT_DATA
            SELECT  NULL,       -- MODEL_KEY 
                    T2.MODEL_NAME,
                    T2.LEVEL_1,  
                    T2.LEVEL_2,
                    T2.LEVEL_3,
                    T2.LEVEL_4,
                    T2.LEVEL_5,
                    T2.LOCATION_ID,
                    T2.THRESHOLD,
                    T2.TARGET                                          
            FROM    MD$IMPORT_DATA t1,
                    HT_UPDATE_MODELS_STAGE t2
            WHERE   t1.MODEL_NAME(+)     = T2.MODEL_NAME
            AND     NVL(t1.LEVEL_1(+),0) = NVL(T2.LEVEL_1,0)
            AND     NVL(t1.LEVEL_2(+),0) = NVL(T2.LEVEL_2,0)
            AND     NVL(t1.LEVEL_3(+),0) = NVL(T2.LEVEL_3,0)
            AND     NVL(t1.LEVEL_4(+),0) = NVL(T2.LEVEL_4,0)
            AND     NVL(t1.LEVEL_5(+),0) = NVL(T2.LEVEL_5,0)
            AND     t1.LOCATION_ID(+)    = T2.LOCATION_ID
            AND     t1.THRESHOLD(+)      = T2.THRESHOLD
            AND     NVL(t1.TARGET(+),0)  = NVL(T2.TARGET,0)
            AND     T1.MODEL_NAME is null;
            COMMIT;
                        
            -- Now Delete records from the $Import table
            DELETE  
            FROM  MD$IMPORT_DATA t3
            WHERE EXISTS
            (
            SELECT  T1.MODEL_NAME,
                    T1.LEVEL_1,  
                    T1.LEVEL_2,
                    T1.LEVEL_3,
                    T1.LEVEL_4,
                    T1.LEVEL_5,
                    T1.LOCATION_ID,
                    T1.THRESHOLD,
                    T1.TARGET        
            FROM    MD$IMPORT_DATA t1,
                    HT_UPDATE_MODELS_STAGE t2
            WHERE   t1.MODEL_NAME       = T2.MODEL_NAME(+)
            AND     NVL(t1.LEVEL_1,0)   = NVL(T2.LEVEL_1(+),0)          
            AND     NVL(t1.LEVEL_2,0)   = NVL(T2.LEVEL_2(+),0)
            AND     NVL(t1.LEVEL_3,0)   = NVL(T2.LEVEL_3(+),0)
            AND     NVL(t1.LEVEL_4,0)   = NVL(T2.LEVEL_4(+),0)
            AND     NVL(t1.LEVEL_5,0)   = NVL(T2.LEVEL_5(+),0)            
            AND     t1.LOCATION_ID      = T2.LOCATION_ID(+)
            AND     t1.THRESHOLD        = T2.THRESHOLD(+)
            AND     NVL(t1.TARGET,0)    = NVL(T2.TARGET(+),0)
--            AND     t1.model_name       IN (SELECT DISTINCT Model_name
--                                           FROM   HT_UPDATE_MODELS_STAGE) 
            AND     T2.MODEL_NAME       IS NULL     
            /* Join back to t3 for delete */                   
            AND     T1.MODEL_NAME       = t3.model_name
            AND     NVL(T1.LEVEL_1,0)   = NVL(t3.level_1,0)
            AND     NVL(T1.LEVEL_2,0)   = NVL(t3.level_2,0)                 
            AND     NVL(T1.LEVEL_3,0)   = NVL(t3.level_3,0)
            AND     NVL(T1.LEVEL_4,0)   = NVL(t3.level_4,0)
            AND     NVL(T1.LEVEL_5,0)   = NVL(t3.level_5,0)                        
            AND     T1.LOCATION_ID      = t3.location_id
            AND     T1.THRESHOLD        = t3.threshold
            AND     NVL(T1.TARGET,0)    = NVL(t3.target,0)            
            );
            COMMIT;
            
            
            -- See if we have any new models and call procedure to
            -- generate key if needed
            SELECT COUNT(*)
            INTO   v_RecCount
            FROM   MD$IMPORT_DATA
            WHERE  MODEL_KEY IS NULL; 
            
            IF v_RecCount > 0 THEN
                ALLOCMODELS.UPDATEIMPORTDATA;
            END IF;
            
            
            -- Now do some housecleaning
            EXECUTE IMMEDIATE('TRUNCATE TABLE HT_UPDATE_MODELS_STAGE');
            COMMIT;
            
   
   EXCEPTION
        WHEN OTHERS THEN
             RAISE_APPLICATION_ERROR(-20006, SQLERRM);
             
END PROCESS_MODEL_DATA;
             
END HT_MODEL_UPDATE;
/
