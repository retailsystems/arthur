CREATE OR REPLACE PACKAGE "HT_MODEL_UPDATE" IS
/******************************************************************************
   NAME:       HT_MODEL_UPDATE
   PURPOSE:    This Procedure is called by the AA Models update .NET app 
               It updates the MD$IMPORT_DATA table with data from the 
               Models Excel application maintained by the Allocation Analysts

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        3/09/2009   Karl Zeutzius    Created this procedure.

                                           
                                             
******************************************************************************/
                                                                               
    --TYPE T_CURSOR IS REF CURSOR;
      
    PROCEDURE PROCESS_MODEL_DATA;

END HT_MODEL_UPDATE;
/