using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for User
/// </summary>
public class User
{
    private string _userName;
    private string _password;

    public User()
    {
    }
    public string UserName
    {
        get { return _userName; }
        set { _userName = value; }
    }

    public string Password
    {
        get { return _password; }
        set { _password = value; }
    }
	
}
