using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.IO;
using HottopicUtilities;
using WSReference;



public partial class UploadModels : System.Web.UI.Page
{
    private LogUtil fLog;

    protected void Page_Load(object sender, EventArgs e)
    {
        fLog = (LogUtil)Application["flLogFile"];        
        Upload.Attributes.Add("onclick", "return ShowPopup();"); 

        //Commenting the below user section not to use the screen - by Vijay
        //if (Session["User"] == null)
        //    Response.Redirect("UserLogin.aspx");
      
    }
    protected void Cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Default.aspx");
    }
    protected void Upload_Click(object sender, EventArgs e)
    {
        String strProcessMode = ConfigurationManager.AppSettings["AAModelsUploadMode"];
        if (strProcessMode == "DOTNET")
        {
            Process();
        }
        else //Default ETL mode
        {
            IntiateETL();
        }        
    }

    private void IntiateETL()
    {
        WebService ws = null;
        try
        {
            ws = new WebService();
            String strStatus = ws.LaunchPackageUsingBatchFile(ConfigurationManager.AppSettings["ETLBatchFileName"].ToString());
            if (strStatus.ToUpper().Trim() == "SUCCESS")
            {
                lblError.Text = "AA Models update Process has been initiated. <br> An email will be sent out as soon as the Models update is complete.";
            }
            else
            {
                lblError.Text = "An error occurred while initiating the AA Models Update ETL <br>" + strStatus;
            }
        }
        catch (Exception ex)
        {
            fLog.WriteItem(ErrorHandler.GetErrorMessage(ex));            
            lblError.Text = "An error occurred while initiating the AA Models Update ETL <br>" + ex.ToString();
        }
        finally
        {
            if (ws != null)
                ws = null;
        }
    }
    protected void Close_Click(object sender, EventArgs e)
    {
        Response.Write("<script language='javascript'> { window.close();}</script>");
    }
    private void Process()
    {        
        String strStartTimeStamp = DateTime.Now.ToString();
        String strModelsFileExt = ConfigurationManager.AppSettings["MODELS_FILE_EXT"];
        String strModelsFilePath = ConfigurationManager.AppSettings["MODELS_FILE_PATH"];

        string sql = "SELECT * FROM [Sheet1$] WHERE Model_name IS NOT NULL";
        
        //Get the Excel file list for loading the staging table
        String[] strModelFileEntries = Directory.GetFiles(strModelsFilePath, strModelsFileExt);
        
        //find it out whether it is single or multiple files
        
        try
        {
            ModelsManager modelManager = new ModelsManager();
            //Truncate stage table
            modelManager.TruncateStgTbl();
            foreach (String strModelFileName in strModelFileEntries)
            {
                ExcelManager em = new ExcelManager(strModelFileName);                
                modelManager.UploadModelsToStg(em.QueryExcel(sql));
            }
            if (strModelFileEntries.Length > 0)
                modelManager.ProcessModels(); 

            lblError.Text = "Models updated successful! <br>" + "Process Start time: " + strStartTimeStamp + " | Process End time: " + DateTime.Now.ToString();
        }
        catch (Exception ex)
        {
            fLog.WriteItem(ErrorHandler.GetErrorMessage(ex));
            lblError.Text = Convert.ToString(ex.Message);
            //lblError.Text = "Error uploading models.  Please try again later!"; 

        }             
    }
}
