

--DROP TABLE XXX_AAM.HT_UPDATE_MODELS_TMP CASCADE CONSTRAINTS;

CREATE TABLE XXX_AAM.HT_UPDATE_MODELS_TMP
(
  MODEL_KEY    NUMBER,
  MODEL_NAME   VARCHAR2(50 BYTE)                NOT NULL,
  LEVEL_1      VARCHAR2(50 BYTE),
  LEVEL_2      VARCHAR2(50 BYTE),
  LEVEL_3      VARCHAR2(50 BYTE),
  LEVEL_4      VARCHAR2(50 BYTE),
  LEVEL_5      VARCHAR2(50 BYTE),
  LOCATION_ID  NUMBER                           NOT NULL,
  THRESHOLD    NUMBER,
  TARGET       NUMBER
)
TABLESPACE AAMDATA
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          2080M
            NEXT             8M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING;


GRANT SELECT ON XXX_AAM.HT_UPDATE_MODELS_TMP TO XXX_AAM_INQ_ROLE;
