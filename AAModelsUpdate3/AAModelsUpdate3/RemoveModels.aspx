<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RemoveModels.aspx.cs" Inherits="RemoveModels" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Remove Models</title>
    <link href="Styles2.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div style="text-align:center">
        <!-- #include file="include/Header.inc" -->
            <table>
                <tbody>
                    <tr><th style="color: Red; font-size:x-large;" colspan="3">Remove Models</th></tr>
                    <tr><td><asp:Label ID="lblError" runat="server" Visible="false"></asp:Label></td></tr>
                    <tr>
                        <td colspan="2"><asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Select a model to remove:"></asp:Label></td>
                        <td><asp:DropDownList ID="lstModelName" runat="server" CausesValidation="true"></asp:DropDownList></td>
                     </tr>
                     <tr><td style="height:80"></td></tr>
                     <tr>
                        <td><asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Main Page" CssClass="btnSmall"  /></td>
                        <td><asp:Button ID="btnRemove" runat="server" CssClass="btnSmall" OnClick="btnRemove_Click" Text="Remove" /></td>
                        <td><asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btnSmall"  OnClick="btnClose_Click" Width="83px" /></td>
                     </tr>
                </tbody>
            </table>
          </div>
    </form>
</body>
</html>
