using System;
using System.Data.OracleClient;
using System.Data;
using System.Collections;
using System.Windows.Forms;
using System.Data.SqlClient;
//using HotTopic.AIMS.Logging;
using HotTopic.DCSS.Services;
using HottopicUtilities;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
//using Microsoft.Office.Tools.Excel; 
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for ExcelManager
/// </summary>
public class ExcelManager
{
    private string _conStr = String.Empty;
    private string _fileLoc = String.Empty;
    private OleDbConnection dbConnection;

	public ExcelManager(string fileLoc)
	{
        _fileLoc = fileLoc;
        dbConnection = new OleDbConnection(GetConnectionString(_fileLoc));
	}
    
    public DataTable QueryExcel(string sql)
    {
        DataTable dt = new DataTable();
        
        try
        {
            dbConnection.Open();
            OleDbDataAdapter dbAdapter = new OleDbDataAdapter(sql, dbConnection);
            dbAdapter.Fill(dt);
        }
        catch (Exception)
        {
            throw; 
        }
        finally
        {
            dbConnection.Close();
        }
        return dt;

    }
    
    private String GetConnectionString(string excelLocn)
    {
        //string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties=""Excel 8.0;HDR=YES;IMEX=1""";
        string connectionString = @ConfigurationManager.AppSettings["ExcelConStr"];      
        connectionString += ";Data Source=" + excelLocn; 
        return connectionString;
    }
    
}
