<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UploadModels.aspx.cs" Inherits="UploadModels" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Upload Models</title>
    <link href="Styles2.css" rel="stylesheet" type="text/css" />
</head>
<body id="tagBody" runat="server">
<!-- #include file="include/JScript.js" -->
    <form id="UploadModel" runat="server">
        <div style="text-align:center">
            <!-- #include file="include/Header.inc" -->
            <table>
                <tbody>
                    <tr><th style="color: Red; font-size:x-large;" colspan="3">Update Models</th></tr>
                    <tr><td colspan="3" style="height:10; vertical-align:top"><asp:Label runat="server" ID="lblError"></asp:Label></td></tr>
                    <tr><td style="height:50; vertical-align:bottom" colspan="3">Click on Upload button to upload models</td></tr>
                    <tr>
                        <td><asp:Button ID="Cancel" runat="server" Text="Main Page" CssClass="btnSmall" OnClick="Cancel_Click" /></td>
                        <td><asp:Button ID="Upload" runat="server" Text="Upload" CssClass="btnSmall" OnClick="Upload_Click" /></td>
                        <td><asp:Button ID="Close" runat="server" Text="Close" CssClass="btnSmall" OnClick="Close_Click" /></td>
                   </tr>
                </tbody>
            </table>
            <input type="hidden" id="progress" />
         </div>
    </form>
</body>
</html>
