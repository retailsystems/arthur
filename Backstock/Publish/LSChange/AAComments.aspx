<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AAComments.aspx.vb" Inherits="BackStock.AAComments" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<!-- #include file="include/header.inc" -->        
</head>
<body>
    <form id="form1" runat="server">
     <TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
				cellSpacing="0" cellPadding="2" width="750" align="center" border="0">
				<TR height="3"> 
							<TD align="center" style="height: 3px" colspan="3">
							    <asp:label id="lblError" Runat="server" CssClass="errStyle" Visible="False"></asp:label>
								<asp:validationsummary id="ValidationSummary1" Runat="server" CssClass="errStyle" EnableClientScript="false" DisplayMode="BulletList" ShowMessageBox="False"></asp:validationsummary>
							</TD>
							</TR>
				<TR height="15" >
					<TD class="cellvaluecaption" align="center" bgColor="#990000" colspan="3">Search Item Number
					</TD>
					
				</TR>
				<TR height="5">
					<TD></TD>
				</TR>
     <tr><TD vAlign="top" align="center" colspan="3" style="width: 747px">Item Number#:
         <asp:TextBox ID="txtItm_Nbr" runat="server"></asp:TextBox>
         <INPUT id="hdnWL_Key" type="hidden" value="0" name="hdnItm_Nbr" runat="server">
            &nbsp;&nbsp;
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btnSmall"/>&nbsp;&nbsp;
            &nbsp;&nbsp;
            <input class="btnSmall" id="btnClear" onclick="location.href=location.href" type="button" value="Clear" name="btnClear"></td>
        </tr>
         <TR height="15" >
					<TD class="cellvaluecaption" align="center" bgColor="#990000" colspan="3">
                        Add/Update/Delete Item Size</TD>					
				</TR>
     <tr><td vAlign="top" align="center" colspan="2" style="width: 747px; height: 313px;" >
    
         <asp:Panel ID="PnlDelUpd" runat="server">
         
         <asp:GridView HeaderStyle-CssClass ="DATAGRID_HeaderSm" ID="grdvwStyleDetail" runat="server" AutoGenerateColumns="False" DataSourceID="sqlArthurDS" DataKeyNames="ITM_NBR" DataMember="DefaultView" AllowPaging="True" AllowSorting="True" 
         OnRowEditing="ItmStyleGridView_OnRowEditing" OnRowUpdated = "ItmStyleGridView_OnRowUpdated" OnRowDeleted ="ItmStyleGridView_OnRowDeleted" OnPageIndexChanged="ItmStyleGridView_OnPageIndexChanged" CssClass="blueBottomBorder">         
        <Columns>
            <asp:BoundField DataField="ITM_NBR" HeaderText="ITM_NBR" SortExpression="ITM_NBR" ReadOnly="True" >
                <ItemStyle CssClass="cellValueWhite" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="TEXT" SortExpression="TEXT">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("TEXT") %>' TextMode="MultiLine"></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("TEXT") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle CssClass="cellValueWhite" Height="30%" HorizontalAlign="Left" Width="30%" />
            </asp:TemplateField>
            <asp:CommandField HeaderText="EDIT"   ShowEditButton="True"/>
            <asp:CommandField HeaderText="DELETE" ShowDeleteButton="True"/>        
        </Columns>
             <HeaderStyle CssClass="DATAGRID_HeaderSm" />
    
    </asp:GridView> 
         </asp:Panel>
    </td><td style="height: 313px"> 
         
     <asp:Panel ID="PnlInsert" runat="server" GroupingText="Click New to add a new comment" BorderStyle="double" BorderColor="#990000" ForeColor ="white" Font-Bold="true"
      Font-Strikeout="False" Font-Underline="False" BorderWidth="1px">
     
         <asp:DetailsView ID="dtlvwAddNewCmt" runat="server" DataSourceID="sqlArthurDS"  Height="50px" Width="125px" OnItemInserting="dtlvwAddNewCmt_OnItemInserting" OnItemInserted="dtlvwAddNewCmt_OnItemInserted"  AutoGenerateRows="false" BorderColor="white" >
         
         <EmptyDataTemplate> 
                <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Insert New Record</asp:LinkButton> 
            </EmptyDataTemplate>          
               
             <Fields>
                 <asp:BoundField DataField="ITM_NBR" HeaderText="ITM_NBR" />
                 <asp:BoundField DataField="TEXT" HeaderText="TEXT" />
                 <asp:CommandField ShowInsertButton="True" />
             </Fields>
            <HeaderStyle BorderColor="#990000"  BackColor="#440000" Font-Bold="True" ForeColor="White" />              
            <CommandRowStyle BorderColor="#990000" BackColor="#440000" Font-Bold="True" />
            <FieldHeaderStyle BorderColor="#990000" BackColor="#440000" Font-Bold="True" />
            
            
         </asp:DetailsView>
         &nbsp;&nbsp;</asp:Panel>
    
    </td>
    </tr>
    
    <!-- #include file="include/footer.inc" -->
    <asp:SqlDataSource ID="sqlArthurDS" runat="server" ConnectionString="<%$ appSettings:AAConnectionString %>" ProviderName="System.Data.OracleClient" SelectCommand='SELECT * FROM AAM.STYLE_DETAILS' UpdateCommand ="update AAM.STYLE_DETAILS set TEXT = :TEXT WHERE ITM_NBR=:ITM_NBR" DeleteCommand ="delete from AAM.STYLE_DETAILS WHERE ITM_NBR=:ITM_NBR"
    InsertCommand="INSERT INTO AAM.STYLE_DETAILS(ITM_NBR, TEXT) VALUES (:ITM_NBR,:TEXT)" >
    <InsertParameters>
     <asp:Parameter Name="ITM_NBR" Type="String" />
     <asp:Parameter Name="TEXT" Type="String" />
    </InsertParameters>
    
    <UpdateParameters>
     <asp:Parameter Name="ITM_NBR" Type="String" />
     <asp:Parameter Name="TEXT" Type="String" />
    </UpdateParameters>
    <DeleteParameters>
     <asp:Parameter Name="ITM_NBR" Type="String" />
     <asp:Parameter Name="TEXT" Type="String" />
    </DeleteParameters>
     </asp:SqlDataSource>
    </table>
     </form>
</body>
</html>
