<%@ Page Language="vb"  EnableViewState=False  EnableViewStateMac="False" AutoEventWireup="false" Codebehind="WaveReport.aspx.vb" Inherits="BackStock.WaveReport"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>ExcelReport</title>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="600" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td align="center"><B>WAVE CHECK REPORT (WAVE #
							<%=request("waveNbr")%>
							)</B></td>
				</tr>
				<tr>
					<td></td>
				</tr>
				<tr>
					<td>
						<asp:DataGrid ID="dgResults" Width="600px" runat="server" AutoGenerateColumns="False" HeaderStyle-Font-Bold="True"
							HeaderStyle-BackColor="#ffa500" HeaderStyle-Font-Size="8pt" ItemStyle-Font-Size="8pt">
							<Columns>
								<asp:BoundColumn DataField="DEPT" HeaderText="DEPT"></asp:BoundColumn>
								<asp:BoundColumn DataField="DISTRO_NBR" HeaderText="DISTRO_NBR"></asp:BoundColumn>
								<asp:BoundColumn DataField="SKU_NUM" HeaderText="SKU_NUM"></asp:BoundColumn>
								<asp:BoundColumn DataField="REQD_QTY" HeaderText="REQD_QTY"></asp:BoundColumn>
								<asp:BoundColumn DataField="ORIG_REQ_QTY" HeaderText="ORIG_REQ_QTY"></asp:BoundColumn>
							</Columns>
						</asp:DataGrid>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
