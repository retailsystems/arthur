Imports BackStock.Classes
Partial Class CancelStoreDistro
    Inherits System.Web.UI.Page
    Protected ds As New DataSet
    Protected dr As DataRow
    Protected strErrorMsg As String = String.Empty
    Private _workFlowManager As New WorkFlowManager
    Protected htSecurityGrp As New Hashtable()
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If User.Identity.IsAuthenticated Then
            Session("UserName") = User.Identity.Name
        Else
            Session("UserName") = "No user identity available."
        End If

        lblError.Text = ""
        lblError.Visible = False

        If (AppSetting.SecurityGroupCheck IsNot Nothing) Then
            'Get the security group settings
            Dim strSecurityGroupCheck() As String = AppSetting.SecurityGroupCheck.Split("|")
            For Each strField As String In strSecurityGroupCheck
                Dim strSecurityGroupCheck1() As String = strField.Split(">")
                htSecurityGrp.Add(strSecurityGroupCheck1(0), strSecurityGroupCheck1(1))
            Next

            If (_workFlowManager.IsInGroup(htSecurityGrp("ARTHUR").ToString()) = True) Then
                htSecurityGrp.Add("CURRENT_USER_GRP", "ARTHUR")
            ElseIf (_workFlowManager.IsInGroup(htSecurityGrp("WMS").ToString()) = True) Then
                htSecurityGrp.Add("CURRENT_USER_GRP", "WMS")
            Else
                htSecurityGrp.Add("CURRENT_USER_GRP", "INVALID")
            End If

        End If

    End Sub
    Private Sub ShowMsg(ByVal msg As String)
        lblError.Text &= IIf(lblError.Text.Trim <> "", "<BR>", "") & msg
        'lblError.Text = msg
        lblError.Visible = True
    End Sub
    Private Sub BindData()
        'ds = _workFlowManager.ListDistros(lstWhse.SelectedItem.Value, txtShipmentNbr.Text.Trim, txtDistroNbr.Text.Trim)
        'pnlResults.Visible = True

        If (rbDistroType.SelectedItem.Text.ToLower().Trim() = "shipment") Then
            Try
                If ValidateRequest("shipment") Then
                    ds = _workFlowManager.ListShipmentDistros(lstWhse.SelectedItem.Value, txtShipmentNbr.Text.Trim, txtDistroNbr.Text.Trim, Session("UserName"), strErrorMsg)
                    pnlResults.Visible = True
                    hdnWhse.Value = lstWhse.SelectedItem.Value
                    hdnShpmtNbr.Value = txtShipmentNbr.Text
                Else
                    pnlResults.Visible = False
                    ShowMsg("Following error(s) occured while processing your request!")
                End If
            Catch ex As Exception
                ShowMsg(ex.Message)
            End Try
        Else
            Try
                If ValidateRequest("backstock") Then
                    ds = _workFlowManager.ListBackstockDistros(lstWhse.SelectedItem.Value, txtDistroNbr.Text.Trim, Session("UserName"), strErrorMsg)
                    pnlResults.Visible = True
                    hdnWhse.Value = lstWhse.SelectedItem.Value
                    hdnShpmtNbr.Value = txtShipmentNbr.Text
                Else
                    pnlResults.Visible = False
                    ShowMsg("Following error(s) occured while processing your request!")
                End If
            Catch ex As Exception
                ShowMsg(ex.Message)
            End Try
        End If

    End Sub
    Private Function ValidateRequest(ByVal strReqType As String) As Boolean
        Dim validReqFlag As Boolean = False
        If (strReqType = "shipment") Then
            reqShpmtNbr.Enabled = True
            If Page.IsValid AndAlso txtShipmentNbr.Text.Trim <> "" AndAlso txtDistroNbr.Text.Trim <> "" AndAlso lstWhse.SelectedItem.Value <> "" Then
                validReqFlag = True
            End If
        Else
            reqShpmtNbr.Enabled = False
            If Page.IsValid AndAlso txtDistroNbr.Text.Trim <> "" AndAlso lstWhse.SelectedItem.Value <> "" Then
                validReqFlag = True
            End If
        End If
        Return validReqFlag
    End Function

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        BindData()
        'Try
        '    If ValidateRequest() Then
        '        BindData()
        '        hdnWhse.Value = lstWhse.SelectedItem.Value
        '        hdnShpmtNbr.Value = txtShipmentNbr.Text
        '    Else
        '        ShowMsg("Please fill in at least one search parameter!")
        '    End If
        'Catch ex As Exception
        '    ShowMsg(ex.Message)
        'End Try
    End Sub

    Private Sub btnCancelDistro_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelDistro.Click
        Try
            Dim li As ListItem
            strErrorMsg = String.Empty
            If hdnDistroNbr.Value.Trim <> "" Then
                If (rbDistroType.SelectedItem.Text.ToLower().Trim() = "shipment") Then
                    _workFlowManager.CancelDistroShipment(hdnWhse.Value, hdnShpmtNbr.Value, hdnDistroNbr.Value, Session("UserName"), strErrorMsg)
                Else
                    _workFlowManager.CancelDistroBackstock(hdnWhse.Value, hdnDistroNbr.Value, Session("UserName"), strErrorMsg)
                End If

                ShowMsg("Distro # " & hdnDistroNbr.Value & " has been cancelled!")
                'If Not ValidateRequest() OrElse hdnWhse.Value <> lstWhse.SelectedItem.Value Then
                '    txtDistroNbr.Text = hdnDistroNbr.Value
                '    txtShipmentNbr.Text = hdnShpmtNbr.Value
                '    lstWhse.SelectedIndex = -1
                '    For Each li In lstWhse.Items
                '        If li.Value = hdnWhse.Value.ToString Then
                '            li.Selected = True
                '            Exit For
                '        End If
                '    Next
                'End If
                txtDistroNbr.Text = hdnDistroNbr.Value
                txtShipmentNbr.Text = hdnShpmtNbr.Value
                If hdnWhse.Value <> lstWhse.SelectedItem.Value Then
                    lstWhse.SelectedIndex = -1
                    For Each li In lstWhse.Items
                        If li.Value = hdnWhse.Value.ToString Then
                            li.Selected = True
                            Exit For
                        End If
                    Next
                End If
                hdnDistroNbr.Value = 0
                'BindData()
            Else
                ShowMsg("Invalid Distro #" & hdnDistroNbr.Value & " submitted for cancellation")
            End If
        Catch ex As Exception
            ShowMsg(ex.Message)
        End Try
    End Sub
    Private Sub btnCancelAndUpdateArthur_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelAndUpdateArthur.Click
        Try
            Dim li As ListItem
            If hdnDistroNbr.Value.Trim <> "" Then

                If (rbDistroType.SelectedItem.Text.ToLower().Trim() = "shipment") Then
                    _workFlowManager.CancelDistroShipment(hdnWhse.Value, hdnShpmtNbr.Value, hdnDistroNbr.Value, Session("UserName"), strErrorMsg)
                Else
                    _workFlowManager.CancelDistroBackstock(hdnWhse.Value, hdnDistroNbr.Value, Session("UserName"), strErrorMsg)
                End If
                '_workFlowManager.CancelDistro(hdnWhse.Value, hdnShpmtNbr.Value, hdnDistroNbr.Value)
                ShowMsg("Distro # " & hdnDistroNbr.Value & " has been cancelled!")
                _workFlowManager.PopulateWorkList(hdnWhse.Value, hdnShpmtNbr.Value, hdnDistroNbr.Value)
                'If Not ValidateRequest() OrElse hdnWhse.Value <> lstWhse.SelectedItem.Value Then
                '    txtDistroNbr.Text = hdnDistroNbr.Value
                '    txtShipmentNbr.Text = hdnShpmtNbr.Value
                '    lstWhse.SelectedIndex = -1
                '    For Each li In lstWhse.Items
                '        If li.Value = hdnWhse.Value.ToString Then
                '            li.Selected = True
                '            Exit For
                '        End If
                '    Next
                'End If
                txtDistroNbr.Text = hdnDistroNbr.Value
                txtShipmentNbr.Text = hdnShpmtNbr.Value
                If hdnWhse.Value <> lstWhse.SelectedItem.Value Then
                    lstWhse.SelectedIndex = -1
                    For Each li In lstWhse.Items
                        If li.Value = hdnWhse.Value.ToString Then
                            li.Selected = True
                            Exit For
                        End If
                    Next
                End If
                hdnDistroNbr.Value = 0
                BindData()
            Else
                ShowMsg("Invalid Distro #" & hdnDistroNbr.Value & " submitted for cancellation")
            End If
        Catch ex As Exception
            ShowMsg(ex.Message)
        End Try
    End Sub
    
    Protected Sub ValidateDistroType_Index_Changed(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbDistroType.SelectedIndexChanged

    End Sub
End Class
