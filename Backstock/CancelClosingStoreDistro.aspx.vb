Imports BackStock.Classes
Partial Public Class CancelClosingStoreDistro
    Inherits System.Web.UI.Page
    Protected ds As New DataSet
    Protected dr As DataRow
    Protected strErrorMsg As String = String.Empty
    Private _workFlowManager As New WorkFlowManager
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblError.Text = ""
        lblError.Visible = False
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        BindData()
    End Sub
    Private Sub BindData()
        Try
            If ValidateRequest() Then
                'Check the allocation/WMS number does exist in Arthur
                ds = _workFlowManager.GetClosingStoreDistros(lstWhse.Text.Trim, txtStore_No.Text.Trim(), Session("UserName"), strErrorMsg)
                'If (strErrorMsg.Length > 0) Then
                'ShowMsg(strErrorMsg)
                'End If
                pnlResults.Visible = True
                hdnWarehouseNbr.Value = lstWhse.Text.ToString()
                hdnStore_No.Value = txtStore_No.Text.ToString()
            Else
                'pnlResults.Visible = False
                ShowMsg("Following error(s) occured while processing your request!")
            End If
        Catch ex As Exception
            ShowMsg(ex.Message)
        End Try
    End Sub
    Private Function ValidateRequest() As Boolean
        Dim validReqFlag As Boolean = False
        If Page.IsValid AndAlso lstWhse.Text.Trim <> "" AndAlso txtStore_No.Text.Trim <> "" Then
            validReqFlag = True
        End If
        Return validReqFlag
    End Function
    Private Sub ShowMsg(ByVal msg As String)
        lblError.Text &= IIf(lblError.Text.Trim <> "", "<BR>", "") & msg
        'lblError.Text = msg
        lblError.Visible = True
    End Sub

    Protected Sub btnCancelClosingStore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelClosingStore.Click
        Try
            Dim strErrorMsg As String = String.Empty
            If (hdnWarehouseNbr.Value.Trim().Length > 0 AndAlso hdnStore_No.Value.Trim().Length > 0) Then
                ds = _workFlowManager.CancelClosingStoreDistros(hdnWarehouseNbr.Value.Trim(), hdnStore_No.Value.Trim(), Session("UserName"), strErrorMsg)
                ShowMsg(strErrorMsg)
                pnlResults.Visible = True
            Else
                ShowMsg("Invalid Warehouse " & hdnWarehouseNbr.Value & " and Store # " & hdnStore_No.Value & " submitted for Cancellation")
            End If

        Catch ex As Exception
            ShowMsg(ex.Message)
        End Try
    End Sub
End Class