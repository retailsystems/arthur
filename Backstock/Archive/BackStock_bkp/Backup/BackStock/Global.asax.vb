Imports System.Web
Imports System.Web.SessionState
Imports BackStock.Classes
Public Class Global
    Inherits System.Web.HttpApplication

#Region " Component Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container
    End Sub

#End Region

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
        Application("AAConnectionString") = ConfigurationSettings.AppSettings("AAConnectionString")
        Application("WMSConnectionString") = ConfigurationSettings.AppSettings("WMSConnectionString")
        Application("GERSConnectionString") = ConfigurationSettings.AppSettings("GERSConnectionString")

        Dim settings As New AppSetting
        With settings
            .AAConnectionString = ConfigurationSettings.AppSettings("AAConnectionString")
            .WMSConnectionString = ConfigurationSettings.AppSettings("WMSConnectionString")
            .GERSConnectionString = ConfigurationSettings.AppSettings("GERSConnectionString")
            .EMailReceivers = ConfigurationSettings.AppSettings("NotificationEmailReceivers")
            .CacheDependencyFile = ConfigurationSettings.AppSettings("CacheDependency")
            .SmtpServer = ConfigurationSettings.AppSettings("SmtpServer")
            .SmtpSender = ConfigurationSettings.AppSettings("SmtpSender")
            .WebmasterEmail = ConfigurationSettings.AppSettings("WebmasterEmail")
            .ProjectName = ConfigurationSettings.AppSettings("ProjectName")
            .ReserveClearEmailBody = ConfigurationSettings.AppSettings("ReserveClearEmailBody")
            .ReserveUpdateEmailBody = ConfigurationSettings.AppSettings("ReserveUpdateEmailBody")
            .ReserveBatchEmailBody = ConfigurationSettings.AppSettings("ReserveBatchEmailBody")

            .WmsLocalFileDir = ConfigurationSettings.AppSettings("WmsLocalFileDir")
            .FtpUser = ConfigurationSettings.AppSettings("FtpUser")
            .FtpPwd = ConfigurationSettings.AppSettings("FtpPwd")
            .FtpHost = ConfigurationSettings.AppSettings("FtpHost")
            .FtpFolder = ConfigurationSettings.AppSettings("FtpFolder")
            .BatchBizTalkFileDir = ConfigurationSettings.AppSettings("BatchBizTalkFileDir")
            .AABackstockPkg = ConfigurationSettings.AppSettings("AABackstockPkg")
            .WMBackstockPkg = ConfigurationSettings.AppSettings("WMBackstockPkg")
            .GERSBackstockPkg = ConfigurationSettings.AppSettings("GERSBackstockPkg")
            .ErrorLogFile = ConfigurationSettings.AppSettings("ErrorLog")
            .WarehouseNbr = ConfigurationSettings.AppSettings("WHSE")
            .AckFileDir = ConfigurationSettings.AppSettings("AckFileDir")
            .UtlFileDir = ConfigurationSettings.AppSettings("UtlFileDir")
            '06/2/05 - SRI
            .TnDcWMSConnectionString = ConfigurationSettings.AppSettings("TNDC_WMSConnectionString")


        End With
        Application("AppSetting") = settings

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session is started
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the session ends
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application ends
    End Sub

End Class
