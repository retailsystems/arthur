<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Reports.aspx.vb" Inherits="BackStock.Reports"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<!-- #include file="include/header.inc" -->
		<script language="javascript">
			function RunWaveChek(){							
				if(document.Form1.lstWhse.selectedIndex == 0){
					alert("Please select a Warehouse!");
					document.Form1.lstWhse.focus();
				}else if(document.Form1.txtWaveNbr.value == "" || isNaN(document.Form1.txtWaveNbr.value)){
					alert("Invalid Ship Wave #");
					document.Form1.txtWaveNbr.focus();
				}else{				
					var url = "WaveReport.aspx?WaveNbr=" + document.Form1.txtWaveNbr.value + "&WarehouseNbr=" + document.Form1.lstWhse[document.Form1.lstWhse.selectedIndex].value;
					document.Form1.action = url;
					//alert(url);					
					document.Form1.submit();					
				}
				
			}
			function RunReserveBatch(){
				if(document.Form1.lstWhse.selectedIndex == 0){
					alert("Please select a Warehouse!");
					document.Form1.lstWhse.focus();
				}else if(document.Form1.txtBatchID.value == "" || isNaN(document.Form1.txtBatchID.value)){
					alert("Invalid Reserve Batch #");
					document.Form1.txtBatchID.focus();
				}else{	
					var url = "ReserveBatchDetailsReport.aspx?BatchID=" + document.Form1.txtBatchID.value+ "&WarehouseNbr=" + document.Form1.lstWhse[document.Form1.lstWhse.selectedIndex].value;
					//OpenPop(url,500,500);
					document.Form1.action = url;				
					document.Form1.submit();	
				}	
			}
			function RunStatusCheck(){
				if(document.Form1.txtBatchNbr.value == "" || isNaN(document.Form1.txtBatchNbr.value)){
					alert("Invalid Reserve Batch #");
					document.Form1.txtBatchNbr.focus();
				}else{	
					var url = "StatusCheck.aspx?BatchID=" + document.Form1.txtBatchNbr.value;
					OpenPop(url,600,500);
					//document.Form1.action = url;				
					//document.Form1.submit();	
				}	
			}
			function RunReserveUpdateCheck(){
				var url = "StatusCheck.aspx?BatchID=WRU"; // (WRU = Weekly Reserve Update)
				OpenPop(url,600,600);
			}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<!--#include file="include/wait.inc"-->
			<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td height="10"></td>
				</tr>
				<TR>
					<TD align="center" colSpan="3">
						<!-- #1/04/2006# Sri Bajjuri: B/S move changes --><FONT class="cellvaluecaption">Warehouse:</FONT>
						<select class="cellvalueleft" name="lstWhse">
							<option value="" selected>--Select One--</option>
							<option value="999">DC-Industry,CA</option>
							<option value="997">DC-LaVergne,TN</option>
						</select>
					</TD>
				</TR>
				<TR height="15">
					<TD></TD>
				</TR>
				<TR>
					<!-- wave check -->
					<TD vAlign="top" width="380">
						<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
							cellSpacing="0" cellPadding="2" width="100%" align="center" border="0">
							<TR height="15">
								<TD class="cellvaluecaption" bgColor="#990000">&nbsp;WAVE CHECK
								</TD>
							</TR>
							<TR height="5">
								<TD></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE borderColor="#990000" height="100%" cellSpacing="0" cellPadding="0" width="100%"
										border="0">
										<TR height="3">
											<TD colSpan="3"></TD>
										</TR>
										<tr>
											<td width="5"></td>
											<td class="cellvaluecaption" width="100">SHIP WAVE #:</td>
											<td align="left" width="100"><input class="cellvalueleft" id="txtWaveNbr" style="WIDTH: 200px" type="text" name="txtWaveNbr">
											</td>
										</tr>
										<TR height="10">
											<TD colSpan="3"></TD>
										</TR>
										<tr>
											<td align="center" colSpan="3"><input class="btnSmall" id="btnWaveCheck" onclick="RunWaveChek()" type="button" value="Run Wave Check"
													name="btnWaveCheck">
											</td>
										</tr>
										<TR height="5">
											<TD colSpan="3"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR height="20">
								<TD></TD>
							</TR>
						</TABLE>
					</TD>
					<td width="40"></td>
					<!-- AA Allocs VS WMS Distros -->
					<TD vAlign="top" width="380">
						<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
							cellSpacing="0" cellPadding="2" width="100%" align="center" border="0">
							<TR height="15">
								<TD class="cellvaluecaption" bgColor="#990000">&nbsp;AA Allocs VS WMS Distros
								</TD>
							</TR>
							<TR height="5">
								<TD></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE borderColor="#990000" height="100%" cellSpacing="0" cellPadding="0" width="100%"
										border="0">
										<TR height="3">
											<TD colSpan="3"></TD>
										</TR>
										<tr>
											<td width="5"></td>
											<td class="cellvaluecaption" width="100">RESERVE BATCH #:</td>
											<td align="left" width="100"><input class="cellvalueleft" id="txtBatchID" style="WIDTH: 200px" type="text" name="txtBatchID">
											</td>
										</tr>
										<TR height="10">
											<TD colSpan="3"></TD>
										</TR>
										<tr>
											<td align="center" colSpan="3"><input class="btnSmall" id="btnSubmit" onclick="RunReserveBatch()" type="button" value="Submit"
													name="btnSubmit">
											</td>
										</tr>
										<TR height="5">
											<TD colSpan="3"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR height="20">
								<TD></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
				<tr>
					<td height="15"></td>
				</tr>
				<TR>
					<!-- wave check -->
					<TD vAlign="top" width="380">
						<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
							cellSpacing="0" cellPadding="2" width="100%" align="center" border="0">
							<TR height="15">
								<TD class="cellvaluecaption" bgColor="#990000">&nbsp;STATUS CHECK
								</TD>
							</TR>
							<TR height="5">
								<TD></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE borderColor="#990000" height="100%" cellSpacing="0" cellPadding="0" width="100%"
										border="0">
										<TR height="3">
											<TD colSpan="3"></TD>
										</TR>
										<tr>
											<td width="5"></td>
											<td class="cellvaluecaption" width="100">RESERVE BATCH #:</td>
											<td align="left" width="100"><input class="cellvalueleft" id="txtBatchNbr" style="WIDTH: 200px" type="text" name="txtBatchNbr">
											</td>
										</tr>
										<TR height="10">
											<TD colSpan="3"></TD>
										</TR>
										<tr>
											<td align="center" colSpan="3"><input class="btnSmall" id="btnStatusCheck" onclick="RunStatusCheck()" type="button" value="Check Status"
													name="btnStatusCheck">
											</td>
										</tr>
										<TR height="5">
											<TD colSpan="3"></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR height="20">
								<TD></TD>
							</TR>
						</TABLE>
					</TD>
					<td width="40"></td>
					<!-- AA Allocs VS WMS Distros -->
					<TD vAlign="top" width="380">
						<!--<TABLE style="BORDER-RIGHT: #990000 1px solid; BORDER-TOP: #990000 1px solid; BORDER-LEFT: #990000 1px solid; BORDER-BOTTOM: #990000 1px solid"
							cellSpacing="0" cellPadding="2" width="100%" align="center" border="0">
							<TR height="15">
								<TD class="cellvaluecaption" bgColor="#990000">&nbsp;RESERVE UPDATE/BATCH STATUS 
									CHECK
								</TD>
							</TR>
							<TR height="5">
								<TD></TD>
							</TR>
							<TR>
								<TD vAlign="top" align="center">
									<TABLE borderColor="#990000" height="100%" cellSpacing="0" cellPadding="0" width="100%"
										border="0">
										<TR height="3">
											<TD colSpan="3"></TD>
										</TR>
										<tr>
											<td width="5"></td>
											<td class="cellvaluecaption" width="100"></td>
											<td align="left" width="100" height="20"></td>
										</tr>
										<TR height="10">
											<TD colSpan="3"></TD>
										</TR>
										<tr>
											<td align="center" colSpan="3"><input class="btnSmall" onclick="RunReserveUpdateCheck()" type="button" value="Check Reserve Update/Batch Status"
													name="btnReserveUpdateStatus">
											</td>
										</tr>
										<TR height="5">
											<TD colSpan="3"></TD>
										</TR>
									</TABLE>-->
								</TD>
							</TR>
							<TR height="20">
								<TD></TD>
							</TR>
						</TABLE>
					</TD>
				</TR>
			</TABLE>
		</form>
		<!-- #include file="include/footer.inc" -->
	</body>
</HTML>
