Imports System.IO
Imports System.Text
Imports Backstock.Classes.SqlServices
Imports System.Data.OracleClient
Public Class WorkFlowManager
    Private _aaCn As New AAOracleDataManager
    Private _gersCn As New GersOracleDataManager
    Private _wmsCn As New WMSOracleDataManager
    Private _tnDcCn As New TNDCWMSOracleDataManager
    Private _strSql As String
    Private _errMsg As String = ""
    Public Function ReserveClear(ByVal WarehouseNbr As Int16, ByVal deptStr As String, Optional ByVal emailFlag As Boolean = True) As Int16
        Dim rtnVal As Int16
        Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}

        cmdParameters(0) = New OracleParameter("p_warehouse_nbr", OracleType.VarChar, 4)
        cmdParameters(0).Value = WarehouseNbr

        cmdParameters(1) = New OracleParameter("p_dept_str", OracleType.VarChar, 4000)
        cmdParameters(1).Value = deptStr
        _errMsg = ""
        Try
            rtnVal = _aaCn.ExecuteNonQuery(AppSetting.AABackstockPkg & ".PROC_WORKLIST_CLEAR_BY_DEPT", cmdParameters)
            If rtnVal = 1 AndAlso emailFlag Then
                SendReserveClearEmail(WarehouseNbr, deptStr, True)
                UpdateBackStockHistory(WarehouseNbr, deptStr, "RESERVE_CLEAR_DATE")
            End If
            Return rtnVal
        Catch ex As OracleException
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Catch ex As Exception
            _errMsg = ex.Message
            Throw ex
        Finally
            If _errMsg <> "" Then
                SendReserveClearEmail(WarehouseNbr, deptStr, False, _errMsg)
            End If
        End Try

    End Function

    Public Function ReserveClear(ByVal WarehouseNbr As Int16, Optional ByVal emailFlag As Boolean = True) As Int16
        Try
            Dim rtnVal As Int16
            Dim cmdParameters() As OracleParameter = New OracleParameter(0) {}
            cmdParameters(0) = New OracleParameter("p_warehouse_nbr", OracleType.VarChar, 4)
            'cmdParameters(0).Value = AppSetting.WarehouseNbr
            cmdParameters(0).Value = WarehouseNbr
            _errMsg = ""
            'rtnVal = _aaCn.ExecuteNonQuery("HT_RESERVE.PROC_WORKLIST_RESERVE_CLEAR", cmdParameters)
            rtnVal = _aaCn.ExecuteNonQuery(AppSetting.AABackstockPkg & ".PROC_WORKLIST_RESERVE_CLEAR", cmdParameters)
            'If rtnVal = 1 AndAlso emailFlag Then
            '    SendReserveClearEmail(WarehouseNbr, True)
            'End If
            Return rtnVal
        Catch ex As OracleException
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Catch ex As Exception
            _errMsg = ex.Message
            Throw ex
        Finally
            If _errMsg <> "" Then
                SendReserveClearEmail(WarehouseNbr, False, _errMsg)
            End If
        End Try
    End Function
  
    Public Function UpdateBackStockHistory(ByRef ds As DataSet)
        If ds.Tables.Count > 0 And ds.Tables(0).Rows.Count > 0 Then
            Dim dr As DataRow
            Dim strBD As New StringBuilder
            Dim now As String = DateTime.Now.ToString("dd-MMM-yy hh:mm:ss")
            Try
                For Each dr In ds.Tables(0).Rows
                    strBD.Append("INSERT INTO HT_BACKSTOCK_HISTORY " & _
                              "(WAREHOUSE_NBR,DEPT_NBR,DIV_NBR,ITM_NBR,AVAIL_QTY) " & _
                              " VALUES (")

                    strBD.Append("'" & dr("WHSE") & "'")
                    strBD.Append(",'" & dr("MISC_ALPHA_1") & "'")
                    strBD.Append(",'" & dr("STORE_DEPT") & "'")
                    strBD.Append(",'" & dr("STYLE") & "'")
                    strBD.Append("," & dr("QTY_AVAIL") & ")")
                    'strBD.Append(",'" & DateTime.Now.ToString("dd-MMM-yy HH:mm:ss") & "')")
                    'strBD.Append(",'" & Now().ToString("dd-MMM-yy") & "')")

                    If (strBD.ToString().Length > 0) Then
                        _aaCn.ExecuteNonQuery(strBD.ToString())
                    End If
                    strBD.Length = 0
                Next
            Catch ex As OracleException
                _errMsg = ex.Message
                Throw New BackStockAppException(ex.Message)
            End Try
        End If
    End Function
    Public Function UpdateBackStockHistory(ByVal warehouseNbr As String, ByVal depts As String, ByVal task As String)
        Dim strBD As New StringBuilder
        Dim arrDepts() As String = depts.Split(",")
        Dim i As Int16 = 0
        If (arrDepts.Length > 0) Then
            Try
                For i = 0 To arrDepts.GetUpperBound(0)
                    If HasRun(warehouseNbr, arrDepts(i)) Then
                        UpdateBackStockHist(warehouseNbr, arrDepts(i), task)
                    Else
                        InsertBackStockHist(warehouseNbr, arrDepts(i), task)
                    End If

                    If (strBD.ToString().Length > 0) Then
                        _aaCn.ExecuteNonQuery(strBD.ToString())
                    End If

                    strBD.Length = 0
                Next
            Catch ex As Exception
                _errMsg = ex.Message
                Throw New BackStockAppException(ex.Message)
            End Try
        End If
    End Function
    Private Function UpdateBackStockHist(ByVal warehouseNbr, ByVal dept, ByVal task)
        Dim cmdParameters() As OracleParameter = New OracleParameter(2) {}
        Dim strBD As New StringBuilder

        cmdParameters(0) = New OracleParameter("warehouse_nbr", OracleType.VarChar, 4)
        cmdParameters(0).Value = warehouseNbr

        cmdParameters(1) = New OracleParameter("dept_str", OracleType.VarChar, 4000)
        cmdParameters(1).Value = dept

        cmdParameters(2) = New OracleParameter("date_run", OracleType.DateTime)
        cmdParameters(2).Value = DateTime.Now

        strBD.Append("UPDATE HT_BACKSTOCK_HISTORY SET ")
        strBD.Append(task & " = :date_run ")
        strBD.Append("WHERE WAREHOUSE_NBR = :warehouse_nbr AND DEPT_NBR = :dept_str")


        Dim aaCon As New OracleConnection(_aaCn.strConn)

        Dim cmd As New OracleCommand
        cmd.Connection = aaCon

        cmd.Parameters.Add(cmdParameters(0))
        cmd.Parameters.Add(cmdParameters(1))
        cmd.Parameters.Add(cmdParameters(2))

        Try
            aaCon.Open()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = strBD.ToString()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Finally
            If aaCon.State = ConnectionState.Open Then
                aaCon.Close()
                aaCon.Dispose()
            End If
        End Try

    End Function
    Private Function InsertBackStockHist(ByVal warehouseNbr, ByVal dept, ByVal task)

        Dim cmdParameters() As OracleParameter = New OracleParameter(2) {}
        Dim strBD As New StringBuilder

        cmdParameters(0) = New OracleParameter("warehouse_nbr", OracleType.VarChar, 4)
        cmdParameters(0).Value = warehouseNbr

        cmdParameters(1) = New OracleParameter("dept_str", OracleType.VarChar, 4000)
        cmdParameters(1).Value = dept

        cmdParameters(2) = New OracleParameter("date_run", OracleType.DateTime)
        cmdParameters(2).Value = DateTime.Now

        strBD.Append("INSERT INTO HT_BACKSTOCK_HISTORY (WAREHOUSE_NBR,DEPT_NBR, " & task & ") VALUES ( :warehouse_nbr, :dept_str, :date_run)")

        Dim aaCon As New OracleConnection(_aaCn.strConn)

        Dim cmd As New OracleCommand
        cmd.Connection = aaCon

        cmd.Parameters.Add(cmdParameters(0))
        cmd.Parameters.Add(cmdParameters(1))
        cmd.Parameters.Add(cmdParameters(2))

        Try
            aaCon.Open()
            cmd.CommandType = CommandType.Text
            cmd.CommandText = strBD.ToString()
            cmd.ExecuteNonQuery()
        Catch ex As Exception
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Finally
            If aaCon.State = ConnectionState.Open Then
                aaCon.Close()
                aaCon.Dispose()
            End If
        End Try
    End Function

    Public Function HasRun(ByVal warehouseNbr As String, ByVal dept As String) As Boolean
        Dim strBD As New StringBuilder
        Dim ds As DataSet

        strBD.Append("SELECT DEPT_NBR FROM HT_BACKSTOCK_HISTORY WHERE ")
        strBD.Append("DEPT_NBR = " & dept)
        strBD.Append(" AND")
        strBD.Append(" WAREHOUSE_NBR = " & warehouseNbr)

        Try
            ds = _aaCn.ExecuteDataSet(strBD.ToString())
            If ds.Tables(0).Rows.Count >= 1 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    'Public Function ReserveUpdateNew(ByVal WarehouseNbr As Int16) As String
    '    Dim reserveFile As String = ""
    '    Dim successMsg As String = ""
    '    Dim i, skuCnt As Int16
    '    Try
    '        'Call Resserve clear to backstock data in Arthur
    '        'ReserveClear(WarehouseNbr, False)
    '        _errMsg = ""

    '        Dim dr, dr1 As DataRow
    '        Dim ds, ds1 As DataSet

    '        Dim strSql, cur_style, UDF1, UDF2, UDF3, UDF4, UDF5, UDF6, _timeStamp, _filePath, _localFilePath As String

    '        Dim cmdParameters() As OracleParameter = New OracleParameter(0) {}

    '        cmdParameters(0) = New OracleParameter("p_cur_Backstock", OracleType.Cursor)
    '        cmdParameters(0).Direction = ParameterDirection.Output


    '        Select Case WarehouseNbr
    '            Case EnumWarehouse.CADC
    '                ds = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_GET_BACKSTOCK_DATA", cmdParameters)
    '            Case EnumWarehouse.TNDC
    '                ds = _tnDcCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_GET_BACKSTOCK_DATA", cmdParameters)
    '        End Select
    '        'ds = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_GET_BACKSTOCK_DATA", cmdParameters)
    '        Dim objStreamWriter As StreamWriter
    '        _timeStamp = Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Year.ToString & "_" & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0")
    '        reserveFile = "Reserve_" & WarehouseNbr & "_" & _timeStamp & ".txt"
    '        _filePath = AppSetting.FtpFolder & IIf(AppSetting.FtpFolder.EndsWith("\"), "", "\") & reserveFile
    '        _localFilePath = AppSetting.WmsLocalFileDir & IIf(AppSetting.WmsLocalFileDir.EndsWith("\"), "", "\") & reserveFile

    '        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
    '            objStreamWriter = New StreamWriter(_localFilePath, False)
    '            'objStreamWriter.WriteLine("RESERVE_HDR:" & _timeStamp)
    '            skuCnt = ds.Tables(0).Rows.Count
    '            For Each dr In ds.Tables(0).Rows
    '                'If n > 0 Then
    '                '    objStreamWriter.Write(objStreamWriter.NewLine)
    '                'End If
    '                'n += 1
    '                'write to backstock file
    '                'objStreamWriter.Write("ITM_DTL:")
    '                objStreamWriter.Write(dr("WHSE") & "|") ' -- Warehouse #
    '                objStreamWriter.Write(dr("QTY_AVAIL") & "|") '-- Qty Available
    '                objStreamWriter.Write(dr("STYLE") & "|") '-- Item #
    '                objStreamWriter.Write(dr("DFLT_VENDOR") & "|") '-- Vendor #
    '                objStreamWriter.Write(dr("STORE_DEPT") & "|") ' -- Div #
    '                objStreamWriter.Write(dr("MISC_ALPHA_1") & "|") '-- Dept #
    '                objStreamWriter.Write(UtilityManager.NullToString(dr("MISC_NUMERIC_1")).PadLeft(4, "0") & "|") '-- Class #
    '                objStreamWriter.Write(dr("SALE_GRP") & "|") ' -- Sub Class #
    '                objStreamWriter.Write(dr("SKU_DESC") & "|") '-- Sku Desc
    '                objStreamWriter.Write(dr("PROD_GROUP") & "|") '-- UOM Cd
    '                objStreamWriter.Write(dr("PROD_GROUP") & "|") '-- UOM Qty
    '                objStreamWriter.Write(dr("RETAIL_PRICE") & "|") '-- Retail Price
    '                objStreamWriter.Write(dr("UNIT_PRICE") & "|") '-- Cost Price
    '                objStreamWriter.Write(dr("STYLE") & "-" & dr("STYLE_SFX") & "|") '-- SKU Nbr
    '                objStreamWriter.Write(dr("PROD_SUB_GRP") & "|") '--Size Of Multiple
    '                objStreamWriter.Write(dr("MISC_ALPHA_3") & "|") ' -- size #
    '                objStreamWriter.Write(dr("MISC_ALPHA_2") & dr("COLOR_SFX") & "|") ' -- color #

    '                '******************************************************
    '                'GET UDF info from GERS
    '                '******************************************************
    '                If cur_style <> dr("style") Then
    '                    cur_style = dr("style")
    '                    UDF1 = ""
    '                    UDF2 = ""
    '                    UDF3 = ""
    '                    UDF4 = ""
    '                    UDF5 = ""
    '                    UDF6 = ""

    '                    strSql = "select UDF1,UDF2, UDF3,UDF4,UDF5,UDF6 from GM_ITM WHERE ITM_CD = '" & dr("style") & "'"
    '                    ds1 = _gersCn.ExecuteDataSet(strSql)

    '                    'Exit For
    '                    If ds1.Tables.Count > 0 AndAlso ds1.Tables(0).Rows.Count > 0 Then
    '                        dr1 = ds1.Tables(0).Rows(0)
    '                        UDF1 = UtilityManager.NullToString(dr1("UDF1"))
    '                        UDF2 = UtilityManager.NullToString(dr1("UDF2"))
    '                        UDF3 = UtilityManager.NullToString(dr1("UDF3"))
    '                        UDF4 = UtilityManager.NullToString(dr1("UDF4"))
    '                        UDF5 = UtilityManager.NullToString(dr1("UDF5"))
    '                        UDF6 = UtilityManager.NullToString(dr1("UDF6"))
    '                    End If
    '                End If


    '                objStreamWriter.Write(UDF1 & "|" & UDF2 & "|" & UDF3 & "|" & UDF4 & "|" & UDF5 & "|" & UDF6)
    '                objStreamWriter.Write("|" & Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year)
    '                'objStreamWriter.Write("|")
    '                objStreamWriter.Write(vbCrLf)
    '            Next
    '            objStreamWriter.Close()
    '            Dim _file As File
    '            If _file.Exists(_localFilePath) Then
    '                _file.Move(_localFilePath, _filePath)
    '                SendAckXmlFile(AppSetting.AckFileDir, reserveFile, AppSetting.UtlFileDir, WarehouseNbr)
    '            End If
    '            successMsg = skuCnt & " records found, "
    '            successMsg &= "Backstock File '" & reserveFile & "' submitted to Biztalk process"
    '            'SendReserveUpdateEmail(WarehouseNbr, True, reserveFile, skuCnt)
    '        Else
    '            successMsg = "No records found"
    '        End If
    '        Return successMsg
    '    Catch ex As OracleException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As BackStockAppException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As FileNotFoundException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As DirectoryNotFoundException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As Security.SecurityException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As Exception
    '        _errMsg = ex.Message
    '        Throw ex
    '    Finally
    '        If _errMsg <> "" Then
    '            SendReserveUpdateEmail(WarehouseNbr, False, reserveFile, skuCnt, _errMsg)
    '        End If
    '    End Try
    'End Function



    Public Function ReserveUpdateNew(ByVal WarehouseNbr As Int16, ByVal deptStr As String) As String
        ' KJZ 8-27-08 -- Reworked this function in order to add PrePack functionality and pull additional UDF's
        '' HG  12-18-2008 -- commented out prepack functionality and leave only pull additional UDF's 
        Dim reserveFile As String = ""
        Dim successMsg As String = ""
        Dim i, skuCnt  '', PrePackskuCnt As Int16
        Try
            'Call Resserve clear to backstock data in Arthur
            ReserveClear(WarehouseNbr, deptStr, False)
            _errMsg = ""

            Dim drWMS, drGERS As DataRow
            Dim dsWMS, dsGERS As DataSet
            'Dim strSql, cur_style, _timeStamp, _filePath, _localFilePath As String
            Dim strSql, _timeStamp, _filePath, _localFilePath As String
            Dim cmdParametersWMS() As OracleParameter = New OracleParameter(1) {}
            Dim cmdParametersGERS() As OracleParameter = New OracleParameter(1) {}

            'Create an array to add an empty row incase we have no data in our dataset.
            Dim EmptyRowArray As Array = New ArrayList(35) {}

            ' Setup for WMS 
            cmdParametersWMS(0) = New OracleParameter("p_cur_Backstock", OracleType.Cursor)
            cmdParametersWMS(0).Direction = ParameterDirection.Output
            cmdParametersWMS(1) = New OracleParameter("p_dept_str", OracleType.VarChar, 4000)
            cmdParametersWMS(1).Value = deptStr

            ' Setup for GERS
            cmdParametersGERS(0) = New OracleParameter("p_Sku", OracleType.VarChar)
            cmdParametersGERS(1) = New OracleParameter("p_Cur_UDF", OracleType.Cursor)
            cmdParametersGERS(1).Direction = ParameterDirection.Output

            'Get data from WMS backstock
            Select Case WarehouseNbr
                Case EnumWarehouse.CADC
                    dsWMS = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_GET_BACKSTOCK_BY_DEPT", cmdParametersWMS)
                Case EnumWarehouse.TNDC
                    dsWMS = _tnDcCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_GET_BACKSTOCK_BY_DEPT", cmdParametersWMS)
            End Select

            Dim objStreamWriter As StreamWriter
            _timeStamp = Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Year.ToString & "_" & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0")
            reserveFile = "Reserve_" & WarehouseNbr & "_" & _timeStamp & ".txt"
            _filePath = AppSetting.FtpFolder & IIf(AppSetting.FtpFolder.EndsWith("\"), "", "\") & reserveFile
            _localFilePath = AppSetting.WmsLocalFileDir & IIf(AppSetting.WmsLocalFileDir.EndsWith("\"), "", "\") & reserveFile

            '''Initilaze PrePack sku count.
            'PrePackskuCnt = 0

            If dsWMS.Tables.Count > 0 AndAlso dsWMS.Tables(0).Rows.Count > 0 Then
                objStreamWriter = New StreamWriter(_localFilePath, False)
                skuCnt = dsWMS.Tables(0).Rows.Count

                For Each drWMS In dsWMS.Tables(0).Rows
                    'Set parameters & get data from GERS passing sku from drWMS
                    cmdParametersGERS(0).Value = drWMS("STYLE") & "-" & drWMS("STYLE_SFX")
                    dsGERS = _gersCn.ExecuteDataSet(AppSetting.GERSBackstockPkg & ".GET_UDF_DATA", cmdParametersGERS)

                    ''If dsGERS.Tables.Count > 0 AndAlso dsGERS.Tables(0).Rows.Count > 1 Then
                    ''PrePackskuCnt = PrePackskuCnt + 1
                    '' ' ** PRE-PACK ** We have a Pre-Pack sku if more than 1 row so loop thru each row
                    ''For Each drGERS In dsGERS.Tables(0).Rows
                    ''With objStreamWriter
                    ''.Write(drWMS("WHSE") & "|")                               '-- Warehouse #
                    ''Write(drWMS("QTY_AVAIL") * drGERS("PRE_PACK_QTY") & "|") '-- Qty Available * Prepack
                    ''.Write(drWMS("STYLE") & "|")                              '-- Item #
                    ''.Write(drWMS("DFLT_VENDOR") & "|")                        '-- Vendor #
                    ''.Write(drWMS("STORE_DEPT") & "|")                         '-- Div #
                    ''.Write(drWMS("MISC_ALPHA_1") & "|")                       '-- Dept #
                    ''.Write(UtilityManager.NullToString(drWMS("MISC_NUMERIC_1")).PadLeft(4, "0") & "|") '-- Class #
                    ''.Write(drWMS("SALE_GRP") & "|")                           '-- Sub Class #
                    ''.Write(drWMS("SKU_DESC") & "|")                           '-- Sku Desc
                    ''.Write(drWMS("PROD_GROUP") & "|")                         '-- UOM Cd
                    ''.Write(drWMS("PROD_SUB_GRP") & "|")                       '-- UOM Qty 
                    ''.Write(drWMS("RETAIL_PRICE") & "|")                       '-- Retail Price
                    ''.Write(drGERS("LIST_PRC") & "|")                          '-- List Price from GERS                                
                    ''.Write(drGERS("SKU_NUM") & "|")                           '-- Component Sku from GERS
                    ''.Write(drWMS("PROD_SUB_GRP") & "|")                       '-- Size Of Multiple
                    ''.Write(drGERS("SIZE_CD") & "|")                           '-- size # from GERS
                    ''.Write(drGERS("COLOR_CD") & "|")                          '-- color # from GERS
                    ''.Write(drGERS("UDF1") & "|")
                    ''.Write(drGERS("UDF2") & "|")
                    ''.Write(drGERS("UDF3") & "|")
                    ''.Write(drGERS("UDF4") & "|")
                    ''.Write(drGERS("UDF5") & "|")
                    ''.Write(drGERS("UDF6") & "|")
                    ''.Write(Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year & "|")
                    ''.Write(drGERS("PACK_ID") & "|")                           '-- Prepack ID from GERS
                    ''.Write(drGERS("PRE_PACK_QTY") & "|")                      '-- Prepack quantity
                    ''.Write(drGERS("UDF030") & "|")
                    ''.Write(drGERS("UDF072") & "|")
                    ''.Write(drGERS("UDF073") & "|")
                    ''.Write(drGERS("UDF074") & "|")
                    ''.Write(drGERS("UDF075") & "|")
                    ''.Write(drGERS("UDF076") & "|")
                    ''.Write(drGERS("UDF077") & "|")
                    ''.Write(drGERS("UDF078") & "|")
                    ''.Write(drGERS("UDF079") & "|")
                    ''.Write(drGERS("UDF080") & "|")
                    ''.Write(drGERS("UDF081") & "|")
                    ''.Write(drGERS("UDF082") & "|")
                    ''.Write(drGERS("UDF083") & "|")
                    ''.Write(drGERS("UDF084") & "|")
                    ''.Write(drGERS("UDF085") & "|")
                    ''.Write(drGERS("UDF086") & "|")
                    ''.Write(drGERS("UDF087") & "|")
                    ''.Write(drGERS("UDF088") & "|")
                    ''.Write(drGERS("UDF089") & "|")
                    ''.Write(drGERS("UDF090") & "|")
                    ''.Write(drGERS("UDF091") & "|")
                    ''.Write(drGERS("UDF092") & "|")
                    ''.Write(drGERS("UDF093") & "|")
                    ''.Write(drGERS("UDF094") & "|")
                    ''.Write(drGERS("UDF095") & "|")
                    ''.Write(drGERS("UDF096") & "|")
                    ''.Write(drGERS("UDF097") & "|")
                    ''.Write(drGERS("UDF098") & "|")
                    ''.Write(drGERS("UDF099") & "|")
                    ''.Write(drGERS("UDF029"))
                    ' .Write(Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year)
                    ''.Write(vbCrLf)
                    ''End With
                    ''Next
                    ''Else ' We have a single sku from GERS (Non PrePack) or no row from GERS
                    'Add empty row if we have none in GERS to write null values in file
                    If dsGERS.Tables(0).Rows.Count = 0 Then
                        drGERS = dsGERS.Tables(0).Rows.Add(EmptyRowArray)
                    End If
                    For Each drGERS In dsGERS.Tables(0).Rows
                        With objStreamWriter
                            .Write(drWMS("WHSE") & "|")                               '-- Warehouse #
                            .Write(drWMS("QTY_AVAIL") & "|")                          '-- Qty Available 
                            .Write(drWMS("STYLE") & "|")                              '-- Item #
                            .Write(drWMS("DFLT_VENDOR") & "|")                        '-- Vendor #
                            .Write(drWMS("STORE_DEPT") & "|")                         '-- Div #
                            .Write(drWMS("MISC_ALPHA_1") & "|")                       '-- Dept #
                            .Write(UtilityManager.NullToString(drWMS("MISC_NUMERIC_1")).PadLeft(4, "0") & "|") '-- Class #
                            .Write(drWMS("SALE_GRP") & "|")                           '-- Sub Class #
                            .Write(drWMS("SKU_DESC") & "|")                           '-- Sku Desc
                            .Write(drWMS("PROD_GROUP") & "|")                         '-- UOM Cd
                            .Write(drWMS("PROD_SUB_GRP") & "|")                       '-- UOM Qty 
                            .Write(drWMS("RETAIL_PRICE") & "|")                       '-- Retail Price
                            .Write(drWMS("UNIT_PRICE") & "|")                         '-- Cost Price
                            .Write(drWMS("STYLE") & "-" & drWMS("STYLE_SFX") & "|")   '-- SKU Nbr
                            .Write(drWMS("PROD_SUB_GRP") & "|")                       '-- Size Of Multiple 
                            .Write(drWMS("MISC_ALPHA_3") & "|")                       '-- size #
                            .Write(drWMS("MISC_ALPHA_2") & drWMS("COLOR_SFX") & "|")  '-- color #
                            .Write(drGERS("UDF1") & "|")
                            .Write(drGERS("UDF2") & "|")
                            .Write(drGERS("UDF3") & "|")
                            .Write(drGERS("UDF4") & "|")
                            .Write(drGERS("UDF5") & "|")
                            .Write(drGERS("UDF6") & "|")
                            .Write(Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year & "|")
                            ''.Write(drGERS("PACK_ID") & "|")                           '-- Prepack ID from GERS (Will be NULL)
                            ''.Write(drGERS("PRE_PACK_QTY") & "|")                      '-- Prepack quantity from GERS (WIll be NULL) 
                            '.Write(drGERS("UDF029") & "|")
                            .Write(drGERS("UDF030") & "|")
                            .Write(drGERS("UDF072") & "|")
                            .Write(drGERS("UDF073") & "|")
                            .Write(drGERS("UDF074") & "|")
                            .Write(drGERS("UDF075") & "|")
                            .Write(drGERS("UDF076") & "|")
                            .Write(drGERS("UDF077") & "|")
                            .Write(drGERS("UDF078") & "|")
                            .Write(drGERS("UDF079") & "|")
                            .Write(drGERS("UDF080") & "|")
                            .Write(drGERS("UDF081") & "|")
                            .Write(drGERS("UDF082") & "|")
                            .Write(drGERS("UDF083") & "|")
                            .Write(drGERS("UDF084") & "|")
                            .Write(drGERS("UDF085") & "|")
                            .Write(drGERS("UDF086") & "|")
                            .Write(drGERS("UDF087") & "|")
                            .Write(drGERS("UDF088") & "|")
                            .Write(drGERS("UDF089") & "|")
                            .Write(drGERS("UDF090") & "|")
                            .Write(drGERS("UDF091") & "|")
                            .Write(drGERS("UDF092") & "|")
                            .Write(drGERS("UDF093") & "|")
                            .Write(drGERS("UDF094") & "|")
                            .Write(drGERS("UDF095") & "|")
                            .Write(drGERS("UDF096") & "|")
                            .Write(drGERS("UDF097") & "|")
                            .Write(drGERS("UDF098") & "|")
                            .Write(drGERS("UDF099") & "|")
                            .Write(drGERS("UDF029"))
                            '.Write(Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year)
                            .Write(vbCrLf)
                        End With


                        ''End If

                    Next
                Next

                'clean house 
                objStreamWriter.Close()
                Dim _file As File

                'Send xml file to BizTalk if the file exists
                If _file.Exists(_localFilePath) Then
                    _file.Move(_localFilePath, _filePath)
                    SendAckXmlFile(AppSetting.AckFileDir, reserveFile, AppSetting.UtlFileDir, WarehouseNbr)
                    UpdateBackStockHistory(WarehouseNbr.ToString(), deptStr, "RESERVE_UPDATE_DATE")
                End If

                successMsg = skuCnt & " records found, "
                successMsg &= "Backstock File '" & reserveFile & "' submitted to Biztalk process"
                SendReserveUpdateEmail(WarehouseNbr, deptStr, True, reserveFile, skuCnt) '', PrePackskuCnt)
            Else
                    successMsg = "No records found"
            End If
            Return successMsg

        Catch ex As OracleException
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Catch ex As BackStockAppException
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Catch ex As FileNotFoundException
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Catch ex As DirectoryNotFoundException
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Catch ex As Security.SecurityException
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Catch ex As Exception
            _errMsg = ex.Message
            Throw ex
        Finally
            If _errMsg <> "" Then
                SendReserveUpdateEmail(WarehouseNbr, deptStr, False, reserveFile, skuCnt, _errMsg)
                ''PrePackskuCnt, _errMsg)
            End If
        End Try

    End Function


    '*********** kjz version 1.0 of this function backup.  See above and delete when above is working *******
    '''Public Function ReserveUpdateNew(ByVal WarehouseNbr As Int16, ByVal deptStr As String) As String
    '''    Dim reserveFile As String = ""
    '''    Dim successMsg As String = ""
    '''    Dim i, skuCnt As Int16
    '''    Try
    '''        'Call Resserve clear to backstock data in Arthur
    '''        ReserveClear(WarehouseNbr, deptStr, False)
    '''        _errMsg = ""

    '''        Dim dr, dr1 As DataRow
    '''        Dim ds, ds1 As DataSet
    '''        'Dim sku As String
    '''        Dim strSql, cur_style, _timeStamp, _filePath, _localFilePath As String
    '''        Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
    '''        Dim cmdParametersUDF() As OracleParameter = New OracleParameter(1) {}

    '''        cmdParameters(0) = New OracleParameter("p_cur_Backstock", OracleType.Cursor)
    '''        cmdParameters(0).Direction = ParameterDirection.Output
    '''        cmdParameters(1) = New OracleParameter("p_dept_str", OracleType.VarChar, 4000)
    '''        cmdParameters(1).Value = deptStr

    '''        cmdParametersUDF(0) = New OracleParameter("p_Sku", OracleType.VarChar)
    '''        cmdParametersUDF(1) = New OracleParameter("p_Cur_UDF_PrePack", OracleType.Cursor)
    '''        cmdParametersUDF(1).Direction = ParameterDirection.Output

    '''        Select Case WarehouseNbr
    '''            Case EnumWarehouse.CADC
    '''                ds = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_GET_BACKSTOCK_BY_DEPT", cmdParameters)
    '''            Case EnumWarehouse.TNDC
    '''                ds = _tnDcCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_GET_BACKSTOCK_BY_DEPT", cmdParameters)
    '''        End Select

    '''        Dim objStreamWriter As StreamWriter
    '''        _timeStamp = Now.Month.ToString.PadLeft(2, "0") & Now.Day.ToString.PadLeft(2, "0") & Now.Year.ToString & "_" & Now.Hour.ToString.PadLeft(2, "0") & Now.Minute.ToString.PadLeft(2, "0") & Now.Second.ToString.PadLeft(2, "0")
    '''        reserveFile = "Reserve_" & WarehouseNbr & "_" & _timeStamp & ".txt"
    '''        _filePath = AppSetting.FtpFolder & IIf(AppSetting.FtpFolder.EndsWith("\"), "", "\") & reserveFile
    '''        _localFilePath = AppSetting.WmsLocalFileDir & IIf(AppSetting.WmsLocalFileDir.EndsWith("\"), "", "\") & reserveFile

    '''        If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
    '''            objStreamWriter = New StreamWriter(_localFilePath, False)
    '''            'objStreamWriter.WriteLine("RESERVE_HDR:" & _timeStamp)
    '''            skuCnt = ds.Tables(0).Rows.Count
    '''            For Each dr In ds.Tables(0).Rows
    '''                'If n > 0 Then
    '''                '    objStreamWriter.Write(objStreamWriter.NewLine)
    '''                'End If
    '''                'n += 1
    '''                'write to backstock file
    '''                'objStreamWriter.Write("ITM_DTL:")
    '''                objStreamWriter.Write(dr("WHSE") & "|") ' -- Warehouse #
    '''                objStreamWriter.Write(dr("QTY_AVAIL") & "|") '-- Qty Available
    '''                objStreamWriter.Write(dr("STYLE") & "|") '-- Item #
    '''                objStreamWriter.Write(dr("DFLT_VENDOR") & "|") '-- Vendor #
    '''                objStreamWriter.Write(dr("STORE_DEPT") & "|") ' -- Div #
    '''                objStreamWriter.Write(dr("MISC_ALPHA_1") & "|") '-- Dept #
    '''                objStreamWriter.Write(UtilityManager.NullToString(dr("MISC_NUMERIC_1")).PadLeft(4, "0") & "|") '-- Class #
    '''                objStreamWriter.Write(dr("SALE_GRP") & "|") ' -- Sub Class #
    '''                objStreamWriter.Write(dr("SKU_DESC") & "|") '-- Sku Desc
    '''                objStreamWriter.Write(dr("PROD_GROUP") & "|") '-- UOM Cd
    '''                objStreamWriter.Write(dr("PROD_GROUP") & "|") '-- UOM Qty
    '''                objStreamWriter.Write(dr("RETAIL_PRICE") & "|") '-- Retail Price
    '''                objStreamWriter.Write(dr("UNIT_PRICE") & "|") '-- Cost Price
    '''                objStreamWriter.Write(dr("STYLE") & "-" & dr("STYLE_SFX") & "|") '-- SKU Nbr
    '''                objStreamWriter.Write(dr("PROD_SUB_GRP") & "|") '--Size Of Multiple
    '''                objStreamWriter.Write(dr("MISC_ALPHA_3") & "|") ' -- size #
    '''                objStreamWriter.Write(dr("MISC_ALPHA_2") & dr("COLOR_SFX") & "|") ' -- color #

    '''                '******************************************************
    '''                'GET UDF info from GERS
    '''                '******************************************************
    '''                If cur_style <> dr("style") Then
    '''                    cur_style = dr("style")

    '''                    'Set SKU value  
    '''                    cmdParametersUDF(0).Value = dr("STYLE") & "-" & dr("STYLE_SFX")
    '''                    ds1 = _gersCn.ExecuteDataSet(AppSetting.GERSBackstockPkg & ".GET_UDF_PREPACK_DATA", cmdParametersUDF)

    '''                    If ds1.Tables.Count > 0 AndAlso ds1.Tables(0).Rows.Count > 0 Then

    '''                        ' not sure if this next line is needed but was in old code
    '''                        dr1 = ds1.Tables(0).Rows(0)

    '''                        For Each dr1 In ds1.Tables(0).Rows
    '''                            With objStreamWriter
    '''                                .Write(dr1("PACK_ID") & "|")
    '''                                .Write(dr1("UDF1") & "|")
    '''                                .Write(dr1("UDF2") & "|")
    '''                                .Write(dr1("UDF3") & "|")
    '''                                .Write(dr1("UDF4") & "|")
    '''                                .Write(dr1("UDF5") & "|")
    '''                                .Write(dr1("UDF6") & "|")
    '''                                .Write(dr1("UDF030") & "|")
    '''                                .Write(dr1("UDF072") & "|")
    '''                                .Write(dr1("UDF073") & "|")
    '''                                .Write(dr1("UDF074") & "|")
    '''                                .Write(dr1("UDF075") & "|")
    '''                                .Write(dr1("UDF076") & "|")
    '''                                .Write(dr1("UDF077") & "|")
    '''                                .Write(dr1("UDF078") & "|")
    '''                                .Write(dr1("UDF079") & "|")
    '''                                .Write(dr1("UDF080") & "|")
    '''                                .Write(dr1("UDF081") & "|")
    '''                                .Write(dr1("UDF082") & "|")
    '''                                .Write(dr1("UDF083") & "|")
    '''                                .Write(dr1("UDF084") & "|")
    '''                                .Write(dr1("UDF085") & "|")
    '''                                .Write(dr1("UDF086") & "|")
    '''                                .Write(dr1("UDF087") & "|")
    '''                                .Write(dr1("UDF088") & "|")
    '''                                .Write(dr1("UDF089") & "|")
    '''                                .Write(dr1("UDF090") & "|")
    '''                                .Write(dr1("UDF091") & "|")
    '''                                .Write(dr1("UDF092") & "|")
    '''                                .Write(dr1("UDF093") & "|")
    '''                                .Write(dr1("UDF094") & "|")
    '''                                .Write(dr1("UDF095") & "|")
    '''                                .Write(dr1("UDF096") & "|")
    '''                                .Write(dr1("UDF097") & "|")
    '''                                .Write(dr1("UDF098") & "|")
    '''                                .Write(dr1("UDF099") & "|")

    '''                                .Write(Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year)
    '''                                .Write(vbCrLf)
    '''                            End With
    '''                        Next

    '''                    Else
    '''                        'we have no data return from GERS so write blanks
    '''                        With objStreamWriter
    '''                            For i = 1 To 36
    '''                                .Write("" & "|")
    '''                            Next
    '''                            .Write(Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year)
    '''                            .Write(vbCrLf)
    '''                        End With

    '''                    End If
    '''                Else
    '''                    ' we have the same sku so write blanks
    '''                    With objStreamWriter
    '''                        For i = 1 To 36
    '''                            .Write("" & "|")
    '''                        Next
    '''                        .Write(Now.Day.ToString & "-" & MonthName(Now.Month) & "-" & Now.Year)
    '''                        .Write(vbCrLf)
    '''                    End With

    '''                End If

    '''            Next
    '''            objStreamWriter.Close()
    '''            Dim _file As File
    '''            If _file.Exists(_localFilePath) Then
    '''                _file.Move(_localFilePath, _filePath)
    '''                SendAckXmlFile(AppSetting.AckFileDir, reserveFile, AppSetting.UtlFileDir, WarehouseNbr)
    '''                UpdateBackStockHistory(WarehouseNbr.ToString(), deptStr, "RESERVE_UPDATE_DATE")
    '''            End If
    '''            successMsg = skuCnt & " records found, "
    '''            successMsg &= "Backstock File '" & reserveFile & "' submitted to Biztalk process"
    '''            SendReserveUpdateEmail(WarehouseNbr, deptStr, True, reserveFile, skuCnt)
    '''        Else
    '''            successMsg = "No records found"
    '''        End If
    '''        Return successMsg
    '''    Catch ex As OracleException
    '''        _errMsg = ex.Message
    '''        Throw New BackStockAppException(ex.Message)
    '''    Catch ex As BackStockAppException
    '''        _errMsg = ex.Message
    '''        Throw New BackStockAppException(ex.Message)
    '''    Catch ex As FileNotFoundException
    '''        _errMsg = ex.Message
    '''        Throw New BackStockAppException(ex.Message)
    '''    Catch ex As DirectoryNotFoundException
    '''        _errMsg = ex.Message
    '''        Throw New BackStockAppException(ex.Message)
    '''    Catch ex As Security.SecurityException
    '''        _errMsg = ex.Message
    '''        Throw New BackStockAppException(ex.Message)
    '''    Catch ex As Exception
    '''        _errMsg = ex.Message
    '''        Throw ex
    '''    Finally
    '''        If _errMsg <> "" Then
    '''            SendReserveUpdateEmail(WarehouseNbr, deptStr, False, reserveFile, skuCnt, _errMsg)
    '''        End If
    '''    End Try

    '''End Function

    'Public Function ReserveUpdate() As String
    '    Dim reserveFile As String = ""
    '    Dim successMsg As String = ""
    '    Try
    '        'Call Resserve clear to backstock data in Arthur
    '        ReserveClear(False)
    '        _errMsg = ""
    '        Dim dr As OracleDataReader
    '        Dim cmdParameters() As OracleParameter = New OracleParameter(6) {}
    '        cmdParameters(0) = New OracleParameter("p_localfile_dir", OracleType.VarChar, 40)
    '        cmdParameters(0).Value = AppSetting.WmsLocalFileDir
    '        cmdParameters(1) = New OracleParameter("p_ftp_user", OracleType.VarChar, 30)
    '        cmdParameters(1).Value = AppSetting.FtpUser
    '        cmdParameters(2) = New OracleParameter("p_ftp_pwd", OracleType.VarChar, 30)
    '        cmdParameters(2).Value = AppSetting.FtpPwd
    '        cmdParameters(3) = New OracleParameter("p_ftp_host", OracleType.VarChar, 30)
    '        cmdParameters(3).Value = AppSetting.FtpHost
    '        cmdParameters(4) = New OracleParameter("p_ftp_remotepath", OracleType.VarChar, 40)
    '        cmdParameters(4).Value = AppSetting.FtpFolder
    '        cmdParameters(5) = New OracleParameter("p_file_name", OracleType.VarChar, 40)
    '        cmdParameters(5).Direction = ParameterDirection.Output
    '        cmdParameters(6) = New OracleParameter("p_rec_cnt", OracleType.Int32)
    '        cmdParameters(6).Direction = ParameterDirection.Output

    '        'dr = _wmsCn.ExecuteReader("HT_BACKSTOCK.PROC_PIC_LOCN_FTP", cmdParameters)
    '        dr = _wmsCn.ExecuteReader(AppSetting.WMBackstockPkg & ".PROC_PIC_LOCN_FTP", cmdParameters)
    '        reserveFile = cmdParameters(5).Value
    '        If reserveFile <> "" Then
    '            _errMsg = ""
    '            successMsg = cmdParameters(6).Value & " records found, "
    '            successMsg &= "Backstock File '" & reserveFile & "' submitted to Biztalk process"
    '            SendReserveUpdateEmail(True, reserveFile, cmdParameters(6).Value)
    '        Else
    '            _errMsg = "Failed to create backstock file"
    '            Throw New BackStockAppException(_errMsg)
    '        End If
    '        Return successMsg
    '    Catch ex As OracleException
    '        _errMsg = ex.Message
    '        Throw New BackStockAppException(ex.Message)
    '    Catch ex As Exception
    '        _errMsg = ex.Message
    '        Throw ex
    '    Finally
    '        If _errMsg <> "" Then
    '            SendReserveUpdateEmail(False, reserveFile, _errMsg)
    '        End If
    '    End Try
    'End Function
    Public Function ReserveBatch(ByVal WarehouseNbr As Int16, Optional ByVal deptStr As String = "") As Int16
        Try
            Dim rtnVal As Int16

            If deptStr.Length > 4000 Then
                Throw New BackStockAppException("Too many departments selected, max 80  departments can be selected.")
            End If
            Dim cmdParameters() As OracleParameter = New OracleParameter(4) {}
            cmdParameters(0) = New OracleParameter("p_warehouse_nbr", OracleType.VarChar, 4)
            cmdParameters(0).Value = WarehouseNbr
            cmdParameters(1) = New OracleParameter("p_biztalkfile_path", OracleType.VarChar, 40)
            cmdParameters(1).Value = AppSetting.BatchBizTalkFileDir
            cmdParameters(2) = New OracleParameter("p_dept_str", OracleType.VarChar, 4000)
            cmdParameters(2).Value = deptStr
            cmdParameters(3) = New OracleParameter("p_rec_cnt", OracleType.Int32)
            cmdParameters(3).Direction = ParameterDirection.Output
            cmdParameters(3).Value = 0
            cmdParameters(4) = New OracleParameter("p_batch_id", OracleType.Int32)
            cmdParameters(4).Direction = ParameterDirection.Output
            cmdParameters(4).Value = 0

            _errMsg = ""
            rtnVal = _aaCn.ExecuteNonQuery(AppSetting.AABackstockPkg & ".PROC_WORKLIST_RESERVE_BATCH", cmdParameters)
            If rtnVal = 1 Then
                If cmdParameters(3).Value > 0 Then
                    SendReserveBatchEmail(WarehouseNbr, True, deptStr, cmdParameters(3).Value, cmdParameters(4).Value)
                    UpdateBackStockHistory(WarehouseNbr.ToString(), deptStr, "RESERVE_BATCH_DATE")
                Else
                    Throw New BackStockAppException("No records found")
                End If
            End If
            Return rtnVal
        Catch ex As OracleException
            _errMsg = ex.Message
            Throw New BackStockAppException(ex.Message)
        Catch ex As Exception
            _errMsg = ex.Message
            Throw ex
        End Try
    End Function
    Public Function ListDepartMents_AA(Optional ByVal divCd As EnumDivCd = EnumDivCd.None) As DataSet
        Try
            Dim companyFilterStr As String = ""
            Select Case divCd
                Case EnumDivCd.HotTopic
                    companyFilterStr = "DIV_CD IN (" & EnumDivCd.HotTopic & ") AND DEPT_CD NOT BETWEEN '7000' AND '7099'"
                Case EnumDivCd.Torrid
                    companyFilterStr = "DIV_CD IN (" & EnumDivCd.Torrid & ")"
                Case EnumDivCd.HotTopicCanada
                    companyFilterStr = "LPAD(DEPT_CD, 4, '0')BETWEEN '7000' AND '7099' "
                Case EnumDivCd.ShockHound
                    companyFilterStr = "DIV_CD IN (" & EnumDivCd.ShockHound & ")  "
                Case Else
                    companyFilterStr = EnumDivCd.HotTopic & "," & EnumDivCd.Torrid & "," & EnumDivCd.ShockHound
            End Select
            _strSql = "SELECT LPAD(DEPT_CD, 4, '0')as DEPT_CD, DIV_CD , DES as Description FROM C$_LS_DEPT " & _
                        " WHERE " & companyFilterStr & " ORDER BY DIV_CD, DEPT_CD"
            Return _aaCn.ExecuteDataSet(_strSql)
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    'Public Function ListDepartMents(Optional ByVal divCd As EnumDivCd = EnumDivCd.None) As DataSet
    '    Try
    '        Dim companyFilterStr As String = ""
    '        Select Case divCd
    '            Case EnumDivCd.HotTopic
    '                companyFilterStr = "DIV_CD IN (" & EnumDivCd.HotTopic & ") AND DEPT_CD NOT BETWEEN '7000' AND '7099'"
    '            Case EnumDivCd.Torrid
    '                companyFilterStr = "DIV_CD IN (" & EnumDivCd.Torrid & ")"
    '            Case EnumDivCd.HotTopicCanada
    '                companyFilterStr = "DEPT_CD BETWEEN '7000' AND '7099' "
    '            Case EnumDivCd.ShockHound
    '                companyFilterStr = "DIV_CD IN (" & EnumDivCd.ShockHound & ")  "
    '            Case Else
    '                companyFilterStr = EnumDivCd.HotTopic & "," & EnumDivCd.Torrid & "," & EnumDivCd.ShockHound
    '        End Select
    '        _strSql = "SELECT DEPT_CD, DIV_CD , DES as Description FROM DEPT " & _
    '                    " WHERE " & companyFilterStr & " ORDER BY DIV_CD, DEPT_CD"
    '        Return _gersCn.ExecuteDataSet(_strSql)
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function
    Private Sub SendReserveClearEmail(ByVal WarehouseNbr As Int16, ByVal deptStr As String, ByVal successFlag As Boolean, Optional ByVal failureReason As String = "N/A")
        Try
            Dim emailMgr As EmailManager = New EmailManager(AppSetting.SmtpServer)
            Dim mailBody, sucessStr, subject As String
            If successFlag Then
                sucessStr = "SUCCESS"
                subject = "Reserve clear completed"
            Else
                sucessStr = "FAILED"
                subject = "Reserve clear failed"
            End If
            mailBody = AppSetting.ReserveClearEmailBody
            mailBody = mailBody.Replace("%processDate%", Now)
            mailBody = mailBody.Replace("%whse%", WarehouseNbr)
            mailBody = mailBody.Replace("%deptStr%", deptStr)
            mailBody = mailBody.Replace("%status%", sucessStr)
            mailBody = mailBody.Replace("%reason%", failureReason)
            With emailMgr
                .AddRecipient(AppSetting.EMailReceivers)
                .Sender = AppSetting.SmtpSender
                .Subject = subject
                .Format = Web.Mail.MailFormat.Html
                .Priority = Web.Mail.MailPriority.Normal
                .Send(mailBody)
            End With
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    Private Sub SendReserveBatchEmail(ByVal WarehouseNbr As Int16, ByVal successFlag As Boolean, ByVal deptStr As String, ByVal skuCnt As Int16, ByVal batchID As Int16, Optional ByVal failureReason As String = "N/A")
        Try
            Dim emailMgr As EmailManager = New EmailManager(AppSetting.SmtpServer)
            Dim mailBody, sucessStr, subject As String
            If successFlag Then
                sucessStr = "SUCCESS"
                subject = "Reserve batch completed"
            Else
                sucessStr = "FAILED"
                subject = "Reserve batch failed"
            End If
            mailBody = AppSetting.ReserveBatchEmailBody
            mailBody = mailBody.Replace("%processDate%", Now)
            mailBody = mailBody.Replace("%whse%", WarehouseNbr)
            mailBody = mailBody.Replace("%deptStr%", deptStr)
            mailBody = mailBody.Replace("%skuCnt%", skuCnt)
            mailBody = mailBody.Replace("%batchID%", batchID)
            mailBody = mailBody.Replace("%status%", sucessStr)
            mailBody = mailBody.Replace("%reason%", failureReason)
            With emailMgr
                .AddRecipient(AppSetting.EMailReceivers)
                .Sender = AppSetting.SmtpSender
                .Subject = subject
                .Format = Web.Mail.MailFormat.Html
                .Priority = Web.Mail.MailPriority.Normal
                .Send(mailBody)
            End With
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    Private Sub SendReserveUpdateEmail(ByVal WarehouseNbr As Int16, ByVal deptStr As String, ByVal successFlag As Boolean, ByVal reserveFile As String, ByVal skuCnt As Int32, Optional ByVal failureReason As String = "N/A")
        '' ByVal PrePackskuCnt As Int32, Optional ByVal failureReason As String = "N/A")
        Try
            Dim emailMgr As EmailManager = New EmailManager(AppSetting.SmtpServer)
            Dim mailBody, sucessStr, subject As String
            If successFlag Then
                sucessStr = "SUCCESS"
                subject = "Reserve update - backstock file submitted to biztalk process"
            Else
                sucessStr = "FAILED"
                subject = "Reserve update failed"
            End If
            mailBody = AppSetting.ReserveUpdateEmailBody
            mailBody = mailBody.Replace("%processDate%", Now)
            mailBody = mailBody.Replace("%whse%", WarehouseNbr)
            mailBody = mailBody.Replace("%deptStr%", deptStr)
            mailBody = mailBody.Replace("%reserveFile%", reserveFile)
            mailBody = mailBody.Replace("%skuCnt%", skuCnt)
            mailBody = mailBody.Replace("%status%", sucessStr)
            mailBody = mailBody.Replace("%reason%", failureReason)
            With emailMgr
                .AddRecipient(AppSetting.EMailReceivers)
                .Sender = AppSetting.SmtpSender
                .Subject = subject
                .Format = Web.Mail.MailFormat.Html
                .Priority = Web.Mail.MailPriority.Normal
                .Send(mailBody)
            End With
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    Public Sub CheckExportStatus(ByVal WHSE As Int16)
        Try
            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            Dim dr As DataRow
            Dim ds As DataSet
            Dim dr1 As OracleDataReader

            cmdParameters(0) = New OracleParameter("p_warehouse_nbr", OracleType.VarChar)
            cmdParameters(0).Value = WHSE
            cmdParameters(1) = New OracleParameter("p_cur_Allocs", OracleType.Cursor)
            cmdParameters(1).Direction = ParameterDirection.Output
            ds = _aaCn.ExecuteDataSet(AppSetting.AABackstockPkg & ".LIST_EXPORT_PENDING_ALLOCS", cmdParameters)

            For Each dr In ds.Tables(0).Rows
                'get the count from WM STORE_DISTRO table
                Dim cmdParams() As OracleParameter = New OracleParameter(2) {}
                cmdParams(0) = New OracleParameter("p_distro_nbr", OracleType.VarChar)
                cmdParams(0).Value = dr("ALLOC_NBR")
                cmdParams(1) = New OracleParameter("p_batch_id", OracleType.VarChar)
                cmdParams(1).Value = dr("BATCH_ID")
                cmdParams(2) = New OracleParameter("p_rec_cnt", OracleType.Int16)
                cmdParams(2).Direction = ParameterDirection.Output
                Select Case WHSE
                    Case EnumWarehouse.CADC
                        dr1 = _wmsCn.ExecuteReader(AppSetting.WMBackstockPkg & ".PROC_GET_DISTRO_COUNT", cmdParams)
                    Case EnumWarehouse.TNDC
                        dr1 = _tnDcCn.ExecuteReader(AppSetting.WMBackstockPkg & ".PROC_GET_DISTRO_COUNT", cmdParams)
                End Select
                'dr1 = _wmsCn.ExecuteReader(AppSetting.WMBackstockPkg & ".PROC_GET_DISTRO_COUNT", cmdParams)
                If cmdParams(2).Value > 0 Then
                    '-- update AA 
                    Dim cmdParams1() As OracleParameter = New OracleParameter(5) {}
                    cmdParams1(0) = New OracleParameter("p_batch_id", OracleType.Int32)
                    cmdParams1(0).Value = dr("BATCH_ID")
                    cmdParams1(1) = New OracleParameter("p_alloc_nbr", OracleType.Int32)
                    cmdParams1(1).Value = dr("ALLOC_NBR")
                    cmdParams1(2) = New OracleParameter("p_warehouse_nbr", OracleType.VarChar)
                    cmdParams1(2).Value = WHSE
                    cmdParams1(3) = New OracleParameter("p_export_flag", OracleType.Int32)
                    cmdParams1(3).Value = 1
                    cmdParams1(4) = New OracleParameter("p_email_sent_flag", OracleType.Int32)
                    cmdParams1(4).Value = 0
                    cmdParams1(5) = New OracleParameter("p_user_id", OracleType.VarChar)
                    cmdParams1(5).Value = "SYSTEM"
                    _aaCn.ExecuteNonQuery(AppSetting.AABackstockPkg & ".PROC_UPDATE_HT_RESERVE_BATCH", cmdParams1)
                End If
            Next
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    Public Function GetWaveCheckReport(ByVal waveNbr As String, ByVal WarehouseNbr As Int16) As DataSet
        Try
            Dim ds As DataSet
            _strSql = "SELECT IWM.MISC_ALPHA_1 AS DEPT, DISTRO_NBR,SHIP_WAVE_NBR, " & _
                        " I.STYLE || '-' || I.STYLE_SFX AS SKU_NUM, SUM(REQD_QTY)  AS REQD_QTY, " & _
                        " SUM(ORIG_REQ_QTY) AS ORIG_REQ_QTY " & _
                        " FROM STORE_DISTRO D , ITEM_MASTER I, ITEM_WHSE_MASTER IWM " & _
                        " WHERE SHIP_WAVE_NBR = '" & waveNbr & "' AND D.SKU_ID = I.SKU_ID AND DISTRO_TYPE = '1'  AND I.SKU_ID = IWM.SKU_ID " & _
                        " GROUP BY IWM.MISC_ALPHA_1,DISTRO_NBR, SHIP_WAVE_NBR, I.STYLE || '-' || I.STYLE_SFX  " & _
                        " ORDER BY 1,2,3,4"
            Select Case WarehouseNbr
                Case EnumWarehouse.CADC
                    ds = _wmsCn.ExecuteDataSet(_strSql)
                Case EnumWarehouse.TNDC
                    ds = _tnDcCn.ExecuteDataSet(_strSql)
            End Select
            'ds = _wmsCn.ExecuteDataSet(_strSql)
            Return ds
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function ReserveUpdateStatus() As DataSet
        Dim ds As DataSet
        Dim sb As New StringBuilder

        sb.Append("SELECT his.WAREHOUSE_NBR AS WAREHOUSE , his.DEPT_NBR, his.RESERVE_UPDATE_DATE, his.RESERVE_BATCH_DATE , his.RESERVE_CLEAR_DATE ")
        sb.Append("FROM ht_backstock_history his ")
        'sb.Append("RIGHT JOIN ls_dept dep ")
        'sb.Append("ON his.dept_nbr = dep.dept_cd ")
        'sb.Append("INNER JOIN ls_div div ")
        'sb.Append("ON dep.DIV_CD = div.DIV_CD ")
        'sb.Append("WHERE dep.DIV_CD in (1,5) ")
        sb.Append("ORDER BY his.WAREHOUSE_NBR, his.DEPT_NBR")

        Try
            ds = _aaCn.ExecuteDataSet(sb.ToString())
            Return ds
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function RunStatusCheckByBatch(ByVal batchID As Int16) As DataSet
        Try
            Dim ds As DataSet
            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            cmdParameters(0) = New OracleParameter("p_batch_id", OracleType.Int32)
            cmdParameters(0).Value = batchID
            cmdParameters(1) = New OracleParameter("p_cur_Allocs", OracleType.Cursor)
            cmdParameters(1).Direction = ParameterDirection.Output
            ds = _aaCn.ExecuteDataSet(AppSetting.AABackstockPkg & ".EXPORT_STATUS_BY_BATCH", cmdParameters)
            Return ds
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function RunReserveBatchReport(ByVal batchID As Int16, ByVal WarehouseNbr As Int16) As DataSet
        Try
            Dim ds, ds1, ds2 As DataSet
            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            cmdParameters(0) = New OracleParameter("p_batch_id", OracleType.Int32)
            cmdParameters(0).Value = batchID
            cmdParameters(1) = New OracleParameter("p_cur_Allocs", OracleType.Cursor)
            cmdParameters(1).Direction = ParameterDirection.Output
            ds1 = _aaCn.ExecuteDataSet(AppSetting.AABackstockPkg & ".LIST_ALLOCS_BY_BATCH", cmdParameters)
            ds = New DataSet
            ds.Tables.Add(ds1.Tables(0).Copy)
            ds.Tables(0).TableName = "AAAllocs"
            Dim cmdParams() As OracleParameter = New OracleParameter(1) {}
            cmdParams(0) = New OracleParameter("p_batch_id", OracleType.VarChar)
            cmdParams(0).Value = batchID
            cmdParams(1) = New OracleParameter("p_cur_distros", OracleType.Cursor)
            cmdParams(1).Direction = ParameterDirection.Output
            'ds2 = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_DISTROS_BY_BACTH", cmdParams)
            Select Case WarehouseNbr
                Case EnumWarehouse.CADC
                    ds2 = _wmsCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_DISTROS_BY_BACTH", cmdParams)
                Case EnumWarehouse.TNDC
                    ds2 = _tnDcCn.ExecuteDataSet(AppSetting.WMBackstockPkg & ".PROC_LIST_DISTROS_BY_BACTH", cmdParams)
            End Select
            ds.Tables.Add(ds2.Tables(0).Copy)
            ds.Tables(1).TableName = "WMDistros"
            Return ds
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    'RunReserveBatchReport
    Public Function GetDbServerName() As String
        Dim dbServer As String = ""
        Dim strConn As String = ""
        Dim i As Int16 = 0
        Try
            strConn = AppSetting.AAConnectionString
            Dim strArr As String() = strConn.Split(";")
            For i = 0 To UBound(strArr)
                If strArr(i).ToUpper.IndexOf("DATA SOURCE") > -1 Then
                    dbServer = strArr(i).Substring(strArr(i).IndexOf("=") + 1)
                    Exit For
                End If
            Next
        Catch ex As Exception

        End Try
        Return dbServer
    End Function
    Private Sub SendAckXmlFile(ByVal biztalkFilePath As String, ByVal fileNme As String, ByVal filePath As String, ByVal whse As String)
        Try
            Dim xmlStr As String = ""
            Dim ackFile As String = fileNme.Replace(".txt", "_ACK.xml")
            biztalkFilePath = biztalkFilePath.TrimEnd("/")
            biztalkFilePath &= IIf(biztalkFilePath.EndsWith("\"), "", "\") & ackFile

            Dim objStreamWriter As StreamWriter
            objStreamWriter = New StreamWriter(biztalkFilePath, False)
            xmlStr = "<ReserveUpdateAck>" & _
                    "<FileName>" & fileNme & "</FileName>" & _
                    "<FilePath>" & filePath & "</FilePath>" & _
                    "<WarehouseNbr>" & whse & "</WarehouseNbr>" & _
                    "</ReserveUpdateAck>"
            objStreamWriter.Write(xmlStr)
            objStreamWriter.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    '*********************************************************
    '##BEGIN## 6/2/05 Sri: Cancel distro code
    '********************************************************
    Public Function ListDistros(ByVal whse As Int16, ByVal shpmtNbr As String, ByVal distroNbr As String) As DataSet
        Try
            Dim ds As DataSet
            Dim cmdParameters() As OracleParameter = New OracleParameter(2) {}
            cmdParameters(0) = New OracleParameter("p_shpmt_nbr", OracleType.VarChar, 20)
            cmdParameters(0).Value = shpmtNbr.Trim
            cmdParameters(1) = New OracleParameter("p_distro_nbr", OracleType.VarChar, 12)
            cmdParameters(1).Value = distroNbr.Trim
            cmdParameters(2) = New OracleParameter("DistroSummary", OracleType.Cursor)
            cmdParameters(2).Direction = ParameterDirection.Output
            Select Case whse
                Case EnumWarehouse.CADC
                    ds = _wmsCn.ExecuteDataSet("HT_WAVE_BY_TOOL.PROC_CANCELDISTRO_SUMMARY", cmdParameters)
                Case EnumWarehouse.TNDC
                    ds = _tnDcCn.ExecuteDataSet("HT_WAVE_BY_TOOL.PROC_CANCELDISTRO_SUMMARY", cmdParameters)
            End Select
            Return ds
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Sub CancelDistro(ByVal whse As Int16, ByVal shpmtNbr As String, ByVal distroNbr As String)
        Try
            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            cmdParameters(0) = New OracleParameter("p_shpmt_nbr", OracleType.VarChar, 20)
            cmdParameters(0).Value = shpmtNbr.Trim
            cmdParameters(1) = New OracleParameter("p_distro_nbr", OracleType.VarChar, 12)
            cmdParameters(1).Value = distroNbr.Trim
            Select Case whse
                Case EnumWarehouse.CADC
                    _wmsCn.ExecuteNonQuery("HT_WAVE_BY_TOOL.PROC_CANCELDISTRO", cmdParameters)
                Case EnumWarehouse.TNDC
                    _tnDcCn.ExecuteNonQuery("HT_WAVE_BY_TOOL.PROC_CANCELDISTRO", cmdParameters)
            End Select
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    Public Sub PopulateWorkList(ByVal whse As Int16, ByVal shpmtNbr As String, ByVal distroNbr As String)
        Try
            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            cmdParameters(0) = New OracleParameter("p_shpmt_nbr", OracleType.VarChar, 20)
            cmdParameters(0).Value = shpmtNbr.Trim
            cmdParameters(1) = New OracleParameter("p_distro_nbr", OracleType.VarChar, 12)
            cmdParameters(1).Value = distroNbr.Trim
            Select Case whse
                Case EnumWarehouse.CADC
                    _wmsCn.ExecuteNonQuery("HT_WAVE_BY_TOOL.PROC_POPULATE_WORKLIST", cmdParameters)
                Case EnumWarehouse.TNDC
                    _tnDcCn.ExecuteNonQuery("HT_WAVE_BY_TOOL.PROC_POPULATE_WORKLIST", cmdParameters)
            End Select
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    '*********************************************************
    '##END## 6/2/05 Sri: Cancel distro code
    '********************************************************
    '*********************************************************
    '##BEGIN## 7/18/05 Sri: Distro Priority code
    '********************************************************
    'Public Sub SavePriorityGroup(ByVal prtyGrp As PriorityGroup)
    '    Try
    '        Dim cmdParameters() As OracleParameter = New OracleParameter(5) {}
    '        cmdParameters(0) = New OracleParameter("p_grp_id", OracleType.Int32)
    '        cmdParameters(0).Value = prtyGrp.grpId
    '        cmdParameters(1) = New OracleParameter("p_grp_name", OracleType.VarChar, 100)
    '        cmdParameters(1).Value = prtyGrp.grpName
    '        cmdParameters(2) = New OracleParameter("p_grp_desc", OracleType.VarChar, 100)
    '        cmdParameters(2).Value = prtyGrp.grpDesc
    '        cmdParameters(3) = New OracleParameter("p_min_locn_id", OracleType.Int32)
    '        cmdParameters(3).Value = prtyGrp.minLocnID
    '        cmdParameters(4) = New OracleParameter("p_max_locn_id", OracleType.Int32)
    '        cmdParameters(4).Value = prtyGrp.maxLocnId
    '        cmdParameters(5) = New OracleParameter("p_priority_code", OracleType.Int32)
    '        cmdParameters(5).Value = prtyGrp.priorityCode
    '        _aaCn.ExecuteNonQuery("HT_STORE_DISTRO_PRIORITY.SAVE_PRIORITY_GROUP", cmdParameters)
    '    Catch ex As OracleException
    '        If ex.Message.IndexOf("Duplicate") > 0 Then
    '            Throw New BackStockAppException("There is a conflict with another group, please enter valid store range!")
    '        Else
    '            Throw New BackStockAppException(ex.Message)
    '        End If
    '    Catch ex As Exception
    '        Throw New BackStockAppException(ex.Message)
    '    End Try
    'End Sub
    'Public Function ListPriorityGroups() As DataSet
    '    Try
    '        Dim ds As DataSet
    '        Dim cmdParameters() As OracleParameter = New OracleParameter(0) {}
    '        cmdParameters(0) = New OracleParameter("lstPriorityGrp", OracleType.Cursor)
    '        cmdParameters(0).Direction = ParameterDirection.Output
    '        ds = _aaCn.ExecuteDataSet("HT_STORE_DISTRO_PRIORITY.PROC_LIST_PRIORITY_GROUPS", cmdParameters)
    '        Return ds
    '    Catch ex As Exception
    '        Throw New BackStockAppException(ex.Message)
    '    End Try
    'End Function
    'Public Sub DeletePriorityGroupByID(ByVal grpID As Int16)
    '    Try
    '        Dim cmdParameters() As OracleParameter = New OracleParameter(0) {}
    '        cmdParameters(0) = New OracleParameter("p_grp_id", OracleType.Int32)
    '        cmdParameters(0).Value = grpID
    '        _aaCn.ExecuteNonQuery("HT_STORE_DISTRO_PRIORITY.DELETE_PRIORITY_GROUP", cmdParameters)

    '    Catch ex As Exception
    '        Throw New BackStockAppException(ex.Message)
    '    End Try
    'End Sub
    Public Function ListPriorityCodes() As DataSet
        Try
            Dim ds As DataSet
            Dim cmdParameters() As OracleParameter = New OracleParameter(0) {}
            cmdParameters(0) = New OracleParameter("lstPrtyCodes", OracleType.Cursor)
            cmdParameters(0).Direction = ParameterDirection.Output
            ds = _aaCn.ExecuteDataSet("HT_STORE_DISTRO_PRIORITY.PROC_LIST_PRTYCODE", cmdParameters)
            Return ds
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Function ListStoresByPriority(ByVal priorityCode As Int16) As DataSet
        Try
            Dim ds As DataSet
            Dim cmdParameters() As OracleParameter = New OracleParameter(2) {}
            cmdParameters(0) = New OracleParameter("p_prtyCode", OracleType.VarChar, 2)
            cmdParameters(0).Value = priorityCode.ToString.PadLeft(2, "0")
            cmdParameters(1) = New OracleParameter("lstSelectedStore", OracleType.Cursor)
            cmdParameters(1).Direction = ParameterDirection.Output
            cmdParameters(2) = New OracleParameter("lstOtherStore", OracleType.Cursor)
            cmdParameters(2).Direction = ParameterDirection.Output
            ds = _aaCn.ExecuteDataSet("HT_STORE_DISTRO_PRIORITY.PROC_LIST_STORES_BY_PRTYCODE", cmdParameters)
            Return ds
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
    Public Sub SaveStorePriority(ByVal priorityCode As Int16, ByVal storeStr As String)
        Try
            Dim cmdParameters() As OracleParameter = New OracleParameter(1) {}
            cmdParameters(0) = New OracleParameter("p_prtyCode", OracleType.VarChar, 2)
            cmdParameters(0).Value = priorityCode.ToString.PadLeft(2, "0")
            cmdParameters(1) = New OracleParameter("p_StoreStr", OracleType.VarChar, 4000)
            cmdParameters(1).Value = storeStr
            _aaCn.ExecuteNonQuery("HT_STORE_DISTRO_PRIORITY.SAVE_STORE_PRIORITY", cmdParameters)

        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Sub
    '*********************************************************
    '##END## 7/18/05 Sri: Distro Priority code
    '********************************************************
    Public Function ValidateUser(ByVal userId As String, ByVal pwd As String) As String
        Dim userName As String = ""
        Try
            Dim ds As DataSet
            Dim cmdParameters() As OracleParameter = New OracleParameter(3) {}
            cmdParameters(0) = New OracleParameter("p_userid", OracleType.VarChar, 30)
            cmdParameters(0).Value = userId
            cmdParameters(1) = New OracleParameter("p_password", OracleType.VarChar, 30)
            cmdParameters(1).Value = pwd
            cmdParameters(2) = New OracleParameter("p_validflag", OracleType.VarChar, 5)
            cmdParameters(2).Direction = ParameterDirection.Output
            cmdParameters(3) = New OracleParameter("p_fullname", OracleType.VarChar, 100)
            cmdParameters(3).Direction = ParameterDirection.Output
            _aaCn.ExecuteNonQuery("HT_VALIDATE_USER", cmdParameters)
            If Not cmdParameters(2).Value.ToString.ToUpper = "TRUE" Then
                Throw New BackStockAppException("Invalid userId/Password,please try again!")
            End If
            userName = cmdParameters(3).Value
            Return userName
        Catch ex As Exception
            Throw New BackStockAppException(ex.Message)
        End Try
    End Function
End Class
