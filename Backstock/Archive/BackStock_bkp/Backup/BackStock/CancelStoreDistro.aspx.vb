Imports BackStock.Classes
Public Class CancelStoreDistro
    Inherits System.Web.UI.Page
    Protected WithEvents lblError As System.Web.UI.WebControls.Label
    Protected WithEvents ValidationSummary1 As System.Web.UI.WebControls.ValidationSummary
    Protected WithEvents lstWhse As System.Web.UI.WebControls.DropDownList
    Protected WithEvents reqValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents txtShipmentNbr As System.Web.UI.WebControls.TextBox
    Protected WithEvents validateShipmentNbr As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents txtDistroNbr As System.Web.UI.WebControls.TextBox
    Protected WithEvents validateDistroNbr As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected ds As New DataSet
    Protected dr As DataRow
    Protected WithEvents pnlResults As System.Web.UI.WebControls.Panel
    Protected WithEvents btnCancelDistro As System.Web.UI.WebControls.Button
    Protected WithEvents hdnDistroNbr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents hdnWhse As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents reqShpmtNbr As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents reqDistroNbr As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents hdnShpmtNbr As System.Web.UI.HtmlControls.HtmlInputHidden
    Protected WithEvents chkCopyToArthur As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnCancelAndUpdateArthur As System.Web.UI.WebControls.Button
    Private _workFlowManager As New WorkFlowManager
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        lblError.Text = ""
        lblError.Visible = False
    End Sub
    Private Sub ShowMsg(ByVal msg As String)
        lblError.Text &= IIf(lblError.Text.Trim <> "", "<BR>", "") & msg
        'lblError.Text = msg
        lblError.Visible = True
    End Sub
    Private Sub BindData()
        'ds = _workFlowManager.ListDistros(lstWhse.SelectedItem.Value, txtShipmentNbr.Text.Trim, txtDistroNbr.Text.Trim)
        'pnlResults.Visible = True
        Try
            If ValidateRequest() Then
                ds = _workFlowManager.ListDistros(lstWhse.SelectedItem.Value, txtShipmentNbr.Text.Trim, txtDistroNbr.Text.Trim)
                pnlResults.Visible = True
                hdnWhse.Value = lstWhse.SelectedItem.Value
                hdnShpmtNbr.Value = txtShipmentNbr.Text
            Else
                pnlResults.Visible = False
                ShowMsg("Following error(s) occured while processing your request!")
            End If
        Catch ex As Exception
            ShowMsg(ex.Message)
        End Try
    End Sub
    Private Function ValidateRequest() As Boolean
        Dim validReqFlag As Boolean = False
        If Page.IsValid AndAlso txtShipmentNbr.Text.Trim <> "" AndAlso txtDistroNbr.Text.Trim <> "" AndAlso lstWhse.SelectedItem.Value <> "" Then
            validReqFlag = True
        End If
        Return validReqFlag
    End Function

    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        BindData()
        'Try
        '    If ValidateRequest() Then
        '        BindData()
        '        hdnWhse.Value = lstWhse.SelectedItem.Value
        '        hdnShpmtNbr.Value = txtShipmentNbr.Text
        '    Else
        '        ShowMsg("Please fill in at least one search parameter!")
        '    End If
        'Catch ex As Exception
        '    ShowMsg(ex.Message)
        'End Try
    End Sub

    Private Sub btnCancelDistro_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelDistro.Click
        Try
            Dim li As ListItem
            If hdnDistroNbr.Value.Trim <> "" Then
                _workFlowManager.CancelDistro(hdnWhse.Value, hdnShpmtNbr.Value, hdnDistroNbr.Value)
                ShowMsg("Distro # " & hdnDistroNbr.Value & " has been cancelled!")
                'If Not ValidateRequest() OrElse hdnWhse.Value <> lstWhse.SelectedItem.Value Then
                '    txtDistroNbr.Text = hdnDistroNbr.Value
                '    txtShipmentNbr.Text = hdnShpmtNbr.Value
                '    lstWhse.SelectedIndex = -1
                '    For Each li In lstWhse.Items
                '        If li.Value = hdnWhse.Value.ToString Then
                '            li.Selected = True
                '            Exit For
                '        End If
                '    Next
                'End If
                txtDistroNbr.Text = hdnDistroNbr.Value
                txtShipmentNbr.Text = hdnShpmtNbr.Value
                If hdnWhse.Value <> lstWhse.SelectedItem.Value Then
                    lstWhse.SelectedIndex = -1
                    For Each li In lstWhse.Items
                        If li.Value = hdnWhse.Value.ToString Then
                            li.Selected = True
                            Exit For
                        End If
                    Next
                End If
                hdnDistroNbr.Value = 0
                BindData()
            Else
                ShowMsg("Invalid Distro #" & hdnDistroNbr.Value & " submitted for cancellation")
            End If
        Catch ex As Exception
            ShowMsg(ex.Message)
        End Try
    End Sub
    Private Sub btnCancelAndUpdateArthur_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancelAndUpdateArthur.Click
        Try
            Dim li As ListItem
            If hdnDistroNbr.Value.Trim <> "" Then
                _workFlowManager.CancelDistro(hdnWhse.Value, hdnShpmtNbr.Value, hdnDistroNbr.Value)
                ShowMsg("Distro # " & hdnDistroNbr.Value & " has been cancelled!")
                _workFlowManager.PopulateWorkList(hdnWhse.Value, hdnShpmtNbr.Value, hdnDistroNbr.Value)
                'If Not ValidateRequest() OrElse hdnWhse.Value <> lstWhse.SelectedItem.Value Then
                '    txtDistroNbr.Text = hdnDistroNbr.Value
                '    txtShipmentNbr.Text = hdnShpmtNbr.Value
                '    lstWhse.SelectedIndex = -1
                '    For Each li In lstWhse.Items
                '        If li.Value = hdnWhse.Value.ToString Then
                '            li.Selected = True
                '            Exit For
                '        End If
                '    Next
                'End If
                txtDistroNbr.Text = hdnDistroNbr.Value
                txtShipmentNbr.Text = hdnShpmtNbr.Value
                If hdnWhse.Value <> lstWhse.SelectedItem.Value Then
                    lstWhse.SelectedIndex = -1
                    For Each li In lstWhse.Items
                        If li.Value = hdnWhse.Value.ToString Then
                            li.Selected = True
                            Exit For
                        End If
                    Next
                End If
                hdnDistroNbr.Value = 0
                BindData()
            Else
                ShowMsg("Invalid Distro #" & hdnDistroNbr.Value & " submitted for cancellation")
            End If
        Catch ex As Exception
            ShowMsg(ex.Message)
        End Try
    End Sub
End Class
