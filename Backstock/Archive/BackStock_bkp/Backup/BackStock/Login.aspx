<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Login.aspx.vb" Inherits="BackStock.Login"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>BackstockLogin</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="include/Styles.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body onload="javascript:document.loginForm.txtUserName.focus();" bgcolor="black" text="white"
		vlink="white" alink="white" link="white" style="MARGIN: 5px 0px 0px 5px">
		<form id="loginForm" method="post" runat="server">
			<table border='0' cellspacing='0' cellpadding='0' width='770' ID="HeaderTable">
				<tr>
					<td width="200">
						<A HREF='http://www.hottopic.com'><img src='Images/hdr_main2.gif' border='0' alt='Hottopic'></A>
					</td>
					<td align='center' class="Pagecaption">Backstock Administration Console</td>
					<td align="right">
						<A HREF='http://www.torrid.com'><img src='Images/torridPink.jpg' border='0' alt='Torrid'></A>
					</td>
				</tr>
				<tr>
					<td height="10">
					</td>
				</tr>
				<tr style="HEIGHT:2px">
					<td colspan='3' style="HEIGHT:2px" bgcolor="#990000"></td>
				</tr>
			</table>
			<table id="mainTable">
				<tr>
					<td height="20">
					</td>
				</tr>
				<tr>
					<td align="center" class="Pagecaption">
						Login
					</td>
				</tr>
				<tr>
					<td>
						<table id="messageDisplay" style="WIDTH: 763px; HEIGHT: 30px">
							<tr>
								<td align="center" colSpan="4">
									<asp:validationsummary id="Validationsummary1" EnableClientScript="False" runat="server" width="266px"
										displaymode="List" CssClass="errStyle" Font-Size="Smaller"></asp:validationsummary></td>
							</tr>
							<tr>
								<td align="center" colSpan="4">
									<asp:Label id="lblError" Runat="server" Visible="True" CssClass="errStyle" Height="20px"></asp:Label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table align="center" class="t_border" id="loginTable" cellspacing="15" cellpadding="0"
							style="WIDTH: 422px; HEIGHT: 107px">
							<tr>
								<td class="cellvaluecaption">
									<P align="right">UserID:
									</P>
								</td>
								<td><asp:textbox CssClass="cellvalueleft" id="txtUserName" runat="server" width="160px"></asp:textbox><asp:requiredfieldvalidator id="rvUserValidator" runat="server" display="None" errormessage="Please enter your UserID."
										controltovalidate="txtUserName" EnableClientScript="False"></asp:requiredfieldvalidator></td>
							</tr>
							<tr>
								<td class="cellvaluecaption">
									<P align="right">Password:
									</P>
								</td>
								<td><asp:textbox CssClass="cellvalueleft" id="txtPassword" runat="server" width="160px" textmode="Password"></asp:textbox><asp:requiredfieldvalidator id="rvPasswordValidator" runat="server" display="None" errormessage="Please enter your password."
										controltovalidate="txtPassword" EnableClientScript="False"></asp:requiredfieldvalidator></td>
							</tr>
							<tr>
								<td align="center" colspan="2"><asp:button id="cmdSubmit" CssClass="btnSmall" runat="server" borderstyle="Solid" text="Submit"></asp:button></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table border='0' cellspacing='0' cellpadding='0' width='770' ID="HeaderTable">
				<tr>
					<td height="50"></td>
				</tr>
				<tr style="HEIGHT:2px">
					<td colspan='3' style="HEIGHT:2px" bgcolor="#990000"></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
