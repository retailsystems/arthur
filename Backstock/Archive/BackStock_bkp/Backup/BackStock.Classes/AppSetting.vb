Public Class AppSetting

    Private Shared _aaConnString As String
    Private Shared _wmsConnString As String
    Private Shared _gersConnString As String
    Private Shared _cacheDependency As String
    Private Shared _smtpServer As String
    Private Shared _smtpSender As String
    Private Shared _webmasterEmail As String
    Private Shared _projectName As String
    Private Shared _errorLogFile As String
    Private Shared _eMailReceivers As String
    Private Shared _reserveClearEmailBody As String
    Private Shared _reserveUpdateEmailBody As String
    Private Shared _reserveBatchEmailBody As String

    Private Shared _wmsLocalFileDir As String
    Private Shared _ftpUser As String
    Private Shared _ftpPwd As String
    Private Shared _ftpHost As String
    Private Shared _ftpFolder As String
    Private Shared _batchBizTalkFileDir As String
    Private Shared _ackFileDir As String
    Private Shared _UtlFileDir As String
    Private Shared _aaBackstockPkg As String
    Private Shared _wmBackstockPkg As String
    Private Shared _gersBackstockPkg As String  'kjz
    Private Shared _whse As String
    Private Shared _tndcWmsConnString As String

    Public Shared Property AAConnectionString() As String
        Get
            Return _aaConnString
        End Get
        Set(ByVal Value As String)
            _aaConnString = Value
        End Set
    End Property
    Public Shared Property WMSConnectionString() As String
        Get
            Return _wmsConnString
        End Get
        Set(ByVal Value As String)
            _wmsConnString = Value
        End Set
    End Property
    Public Shared Property GERSConnectionString() As String
        Get
            Return _gersConnString
        End Get
        Set(ByVal Value As String)
            _gersConnString = Value
        End Set
    End Property
    Public Shared Property CacheDependencyFile() As String
        Get
            Return _cacheDependency
        End Get
        Set(ByVal Value As String)
            _cacheDependency = Value
        End Set
    End Property

    Public Shared Property SmtpServer() As String
        Get
            Return _smtpServer
        End Get
        Set(ByVal Value As String)
            _smtpServer = Value
        End Set
    End Property

    Public Shared Property SmtpSender() As String
        Get
            Return _smtpSender
        End Get
        Set(ByVal Value As String)
            _smtpSender = Value
        End Set
    End Property


    Public Shared Property ProjectName() As String
        Get
            Return _projectName
        End Get
        Set(ByVal Value As String)
            _projectName = Value
        End Set
    End Property

    Public Shared Property WebmasterEmail() As String
        Get
            Return _webmasterEmail
        End Get
        Set(ByVal Value As String)
            _webmasterEmail = Value
        End Set
    End Property


    Public Shared Property EMailReceivers() As String
        Get
            Return _eMailReceivers
        End Get
        Set(ByVal Value As String)
            _eMailReceivers = Value
        End Set
    End Property

    Public Shared Property ReserveClearEmailBody() As String
        Get
            Return _reserveClearEmailBody
        End Get
        Set(ByVal Value As String)
            _reserveClearEmailBody = Value
        End Set
    End Property
    Public Shared Property ReserveBatchEmailBody() As String
        Get
            Return _reserveBatchEmailBody
        End Get
        Set(ByVal Value As String)
            _reserveBatchEmailBody = Value
        End Set
    End Property
    Public Shared Property ReserveUpdateEmailBody() As String
        Get
            Return _reserveUpdateEmailBody
        End Get
        Set(ByVal Value As String)
            _reserveUpdateEmailBody = Value
        End Set
    End Property


    Public Shared Property WmsLocalFileDir() As String
        Get
            Return _wmsLocalFileDir
        End Get
        Set(ByVal Value As String)
            _wmsLocalFileDir = Value
        End Set
    End Property
    Public Shared Property FtpUser() As String
        Get
            Return _ftpUser
        End Get
        Set(ByVal Value As String)
            _ftpUser = Value
        End Set
    End Property
    Public Shared Property FtpPwd() As String
        Get
            Return _ftpPwd
        End Get
        Set(ByVal Value As String)
            _ftpPwd = Value
        End Set
    End Property

    Public Shared Property FtpHost() As String
        Get
            Return _ftpHost
        End Get
        Set(ByVal Value As String)
            _ftpHost = Value
        End Set
    End Property

    Public Shared Property FtpFolder() As String
        Get
            Return _ftpFolder
        End Get
        Set(ByVal Value As String)
            _ftpFolder = Value
        End Set
    End Property

    Public Shared Property BatchBizTalkFileDir() As String
        Get
            Return _batchBizTalkFileDir
        End Get
        Set(ByVal Value As String)
            _batchBizTalkFileDir = Value
        End Set
    End Property

    Public Shared Property AABackstockPkg() As String
        Get
            Return _aaBackstockPkg
        End Get
        Set(ByVal Value As String)
            _aaBackstockPkg = Value
        End Set
    End Property

    Public Shared Property GERSBackstockPkg() As String
        Get
            Return _gersBackstockPkg
        End Get
        Set(ByVal Value As String)
            _gersBackstockPkg = Value
        End Set
    End Property

    Public Shared Property WMBackstockPkg() As String
        Get
            Return _wmBackstockPkg
        End Get
        Set(ByVal Value As String)
            _wmBackstockPkg = Value
        End Set
    End Property

    Public Shared Property ErrorLogFile() As String
        Get
            Return _errorLogFile
        End Get
        Set(ByVal Value As String)
            _errorLogFile = Value
        End Set
    End Property

    Public Shared Property WarehouseNbr() As String
        Get
            Return _whse
        End Get
        Set(ByVal Value As String)
            _whse = Value
        End Set
    End Property

    Public Shared Property AckFileDir() As String
        Get
            Return _ackFileDir
        End Get
        Set(ByVal Value As String)
            _ackFileDir = Value
        End Set
    End Property
    Public Shared Property UtlFileDir() As String
        Get
            Return _UtlFileDir
        End Get
        Set(ByVal Value As String)
            _UtlFileDir = Value
        End Set
    End Property
    Public Shared Property TnDcWMSConnectionString() As String
        Get
            Return _tndcWmsConnString
        End Get
        Set(ByVal Value As String)
            _tndcWmsConnString = Value
        End Set
    End Property

End Class
