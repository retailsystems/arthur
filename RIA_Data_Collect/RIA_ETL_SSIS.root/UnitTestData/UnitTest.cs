﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using System.Data.OracleClient;
using System.IO;

namespace UnitTestData
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class UnitTest1
    {


        public UnitTest1()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [Priority(1), Description("RIA - Validate Sales"), TestMethod]
        public void TestTotalSales()
        {

            OracleConnection GersConn = new OracleConnection(Properties.Settings.Default.GersConnStr);
            GersConn.Open();
            DataSet expectedDs = new DataSet();

            //get expected sales
           
            string expectedSql = @"SELECT  Sum(tot_qty) as TOT_SLS, Sum(tot_qty - md_qty)as REG_SLS, sum(md_qty) as Md_SLS "
            + " FROM sh_sku_store WHERE wk_num = " + Properties.Settings.Default.WkNum + " AND yr = " + Properties.Settings.Default.Year 
            + " and Store_cd = '" + Properties.Settings.Default.StoreCD + "'"
            + " and Sku_num = '" + Properties.Settings.Default.Sku_Num + "' ";

            OracleCommand expectedCmd = new OracleCommand(expectedSql, GersConn);
            OracleDataAdapter ad1 = new OracleDataAdapter(expectedCmd);
            ad1.Fill(expectedDs);
            //get actual sales
            DataSet actualDs = new DataSet();

            OracleConnection RIADEVConn = new OracleConnection(Properties.Settings.Default.RIADEVConnStr);
            RIADEVConn.Open();

            string actualSql = @"SELECT SUM(TOT_SLS)as TOT_SLS, SUM(REG_SLS) AS REG_SLS FROM HTPROG.STAGE_SKU A "
                               +"join htprog.RIA_CAL_MAPPING C on  WHERE A.Week_nbr = C.riaweek WHERE "
                                + " C.CALWEEK = " + Properties.Settings.Default.WkNum + " AND YR =" + Properties.Settings.Default.Year
                                + " AND STORE_CD = '" + Properties.Settings.Default.StoreCD + "'"
                                + " AND SKU_NUM = '" + Properties.Settings.Default.Sku_Num + "'";
            OracleCommand actualCmd = new OracleCommand(actualSql, RIADEVConn);
            OracleDataAdapter ad2 = new OracleDataAdapter(actualCmd);
            ad1.Fill(actualDs);

            Int32 expectedSls, actualSls;
            Int32.TryParse(expectedDs.Tables[0].Rows[0]["TOT_SLS"].ToString(), out expectedSls);
            Int32.TryParse(actualDs.Tables[0].Rows[0]["TOT_SLS"].ToString(), out actualSls);
            //Added by me
            Int32 expectedRegSls, actualRegSls;
            Int32.TryParse(expectedDs.Tables[0].Rows[0]["REG_SLS"].ToString(), out expectedRegSls);
            Int32.TryParse(actualDs.Tables[0].Rows[0]["REG_SLS"].ToString(), out actualRegSls);


            //compare results
            Assert.IsNotNull(actualSls);
            Assert.AreNotEqual(0, actualSls);
            Assert.AreEqual(expectedSls, actualSls);

            //Added by me
            Assert.IsNotNull(actualRegSls);
            Assert.AreNotEqual(0, actualRegSls);
            Assert.AreEqual(expectedRegSls, actualRegSls);

            GersConn.Close();
        }
        
        [Priority(2), Description("RIA - Validate OnHand"), TestMethod]
        public void TestTotalOnHand()
        {

            OracleConnection GersConn = new OracleConnection(Properties.Settings.Default.GersConnStr);
            GersConn.Open();
            DataSet expectedDs = new DataSet();

            //get expected sales
            string expectedSql = @"select /*+ PARALLEL(OH,3)*/ SUM(TOT_QTY) AS TOT_OH_U FROM EI_SKU_STR OH WHERE store_cd = '" + Properties.Settings.Default.StoreCD + "'"
                        + " AND '" + Properties.Settings.Default.WkEndDt + "' BETWEEN BEG_DT AND END_DT and Sku_num = '" +Properties.Settings.Default.Sku_Num + "'";

            OracleCommand expectedCmd = new OracleCommand(expectedSql, GersConn);
            OracleDataAdapter ad1 = new OracleDataAdapter(expectedCmd);
            ad1.Fill(expectedDs);
            //get actual sales


            OracleConnection RIADEVConn = new OracleConnection(Properties.Settings.Default.RIADEVConnStr);
            RIADEVConn.Open();

            DataSet actualDs = new DataSet();
            string actualSql = " SELECT SUM(TOT_EI) as TOT_OH_U FROM HTPROG.STAGE_SKU A join RIA_CAL_MAPPING C on "
                                + " C.riaweek = A.week_nbr WHERE "
                                + " C.Calweek = " + Properties.Settings.Default.WkNum + " AND YR =" + Properties.Settings.Default.Year
                                + "  AND STORE_CD = '" + Properties.Settings.Default.StoreCD + "' And Sku_num = '"+ Properties.Settings.Default.Sku_Num + "'";

            OracleCommand actualCmd = new OracleCommand(actualSql, RIADEVConn);
            OracleDataAdapter ad2 = new OracleDataAdapter(actualCmd);
            ad1.Fill(actualDs);

            Int32 expectedOH, actualOH;
            Int32.TryParse(expectedDs.Tables[0].Rows[0]["TOT_OH_U"].ToString(), out expectedOH);
            Int32.TryParse(actualDs.Tables[0].Rows[0]["TOT_OH_U"].ToString(), out actualOH);
            //compare results
            Assert.IsNotNull(actualOH);
            Assert.AreNotEqual(0, actualOH);
            Assert.AreEqual(expectedOH, actualOH);
            GersConn.Close();
            RIADEVConn.Close();
        }

       

        //Validate LY EI
        [Priority(3), Description("RIA - Validate STD SLS"), TestMethod]
        public void TestSTDSLS()
        {

            OracleConnection RIAPRDConn = new OracleConnection(Properties.Settings.Default.RIAPRDConnStr);
            RIAPRDConn.Open();
            DataSet expectedDs = new DataSet();

            //get expected sales
            //string expectedSql = @"select /*+ PARALLEL(OH,3)*/ SUM(TOT_QTY) AS TOT_OH_U FROM EI_SKU_STR OH WHERE store_cd = '" + Properties.Settings.Default.StoreCD + "'"
            //            + " AND '" + Properties.Settings.Default.WkEndDt + "' BETWEEN BEG_DT AND END_DT and Sku_num = '" + Properties.Settings.Default.Sku_Num + "'";

            

            string expectedSql = @"Select Sum(TOT_STD_SLS) as STD_SLS from WTD_sku "
                                +" where sku_num = '" + Properties.Settings.Default.Sku_Num + "' and store_cd = '" + Properties.Settings.Default.StoreCD + "'";


            OracleCommand expectedCmd = new OracleCommand(expectedSql, RIAPRDConn);
            OracleDataAdapter ad1 = new OracleDataAdapter(expectedCmd);
            ad1.Fill(expectedDs);
            //get actual sales


            OracleConnection RIADEVConn = new OracleConnection(Properties.Settings.Default.RIADEVConnStr);
            RIADEVConn.Open();

            DataSet actualDs = new DataSet();
            string actualSql = @"Select Sum(TOT_STD_SLS) as STD_SLS from WTD_sku"
                                + " where sku_num = '" + Properties.Settings.Default.Sku_Num + "' and store_cd = '" + Properties.Settings.Default.StoreCD + "'";


            OracleCommand actualCmd = new OracleCommand(actualSql, RIADEVConn);
            OracleDataAdapter ad2 = new OracleDataAdapter(actualCmd);
            ad1.Fill(actualDs);

            Int32 expectedSTDSLS, actualSTDSLS;
            Int32.TryParse(expectedDs.Tables[0].Rows[0]["STD_SLS"].ToString(), out expectedSTDSLS);
            Int32.TryParse(actualDs.Tables[0].Rows[0]["STD_SLS"].ToString(), out actualSTDSLS);
            //compare results
            Double i = 0;
            i = (expectedSTDSLS * 0.01) + actualSTDSLS;

            Assert.IsNotNull(actualSTDSLS);
            Assert.AreNotEqual(0, actualSTDSLS);
            Assert.AreEqual(expectedSTDSLS, actualSTDSLS,i);
            RIADEVConn.Close();
            RIAPRDConn.Close();
        }

        //[Description("RIA - Validate row count in output file"), TestMethod]
        //public void TestRowCount()
        //{
        //    OracleConnection GersConn = new OracleConnection(Properties.Settings.Default.GersConnStr);
        //    GersConn.Open();
        //    DataSet expectedDs = new DataSet();

        //    //get expected count
        //    string expectedSql = @"SELECT COUNT(*) AS RECORD_COUNT FROM BRIC_REFRESH.HT_SIZE_NEW WHERE "
        //                        + " WK_NUM = " + Properties.Settings.Default.WkNum + " AND YR =" + Properties.Settings.Default.Yr;

        //    OracleCommand expectedCmd = new OracleCommand(expectedSql, GersConn);
        //    OracleDataAdapter ad1 = new OracleDataAdapter(expectedCmd);
        //    ad1.Fill(expectedDs);
        //    //get actual record count in file  
        //    Int32 expectedCnt, actualCnt;
        //    actualCnt = 0;
        //    using (StreamReader reader = File.OpenText(Properties.Settings.Default.ComboFile))
        //    {
        //        while (reader.ReadLine() != null)
        //        {
        //            actualCnt++;
        //        }
        //    }

        //    Int32.TryParse(expectedDs.Tables[0].Rows[0]["RECORD_COUNT"].ToString(), out expectedCnt);
        //    //compare results
        //    Assert.IsNotNull(actualCnt);
        //    Assert.AreNotEqual(0, actualCnt);
        //    Assert.AreEqual(expectedCnt, actualCnt, 1);
        //    GersConn.Close();
        //}

    }
}
